/*
	Site Name: SQM
	URI: http:http://
	Description: This is the custom js
	Version: 1.0
	Author: Amal-Techware Solution
	Author URI:
	Tags:

	---------------------------
	HOME-INDEX-STYLES
	---------------------------

	TABLE OF CONTENTS
	---------------------------

    1.0 LOGIN-QUOTE-SLIDER


*/

$(document).ready(function() {


    /* 1.0 SIDE-MENU */


    /* 2.0 LOGIN-QUOTE-SLIDER */

    $('.login_slider_quotes ul').slick(
        {
            dots: true,
            arrows: false,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 2000
        }
    );

});
