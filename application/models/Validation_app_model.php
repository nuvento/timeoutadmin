<?php 

class Validation_app_model extends CI_Model {
	public $validation_array = array(
		'login'=> array(
			'email'=>array(
				'required'=>array(
					'code'=>'ER02',
					'message'=>'Email id is null or empty'
				),
				'email'=>array(
					'code'=>'ER03',
					'message'=>'Invalid Email id'
				)
			),
			'password'=>array(
				'required'=>array(
					'code'=>'ER04',
					'message'=>'Password is null or empty'
				)
			),
			'country_id'=>array(
				'required'=>array(
					'code'=>'ER05',
					'message'=>'Country Id is null or empty'
				)
			)
		),
		'check_email_availability'=> array(
			'email'=>array(
				'required'=>array(
					'code'=>'ER02',
					'message'=>'Email id is null or empty'
				),
				'email'=>array(
					'code'=>'ER03',
					'message'=>'Invalid Email id'
				)
			),
			'phone'=>array(
				'required'=>array(
					'code'=>'ER07',
					'message'=>'Phone no is null or empty'
				),
				'phone'=>array(
					'code'=>'ER08',
					'message'=>'Invalid Phone no'
				)
			)
		),
		'registration'=> array(
			'email'=>array(
				'required'=>array(
					'code'=>'ER02',
					'message'=>'Email id is null or empty'
				),
				'email'=>array(
					'code'=>'ER03',
					'message'=>'Invalid Email id'
				)
			),
			'phone'=>array(
				'required'=>array(
					'code'=>'ER07',
					'message'=>'Phone no is null or empty'
				),
				'phone'=>array(
					'code'=>'ER08',
					'message'=>'Invalid Phone no'
				)
			),
			'name'=>array(
				'required'=>array(
					'code'=>'ER04',
					'message'=>'Name is null or empty'
				)
			),
			'password'=>array(
				'required'=>array(
					'code'=>'ER04',
					'message'=>'Password is null or empty'
				)
			)
		),
		'get_events_list'=>array(
			'auth_token'=>array(
				'required'=>array(
					'code'=>'ER17',
					'message'=>'User Id is null or empty'
				)
			)
		),
		'filters'=>array(
			'auth_token'=>array(
				'required'=>array(
					'code'=>'ER17',
					'message'=>'User Id is null or empty'
				)
			)
		),
		'events_details'=>array(
			'auth_token'=>array(
				'required'=>array(
					'code'=>'ER17',
					'message'=>'User Id is null or empty'
				)
			)
		),
		'get_category_list'=>array(
			'auth_token'=>array(
				'required'=>array(
					'code'=>'ER17',
					'message'=>'User Id is null or empty'
				)
			)
		),
		'event_search'=>array(),
		'get_last_booking'=>array(
			'auth_token'=>array(
				'required'=>array(
					'code'=>'ER17',
					'message'=>'User Id is null or empty'
				)
			)
		),
		'user_language'=> array(
			'auth_token'=>array(
				'required'=>array(
					'code'=>'ER17',
					'message'=>'User Id is null or empty'
				)
			),
			'country_id'=>array(
				'required'=>array(
					'code'=>'ER16',
					'message'=>'Country ID is null or empty'
				)
			)
		),
		'add_favorites'=> array(
			'auth_token'=>array(
				'required'=>array(
					'code'=>'ER17',
					'message'=>'User Id is null or empty'
				)
			),
			'event_id'=>array(
				'required'=>array(
					'code'=>'ER16',
					'message'=>'Event id is null or empty'
				)
			)
		),
		'get_cities_list'=>array(
			'auth_token'=>array(
				'required'=>array(
					'code'=>'ER19',
					'message'=>'User Id is null or empty'
				)
			)
		),
		'update_city'=>array(
			'auth_token'=>array(
				'required'=>array(
					'code'=>'ER19',
					'message'=>'User Id is null or empty'
				)
			)
		),
		'booking_summary'=>array(
			'auth_token'=>array(
				'required'=>array(
					'code'=>'ER17',
					'message'=>'User Id is null or empty'
				)
			)
		),
		'payment'=> array(
			'auth_token'=>array(
				'required'=>array(
					'code'=>'ER17',
					'message'=>'User Id is null or empty'
				)
			),
			'booking_id'=>array(
				'required'=>array(
					'code'=>'ER34',
					'message'=>'Booking id is null or empty'
				)
			)
		),
		'event_rating'=> array(
			'auth_token'=>array(
				'required'=>array(
					'code'=>'ER17',
					'message'=>'User Id is null or empty'
				)
			),
			'rating'=>array(
				'required'=>array(
					'code'=>'ER26',
					'message'=>'Rating is null or empty'
				)
			),
			'event_id'=>array(
				'required'=>array(
					'code'=>'ER27',
					'message'=>'Event id is null or empty'
				)
			),
			'description'=>array(
				'required'=>array(
					'code'=>'ER28',
					'message'=>'description id is null or empty'
				)
			)
		),
		'update_notification_email_status'=>array(
			'auth_token'=>array(
				'required'=>array(
					'code'=>'ER17',
					'message'=>'User Id is null or empty'
				)
			)
		),
		'get_app_version'=>array(),
		'logout'=>array(
			'auth_token'=>array(
				'required'=>array(
					'code'=>'ER17',
					'message'=>'User Id is null or empty'
				)
			)
		),
		'profile_details'=>array(
			'auth_token'=>array(
				'required'=>array(
					'code'=>'ER17',
					'message'=>'User Id is null or empty'
				)
			)
		),
		'profile_edit'=> array(
			'auth_token'=>array(
				'required'=>array(
					'code'=>'ER17',
					'message'=>'User Id is null or empty'
				)
			)
		),
		'booking'=> array(
			'auth_token'=>array(
				'required'=>array(
					'code'=>'ER17',
					'message'=>'User Id is null or empty'
				)
			),
			'event_id'=>array(
				'required'=>array(
					'code'=>'ER20',
					'message'=>'Event id is null or empty'
				)
			),
			'customer_id'=>array(
				'required'=>array(
					'code'=>'ER29',
					'message'=>'Customer id is null or empty'
				)
			),
			'event_date_id'=>array(
				'required'=>array(
					'code'=>'ER30',
					'message'=>'Event date id is null or empty'
				)
			),
			'no_of_ticket'=>array(
				'required'=>array(
					'code'=>'ER31',
					'message'=>'Number of ticket is null or empty'
				)
			),
			'ticket_details'=>array(
				'required'=>array(
					'code'=>'ER32',
					'message'=>'Ticket details is null or empty'
				)
			)
		),
		'cancel_booking'=> array(
			'auth_token'=>array(
				'required'=>array(
					'code'=>'ER17',
					'message'=>'User Id is null or empty'
				)
			),
			'booking_id'=>array(
				'required'=>array(
					'code'=>'ER34',
					'message'=>'Booking Id is null or empty'
				)
			)
		),
		'update_fcm_token'=> array(
			'auth_token'=>array(
				'required'=>array(
					'code'=>'ER17',
					'message'=>'User Id is null or empty'
				)
			),
			'fcm_token'=>array(
				'required'=>array(
					'code'=>'ER16',
					'message'=>'Fcm token is null or empty'
				)
			)
		),
		'get_favorites_list'=>array(
			'auth_token'=>array(
				'required'=>array(
					'code'=>'ER17',
					'message'=>'User Id is null or empty'
				)
			)
		),
		'get_booking_list'=>array(
			'auth_token'=>array(
				'required'=>array(
					'code'=>'ER17',
					'message'=>'User Id is null or empty'
				)
			)
		),
		'settings'=>array(
			'auth_token'=>array(
				'required'=>array(
					'code'=>'ER17',
					'message'=>'User Id is null or empty'
				)
			)
		),
		'forgot_password'=> array(
			'new_password'=>array(
				'required'=>array(
					'code'=>'ER35',
					'message'=>'New password is null or empty'
				)
			),
			'phone'=>array(
				'required'=>array(
					'code'=>'ER07',
					 'message'=>'Phone is null or empty'
				)
			)
		),
		'convertCurrency'=> array(),
		'sync_contacts' => array(
			'auth_token' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'User id is null or empty'
				)
			),
			'contacts' => array(
				'required' => array(
					'code' => 'ER04',
					'message' => 'Contacts is null or empty'
				),
			)
		),
		'update_friend_request' => array(
			'auth_token' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'User id is null or empty'
				)
			),
			'user_id' => array(
				'required' => array(
					'code' => 'ER04',
					'message' => 'User Id is null or empty'
				),
			),
			'add_as_friend' => array(
				'required' => array(
					'code' => 'ER04',
					'message' => 'Accept/Reject value is null or empty'
				),
			)
		),
		'send_friend_request' => array(
			'auth_token' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'User id is null or empty'
				)
			),
			'user_id' => array(
				'required' => array(
					'code' => 'ER04',
					'message' => 'User Id is null or empty'
				),
			)
		),
		'get_friend_requests' => array(
			'auth_token' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'User id is null or empty'
				)
			)
		),
		'recent_chats' => array(
			'auth_token' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'User id is null or empty'
				)
			)
		),
		'upload_audio_message' => array(
			'auth_token' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'User id is null or empty'
				)
			) 
		),
		'getCountry' => array(),
		'validate_promo_code'=>array(
			'auth_token' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'User id is null or empty'
				)
			),
			'event_id' => array(
				'required' => array(
					'code' => 'ER04',
					'message' => 'Event Id is null or empty'
				),
			),
			'promo_code' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Promocode is null or empty'
				)
			),
			'no_of_tickets' => array(
				'required' => array(
					'code' => 'ER04',
					'message' => 'No of Ticket is null or empty'
				)
			),
			'seat_class' => array(
				'required' => array(
					'code' => 'ER04',
					'message' => 'Seat Class is null or empty'
				),
			),
			'tot_cost' => array(
				'required' => array(
					'code' => 'ER04',
					'message' => 'Total Cost is null or empty'
				)
			)
		),
		'get_hotel_city_list'=>array(),
		'hotel_search'=>array(
			'auth_token' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'User id is null or empty'
				)
			)
		),
		'get_specific_hotel_content'=>array(
			'auth_token' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'User id is null or empty'
				)
			),
			'hotelId' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Hotel Id is null or empty'
				)
			),
			'productId' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Product Id is null or empty'
				)
			),
			'sessionId' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Session Id is null or empty'
				)
			),
			'trackingId' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Tracking Id is null or empty'
				)
			)
		),
		'get_room_rates'=>array(
			'auth_token' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'User id is null or empty'
				)
			),
			'TraceId' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Trace Id is null or empty'
				)
			),
			'propertyid' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Property Id is null or empty'
				)
			),
			'trackingId' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Tracking Id is null or empty'
				)
			),
			'sessionId' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Session Id is null or empty'
				)
			),
			'productId' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Product Id is null or empty'
				)
			),
			'TokenId' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Token Id is null or empty'
				)
			),
			'resultindex' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Result Index Field is null or empty'
				)
			),
			'hotelcode' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Hotel Code is null or empty'
				)
			),
		),
		'get_rate_rules'=>array(
			'auth_token' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'User Id is null or empty'
				)
			),
			'TokenId' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Token Id is null or empty'
				)
			),
			'TraceId' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Trace Id is null or empty'
				)
			),
			'allocationDetails' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Allocation Details is null or empty'
				)
			),
			'booking_token' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Booking Token is null or empty'
				)
			),
			'hotel_name' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Hotel Name is null or empty'
				)
			),
			'hotelcode' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Hotel Code is null or empty'
				)
			),
			'infosource' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Infosource Field is null or empty'
				)
			),
			'meal_code' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Meal Code Field is null or empty'
				)
			),
			'productId' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Product Id is null or empty'
				)
			),
			'propertyid' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Property Id is null or empty'
				)
			),
			'rate_basis_id' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Rate Basis Id is null or empty'
				)
			),
			'resultindex' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Result Index field is null or empty'
				)
			),
			'room_code' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Room Code is null or empty'
				)
			),
			'roomType_runno' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Room Type Runno Field is null or empty'
				)
			),
			'sessionId' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Session Id is null or empty'
				)
			),
			'trackingId' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Tracking Id is null or empty'
				)
			),
		),
		'hotel_book'=>array(
			'auth_token' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'User Id is null or empty'
				)
			),
			'TokenId' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Token Id is null or empty'
				)
			),
			'TraceId' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Trace Id is null or empty'
				)
			),
			'changedOccupancy' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Changed Occupancy Field is null or empty'
				)
			),
			'extrabeds' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Extrabeds Field is null or empty'
				)
			),
			'guests_details' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Guest Details field is null or empty'
				)
			),
			'hotelcode' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Hotel Code is null or empty'
				)
			),
			'refcode' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Refcode Field is null or empty'
				)
			),
			'prebookingtoken' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Pre Booking Token is null or empty'
				)
			),
			'productId' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Product Id is null or empty'
				)
			),
			'propertyid' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Property Id is null or empty'
				)
			),
			'resultindex' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Result Index field is null or empty'
				)
			),
			'sessionId' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Session Id is null or empty'
				)
			),
			'trackingId' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Tracking Id is null or empty'
				)
			),
		),
		'trawex_cancel_booking'=>array(
			'auth_token' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'User Id is null or empty'
				)
			),
			'trackingId' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Tracking Id is null or empty'
				)
			),
			'productId' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Product Id is null or empty'
				)
			),
			'supplierConfirmationNum' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Supplier Confirmation Number is null or empty'
				)
			),
			'reference' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Reference Id is null or empty'
				)
			),
		),
		'flight_authenticate'=>array(
			'auth_token' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'User Id is null or empty'
				)
			)
		),
		'flight_availability_search'=>array(
			'auth_token' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'User Id is null or empty'
				)
			),
			'journey_type' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Journey Type is null or empty'
				)
			),
			'airport_from_code' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Airport From Code is null or empty'
				)
			),
			'airport_to_code' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'airport To Code is null or empty'
				)
			),
			'departure_date' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Departure Date is null or empty'
				)
			),
			'return_date' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Return Date is null or empty'
				)
			),
			'adult_flight' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Adult Flight is null or empty'
				)
			),
			'child_flight' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Child Flight is null or empty'
				)
			),
			'infant_flight' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Infant Flight is null or empty'
				)
			),
			'class' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Class Field is null or empty'
				)
			),
			'target' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Target Field is null or empty'
				)
			),
			'session_id' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Session Id is null or empty'
				)
			),
		),
		'flight_fare_rules'=>array(
			'auth_token' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'User Id is null or empty'
				)
			),
			'fare_source_code' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Fare Source Code is null or empty'
				)
			),
			'session_id' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Session Id is null or empty'
				)
			),
		),
		'flight_revalidate'=>array(
			'auth_token' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'User Id is null or empty'
				)
			),
			'fare_source_code' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Fare Source Code is null or empty'
				)
			),
			'session_id' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Session Id is null or empty'
				)
			),
		),
		'update_user_location'=>array(
			'auth_token' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'User Id is null or empty'
				)
			),
			'latitude' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Latitude Field is null or empty'
				)
			),
			'longitude' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Longitude Field is null or empty'
				)
			),
		),
		'get_nearby_users'=>array(
			'auth_token' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'User Id is null or empty'
				)
			),
			'latitude' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Latitude Field is null or empty'
				)
			),
			'longitude' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Longitude Field is null or empty'
				)
			),
		),
		'flight_book'=>array(
			'auth_token' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'User Id is null or empty'
				)
			),
			'target' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Target Field is null or empty'
				)
			),
			'session_id' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Session Id is null or empty'
				)
			),
			'area_code' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Area Code is null or empty'
				)
			),
			'country_code' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Country Code is null or empty'
				)
			),
			'first_name' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'First Name is null or empty'
				)
			),
			'last_name' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Last Name is null or empty'
				)
			),
			'title' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Title Field is null or empty'
				)
			),
			'email_id' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Email ID is null or empty'
				)
			),
			'mobile_no' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Mobile Number is null or empty'
				)
			),
			'dob' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'DOB is null or empty'
				)
			),
			'gender' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Gender is null or empty'
				)
			),
			'issue_country' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Issue Country is null or empty'
				)
			),
			'passport_expiry' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Passport Expiry Field is null or empty'
				)
			),
			'passport_no' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Passport Number is null or empty'
				)
			),
			'type' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Type Field is null or empty'
				)
			),
			'IsPassportMandatory' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Is Passport Mandatory Field is null or empty'
				)
			),
			'adult_flight' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Adult flight is null or empty'
				)
			),
			'child_flight' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Child Flight is null or empty'
				)
			),
			'infant_flight' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Infant Flight is null or empty'
				)
			),
			'frequentFlyrNum' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Frequent Flyr Number Field is null or empty'
				)
			),
			'adultmealplan' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Adult Meal Plan Field is null or empty'
				)
			),
			'child_dob' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Child Dob is null or empty'
				)
			),
			'child_gender' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Child Gender is null or empty'
				)
			),
			'child_title' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Child Title is null or empty'
				)
			),
			'child_first_name' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Child First Name is null or empty'
				)
			),
			'child_last_name' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Child Last Name is null or empty'
				)
			),
			'child_passport_expiry_date' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Child Passport Expiry Date is null or empty'
				)
			),
			'child_passport_no' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Child Passport Number is null or empty'
				)
			),
			'child_frequentFlyrNum' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Child Frequent Flyr Number is null or empty'
				)
			),
			'childMealplan' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Child Meal Plan is null or empty'
				)
			),
			'infant_dob' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Infant Dob is null or empty'
				)
			),
			'infant_gender' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Infant Gender is null or empty'
				)
			),
			'infant_first_name' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Infant First Name is null or empty'
				)
			),
			'infant_last_name' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Infant Last Name is null or empty'
				)
			),
			'infant_title' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Infant Title is null or empty'
				)
			),
			'infantMealplan' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Infant Meal Plan is null or empty'
				)
			),
			'FareSourceCode' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Fare Source Code is null or empty'
				)
			),
			'PostCode' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'PostCode is null or empty'
				)
			),
		),
		'flight_ticket_order'=>array(
			'auth_token' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'User Id is null or empty'
				)
			),
			'target' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Target Field is null or empty'
				)
			),
			'session_id' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Session Id is null or empty'
				)
			),
			'UniqueID' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Unique Id is null or empty'
				)
			),
		),
		'flight_trip_details'=>array(
			'auth_token' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'User Id is null or empty'
				)
			),
			'target' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Target Field is null or empty'
				)
			),
			'session_id' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Session Id is null or empty'
				)
			),
			'UniqueID' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Unique Id is null or empty'
				)
			),
		),
		'cancel_flights'=>array(
			'auth_token' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'User Id is null or empty'
				)
			),
			'target' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Target Field is null or empty'
				)
			),
			'session_id' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Session Field is null or empty'
				)
			),
			'UniqueID' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Unique ID is null or empty'
				)
			),
		),
		'create_guest_user' => array(),
		'update_user_visibility' => array(
			'visible' => array(
				'required' => array(
					'code' => 'ER18',
					'message' => 'Required field is null or empty'
				)
			)
		)
	);

	public function validation_check($method_name, $parms) {
 		$state = 0;
 		$rules = $this->validation_array[$method_name]; 		
 		$error_key = '';
 		foreach ($rules as $key => $value) {
 			foreach ($value as $keys => $values) {
 				switch ($keys) {
 						case 'required':
 							if(!isset($parms[$key]) || $parms[$key]=='' || $parms[$key]== null){
 								$state = 1;
 								$error_key = $values;
 							} 
 							break;
 						case 'email':
 							if (isset($parms[$key]) && !filter_var($parms[$key], FILTER_VALIDATE_EMAIL)) {
	  							$state = 1;
 								$error_key = $values; 
							} 
 							break;
 						case 'phone':
 							if(isset($parms[$key])){
 							$phone = preg_replace('/[^0-9]/', '', $parms[$key]);
	 							/*if (strlen($phone) !== 10) {
		  							$state = 1;
	 								$error_key = $values; 
								} */
							} 
 							break;


 						
 						default:
 							# code...
 							break;
 					}
 					if($state==1){
 						break;
 					}
 			}
 			if($state==1){
 				break;
 			}
 		}
 		return array('state'=>$state,'response'=>$error_key);
 	} 	
}
?>
