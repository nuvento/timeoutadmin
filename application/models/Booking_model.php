<?php 
class Booking_model extends CI_Model {
	
	public function _consruct(){
		parent::_construct();
 	}

 	public function getBookingData($booking_id='',$provider_id='',$view='0,1,2,3,5,6',$customer_id='',$bookId=''){
 		$cond  = (!empty($view))?" BOK.status IN ($view) ":" BOK.status != '4' ";
 		$cond .= (!empty($bookId))?" AND BOK.bookId='$bookId' ":"";
 		$cond .= (!empty($booking_id))?" AND BOK.id='$booking_id' ":"";
 		$cond .= (!empty($provider_id))?" AND EVT.provider_id='$provider_id' ":""; 
 		$cond .= (!empty($customer_id))?" AND BOK.customer_id='$customer_id' ":"";

 		$sql = "SELECT BOK.id AS booking_id,CUST.customer_id,CUST.name AS customer_name,EVT.provider_id,
					   CUST.phone AS customer_phone,CUST.email AS customer_email,CUST.gender,EVT.venue_id,
					   CUST.dob,CUST.profile_city,CUST.profile_image,BOK.event_id,BOK.bookId,BOK.event_date_id,
					   BOK.qrcode,BOK.no_of_ticket,BOK.ticket_details,BOK.amount,EVT.has_payment,
					   BOK.reserved_by,BOK.status AS book_status,EVT.category_id,EVT.max_booking,
					   EVT.seat_pricing,EVT.custom_seat_layout,EVT.status AS evt_status,EDT.time,
					   HCAT.host_category,HCAT.show_layout,EDT.date,PRV.name AS provider_name,BOK.booking_date,
					   PRV.email AS provider_email,PRV.phone AS provider_phone,VEN.location,
					   PRV.profile_image AS provider_image,TRANS.transaction_id,TRANS.status AS trans_status
			    FROM booking AS BOK 
			    INNER JOIN events AS EVT ON (EVT.event_id=BOK.event_id)
			    INNER JOIN customer AS CUST ON (CUST.customer_id=BOK.customer_id)
			    INNER JOIN event_category AS ECAT ON (ECAT.cat_id=EVT.category_id)
			    INNER JOIN venue AS VEN ON (VEN.id=EVT.venue_id)
			    INNER JOIN host_categories AS HCAT ON (HCAT.host_cat_id=VEN.host_cat_id)
			    INNER JOIN event_date_time AS EDT ON (EDT.id=BOK.event_date_id)
			    LEFT JOIN transaction AS TRANS ON (TRANS.booking_id=BOK.bookId)
		   		LEFT JOIN provider AS PRV ON (PRV.provider_id=EVT.provider_id)
			    WHERE $cond";

	    $bookingData = $this->db->query($sql);
		if($bookingData->num_rows() <= 0){
 			return 0;
 		}

		if(empty($booking_id)){
			$bookingData = $bookingData->result_array();
			foreach ($bookingData AS $index => $book) {
 				$rtlData = langTranslator($book['event_id'],'EVT');
				$bookingData[$index] = array_merge($bookingData[$index],$rtlData);
 				$rtlData = langTranslator($book['venue_id'],'VEN');
				$bookingData[$index] = array_merge($bookingData[$index],$rtlData);
 				$rtlData = langTranslator($book['category_id'],'CAT');
				$bookingData[$index] = array_merge($bookingData[$index],$rtlData);
			}
		} else {
			$bookingData = $bookingData->row_array();
			$rtlData 	 = langTranslator($bookingData['event_id'],'EVT');
			$bookingData = array_merge($bookingData,$rtlData);
			$rtlData 	 = langTranslator($bookingData['venue_id'],'VEN');
			$bookingData = array_merge($bookingData,$rtlData);
			$rtlData 	 = langTranslator($bookingData['category_id'],'CAT');
			$bookingData = array_merge($bookingData,$rtlData);
		}
 		return json_decode(json_encode($bookingData));
 	}

 	function changeStatus($booking_id = '', $status = '0'){
 		if(empty($booking_id)){
 			return 0;
 		}

 		$status = $this->db->update('booking',array('status'=>$status),array('id'=>$booking_id));
 		return $status;
 	}

 	function getDetailBookData($fields=array(),$where_cond=array()){
 		if(empty($fields)){
 			return 0;
 		}
 		$where_clause = '';
 		if(!empty($where_cond)){
 			if(!empty($where_cond['provider_id'])){
 				$where_clause = " WHERE EVT.provider_id = '".$where_cond['provider_id']."' ";
 			}
 			if(!empty($where_cond['start_date']) && !empty($where_cond['end_date'])){
 				$where_clause .= (empty($where_clause))?' WHERE ':' AND ';

 				$end_date = date('Y-m-d',strtotime(trim($where_cond['end_date']).' 24:59'));
 				$start_date = date('Y-m-d',strtotime(trim($where_cond['start_date']).' 12:00'));
 				$where_clause .= " BOK.booking_date >= '".$start_date."' AND 
 								   BOK.booking_date <= '".$end_date."' ";
 			}
 			if(!empty($where_cond['start_date'])){
 				$where_clause .= (empty($where_clause))?' WHERE ':' AND ';

 				$start_date = date('Y-m-d',strtotime(trim($where_cond['start_date']).' 12:00'));
 				$where_clause .= " BOK.booking_date >= '".$start_date."' ";
 			}
 			if(!empty($where_cond['end_date'])){
 				$where_clause .= (empty($where_clause))?' WHERE ':' AND ';

 				$end_date = date('Y-m-d',strtotime(trim($where_cond['end_date']).' 23:59'));
 				$where_clause .= " BOK.booking_date <= '".$end_date."' ";
 			}
 			if($where_cond['status'] != ''){
 				$where_clause .= (empty($where_clause))?' WHERE ':' AND ';

 				$where_clause .= " BOK.status = '".$where_cond['status']."' ";
 			}
 		}
 		$fields = 'BOK.id AS Booking_ID,TRANS.transaction_id,TRANS.status AS trans_status,
 				   EVT.has_payment,'.$fields;

 		$fields = str_replace(array('EVT.event_name,','EVT.event_description,','ECAT.category,','VEN.venue_name,','REG.name_en AS Region,'),'',$fields);

 		$sql = "SELECT EVT.event_id,EVT.category_id,VEN.id AS venue_id,REG.id AS region_id,".$fields."
			    FROM booking AS BOK 
			    INNER JOIN events AS EVT ON (EVT.event_id=BOK.event_id)
			    INNER JOIN customer AS CUST ON (CUST.customer_id=BOK.customer_id)
			    INNER JOIN event_category AS ECAT ON (ECAT.cat_id=EVT.category_id)
			    INNER JOIN venue AS VEN ON (VEN.id=EVT.venue_id)
			    INNER JOIN host_categories AS HCAT ON (HCAT.host_cat_id=VEN.host_cat_id)
			    INNER JOIN event_date_time AS EDT ON (EDT.id=BOK.event_date_id)
			    LEFT JOIN region AS REG ON (REG.id=VEN.region_id)
			    LEFT JOIN provider AS PRV ON (PRV.provider_id=EVT.provider_id)
		    	LEFT JOIN transaction AS TRANS ON (TRANS.booking_id=BOK.bookId)
 				".$where_clause."
 				ORDER BY BOK.id ASC";

 		$reportData = $this->db->query($sql);
		if(empty($reportData)){
 			return 0;
 		}
 		if(empty($reportData = $reportData->result_array())){
 			return 2;
 		}
 		foreach ($reportData AS $key => $data) {
			$evtData = langTranslator($data['event_id'],'EVT');
			$venData = langTranslator($data['venue_id'],'VEN');
			$regData = langTranslator($data['region_id'],'REG');
			$catData = langTranslator($data['category_id'],'CAT');

		    $reportData[$key]['Event Name'] = $evtData['event_name_EN'];
		    $reportData[$key]['Venue Name'] = $venData['venue_name_EN'];
		    $reportData[$key]['Region Name'] = $regData['region_name_EN'];
		    $reportData[$key]['Event Category'] = $catData['category_name_EN'];
		    $reportData[$key]['Event Description'] = $evtData['event_description_EN'];
		
			unset($reportData[$key]['event_id'],$reportData[$key]['venue_id'],
				  $reportData[$key]['category_id'],$reportData[$key]['region_id']);
 		}
 		return $reportData;
 	}
}
?>
