<?php 
class Promocode_model extends CI_Model {
	public function _consruct(){
		parent::_construct();
 	}

 	public function getPromocodeData($promoId='',$view='1'){
 		$cond  = " PM.status IN ($view) ";
 		$cond .= (!empty($promoId))?" AND PM.promocode_id='$promoId' ":"";
 		$promoData = $this->db->query("SELECT *,(SELECT count(PU.id) FROM promocode_used AS PU 
			 								    WHERE PM.promocode_id=PU.promocode_id) AS used 
	 								   FROM promocode_management AS PM 
									   WHERE $cond");
 		if(empty($promoData)){
			return 0;
 		}

 		if(empty($promoId)){
 			$promoData = $promoData->result_array();
 			foreach ($promoData AS $key => $data) {
 				$rtlData = langTranslator($data['promocode_id'],'PROMO');
 				$promoData[$key] = array_merge($promoData[$key],$rtlData);
 			}
 		} else {
			$promoData = $promoData->row_array();
			$rtlData = langTranslator($promoId,'PROMO');
			$promoData = array_merge($promoData,$rtlData);
 		}
 		return json_decode(json_encode($promoData));
 	}

 	public function createPromocode($promodata=array()){
 		if(empty($promodata)){
 			return 0;
 		}
 		$cond = "promocode_name='".$promodata['promocode_name']."' AND status IN (0,1)";
 		$isExit = $this->db->query("SELECT * FROM promocode_management WHERE $cond")->num_rows();
 		if($isExit >= 1){
 			return 2;
 		}
 		$dataArr['city_id'] = $promodata['city_id'];
 		$dataArr['event_id'] = $promodata['event_id'];
 		$dataArr['end_date'] = date('Y-m-d',strtotime($promodata['end_date']));
 		$dataArr['use_limit'] = $promodata['use_limit'];
 		$dataArr['max_redeem'] = $promodata['max_redeem'];
 		$dataArr['start_date'] = date('Y-m-d',strtotime($promodata['start_date']));
 		$dataArr['category_id'] = $promodata['category_id'];
 		$dataArr['discount_type'] = $promodata['discount_type'];
 		$dataArr['promocode_name'] = $promodata['promocode_name'];
 		$dataArr['min_order_amount'] = $promodata['min_order_amount'];
 		$dataArr['discount_percentage'] = $promodata['discount_percentage'];
 		$status = $this->db->insert('promocode_management',$dataArr);

 		if($status){
 			$promo_id = $this->db->insert_id();
 			$languages = getLanguages();
	 		if(!empty($languages)){
	 			$insertArr = array();
	 			foreach ($languages AS $lang) {
	 				if((!isset($promodata['promocode_tc_'.$lang])||empty($promodata['promocode_tc_'.$lang]))&&
	 				 (!isset($promodata['promocode_desc_'.$lang])||empty($promodata['promocode_desc_'.$lang]))){
	 					continue;
	 				}
	 				$promoTc = (isset($promodata['promocode_tc_'.$lang]) && !empty($promodata['promocode_tc_'.$lang]))?$promodata['promocode_tc_'.$lang]:'';
	 				$promoDesc = (isset($promodata['promocode_desc_'.$lang]) && !empty($promodata['promocode_desc_'.$lang]))?$promodata['promocode_desc_'.$lang]:'';

	 				$insertArr[] = array('language_code'=>$lang,
	 									 'promocode_tc'=>$promoTc,
	 									 'promocode_id'=>$promo_id,
	 					                 'promocode_desc'=>$promoDesc);
	 			}
	 			if(!empty($insertArr)){
	 				$this->db->insert_batch('translator_promocode',$insertArr);
	 			}
	 		}
 		}
		return ($status)?1:0;
 	}

 	public function updatePromocode($promocode_id='',$promodata=array()){
 		if(empty($promocode_id) || empty($promodata)){
 			return 0;
 		}
 		$cond = "promocode_id!='$promocode_id' AND promocode_name='".$promodata['promocode_name']."' AND 
 		         status IN (0,1)";
 		$isExit = $this->db->query("SELECT * FROM promocode_management WHERE $cond")->num_rows();
 		if($isExit >= 1){
 			return 2;
 		} 		
 		$dataArr['city_id'] = $promodata['city_id'];
 		$dataArr['event_id'] = $promodata['event_id'];
 		$dataArr['end_date'] = date('Y-m-d',strtotime($promodata['end_date']));
 		$dataArr['use_limit'] = $promodata['use_limit'];
 		$dataArr['max_redeem'] = $promodata['max_redeem'];
 		$dataArr['start_date'] = date('Y-m-d',strtotime($promodata['start_date']));
 		$dataArr['category_id'] = $promodata['category_id'];
 		$dataArr['discount_type'] = $promodata['discount_type'];
 		$dataArr['promocode_name'] = $promodata['promocode_name'];
 		$dataArr['min_order_amount'] = $promodata['min_order_amount'];
 		$dataArr['discount_percentage'] = $promodata['discount_percentage'];
 		$status = $this->db->update('promocode_management',$dataArr,array('promocode_id'=>$promocode_id));
 		if($status){
 			$languages = getLanguages();
	 		if(!empty($languages)){
	 			$insertArr = array();
	 			foreach ($languages AS $lang) {
	 				if((!isset($promodata['promocode_tc_'.$lang])||empty($promodata['promocode_tc_'.$lang]))&&
	 				 (!isset($promodata['promocode_desc_'.$lang])||empty($promodata['promocode_desc_'.$lang]))){
	 					continue;
	 				}
	 				$promoTc = (isset($promodata['promocode_tc_'.$lang]) && !empty($promodata['promocode_tc_'.$lang]))?$promodata['promocode_tc_'.$lang]:'';
	 				$promoDesc = (isset($promodata['promocode_desc_'.$lang]) && !empty($promodata['promocode_desc_'.$lang]))?$promodata['promocode_desc_'.$lang]:'';

	 				$insertArr[] = array('language_code'=>$lang,
	 									 'promocode_tc'=>$promoTc,
	 									 'promocode_id'=>$promocode_id,
	 					                 'promocode_desc'=>$promoDesc);
	 			}
 				$this->db->delete('translator_promocode',array('promocode_id'=>$promocode_id));
	 			if(!empty($insertArr)){
	 				$this->db->insert_batch('translator_promocode',$insertArr);
	 			}
	 		}
 		}
		return ($status)?1:0;
 	}

 	public function changeStatus($promocode_id = '', $status = '0'){
 		if(empty($promocode_id)){
 			return 0;
 		}
 		$status = $this->db->update('promocode_management',
 							 array('status'=>$status),array('promocode_id'=>$promocode_id));
 		return $status;
 	}
}
?>