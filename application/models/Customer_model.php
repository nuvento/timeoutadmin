<?php 
class Customer_model extends CI_Model {
	public function _consruct(){
		parent::_construct();
 	}

 	public function getCustomerData($customer_id='',$view='',$provider_id=''){
 		$cond  = (!empty($view))?" USR.status IN ($view) ":" USR.status != '2' ";
 		$cond .= (!empty($customer_id))?" AND CUST.customer_id='$customer_id' ":"";

 		$sql = "SELECT CUST.customer_id,CUST.name,CUST.email,CUST.phone,CUST.email,CUST.gender,
 					   CUST.dob,CUST.profile_city,CUST.reset_key,CUST.social_id,CUST.profile_image,USR.status
			    FROM customer AS CUST 
			    INNER JOIN users AS USR ON (USR.id=CUST.customer_id)
			    WHERE $cond";

    	if(!empty($provider_id)){
    		$sql = "SELECT CUST.customer_id,CUST.name,CUST.email,CUST.phone,CUST.email,CUST.gender,
					   	   CUST.dob,CUST.profile_city,CUST.reset_key,CUST.social_id,
					   	   CUST.profile_image,USR.status
				    FROM customer AS CUST 
				    INNER JOIN users AS USR ON (USR.id=CUST.customer_id)
				    INNER JOIN booking AS BOK ON (BOK.customer_id=CUST.customer_id)
				    INNER JOIN events AS EVT ON (EVT.event_id=BOK.event_id)
				    WHERE $cond AND EVT.provider_id='$provider_id' AND BOK.status!='4'";
    	}
	    $customerData = $this->db->query($sql);

	    if(!empty($customerData)){
	    	if(empty($customer_id)){
	    		$custData = $customerData->result();
	    		foreach ($custData AS $key => $value) {
	    			$custData[$key]->dob = date("m/d/Y",$value->dob/1000);
	    		}
	    		return $custData;
	    	} else {
	    		$custData = $customerData->row();
	    		$custData->dob = date("m/d/Y",$custData->dob/1000);
	    		return $custData;
	    	}
	    }
	    return 0;
 	}

 	public function addCustomer($customer_data = array()){
 		if(empty($customer_data))
 			return 0;

 		$userNameChk = $this->db->query("SELECT * FROM users 
 									     WHERE status!='2' AND username='".$customer_data['email']."' AND user_type='3'");
 		if(!empty($userNameChk) && $userNameChk->num_rows() > 0) return 4;

 		$emailChk = $this->db->query("SELECT * FROM customer AS CUST 
 									  INNER JOIN users AS USR ON (USR.id=CUST.customer_id)
								      WHERE USR.status!='2' AND CUST.email='".$customer_data['email']."' AND USR.user_type='3'");
 		if(!empty($emailChk) && $emailChk->num_rows() > 0) return 2;

 		$phoneChk = $this->db->query("SELECT * FROM customer AS CUST 
 									  INNER JOIN users AS USR ON (USR.id=CUST.customer_id)
									  WHERE USR.status!='2' AND CUST.phone='".$customer_data['phone']."' AND USR.user_type='3'");
 		if(!empty($phoneChk) && $phoneChk->num_rows() > 0) return 3;

 		$status = $this->db->insert('users',
									array('username'=>$customer_data['email'],
										  'password'=>$customer_data['password'],
										  'display_name'=>$customer_data['name'],
										  'profile_image'=>$customer_data['profile_image'],
										  'user_type'=>'3','status'=>'1'));
 		if(!$status){
 			return 0;
 		}
 		$customer_id = $this->db->insert_id();
		$status = $this->db->insert('customer', 
									 array('customer_id'=>$customer_id,
										   'dob'=>$customer_data['dob'],
										   'profile_city'=>$customer_data['profile_city'],
										   'name'=>$customer_data['name'],
										   'email'=>$customer_data['email'],
										   'phone'=>$customer_data['phone'],
										   'gender'=>$customer_data['gender'],
										   'profile_image'=>$customer_data['profile_image']));
 		return $status;
 	}

 	function updateCustomer($customer_id = '', $customer_data = array()){
 		if(empty($customer_id) || empty($customer_data))
 			return 0;
 		$userIdChk = $this->db->query("SELECT * FROM customer AS CUST 
 									   INNER JOIN users AS USR ON (USR.id = CUST.customer_id)
 									   WHERE USR.status!='2' AND USR.id!='".$customer_id."' AND 
								   			 USR.username='".$customer_data['email']."'");
 		if(!empty($userIdChk) && $userIdChk->num_rows() > 0) { return 4; }

 		$emailChk = $this->db->query("SELECT * FROM customer AS CUST 
 									  INNER JOIN users AS USR ON (USR.id = CUST.customer_id)
 									  WHERE USR.status!='2' AND USR.id!='".$customer_id."' AND 
								   			CUST.email='".$customer_data['email']."'");
 		if(!empty($emailChk) && $emailChk->num_rows() > 0) { return 2; }

 		$phoneChk = $this->db->query("SELECT * FROM customer AS CUST 
 									  INNER JOIN users AS USR ON (USR.id = CUST.customer_id)
 									  WHERE USR.status!='2' AND USR.id!='".$customer_id."' AND 
								   			CUST.phone='".$customer_data['phone']."'");
 		if(!empty($phoneChk) && $phoneChk->num_rows() > 0) { return 3; }

 		$upMecArr = array('dob'=>$customer_data['dob'],
 						  'profile_city'=>$customer_data['profile_city'],
 						  'name'=>$customer_data['name'],
 						  'phone'=>$customer_data['phone'],
 						  'email'=>$customer_data['email'],
 						  'gender'=>$customer_data['gender']);

 		$admUpdateArr = array('username'=>$customer_data['username'],
							  'display_name'=>$customer_data['display_name']);

 		if(isset($customer_data['profile_image']) && !empty($customer_data['profile_image'])){
 			$upMecArr['profile_image'] = $customer_data['profile_image'];
 			$admUpdateArr['profile_image'] = $customer_data['profile_image'];
 		}

 		$status = $this->db->update('users',$admUpdateArr,array('id'=>$customer_id));
 		if(!$status) { return 0; }

		$status = $this->db->update('customer',$upMecArr,array('customer_id'=>$customer_id));
 		return $status;
 	}

 	function changeStatus($customer_id = '', $status = '0'){
 		if(empty($customer_id)){
 			return 0;
 		}

 		$status = $this->db->update('users',array('status'=>$status),array('id'=>$customer_id));
 		return $status;
 	}

 	function getNearByCustomers($location_data = array(),$sub_issues = array()){
 		if(empty($location_data) || empty($sub_issues)){
 			return 0;
 		}

 		$current_lat = $location_data['pickup_lat'];
        $current_lng = $location_data['pickup_lng'];
 		$issue_cat_id = implode(',',$sub_issues);

        $sql = "SELECT USR.display_name,USR.profile_image,ME.*,MS.shop_name,MS.address AS shop_address,
        			   MS.phone AS shop_phone,MS.email_id AS shop_email_id,
        			   3956*2*ASIN(SQRT(POWER(SIN(($current_lat-ME.location_lat)*pi()/180/2),2)+
        			   COS($current_lat*pi()/180 )*COS(ME.location_lat*pi()/180)*
        			   POWER(SIN(($current_lng-ME.location_lng)*pi()/180/2),2) )) AS distance
    			FROM customer AS ME
    			INNER JOIN users AS USR ON (USR.id=ME.customer_id)
    			LEFT JOIN customer_shop AS MS ON (MS.shop_id=ME.shop_id AND MS.status='1')
                WHERE USR.status='1'
                -- HAVING distance<30";

        $mechData = $this->db->query($sql);
        if(empty($mechData) || empty($mechData = $mechData->result_array())){
        	return 0;
        }

        $estimate = 0;
        $mechDataArr = array();
        foreach($mechData AS $index => $data){
        	if(empty($data['start_time']) || empty($data['end_time'])){
        		$scheduleTiming = array('09:00 AM','10:00 AM','11:00 AM','12:00 PM','01:00 PM',
        							    '02:00 PM','03:00 PM','04:00 PM','05:00 PM','06:00 PM');
        	} else {
        		$endTime = strtotime($data['end_time']);
			    $schTime = strtotime($data['start_time']);
			    $scheduleTiming = array();

			    for( ; $schTime <= ($endTime-3600) ; $schTime += 3600){
			    	$scheduleTiming[] = date('h:i A',$schTime);
			    }
        	}

        	$customer_id = $data['customer_id'];
        	$sql = "SELECT ISS.*, IC.*, MI.*
        			FROM issues_category AS IC
        			INNER JOIN issues AS ISS ON (IC.issue_id=ISS.issue_id)
        			LEFT JOIN customer_issues AS MI ON (MI.issue_cat_id=IC.issue_cat_id AND 
								MI.customer_id='$customer_id' AND MI.status='1')
        			WHERE ISS.status='1' AND IC.status='1' AND IC.issue_cat_id IN ($issue_cat_id)";

		  	$subIssData = $this->db->query($sql);

		  	$sIssueData = array();
	        if(!empty($subIssData) && !empty($subIssData = $subIssData->result_array())){
	        	$sIssueData = $subIssData;
	        }

	        $estimate = 0;
	        foreach($sIssueData AS $sIndex => $sIssue){
		        if(!empty($sIssue['custom_service_fee'])){
		        	$estimate += $sIssue['custom_service_fee'];
		        	$sIssueData[$sIndex]['service_fee'] = $sIssue['custom_service_fee'];
		        } else {
		        	$estimate += $sIssue['default_service_fee'];
		        	$sIssueData[$sIndex]['service_fee'] = $sIssue['default_service_fee'];
		        }
	        }

	        $mechData[$index]['estimate'] = $estimate;
	        $mechData[$index]['sub_issues'] = $sIssueData;
			$mechData[$index]['scheduleTiming'] = $scheduleTiming;
        }
        return $mechData;
 	}
}
?>