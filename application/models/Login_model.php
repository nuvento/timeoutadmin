<?php

class Login_model extends CI_Model {

	public function _construct(){
		parent::_construct();
	}

	public function login($username, $password)	{
		$query = $this->db->query("SELECT * FROM users 
								   WHERE username='$username' AND password='$password' AND 
								   		 user_type IN (1,2,4,5) AND status='1'");

		if($query->num_rows() > 0 && !empty($query)){
			$result = $query->row();

			$result->profile_image;
			if(isset($result->user_type) && !empty($result->user_type)){
				if($result->user_type == 2){
					$this->load->model('Provider_model');
                    $provider = $this->Provider_model->getProviderData();

                    $result->profile_image = $provider->profile_image;
				} else if ($result->user_type == 3){
					$this->load->model('Customer_model');
                    $customer = $this->Customer_model->getCustomerData();

                    $result->profile_image = $customer->profile_image;
				}
			}
		} else {
			$result = 0;
		}
		return $result;
    }
}
?>
