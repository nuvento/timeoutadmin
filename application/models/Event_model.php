<?php 
class Event_model extends CI_Model {
	public function _consruct(){
		parent::_construct();
 	}

 	public function getEventData($event_id='',$view='',$provider_id=''){
 		$cond  = (!empty($view))?" EVT.status IN ($view) ":" EVT.status != '2' ";
 		$cond .= (!empty($event_id))?" AND EVT.event_id='$event_id' ":"";
 		$cond .= (!empty($provider_id))?" AND EVT.provider_id='$provider_id' ":"";

 		$sql = "SELECT EVT.*,EVT.status AS event_status,VEN.*,CAT.*,PRV.*,HST.*
 				FROM events AS EVT 
 				INNER JOIN venue AS VEN ON (EVT.venue_id=VEN.id)
 				INNER JOIN region AS REG ON (REG.id=VEN.region_id)
 				LEFT JOIN provider AS PRV ON (PRV.provider_id=EVT.provider_id)
 				INNER JOIN event_category AS CAT ON (CAT.cat_id=EVT.category_id)
 				INNER JOIN host_categories AS HST ON (HST.host_cat_id=VEN.host_cat_id)
 				WHERE $cond";

 		$eventData = $this->db->query($sql);
 		if(empty($eventData)){
 			return 0;
 		}
 		if(empty($event_id)){
 			$eventData = $eventData->result_array();

 			foreach ($eventData AS $index => $event) {
 				$rtlData = langTranslator($event['event_id'],'EVT');
				$eventData[$index] = array_merge($eventData[$index],$rtlData);
 				$rtlData = langTranslator($event['venue_id'],'VEN');
				$eventData[$index] = array_merge($eventData[$index],$rtlData);
 				$rtlData = langTranslator($event['region_id'],'REG');
				$eventData[$index] = array_merge($eventData[$index],$rtlData);
 				$rtlData = langTranslator($event['category_id'],'CAT');
				$eventData[$index] = array_merge($eventData[$index],$rtlData);

 				$respData = $this->getEventDetails($event['event_id']);
 				$eventData[$index]['eventTags'] = $respData['eventTags'];
 				$eventData[$index]['eventMedia'] = $respData['eventMedia'];
 				$eventData[$index]['eventSchedule'] = $respData['eventSchedule'];
 			}
 		} else {
 			$eventData = $eventData->row_array();

			$rtlData = langTranslator($eventData['event_id'],'EVT');
			$eventData = array_merge($eventData,$rtlData);
			$rtlData = langTranslator($eventData['venue_id'],'VEN');
			$eventData = array_merge($eventData,$rtlData);
			$rtlData = langTranslator($eventData['region_id'],'REG');
			$eventData = array_merge($eventData,$rtlData);
			$rtlData = langTranslator($eventData['category_id'],'CAT');
			$eventData = array_merge($eventData,$rtlData);

 			$respData = $this->getEventDetails($eventData['event_id']);
			$eventData['eventTags'] = $respData['eventTags'];
			$eventData['eventMedia'] = $respData['eventMedia'];
			$eventData['eventSchedule'] = $respData['eventSchedule'];
 		}
 		return json_decode(json_encode($eventData));
 	}

 	function getEventDetails($event_id = ''){
 		if(empty($event_id)){
 			return 0;
 		}
 		$respArr = array('eventMedia'=>'','eventSchedule'=>'','eventTags'=>'');

		$sql = "SELECT DISTINCT `date` FROM event_date_time WHERE event_id='$event_id' AND status='1'";
		$scheduleData = $this->db->query($sql);

		if(!empty($scheduleData)){
			$evtDate = array();
			foreach ($scheduleData->result() AS $date) {
				$evtDate[] = $date->date;
			}
			$respArr['eventSchedule']['date'] = $evtDate;
		}

		$sql = "SELECT DISTINCT `tag_id` FROM event_tags WHERE event_id='$event_id' AND status='1'";
		$tagData = $this->db->query($sql);

		if(!empty($tagData)){
			$evtTags = array();
			foreach ($tagData->result() AS $tag) {
				$evtTags[] = $tag->tag_id;
			}
			$respArr['eventTags'] = $evtTags;
		}

		$sql = "SELECT DISTINCT `time` FROM event_date_time WHERE event_id='$event_id' AND status='1'";
		$scheduleData = $this->db->query($sql);

		if(!empty($scheduleData)){
			$evtTime = array();
			foreach ($scheduleData->result() AS $time) {
				$evtTime[] = $time->time;
			}
			$respArr['eventSchedule']['time'] = $evtTime;
		}

		$sql = "SELECT id,media_type,media_url 
				FROM event_gallery 
				WHERE event_id='$event_id' AND status='1'";

		$evtMediaData = $this->db->query($sql);

		if(!empty($evtMediaData)){
			$mediaData = array();
			foreach ($evtMediaData->result() AS $media) {
				$tempMedia = array('id'=>$media->id,
								   'media_url'=>$media->media_url,
					 			   'media_type'=>$media->media_type);

				$mediaData[$media->media_type][] = $tempMedia;
			}
			$respArr['eventMedia'] = $mediaData;
		}

		return $respArr;
 	}

 	public function createEvent($eventData = array(),$languageArr = array()){
 		if(empty($eventData)){
 			return 0;
 		}
 		if($this->session->userdata('user_type') != '1'){
 			$eventData['status'] = '3';
 		}
 		$event_id = 0;
 		$status = $this->db->insert('events',$eventData);
 		if($status){
 			$event_id = $this->db->insert_id();
 			$languages = getLanguages();
	 		if(!empty($languages)){
	 			$insertArr = array();
	 			foreach ($languages AS $lang) {
	 				if(!isset($languageArr[$lang]) || (empty($languageArr[$lang]['event_name']) && 
	 				   empty($languageArr[$lang]['event_desc']))){
	 					continue;
	 				}
	 				$eName = !empty($languageArr[$lang]['event_name'])?$languageArr[$lang]['event_name']:'';
	 				$eDesc = !empty($languageArr[$lang]['event_desc'])?$languageArr[$lang]['event_desc']:'';
	 				$insertArr[] = array('event_id'=>$event_id,'event_name'=>$eName,
	 									 'event_description'=>$eDesc,'language_code'=>$lang);
	 			}
	 			if(!empty($insertArr)){
	 				$this->db->insert_batch('translator_event',$insertArr);
	 			}
	 		}
 		}
 		return $event_id;
 	}

 	public function updateEvent($event_id = '',$eventData = array(),$languageArr = array()){
 		if(empty($event_id) || empty($eventData) || empty($languageArr)){
 			return 0;
 		}
 		$status = $this->db->update('events',$eventData,array('event_id'=>$event_id));
 		if($status){
 			$languages = getLanguages();
	 		if(!empty($languages)){
	 			$insertArr = array();
	 			foreach ($languages AS $lang) {
	 				if(!isset($languageArr[$lang]) || (empty($languageArr[$lang]['event_name']) && 
	 				   empty($languageArr[$lang]['event_desc']))){
	 					continue;
	 				}
	 				$eName = !empty($languageArr[$lang]['event_name'])?$languageArr[$lang]['event_name']:'';
	 				$eDesc = !empty($languageArr[$lang]['event_desc'])?$languageArr[$lang]['event_desc']:'';
	 				$insertArr[] = array('event_id'=>$event_id,'event_name'=>$eName,
	 									 'event_description'=>$eDesc,'language_code'=>$lang);
	 			}
 				$this->db->delete('translator_event',array('event_id'=>$event_id));
	 			if(!empty($insertArr)){
	 				$this->db->insert_batch('translator_event',$insertArr);
	 			}
	 		}
 		}
 		return ($status)?1:0;
 	}

 	public function createTags($eventData = array()){
 		if(empty($eventData)){
 			return 0;
 		}
 		$status = $this->db->insert_batch('event_tags',$eventData);
 		return $status;
 	}

 	public function updateTags($event_id = '', $eventData = array()){
 		if(empty($event_id) || empty($eventData)){
 			return 0;
 		}
 		$this->db->delete('event_tags',array('event_id'=>$event_id));
 		$status = $this->db->insert_batch('event_tags',$eventData);
 		return $status;
 	}

 	public function createEventDateTime($eventData = array()){
 		if(empty($eventData)){
 			return 0;
 		}
 		$status = $this->db->insert_batch('event_date_time',$eventData);
 		return $status;
 	}

 	public function updateEventDateTime($event_id = '', $eventData = array()){
 		if(empty($event_id) || empty($eventData)){
 			return 0;
 		}
 		$this->db->update('event_date_time',array('status'=>'0'),array('event_id'=>$event_id,'status'=>'1'));
 		$status = $this->db->insert_batch('event_date_time',$eventData);
 		return $status;
 	}

 	public function createEventMedia($eventData = array()){
 		if(empty($eventData)){
 			return 0;
 		}

 		$status = $this->db->insert_batch('event_gallery',$eventData);
 		return $status;
 	}

 	public function updateEventMedia($event_id = '', $eventData = array(), $existingImages = array()){
 		if(empty($event_id)){
 			return 0;
 		}
 		if(!empty($existingImages)){
 			$this->db->query("DELETE FROM event_gallery 
 							  WHERE id NOT IN (".implode(",",$existingImages).") AND event_id=$event_id");
 		} else {
 			$this->db->query("DELETE FROM event_gallery WHERE event_id='$event_id'");
 		}

 		if(!empty($eventData)){
			$status = $this->db->insert_batch('event_gallery',$eventData);
 		}

 		$primaryImg = $this->db->get_where('event_gallery',array('event_id'=>$event_id,'media_type'=>'0'));
 		if(empty($primaryImg) || ($primaryImg->num_rows() <= 0)){
 			$this->db->query("UPDATE `event_gallery` 
							  SET `media_type`=0 
							  WHERE `event_id`='".$event_id."' ORDER BY id LIMIT 1");
 		}
		return $status;
 	}

 	public function updateEvents($event_id = '', $eventData = array()){
 		if(empty($event_id) || empty($eventData)){
 			return 0;
 		}
 		$status = $this->db->update('events',$eventData,array('id'=>$event_id));
 		return $status;
 	}

 	public function changeStatus($event_id = '', $status = '0'){
 		if(empty($event_id)){
 			return 0;
 		}
 		$status = $this->db->update('events',array('status'=>$status),array('event_id'=>$event_id));
 		return $status;
 	}
}
?>
