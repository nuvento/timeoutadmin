<?php 
class Venue_model extends CI_Model {
	public function _consruct(){
		parent::_construct();
 	}

 	public function getVenueData($venue_id='',$view='',$provider_id=''){
 		$cond  = (!empty($view))?" VNE.status IN ($view) ":" VNE.status != '2' ";
 		$cond .= (!empty($venue_id))?" AND VNE.id='$venue_id' ":"";
 		$cond .= (!empty($provider_id))?" AND VNE.provider_id='$provider_id' ":"";

 		$sql = "SELECT VNE.id AS venue_id,VNE.*,HST.host_category,HST.show_layout
 				FROM venue AS VNE
 				INNER JOIN region AS REG ON (VNE.region_id=REG.id)
 				INNER JOIN host_categories AS HST ON (HST.host_cat_id=VNE.host_cat_id)
 				WHERE $cond";

 		$venueData = $this->db->query($sql);
 		if(empty($venueData)){
 			return 0;
 		}

 		if(empty($venue_id)){
 			$venueData = $venueData->result_array();
 			foreach ($venueData AS $key => $data) {
 				$rtlData = langTranslator($data['venue_id'],'VEN');
				$venueData[$key] = array_merge($venueData[$key],$rtlData);
 				$rtlData = langTranslator($data['region_id'],'REG');
				$venueData[$key] = array_merge($venueData[$key],$rtlData);
 			}
 		} else {
			$venueData = $venueData->row_array();
			$rtlData = langTranslator($venue_id,'VEN');
			$venueData = array_merge($venueData,$rtlData);
			$rtlData = langTranslator($venueData['region_id'],'REG');
			$venueData = array_merge($venueData,$rtlData);
 		}
 		return json_decode(json_encode($venueData));
 	}

 	public function createVenue($venueData = array()){
 		if(empty($venueData)){
 			return 0;
 		}

 		$status = $this->db->insert('venue',array('layout'=>$venueData['layout'],
						 						  'location'=>$venueData['location'],
 												  'region_id'=>$venueData['region_id'],
												  'host_cat_id'=>$venueData['host_cat_id'],
												  'locality_id'=>$venueData['locality_id'],
						 						  'provider_id'=>$venueData['provider_id'],
						 						  'location_lat'=>$venueData['location_lat'],
						 						  'location_lng'=>$venueData['location_lng'],
						 						  'layout_details'=>$venueData['layout_details']));
 		if($status){
 			$venue_id = $this->db->insert_id();
 			$languages = getLanguages();
	 		if(!empty($languages)){
	 			$insertArr = array();
	 			foreach ($languages AS $lang) {
	 				if(!isset($venueData['venue_name_'.$lang]) || empty($venueData['venue_name_'.$lang])){
	 					continue;
	 				}
	 				$insertArr[] = array('venue_id'=>$venue_id,
	 									 'venue_name'=>$venueData['venue_name_'.$lang],
	 									 'language_code'=>$lang);
	 			}
	 			if(!empty($insertArr)){
	 				$this->db->insert_batch('translator_venue',$insertArr);
	 			}
	 		}
 		}
 		return $status;
 	}

 	public function updateVenues($venue_id = '', $venueData = array()){
 		if(empty($venue_id) || empty($venueData)){
 			return 0;
 		}

		$insertArr = array();
		$languages = getLanguages();
 		if(!empty($languages)){
 			foreach ($languages AS $lang) {
 				if(!isset($venueData['venue_name_'.$lang])){
 					continue;
 				}
 				$vName = $venueData['venue_name_'.$lang];
 				unset($venueData['venue_name_'.$lang]);
 				unset($venueData['lolocality_name_'.$lang]);
 				if(empty($vName)){
 					continue;
 				}
 				$insertArr[] = array('venue_id'=>$venue_id,'venue_name'=>$vName,'language_code'=>$lang);
 			}
 		}

 		$status = $this->db->update('venue',$venueData,array('id'=>$venue_id));

		$this->db->delete('translator_venue',array('venue_id'=>$venue_id));
		if($status && !empty($insertArr)){
			$this->db->insert_batch('translator_venue',$insertArr);
		}
 		return $status;
 	}

 	public function changeStatus($venue_id = '', $status = '0'){
 		if(empty($venue_id)){
 			return 0;
 		}
 		$status = $this->db->update('venue',array('status'=>$status),array('id'=>$venue_id));
 		return $status;
 	}

 	public function createLocality($language,$data){
 		if(empty($data) || empty($language) || !isset($data['locality_name_EN']) || 
 		   empty($data['locality_name_EN'])){ 
 			return; 
 		}

 		$status = $this->db->insert('locality',array('locality'=>$data['locality_name_EN'],
				 				  	      			 'region_id'=>$data['region_id']));
 		if(!$status || empty($locality_id = $this->db->insert_id())) { 
 			return; 
 		}

 		$insertArr = array();
        foreach($language AS $lang) {
            if(isset($data['locality_name_'.$lang]) && !empty($data['locality_name_'.$lang])){
            	$insertArr[] = array('language_code'=>$lang,
            						'locality_id'=>$locality_id,
            						'locality_name'=>$data['locality_name_'.$lang]);
            }
        }

        if(!empty($insertArr)){
			$this->db->insert_batch('translator_locality',$insertArr);
		}
		return $locality_id;
 	}
}
?>