<?php 
class Staff_model extends CI_Model {
	public function _consruct(){
		parent::_construct();
 	}

 	public function getStaffData($staff_id='',$view=''){
 		$cond  = (!empty($view))?" AND status IN ($view) ":" status != '2' ";
 		$cond .= (!empty($staff_id))?" AND id='$staff_id' ":"";

	    $staffData = $this->db->query("SELECT * FROM users WHERE user_type IN (4,5) ".$cond);

	    if(!empty($staffData)){
	    	return (!empty($staff_id))?$staffData->row():$staffData->result();
	    }
	    return 0;
 	}

 	public function addStaff($staff_data = array()){
 		if(empty($staff_data))
 			return 0;

 		$userNameChk = $this->db->query("SELECT * FROM users 
 									     WHERE status!='2' AND username='".$staff_data['username']."' AND 
 									           user_type IN (4,5)");
 		if(!empty($userNameChk) && $userNameChk->num_rows() > 0) return 2;

 		$status = $this->db->insert('users',
									array('username'=>$staff_data['username'],
										  'password'=>$staff_data['password'],
										  'display_name'=>$staff_data['display_name'],
										  'profile_image'=>$staff_data['profile_image'],
										  'user_type'=>$staff_data['user_type'],'status'=>'1'));
 		return $status;
 	}

 	function updateStaff($staff_id = '', $staff_data = array()){
 		if(empty($staff_id) || empty($staff_data))
 			return 0;
 		$userNameChk = $this->db->query("SELECT * FROM users 
 									     WHERE status!='2' AND username='".$staff_data['username']."' AND 
 									           user_type IN (4,5) AND id!='".$staff_id."'");
 		if(!empty($userNameChk) && $userNameChk->num_rows() > 0) { return 2; }

 		if(!isset($staff_data['profile_image']) || empty($staff_data['profile_image'])){
 			unset($staff_data['profile_image']);
 		}
 		$status = $this->db->update('users',$staff_data,array('id'=>$staff_id));
 		return $status;
 	}

 	function changeStatus($staff_id = '', $status = '0'){
 		if(empty($staff_id)){
 			return 0;
 		}

 		$status = $this->db->update('users',array('status'=>$status),array('id'=>$staff_id));
 		return $status;
 	}
}
?>
