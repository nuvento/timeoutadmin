<?php 
class Notification_model extends CI_Model {
	public function _consruct(){
		parent::_construct();
 	}

 	public function getNotifData(){
 		$notifData = $this->db->query("SELECT * FROM notification_templates");

 		if(!empty($notifData)){
 			return $notifData->row();
 		}
 		return 0;
 	}

 	public function updateNotif($notifData = array()){
 		if(empty($notifData)){
 			return 0;
 		}
 		$status = $this->db->update('notification_templates',$notifData);
 		return $status;
 	}

 	public function getCityUsers($cities='',$view=''){
 		if(empty($cities)){
 			return 0;
 		}
 		$cond  = ($view != '')?" USR.status IN ($view)":" USR.status!='2'";
 		$cond .= ($cities != '')?" AND CUST.city IN ($cities)":"";
 
 		$cityData = $this->db->query("SELECT CUST.* FROM customer AS CUST
 									  INNER JOIN users AS USR ON (USR.id=CUST.customer_id)
 									  WHERE ".$cond);

 		if(!empty($cityData)){
 			return $cityData->result_array();
 		}
 		return 0;
 	}
}
?>