<?php 
class Checker_model extends CI_Model {
	public function _consruct(){
		parent::_construct();
 	}

 	public function getCheckerData($provider_id='',$view=''){
 		$cond  = (!empty($view))?" CHK.status IN ($view) ":" CHK.status != '2' ";
 		$cond .= (!empty($provider_id))?" AND PRV.provider_id='$provider_id' ":"";

 		$checkerData = $this->db->query("SELECT CHK.*,PRV.name
 										 FROM checker AS CHK
 										 INNER JOIN provider AS PRV ON (CHK.provider_id=PRV.provider_id) 
 										 WHERE $cond");
 		return $checkerData->result();
 	}

 	public function createChecker($checkerData = array()){
 		if(empty($checkerData)){
 			return 0;
 		}
 		$usrCheck=$this->db->get_where('checker',array('username'=>$checkerData['username'],'status !='=>'2'));
 		if($usrCheck->num_rows() > 0){
 			return 2;
 		}
 		$status = $this->db->insert('checker',$checkerData);
 		return $status;
 	}

 	public function changeStatus($id = '', $status = '0'){
 		if(empty($id)){
 			return 0;
 		}
 		$status = $this->db->update('checker',array('status'=>$status),array('id'=>$id));
 		return $status;
 	}
}
?>