<?php 
class User_model extends CI_Model {
	public function _consruct(){
		parent::_construct();
 	}
		
 	function getUserData($user_id = ''){
 		if(empty($user_id)){
 			return 0;
 		}
 		$userData = new stdClass();
		$query = $this->db->get_where('users',array('id'=>$user_id));

		if($query->num_rows() > 0 && !empty($query)){
			$result = $query->row();

			if(isset($result->user_type) && !empty($result->user_type)){
				if($result->user_type == 2){
					$this->load->model('Provider_model');
	                $userData = $this->Provider_model->getProviderData($this->session->userdata('id'));
				} else if ($result->user_type == 3){
					$this->load->model('Customer_model');
	                $userData = $this->Customer_model->getCustomerData($this->session->userdata('id'));
				}
			}
			$userData->id = $result->id;
			$userData->status = $result->status;
			$userData->username = $result->username;
			$userData->user_type = $result->user_type;
			$userData->display_name = $result->display_name;
			$userData->profile_image = $result->profile_image;

			return $userData;
		}
		return 0;
 	}

 	function updateUser($user_id = '',$user_type = '',$user_data = array()){
 		if(empty($user_id) || empty($user_type) || empty($user_data)){
 			return 0;
 		}

		$chkUser = $this->db->query("SELECT * FROM users WHERE status!='2' AND id!='".$user_id."' AND 
			   			 		     username='".$user_data['username']."' AND user_type='".$user_type."'");

		if(!empty($chkUser) && $chkUser->num_rows() > 0) {
			return 4;
		}
		$status = '';
 		if ($user_type == 2) {

	 		$chkUser = $this->db->query("SELECT * FROM users AS USR 
	 									   INNER JOIN provider AS PRV ON (USR.id = PRV.provider_id)
	 									   WHERE USR.status!='2' AND USR.id!='".$user_id."' AND 
									   			 PRV.email='".$user_data['email']."'");

 			if(!empty($chkUser) && $chkUser->num_rows() > 0) return 5;

	 		$chkUser = $this->db->query("SELECT * FROM users AS USR 
	 									   INNER JOIN provider AS PRV ON (USR.id = PRV.provider_id)
	 									   WHERE USR.status!='2' AND USR.id!='".$user_id."' AND 
									   			 PRV.phone='".$user_data['phone']."'");
 			if(!empty($chkUser) && $chkUser->num_rows() > 0) return 6;

 			$status = $this->db->update('provider',array('name'=>$user_data['name'],
														 'email'=>$user_data['email'],
														 'phone'=>$user_data['phone']),
 												   array('provider_id'=>$user_id));

 		} else if ($user_type == 3) {

	 		$chkUser = $this->db->query("SELECT * FROM users AS USR 
	 									   INNER JOIN customer AS CUST ON (USR.id = CUST.customer_id)
	 									   WHERE USR.status!='2' AND USR.id!='".$user_id."' AND 
									   			 CUST.email='".$user_data['email']."'");
 			if(!empty($chkUser) && $chkUser->num_rows() > 0) return 5;

	 		$chkUser = $this->db->query("SELECT * FROM users AS USR 
	 									   INNER JOIN customer AS CUST ON (USR.id = CUST.customer_id)
	 									   WHERE USR.status!='2' AND USR.id!='".$user_id."' AND 
									   			 CUST.phone='".$user_data['phone']."'");
 			if(!empty($chkUser) && $chkUser->num_rows() > 0) return 6;

 			$status = $this->db->update('customer',array('name'=>$user_data['name'],
														 'email'=>$user_data['email'],
														 'phone'=>$user_data['phone']),
 												   array('customer_id'=>$user_id));
 		}
 		
 		if($user_type == 1 || $user_type == 4){
 			$userData['username'] = $user_data['username'];
 			$userData['display_name'] = $user_data['display_name'];
 			if(isset($user_data['password']) && !empty($user_data['password'])){
 				$userData['password'] = $user_data['password'];
 			}
 			if(isset($user_data['profile_image']) && !empty($user_data['profile_image'])){
 				$userData['profile_image'] = $user_data['profile_image'];
 			}
 			$this->db->update('users',$userData,array('id'=>$user_id));

			$usrData = $this->getUserData($user_id);

			if(!empty($usrData)){
				$this->session->set_userdata('id',$usrData->id);
				$this->session->set_userdata('user',$usrData);
				$this->session->set_userdata('username',$usrData->username);
				$this->session->set_userdata('logged_in','1');
				$this->session->set_userdata('user_type',$usrData->user_type);
			}
			return 1;
 		}
 		return 0;
 	}
}
?>