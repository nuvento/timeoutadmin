<?php
class Country_model extends CI_Model {
	public function _consruct(){
		parent::_construct();
 	}

 	public function getCountryData($country_id='',$view=''){
 		$cond  = (!empty($view))?" CTRY.status IN ($view) ":" CTRY.status != '2' ";
 		$cond .= (!empty($country_id))?" AND CTRY.country_id='$country_id' ":"";
			
		$sql = "SELECT CTRY.* FROM country AS CTRY WHERE $cond";
 		$countryData = $this->db->query($sql);
 		if(!empty($countryData)){
 			return (empty($country_id))?$countryData->result():$countryData->row();
 		}
 		return 0;
 	}

 	public function createCountry($countryData = array()){
 		if(empty($countryData)){
 			return 0;
 		}
 		$status = $this->db->insert('country',$countryData);
 		return $status;
 	}

 	public function updateCountry($country_id = '', $countryData = array()){
 		if(empty($country_id) || empty($countryData)){
 			return 0;
 		}
 		$status = $this->db->update('country',$countryData,array('country_id'=>$country_id));
 		return $status;
 	}

 	public function changeStatus($country_id = '', $status = '0'){
 		if(empty($country_id)){
 			return 0;
 		}
 		$status = $this->db->update('country',array('status'=>$status),array('country_id'=>$country_id));
 		return $status;
 	}

 	public function getlocalityData($country_id = '',$locality_id = '', $status = '0'){
 		$cond = "status IN (".$status.") ";
 		if(!empty($country_id)){
 			$cond .= " AND country_id='$country_id' ";
 		}
 		if(!empty($locality_id)){
 			$cond .= " AND id='$locality_id' ";
 		}
 		$locData = $this->db->query("SELECT * FROM locality WHERE ".$cond);
 		return (!empty($locality_id))?$locData->row():$locData->result();
 	}
}
?>
