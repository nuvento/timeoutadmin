<?php 
class Host_model extends CI_Model {
	public function _consruct(){
		parent::_construct();
 	}

 	public function getHostCategories($host_id='',$view=''){
 		$cond  = (!empty($view))?" status IN ($view) ":" status != '2' ";
 		$cond .= (!empty($host_id))?" AND host_cat_id='$host_id' ":"";

 		$hostData = $this->db->query("SELECT * FROM host_categories WHERE $cond");

 		if(!empty($hostData)){
 			return (empty($host_id))?$hostData->result():$hostData->row();
 		}
 		return 0;
 	}

 	public function createHostCategory($hostData = array()){
 		if(empty($hostData)){
 			return 0;
 		}
 		$status = $this->db->insert('host_categories',$hostData);
 		return $status;
 	}

 	public function updateHostCategory($host_id = '', $hostData = array()){
 		if(empty($host_id) || empty($hostData)){
 			return 0;
 		}
 		$status = $this->db->update('host_categories',$hostData,array('host_cat_id'=>$host_id));
 		return $status;
 	}

 	public function changeStatus($host_id = '', $status = '0'){
 		if(empty($host_id)){
 			return 0;
 		}
 		$status = $this->db->update('host_categories',array('status'=>$status),
 													  array('host_cat_id'=>$host_id));
 		return $status;
 	}
}
?>