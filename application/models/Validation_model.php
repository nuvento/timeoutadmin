<?php 

class Validation_model extends CI_Model {

	public $validation_array = array(
		'login' => array(
			'email_id' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Email id is null or empty'
				) ,
				'email' => array(
					'code' => 'ER03',
					'message' => 'Invalid Email id'
				)
			) ,
			'password' => array(
				'required' => array(
					'code' => 'ER04',
					'message' => 'Password is null or empty'
				) ,
			)
		) ,
		'register' => array(
			'email_id' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Email id is null or empty'
				) ,
				'email' => array(
					'code' => 'ER03',
					'message' => 'Invalid Email id'
				)
			) ,
			'phone' => array(
				'required' => array(
					'code' => 'ER07',
					'message' => 'Phone no is null or empty'
				) ,
				'phone' => array(
					'code' => 'ER08',
					'message' => 'Invalid Phone no'
				) ,
			) ,
			'password' => array(
				'required' => array(
					'code' => 'ER04',
					'message' => 'Password is null or empty'
				) ,
			)
		) ,
		'forgot' => array(
			'email_id' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Email id is null or empty'
				) ,
				'email' => array(
					'code' => 'ER03',
					'message' => 'Invalid Email id'
				)
			)
		) ,
		'popular' => array() ,
		'getCountry' => array() ,
		'category' => array() ,
		'locality' => array() ,
		'paymentResponse' => array() ,
		'paymentFailureUrl' => array() ,
		'paymentSuccessUrl' => array() ,
		'get_cms_data' => array() ,
		'favourite' => array(
			'event_id' => array(
				'required' => array(
					'code' => 'ER16',
					'message' => 'Event id is null or empty'
				)
			) ,
			'auth_token' => array(
				'required' => array(
					'code' => 'ER17',
					'message' => 'User Id is null or empty'
				) ,
			) ,
			'status' => array(
				'required' => array(
					'code' => 'ER18',
					'message' => 'Favourite status is missing'
				) ,
			) ,
		) ,
		'favouritelist' => array(
			'auth_token' => array(
				'required' => array(
					'code' => 'ER17',
					'message' => 'User Id is null or empty'
				) ,
			) ,
		) ,
		'bookedlist' => array(
			'auth_token' => array(
				'required' => array(
					'code' => 'ER17',
					'message' => 'User Id is null or empty'
				) ,
			) ,
		) ,
		'checkSeatAvailability' => array(
			'event_id' => array(
				'required' => array(
					'code' => 'ER17',
					'message' => 'Event Id is null or empty'
				) ,
			) ,
			'time_id' => array(
				'required' => array(
					'code' => 'ER18',
					'message' => 'Time Id is null or empty'
				)
			)
		) ,
		'bookingdetails' => array(
			'auth_token' => array(
				'required' => array(
					'code' => 'ER17',
					'message' => 'User Id is null or empty'
				) ,
			) ,
			'bookingCode' => array(
				'required' => array(
					'code' => 'ER23',
					'message' => 'Booking code is null or empty'
				) ,
			) ,
		) ,
		'cancel' => array(
			'auth_token' => array(
				'required' => array(
					'code' => 'ER17',
					'message' => 'User Id is null or empty'
				) ,
			) ,
			'bookingCode' => array(
				'required' => array(
					'code' => 'ER23',
					'message' => 'Booking code is null or empty'
				) ,
			) ,
		) ,
		'confirm' => array(
			'auth_token' => array(
				'required' => array(
					'code' => 'ER17',
					'message' => 'User Id is null or empty'
				) ,
			) ,
			'bookingCode' => array(
				'required' => array(
					'code' => 'ER23',
					'message' => 'Booking code is null or empty'
				) ,
			) ,
		) ,
		'userinfo' => array(
			'auth_token' => array(
				'required' => array(
					'code' => 'ER17',
					'message' => 'User Id is null or empty'
				) ,
			) ,
		) ,
		'profile' => array(
			'name' => array(
				'required' => array(
					'code' => 'ER27',
					'message' => 'Name is null or empty'
				)
			) ,
			'gender' => array(
				'required' => array(
					'code' => 'ER28',
					'message' => 'Gender is null or empty'
				)
			) ,
			'dob' => array(
				'required' => array(
					'code' => 'ER29',
					'message' => 'Date of birth is null or empty'
				)
			) ,
			'city' => array(
				'required' => array(
					'code' => 'ER30',
					'message' => 'City no is null or empty'
				)
			) ,
			'auth_token' => array(
				'required' => array(
					'code' => 'ER17',
					'message' => 'User Id is null or empty'
				) ,
			) ,
		) ,
		'tempbooking' => array(
			'event_id' => array(
				'required' => array(
					'code' => 'ER16',
					'message' => 'Event id is null or empty'
				)
			) ,
			'event_date_id' => array(
				'required' => array(
					'code' => 'ER33',
					'message' => 'Event date and time is null or empty'
				)
			),
			'no_of_ticket' => array(
				'required' => array(
					'code' => 'ER36',
					'message' => 'no of ticket is null or empty'
				)
			) ,
			'auth_token' => array(
				'required' => array(
					'code' => 'ER17',
					'message' => 'User Id is null or empty'
				) ,
			) ,
		) ,
		'recommend' => array(
			'auth_token' => array(
				'required' => array(
					'code' => 'ER17',
					'message' => 'User Id is null or empty'
				) ,
			) ,
		) ,
		'verifyMail' => array() ,
		'search' => array(
			/*'auth_token' => array(
				'required' => array(
					'code' => 'ER17',
					'message' => 'User Id is null or empty'
				) ,
			) ,*/
		) ,
		'discover' => array(
			// 'auth_token' => array(
			// 	'required' => array(
			// 		'code' => 'ER17',
			// 		'message' => 'User Id is null or empty'
			// 	) ,
			// ) ,
			'cat_id' => array(
				'required' => array(
					'code' => 'ER38',
					'message' => 'Category id null or empty'
				)
			) ,
		) ,
		'event' => array(
			/*'auth_token' => array(
				'required' => array(
					'code' => 'ER17',
					'message' => 'User Id is null or empty'
				) ,
			) ,*/
			'event_id' => array(
				'required' => array(
					'code' => 'ER16',
					'message' => 'Event id is null or empty'
				)
			) ,
		) ,
		'save_organizer' => array(
			'name' => array(
					'required' => array(
					'code' => 'ER27',
					'message' => 'Name is null or empty'
				)
			) ,
			'phone' => array(
				'required' => array(
					'code' => 'ER08',
					'message' => 'Phone Number is null or empty'
				),
				'phone' => array(
					'code' => 'ER08',
					'message' => 'Invalid Phone no'
				) ,
			),
			'email_id' => array(
				'required' => array(
					'code' => 'ER02',
					'message' => 'Email Id is null or empty'
				),
				'email' => array(
					'code' => 'ER03',
					'message' => 'Invalid Email id'
				) 
			),
			'password' => array(
					'required' => array(
					'code' => 'ER04',
					'message' => 'Password is null or empty'
				) 
			)
		) ,
		'reset_password' => array(
			'reset_key' => array(
				'required' => array(
					'code' => 'ER16',
					'message' => 'Reset Key is null or empty'
				)
			) ,
			'password' => array(
				'required' => array(
					'code' => 'ER18',
					'message' => 'Password is null or empty'
				) ,
			) 
		) ,
		'payNow' => array(),
		'searchEvent' => array(),
		'validate_promo_code' => array(
			'promo_code' => array(
				'required' => array(
					'code' => 'ER16',
					'message' => 'Promocode is null or empty'
				)
			),
			'tot_cost' => array(
				'required' => array(
					'code' => 'ER17',
					'message' => 'Total Cost is null or empty'
				)
			),
			'event_id' => array(
				'required' => array(
					'code' => 'ER18',
					'message' => 'Event ID is null or empty'
				)
			),
			'auth_token' => array(
				'required' => array(
					'code' => 'ER19',
					'message' => 'User Id is null or empty'
				)
			)
		),
		'getSavedCards'=>array(
			'email' => array(
				'required' => array(
					'code' => 'ER18',
					'message' => 'Customer Email ID is null or empty'
				)
			),
			'auth_token' => array(
				'required' => array(
					'code' => 'ER19',
					'message' => 'User Id is null or empty'
				)
			)
		),
		'getTagList'=>array(
			'organiser_id' => array(
				'required' => array(
					'code' => 'ER18',
					'message' => 'Organizer Id is null or empty'
				)
			)
		),
		'addCard' => array(
			'requestData' => array(
				'required' => array(
					'code' => 'ER18',
					'message' => 'Request Data is null or empty'
				)
			)
		)
	);
	
	public function _consruct(){
		parent::_construct();
 	}

 	public function validation_check($method_name, $parms) {
 		$state = 0;
 		$rules = $this->validation_array[$method_name]; 		
 		$error_key = '';
 		foreach ($rules as $key => $value) {
 			foreach ($value as $keys => $values) {
 				switch ($keys) {
					case 'required':
						if(!isset($parms[$key]) || $parms[$key] == '' || $parms[$key] == null){
							$state = 1;
							$error_key = $values;
						} 
						break;
					case 'email':
						if (isset($parms[$key]) && !filter_var($parms[$key], FILTER_VALIDATE_EMAIL)) {
							$state = 1;
							$error_key = $values; 
						} 
						break;
					case 'phone':
						if(isset($parms[$key])){
							$phone = preg_replace('/[^0-9]/', '', $parms[$key]);
							if (strlen($phone) <= 9 && strlen($phone) >= 13) {
  								$state = 1;
								$error_key = $values; 
							}
						} 
						break;
					default: break;
				}
				if($state==1) break;
 			}
 			if($state==1) break;
 		}
 		return array('state'=>$state,'response'=>$error_key);
 	} 	
}

?>
