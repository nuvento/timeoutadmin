<?php 
class HotelCity_model extends CI_Model {
	public function _consruct(){
		parent::_construct();
 	}

 	public function getHotelCityData($hotelCity_id='',$view=''){
 		$cond  = (!empty($view))?" status IN ($view) ":" status != '2' ";
 		$cond .= (!empty($hotelCity_id))?" AND hotel_city_id='$hotelCity_id' ":"";

 		$hotelCityData = $this->db->query("SELECT * FROM hotel_cities WHERE $cond");

 		if(!empty($hotelCityData)){
 			if(empty($hotelCity_id)){
 				$hotelCityData = $hotelCityData->result_array();
 				foreach ($hotelCityData AS $key => $hotelCity) {
 					$regData = langTranslator($hotelCity['hotel_city_id'],'HCTY','');
					$hotelCityData[$key] = array_merge($hotelCityData[$key],$regData);
 				}
 			} else {
 				$regData = langTranslator($hotelCity_id,'HCTY','');
 				$hotelCityData = $hotelCityData->row_array();
				$hotelCityData = array_merge($hotelCityData,$regData);
 			}
 			return json_decode(json_encode($hotelCityData));
 		} else {
 			return 0;
 		}
 	}

 	public function createHotelCity($hotelCityData = array()){
 		if(empty($hotelCityData)){
 			return 0;
 		}
 		$status = $this->db->insert('hotel_cities',array('hotel_city_icon'=>$hotelCityData['hotel_city_icon'],
			 										     'hotel_city_lat'=>$hotelCityData['hotel_city_lat'],
			 										     'hotel_city_lng'=>$hotelCityData['hotel_city_lng']));
 		if($status){
 			$hotelCity_id = $this->db->insert_id();
 			$languages = getLanguages();
	 		if(!empty($languages)){
	 			$insertArr = array();
	 			foreach ($languages AS $lang) {
	 				if(!isset($hotelCityData['hotel_city_name_'.$lang]) || empty($hotelCityData['hotel_city_name_'.$lang])){
	 					continue;
	 				}
	 				$insertArr[] = array('hotel_city_id'=>$hotelCity_id,
	 									 'language_code'=>$lang,
	 					                 'hotel_city_name'=>$hotelCityData['hotel_city_name_'.$lang]);
	 			}
	 			if(!empty($insertArr)){
	 				$this->db->insert_batch('translator_hotel_city',$insertArr);
	 			}
	 		}
 		}
 		return $status;
 	}

 	public function updateHotelCity($hotelCity_id = '', $hotelCityData = array()){
 		if(empty($hotelCity_id) || empty($hotelCityData)){
 			return 0;
 		}
 		$languages = getLanguages();
 		if(!empty($languages)){
 			$insertArr = array();
 			foreach ($languages AS $lang) {
 				if(!isset($hotelCityData['hotel_city_name_'.$lang]) || 
 				    empty($hotelCityData['hotel_city_name_'.$lang])){
 					unset($hotelCityData['hotel_city_name_'.$lang]);
 					continue;
 				}
 				$insertArr[] = array('hotel_city_id'=>$hotelCity_id,
 									 'language_code'=>$lang,
 					                 'hotel_city_name'=>$hotelCityData['hotel_city_name_'.$lang]);
 				unset($hotelCityData['hotel_city_name_'.$lang]);
 			}
			$this->db->delete('translator_hotel_city',array('hotel_city_id'=>$hotelCity_id));
 			if(!empty($insertArr)){
 				$this->db->insert_batch('translator_hotel_city',$insertArr);
 			}
 		}

 		$status = $this->db->update('hotel_cities',$hotelCityData,array('hotel_city_id'=>$hotelCity_id));
 		return $status;
 	}

 	public function changeStatus($hotelCity_id = '', $status = '0'){
 		if(empty($hotelCity_id)){
 			return 0;
 		}
 		$status = $this->db->update('hotel_cities',array('status'=>$status),
 												   array('hotel_city_id'=>$hotelCity_id));
 		return $status;
 	}
}
?>
