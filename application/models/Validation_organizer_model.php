<?php 

class Validation_organizer_model extends CI_Model {

	public $validation_array = array(
		'getOrganizerEventList'=>array(
			'organiser_id' => array(
				'required' => array(
					'code' => 'ER30',
					'message' => 'Organizer ID is null or empty'
				)
			)
		),
		'getOrganizerEventDetails'=>array(
			'organiser_id' => array(
				'required' => array(
					'code' => 'ER30',
					'message' => 'Organizer ID is null or empty'
				)
			),
			'event_id' => array(
				'required' => array(
					'code' => 'ER32',
					'message' => 'Event ID is null or empty'
				)
			)
		),
		'editOrganizerEventDetails'=>array(
			'event_id' => array(
				'required' => array(
					'code' => 'ER32',
					'message' => 'Event ID is null or empty'
				)
			)
		),
		'deleteOrganizerEvent'=>array(
			'organiser_id' => array(
				'required' => array(
					'code' => 'ER30',
					'message' => 'Organizer ID is null or empty'
				)
			),
			'event_id' => array(
				'required' => array(
					'code' => 'ER32',
					'message' => 'Event ID is null or empty'
				)
			)
		),
		'deActivateOrganizerEvent'=>array(
			'organiser_id' => array(
				'required' => array(
					'code' => 'ER30',
					'message' => 'Organizer ID is null or empty'
				)
			),
			'event_id' => array(
				'required' => array(
					'code' => 'ER32',
					'message' => 'Event ID is null or empty'
				)
			)
		),
		'getVenueList'=>array(
			'organiser_id' => array(
				'required' => array(
					'code' => 'ER30',
					'message' => 'Organizer ID is null or empty'
				)
			)
		),
		'getVenueDetails'=>array(
			'organiser_id' => array(
				'required' => array(
					'code' => 'ER30',
					'message' => 'Organizer ID is null or empty'
				)
			),
			'venue_id' => array(
				'required' => array(
					'code' => 'ER18',
					'message' => 'Venue ID is null or empty'
				)
			)
		),
		'getCustomerList'=>array(
			'organiser_id' => array(
				'required' => array(
					'code' => 'ER30',
					'message' => 'Organizer ID is null or empty'
				)
			)
		),
		'getBookingList'=>array(
			'organiser_id' => array(
				'required' => array(
					'code' => 'ER30',
					'message' => 'Organizer ID is null or empty'
				)
			)
		),
		'getBookingDetails'=>array(
			'booking_id' => array(
				'required' => array(
					'code' => 'ER18',
					'message' => 'Booking ID is null or empty'
				)
			)
		),
		'organiserSignUp'=>array(
			'username' => array(
				'required' => array(
					'code' => 'ER18',
					'message' => 'Username is null or empty'
				)
			),
			'email_id' => array(
				'required' => array(
					'code' => 'ER18',
					'message' => 'Email Id is null or empty'
				)
			),
			'contact_no' => array(
				'required' => array(
					'code' => 'ER18',
					'message' => 'Contact Number is null or empty'
				)
			),
			'password' => array(
				'required' => array(
					'code' => 'ER18',
					'message' => 'Password is null or empty'
				)
			)
		),
		'signIn'=>array(
			'email_id' => array(
				'required' => array(
					'code' => 'ER18',
					'message' => 'Email Id is null or empty'
				)
			),
			'password' => array(
				'required' => array(
					'code' => 'ER18',
					'message' => 'Password is null or empty'
				)
			),
			'user_type' => array(
				'required' => array(
					'code' => 'ER18',
					'message' => 'User Type is null or empty'
				)
			),
			'fcm_token' => array(
				'required' => array(
					'code' => 'ER18',
					'message' => 'FCM Token is null or empty'
				)
			),
		),
		'changePassword'=>array(
			'new_password' => array(
				'required' => array(
					'code' => 'ER18',
					'message' => 'New Password is null or empty'
				)
			),
			'confirm_password' => array(
				'required' => array(
					'code' => 'ER18',
					'message' => 'Confirm Password is null or empty'
				)
			),
			'phone_number' => array(
				'required' => array(
					'code' => 'ER18',
					'message' => 'Phone Number is null or empty'
				)
			),
			'user_type' => array(
				'required' => array(
					'code' => 'ER18',
					'message' => 'User Type is null or empty'
				)
			),
		),
		'checkerDeactivate'=>array(
			'checker_id' => array(
				'required' => array(
					'code' => 'ER18',
					'message' => 'Checker Id is null or empty'
				)
			),
			'organiser_id' => array(
				'required' => array(
					'code' => 'ER30',
					'message' => 'Organizer Id is null or empty'
				)
			)
		),
		'getTagList'=>array(
			'organiser_id' => array(
				'required' => array(
					'code' => 'ER30',
					'message' => 'Organizer Id is null or empty'
				)
			)
		),
		'addEvent'=>array(
			'organiser_id' => array(
				'required' => array(
					'code' => 'ER30',
					'message' => 'Organizer Id is null or empty'
				)
			),
			'venue_id' => array(
				'required' => array(
					'code' => 'ER18',
					'message' => 'Venue Id is null or empty'
				)
			),
			'layout_type' => array(
				'required' => array(
					'code' => 'ER18',
					'message' => 'Layout Type is null or empty'
				)
			),
			'event_name' => array(
				'required' => array(
					'code' => 'ER18',
					'message' => 'Event name is null or empty'
				)
			),
			'category_id' => array(
				'required' => array(
					'code' => 'ER18',
					'message' => 'Category Id is null or empty'
				)
			),
			'maximum_seat_booking' => array(
				'required' => array(
					'code' => 'ER18',
					'message' => 'Maximum Seat Booking is null or empty'
				)
			),
			'payment_mode' => array(
				'required' => array(
					'code' => 'ER18',
					'message' => 'Payment Mode is null or empty'
				)
			),
			'event_desc' => array(
				'required' => array(
					'code' => 'ER18',
					'message' => 'Event Description is null or empty'
				)
			),
			'show_type' => array(
				'required' => array(
					'code' => 'ER18',
					'message' => 'Show Type is null or empty'
				)
			),
			'start_date' => array(
				'required' => array(
					'code' => 'ER18',
					'message' => 'Start Date is null or empty'
				)
			),
			'show_timing' => array(
				'required' => array(
					'code' => 'ER18',
					'message' => 'Show Timings is null or empty'
				)
			),
			'tag' => array(
				'required' => array(
					'code' => 'ER18',
					'message' => 'Tag is null or empty'
				)
			),
		),
		'getLanguages'=>array(),
		'getCategories'=>array(),
		'searchEvent'=>array(
			'organiser_id' => array(
				'required' => array(
					'code' => 'ER30',
					'message' => 'Organizer Id is null or empty'
				)
			),
			'keyword' => array(
				'required' => array(
					'code' => 'ER18',
					'message' => 'Search Keyword is null or empty'
				)
			)
		),
		'searchCustomer'=>array(
			'organiser_id' => array(
				'required' => array(
					'code' => 'ER30',
					'message' => 'Organizer Id is null or empty'
				)
			),
			'keyword' => array(
				'required' => array(
					'code' => 'ER18',
					'message' => 'Search Keyword is null or empty'
				)
			)
		),
		'searchBooking'=>array(
			'organiser_id'=>array(
				'required'=>array(
					'code'=>'ER30',
					'message'=>'Organizer Id is null or empty'
				)
			),
			'keyword'=>array(
				'required'=>array(
					'code'=>'ER18',
					'message'=>'Search Keyword is null or empty'
				)
			),

		),
		'searchChecker'=>array(
			'organiser_id'=>array(
				'required'=>array(
					'code'=>'ER30',
					'message'=>'Organizer Id is null or empty'
				)
			),
			'keyword'=>array(
				'required'=>array(
					'code'=>'ER18',
					'message'=>'Search Keyword is null or empty'
				)
			),
		),
		'getCountryCode'=>array(),
		'getCheckerList'=>array(
			'organiser_id'=>array(
				'required'=>array(
					'code'=>'ER30',
					'message'=>'Organizer Id is null or empty'
				)
			),
		),
		'getEvent'=>array(
			'organiser_id'=>array(
				'required'=>array(
					'code'=>'ER30',
					'message'=>'Organizer Id is null or empty'
				)
			),
			'event_id'=>array(
				'required'=>array(
					'code'=>'ER30',
					'message'=>'Event Id is null or empty'
				)
			),
		),
		'checkerDelete'=>array(
			'organiser_id'=>array(
				'required'=>array(
					'code'=>'ER30',
					'message'=>'Organizer Id is null or empty'
				)
			),
			'checker_id'=>array(
				'required'=>array(
					'code'=>'ER30',
					'message'=>'Checker Id is null or empty'
				)
			),
		),
		'checker_bookingDetails' => array(
			'qr_pin' => array(
				'required' => array(
					'code' => 'ER17',
					'message' => 'Booking Id is null or empty'
				) ,
			) ,
			'checker_id' => array(
				'required' => array(
					'code' => 'ER20',
					'message' => 'User Id is null or empty'
				) ,
			) 
		),
		'getCheckerBookList'=>array(
			'checker_id' => array(
				'required' => array(
					'code' => 'ER04',
					'message' => 'Checker ID is null or empty'
				)
			)
		),
		'acceptBooking'=>array(
			'booking_id' => array(
				'required' => array(
					'code' => 'ER04',
					'message' => 'Booking Id is null or empty'
				)
			),
		)
	);
	
	public function _consruct(){
		parent::_construct();
 	}

 	public function validation_check($method_name, $parms) {
 		$state = 0;
 		$rules = $this->validation_array[$method_name]; 		
 		$error_key = '';
 		foreach ($rules as $key => $value) {
 			foreach ($value as $keys => $values) {
 				switch ($keys) {
					case 'required':
						if(!isset($parms[$key]) || $parms[$key] == '' || $parms[$key] == null){
							$state = 1;
							$error_key = $values;
						} 
						break;
					case 'email':
						if (isset($parms[$key]) && !filter_var($parms[$key], FILTER_VALIDATE_EMAIL)) {
							$state = 1;
							$error_key = $values; 
						} 
						break;
					case 'phone':
						if(isset($parms[$key])){
							$phone = preg_replace('/[^0-9]/', '', $parms[$key]);
							if (strlen($phone) <= 9 && strlen($phone) >= 13) {
  								$state = 1;
								$error_key = $values; 
							}
						} 
						break;
					default: break;
				}
				if($state==1) break;
 			}
 			if($state==1) break;
 		}
 		return array('state'=>$state,'response'=>$error_key);
 	} 	
}

?>
