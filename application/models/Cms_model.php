<?php 
class Cms_model extends CI_Model {
	public function _consruct(){
		parent::_construct();
 	}

 	public function getCMSdata(){
 		$cmsData = $this->db->query("SELECT * FROM translator_policies")->result_array();

 		$respData = new StdClass();
 		if(!empty($cmsData)){
 			foreach ($cmsData AS $key => $data) {
 				$respData->{'faq_'.$data['language_code']} = $data['faq'];
 				$respData->{'instruction_'.$data['language_code']} = $data['instruction'];
 				$respData->{'privacy_policy_'.$data['language_code']} = $data['privacy_policy'];
 				$respData->{'terms_and_conditions_'.$data['language_code']} = $data['terms_and_conditions'];
 			}
 			return $respData;
 		}
 		return 0;
 	}

 	public function updateCMS($cmsData = array()){
 		if(empty($cmsData)){
 			return 0;
 		}

 		foreach($cmsData AS $lang => $data) {
 			$isAvail = $this->db->get_where('translator_policies',array('language_code'=>$lang))->num_rows();

 			if($isAvail <= 0){
 				$this->db->insert('translator_policies',$data);
 			} else {
 				$this->db->update('translator_policies',$data,array('language_code'=>$lang));
 			}
 		}
 		return 1;
 	}
}
?>