<?php 
class Category_model extends CI_Model {
	public function _consruct(){
		parent::_construct();
 	}

 	public function getCategoryData($category_id='',$view=''){
 		$cond  = (!empty($view))?" status IN ($view) ":" status != '2' ";
 		$cond .= (!empty($category_id))?" AND cat_id='$category_id' ":"";

 		$catData = $this->db->query("SELECT * FROM event_category WHERE $cond ORDER BY priority");
 		if(empty($catData)){
 			return 0;
 		}

 		if(empty($category_id)){
 			$catData = $catData->result_array();
 			foreach ($catData AS $key => $data) {
 				$rtlData = langTranslator($data['cat_id'],'CAT');
 				$catData[$key] = array_merge($catData[$key],$rtlData);
 			}
 		} else {
			$catData = $catData->row_array();
			$rtlData = langTranslator($category_id,'CAT');
			$catData = array_merge($catData,$rtlData);
 		}
 		return json_decode(json_encode($catData));
 	}

 	public function createCategory($catData = array()){
 		if(empty($catData)){
 			return 0;
 		}
 		$status = $this->db->insert('event_category',array(
 												'category_banner'=>$catData['category_banner']));

 		if($status){
 			$cat_id = $this->db->insert_id();
 			$languages = getLanguages();
	 		if(!empty($languages)){
	 			$insertArr = array();
	 			foreach ($languages AS $lang) {
	 				if((!isset($catData['category_name_'.$lang]) ||
	 				   empty($catData['category_name_'.$lang])) && 
	 				   (!isset($catData['category_image_'.$lang]) || 
	 				   empty($catData['category_image_'.$lang]))){
	 					continue;
	 				}
	 				$catName = (isset($catData['category_name_'.$lang]) && !empty($catData['category_name_'.$lang]))?$catData['category_name_'.$lang]:'';
	 				$catImage = (isset($catData['category_image_'.$lang]) && !empty($catData['category_image_'.$lang]))?$catData['category_image_'.$lang]:'';

	 				$insertArr[] = array('category_id'=>$cat_id,
	 									 'category_name'=>$catName,
	 									 'language_code'=>$lang,
	 					                 'category_image'=>$catImage);
	 			}
	 			if(!empty($insertArr)){
	 				$this->db->insert_batch('translator_category',$insertArr);
	 			}
	 		}
 		}
 		return $status;
 	}

 	public function updateCategory($category_id = '', $catData = array()){
 		if(empty($category_id) || empty($catData)){
 			return 0;
 		}
 		if(isset($catData['category_banner']) && !empty($catData['category_banner'])){
 			$status = $this->db->update('event_category',
 										array('category_banner'=>$catData['category_banner']),
 										array('cat_id'=>$category_id));
 		}

		$languages = getLanguages();
 		if(!empty($languages)){
 			$insertArr = array();
 			foreach ($languages AS $lang) {
 				if((!isset($catData['category_name_'.$lang]) || empty($catData['category_name_'.$lang])) && 
 				   (!isset($catData['category_image_'.$lang]) || empty($catData['category_image_'.$lang]))){
 					continue;
 				}
 				if(isset($catData['category_name_'.$lang]) && !empty($catData['category_name_'.$lang])){
 					$upArr['category_name'] = $catData['category_name_'.$lang];
 				}
 				if(isset($catData['category_image_'.$lang]) && !empty($catData['category_image_'.$lang])){
 					$upArr['category_image'] = $catData['category_image_'.$lang];
 				}
 				$status = $this->db->update('translator_category',$upArr,
								  			array('category_id'=>$category_id,'language_code'=>$lang));
 			}
 		}
 		return $status;
 	}

 	public function changeStatus($category_id = '', $status = '0'){
 		if(empty($category_id)){
 			return 0;
 		}
 		$status = $this->db->update('event_category',array('status'=>$status),array('cat_id'=>$category_id));
 		return $status;
 	}

 	public function catReorder($catArray = array()){
 		if(empty($catArray)){
 			return 0;
 		}
 		$priority = 1;
 		foreach($catArray AS $cat_id) {
 			$this->db->update('event_category',array('priority'=>$priority),array('cat_id'=>$cat_id));
 			$priority += 1;
 		}
 		return 1;
 	}
}
?>