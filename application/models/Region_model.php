<?php 
class Region_model extends CI_Model {
	public function _consruct(){
		parent::_construct();
 	}

 	public function getRegionData($region_id='',$view=''){
 		$cond  = (!empty($view))?" status IN ($view) ":" status != '2' ";
 		$cond .= (!empty($region_id))?" AND id='$region_id' ":"";

 		$regionData = $this->db->query("SELECT * FROM region WHERE $cond");

 		if(!empty($regionData)){
 			if(empty($region_id)){
 				$regionData = $regionData->result_array();
 				foreach ($regionData AS $key => $region) {
 					$regData = langTranslator($region['id'],'REG','');
					$regionData[$key] = array_merge($regionData[$key],$regData);
 				}
 			} else {
 				$regData = langTranslator($region_id,'REG','');
 				$regionData = $regionData->row_array();
				$regionData = array_merge($regionData,$regData);
 			}
 			return json_decode(json_encode($regionData));
 		} else {
 			return 0;
 		}
 	}

 	public function createRegion($regionData = array()){
 		if(empty($regionData)){
 			return 0;
 		}
 		$status = $this->db->insert('region',array('region_icon'=>$regionData['region_icon'],
		 										   'region_lat'=>$regionData['region_lat'],
		 										   'region_lng'=>$regionData['region_lng']));
 		if($status){
 			$region_id = $this->db->insert_id();
 			$languages = getLanguages();
	 		if(!empty($languages)){
	 			$insertArr = array();
	 			foreach ($languages AS $lang) {
	 				if(!isset($regionData['name_'.$lang]) || empty($regionData['name_'.$lang])){
	 					continue;
	 				}
	 				$insertArr[] = array('region_id'=>$region_id,
	 									 'language_code'=>$lang,
	 					                 'region_name'=>$regionData['name_'.$lang]);
	 			}
	 			if(!empty($insertArr)){
	 				$this->db->insert_batch('translator_region',$insertArr);
	 			}
	 		}
 		}
 		return $status;
 	}

 	public function updateRegion($region_id = '', $regionData = array()){
 		if(empty($region_id) || empty($regionData)){
 			return 0;
 		}
 		$languages = getLanguages();
 		if(!empty($languages)){
 			$insertArr = array();
 			foreach ($languages AS $lang) {
 				if(!isset($regionData['name_'.$lang]) || empty($regionData['name_'.$lang])){
 					unset($regionData['name_'.$lang]);
 					continue;
 				}
 				$insertArr[] = array('region_id'=>$region_id,
 									 'language_code'=>$lang,
 					                 'region_name'=>$regionData['name_'.$lang]);
 				unset($regionData['name_'.$lang]);
 			}
			$this->db->delete('translator_region',array('region_id'=>$region_id));
 			if(!empty($insertArr)){
 				$this->db->insert_batch('translator_region',$insertArr);
 			}
 		}

 		$status = $this->db->update('region',$regionData,array('id'=>$region_id));
 		return $status;
 	}

 	public function changeStatus($region_id = '', $status = '0'){
 		if(empty($region_id)){
 			return 0;
 		}
 		$status = $this->db->update('region',array('status'=>$status),
 										     array('id'=>$region_id));
 		return $status;
 	}

 	public function getlocalityData($region_id = '',$locality_id = '', $status = '0'){
 		$cond = "status IN (".$status.") ";
		$cond .= (!empty($region_id))?" AND region_id='$region_id' ":"";
		$cond .= (!empty($locality_id))?" AND id='$locality_id' ":"";
 		$locData = $this->db->query("SELECT *, id AS locality_id FROM locality WHERE ".$cond);

 		if(!empty($locData)){
 			if(empty($locality_id)){
 				$locData = $locData->result_array();
 				foreach ($locData AS $key => $locality) {
 					$regData = langTranslator($locality['id'],'LOC','');
					$locData[$key] = array_merge($locData[$key],$regData);
 				}
 				return json_decode(json_encode($locData));
 			} else if(!empty($locData = $locData->row_array())) {
 				$regData = langTranslator($locality_id,'LOC','');
				$locData = array_merge($locData,$regData);
 				return json_decode(json_encode($locData));
 			}
 		}
		return;
 	}
}
?>
