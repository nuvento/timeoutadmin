<?php 
class Tag_model extends CI_Model {
	public function _consruct(){
		parent::_construct();
 	}

 	public function getTagData($tag_id='',$view=''){
 		$cond  = (!empty($view))?" status IN ($view) ":" status != '2' ";
 		$cond .= (!empty($tag_id))?" AND tag_id='$tag_id' ":"";

 		$tagData = $this->db->query("SELECT * FROM tags WHERE $cond");
 		if(empty($tagData)){
 			return 0;
 		}

 		if(empty($tag_id)){
 			$tagData = $tagData->result_array();
 			foreach ($tagData AS $key => $data) {
 				$rtlData = langTranslator($data['tag_id'],'TAG');
				$tagData[$key] = array_merge($tagData[$key],$rtlData);
 			}
 		} else {
			$tagData = $tagData->row_array();
			$rtlData = langTranslator($tagData['tag_id'],'TAG');
			$tagData = array_merge($tagData,$rtlData);
 		}
		return json_decode(json_encode($tagData));
 	}

 	public function createTag($tagData = array()){
 		if(empty($tagData)){
 			return 0;
 		}
 		$status = $this->db->insert('tags',array('tag'=>$tagData['tag_EN']));
 		if($status){
 			$tag_id = $this->db->insert_id();
 			$languages = getLanguages();
	 		if(!empty($languages)){
	 			$insertArr = array();
	 			foreach ($languages AS $lang) {
	 				if(!isset($tagData['tag_'.$lang]) || empty($tagData['tag_'.$lang])){
	 					continue;
	 				}
	 				$insertArr[] = array('tag_id'=>$tag_id,
	 									 'language_code'=>$lang,
	 					                 'tag'=>$tagData['tag_'.$lang]);
	 			}
	 			if(!empty($insertArr)){
	 				$this->db->insert_batch('translator_tag',$insertArr);
	 			}
	 		}
 		}
 		return $status;
 	}

 	public function updateTags($tag_id = '', $tagData = array()){
 		if(empty($tag_id) || empty($tagData)){
 			return 0;
 		}
 		$status = $this->db->update('tags',array('tag'=>$tagData['tag_EN']),array('tag_id'=>$tag_id));
 		if($status){
 			$languages = getLanguages();
	 		if(!empty($languages)){
	 			$insertArr = array();
	 			foreach ($languages AS $lang) {
	 				if(!isset($tagData['tag_'.$lang]) || empty($tagData['tag_'.$lang])){
	 					continue;
	 				}
	 				$insertArr[] = array('tag_id'=>$tag_id,
	 									 'language_code'=>$lang,
	 					                 'tag'=>$tagData['tag_'.$lang]);
	 			}
 				$this->db->delete('translator_tag',array('tag_id'=>$tag_id));
	 			if(!empty($insertArr)){
	 				$this->db->insert_batch('translator_tag',$insertArr);
	 			}
	 		}
 		}
 		return $status;
 	}

 	public function changeStatus($tag_id = '', $status = '0'){
 		if(empty($tag_id)){
 			return 0;
 		}
 		$status = $this->db->update('tags',array('status'=>$status),array('tag_id'=>$tag_id));
 		return $status;
 	}
}
?>