<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Country extends CI_Controller {

	public function __construct() {
		parent::__construct();
        date_default_timezone_set("Asia/Riyadh");
        $this->load->model('Country_model');
		
        if(!$this->session->userdata('logged_in')) {
			redirect(base_url('Login'));
		}
        $role = roleManagement();
        if(!array_key_exists('Country',$role)){
            redirect(base_url('Dashboard'));
        }
 	}
	
	function listCountry(){
		$template['page'] = 'Country/viewCountryList';
        $template['menu'] = 'Country Management';
        $template['smenu'] = 'View Country List';
        $template['pTitle'] = "Country Management";
        $template['pDescription'] = "View Country List";

        $template['countryData'] = $this->Country_model->getCountryData();
		$this->load->view('template',$template);
	}

	function addCountry(){
        $this->load->model('Country_model');
        $template['countryData'] = $this->Country_model->getCountryData();

		$template['page'] = 'Country/countryAddForm';
        $template['menu'] = 'Country Management';
        $template['smenu'] = 'Add Country';
        $template['pTitle'] = "Add Country";
        $template['pDescription'] = "Create New Country";

		$this->load->view('template',$template);
	}

	function createCountry(){
		$flashMsg = array('message'=>'Something went wrong, please try again..!','class'=>'error');

		if(!isset($_POST) || empty($_POST) || !isset($_FILES) || empty($_FILES)){
            $this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('Country/addCountry'));
        }

		$err = 0;
        $errMsg = '';
        if($err == 0 && (!isset($_POST['country_name']) || empty($_POST['country_name']))){
            $err = 1;
            $errMsg = 'Provide a Country Name';
        } else if($err == 0 && (!isset($_POST['country_code']) || empty($_POST['country_code']))){
            $err = 1;
            $errMsg = 'Provide a Country Code';
        } else if($err == 0 && (!isset($_POST['language']) || empty($_POST['language']))){
            $err = 1;
            $errMsg = 'Provide a Language';
        } else if($err == 0 && (!isset($_POST['language_code']) || empty($_POST['language_code']))){
            $err = 1;
            $errMsg = 'Provide a Language Code';
        } else if($err == 0 && (!isset($_POST['currency']) || empty($_POST['currency']))){
            $err = 1;
            $errMsg = 'Provide a Currency';
        } else if($err == 0 && (!isset($_POST['currency_symbol']) || empty($_POST['currency_symbol']))){
            $err = 1;
            $errMsg = 'Provide a Currency Symbol';
        } else if($err == 0 && (!isset($_FILES) || empty($_FILES) || 
                                !isset($_FILES['country_flag']) || empty($_FILES['country_flag']))){
            $err = 1;
            $errMsg = 'Provide a Category Flag';
        }
        
        if($err == 0){
            $this->load->library('upload');
            $config = set_upload_service("assets/uploads/services");

            $config['file_name'] = time()."_".$_FILES['country_flag']['name'];
            $this->upload->initialize($config);
            if(!$this->upload->do_upload('country_flag')){
                $err = 1;
                $errMsg = $this->upload->display_errors();
            }else{
                $upload_data = $this->upload->data();
                $_POST['country_flag'] = $config['upload_path']."/".$upload_data['file_name'];
            }
        }

        if($err == 1){
            $flashMsg['message'] = $errMsg;
            $this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('Country/addCountry'));
        }

        $status = $this->Country_model->createCountry($_POST);
        if($status == 1){
            $flashMsg['class'] = 'success';
            $flashMsg['message'] = 'Country Created';

            $this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('Country/listCountry'));
        }
        $this->session->set_flashdata('message',$flashMsg);
        redirect(base_url('Country/addCountry'));
	}

	function editCountry($country_id=''){
        $flashMsg = array('message'=>'Something went wrong, please try again..!','class'=>'error');
		if(empty($country_id) || empty(decode_param($country_id))){
			$this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('Country/listCountry'));
		}
		$template['page'] = 'Country/countryAddForm';
        $template['menu'] = 'Country Management';
        $template['smenu'] = 'Edit Country';
        $template['pTitle'] = "Edit Country";
        $template['pDescription'] = "Update Country Data";

        $template['country_id'] = $country_id;
        $template['countryData'] = $this->Country_model->getCountryData(decode_param($country_id));
		$this->load->view('template',$template);
	}

	function updateCountry($country_id=''){
		$flashMsg = array('message'=>'Something went wrong, please try again..!','class'=>'error');

		if(!isset($_POST) || empty($_POST) || empty($country_id)){
            $this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('Country/listCountry'));
        }

		$err = 0;
        $errMsg = '';
        if($err == 0 && (!isset($_POST['country_name']) || empty($_POST['country_name']))){
            $err = 1;
            $errMsg = 'Provide a Country Name';
        } else if($err == 0 && (!isset($_POST['country_code']) || empty($_POST['country_code']))){
            $err = 1;
            $errMsg = 'Provide a Country Code';
        } else if($err == 0 && (!isset($_POST['language']) || empty($_POST['language']))){
            $err = 1;
            $errMsg = 'Provide a Language';
        } else if($err == 0 && (!isset($_POST['language_code']) || empty($_POST['language_code']))){
            $err = 1;
            $errMsg = 'Provide a Language Code';
        } else if($err == 0 && (!isset($_POST['currency']) || empty($_POST['currency']))){
            $err = 1;
            $errMsg = 'Provide a Currency';
        } else if($err == 0 && (!isset($_POST['currency_symbol']) || empty($_POST['currency_symbol']))){
            $err = 1;
            $errMsg = 'Provide a Currency Symbol';
        }

        if($err == 0){
            $this->load->library('upload');
            $config = set_upload_service("assets/uploads/services");

            $config['file_name'] = time()."_".$_FILES['country_flag']['name'];
            $this->upload->initialize($config);
            if($this->upload->do_upload('country_flag')){
                $upload_data = $this->upload->data();
                $_POST['country_flag'] = $config['upload_path']."/".$upload_data['file_name'];
            }
        }

        if($err == 1){
            $flashMsg['message'] = $errMsg;
            $this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('Country/editCountry/'.$country_id));
        }

        $status = $this->Country_model->updateCountry(decode_param($country_id),$_POST);
        if($status == 1){
            $flashMsg['class'] = 'success';
            $flashMsg['message'] = 'Country Created';

            $this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('Country/listCountry'));
        }
        $this->session->set_flashdata('message',$flashMsg);
        redirect(base_url('Country/editCountry/'.$country_id));
	}
    
    function changeStatus(){
        if(!isset($_POST) || !isset($_POST['country_id']) || empty($_POST['country_id']) || 
           !isset($_POST['status']) || $_POST['status'] == ''){
            echo json_encode(array('status'=>'0'));exit;
        }
        $status = $_POST['status'];
        $country_id = decode_param($_POST['country_id']);
        $resp = $this->Country_model->changeStatus($country_id,$status);
        if($resp){
            echo json_encode(array('status'=>'1'));exit;
        }
        echo json_encode(array('status'=>'0'));exit;
    }
}
?>
