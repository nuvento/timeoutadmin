<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
  header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
  header('Access-Control-Allow-Credentials: true');
  header('Access-Control-Max-Age: 86400'); 
}

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
  if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])){
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
  }

  if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])){
    header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
  }
  exit(0);
}

class OrganizerServices extends CI_Controller {

  var $auth_token;
  private $cipher = "AES-256-CBC";
  public function __construct() {
    parent::__construct();
    date_default_timezone_set("Asia/Riyadh");
    $this->load->model('Organizer_model');    
    $this->load->model('Validation_organizer_model');
    $method = $this->router->fetch_method();        
    $data = (array) json_decode(file_get_contents('php://input'));
    if($method == 'addEvent') {
      $data = $_POST;
    }
    if (isset(apache_request_headers()['Auth'])) {
      $this->auth_token = apache_request_headers()['Auth'];
      $data['auth_token'] = $this->auth_token;
    }
    $res = $this->Validation_organizer_model->validation_check($method, $data);
    if($res['state'] == 1) {
      $this->errorResponse($res['response']['code'], $res['response']['message']);
      die;
    }   
  }

  public function response($data) {
    $result =  array(
      'code' => 1,
      'message' => 'Success',
      'successData' =>$data
    );
    print json_encode($result);exit;
  }

  public function errorResponse($errorCode, $errorDesc) {
    $result =  array(
      'code' => 0,
      'message' => 'error',
      'errorCode'=> $errorCode,
      'errorDesc'=> $errorDesc
    );
    print json_encode($result);exit;
  }

  public function successResponse($data) {
    $result =  array(
      'code'=>1,'status' => 'success','message'=>(isset($data['message']) && !empty($data['message']))?$data['message']:'success');
    print json_encode($result);
  }

  public function getOrganizerEventList(){
    $data = (array) json_decode(file_get_contents('php://input'));
    $res = $this->Organizer_model->getOrganizerEventList($data);
    if(isset($res['data']) && !empty($res['data'])){
      $this->response($res['data']);
    }
    else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function getOrganizerEventDetails(){
    $data = (array) json_decode(file_get_contents('php://input'));
    $res = $this->Organizer_model->event($data);
    if(isset($res['data']) && !empty($res['data'])){
      $this->response($res['data']);
    }
    else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function editOrganizerEventDetails(){
    $data =(array)json_decode(file_get_contents('php://input'));
    $res = $this->Organizer_model->editOrganizerEventDetails($data);
    if($res['status'] != 0){
      $this->successResponse($res);
    }else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function deleteOrganizerEvent(){
    $data = (array) json_decode(file_get_contents('php://input'));
    $res = $this->Organizer_model->changeOrganizerEventStatus($data,2);
    if($res['status'] != '0'){
      $this->successResponse($res);
    }else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function deActivateOrganizerEvent(){
    $data = (array) json_decode(file_get_contents('php://input'));
    $res = $this->Organizer_model->changeOrganizerEventStatus($data,0);
    if($res['status'] != '0'){
      $this->successResponse($res);
    }else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function getVenueList(){
    $data = (array) json_decode(file_get_contents('php://input'));
    $res = $this->Organizer_model->getVenueList($data);
    if(isset($res['data']) && !empty($res['data'])){
      $this->response($res['data']);
    }else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function getVenueDetails(){
    $data = (array) json_decode(file_get_contents('php://input'));
    $res = $this->Organizer_model->getVenueDetails($data);
    if(isset($res['data']) && !empty($res['data'])){
      $this->response($res['data']);
    }else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function getCustomerList(){
    $data = (array) json_decode(file_get_contents('php://input'));
    $res = $this->Organizer_model->getCustomerList($data);
    if(isset($res['data']) && !empty($res['data'])){
      $this->response($res['data']);
    }else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function getBookingList(){
    $data = (array) json_decode(file_get_contents('php://input'));
    $res = $this->Organizer_model->getBookingList($data);
    if(isset($res['data']) && !empty($res['data'])){
      $this->response($res['data']);
    }else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function getBookingDetails(){
    $data = (array) json_decode(file_get_contents('php://input'));
    $res = $this->Organizer_model->getBookingDetails($data);
    if(isset($res['data']) && !empty($res['data'])){
      $this->response($res['data']);
    }else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function organiserSignUp(){
    $data = (array) json_decode(file_get_contents('php://input'));
    $res = $this->Organizer_model->organiserSignUp($data);
    if($res['status'] != 0){
      $this->successResponse($res);
    }
    else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function signIn(){
    $data = (array) json_decode(file_get_contents('php://input'));
    $res = $this->Organizer_model->signIn($data);
    if($res['status'] != 0){
      $this->response($res['data']);
    }else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function changePassword(){
    $data = (array) json_decode(file_get_contents('php://input'));
    $res = $this->Organizer_model->changePassword($data);
    if($res['status'] != 0){
      $this->successResponse($res);
    }else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function checkerDeactivate(){
    $data = (array) json_decode(file_get_contents('php://input'));
    $res = $this->Organizer_model->checkerDeactivate($data);
    if($res['status'] != 0){
      $this->successResponse($res);
    }else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function checkerDelete(){
    $data = (array) json_decode(file_get_contents('php://input'));
    $res = $this->Organizer_model->checkerDelete($data);
    if($res['status'] != 0){
      $this->successResponse($res);
    }else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function getTagList(){
    $data = (array) json_decode(file_get_contents('php://input'));
    $res = $this->Organizer_model->getTagList($data);
    if($res['status'] != 0){
      $this->response($res['data']);
    }else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function addEvent(){
    $data = $_POST;
    $res = $this->Organizer_model->addEvent($data);
    if($res['status'] != 0){
      $this->successResponse($res);
    }else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function getLanguages(){
    $data = (array) json_decode(file_get_contents('php://input'));
    $res = $this->Organizer_model->getLanguages();
    if($res['status'] != 0){
      $this->response($res['data']);
    }else{
      $this->errorResponse($res['code'],$res['message']);
    }
  } 

  public function getCategories(){
    $data = (array) json_decode(file_get_contents('php://input'));
    $res = $this->Organizer_model->getCategories($data);
    if($res['status'] != 0){
      $this->response($res['data']);
    }else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function searchEvent(){
    $data = (array) json_decode(file_get_contents('php://input'));
    $res = $this->Organizer_model->searchEvent($data);
    if($res['status'] != 0){
      $this->response($res['data']);
    }else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function searchCustomer(){
    $data = (array) json_decode(file_get_contents('php://input'));
    $res = $this->Organizer_model->searchCustomer($data);
    if($res['status'] != 0){
      $this->response($res['data']);
    }else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function searchBooking(){
    $data = (array) json_decode(file_get_contents('php://input'));
    $res = $this->Organizer_model->searchBooking($data);
    if($res['status'] != 0){
      $this->response($res['data']);
    }else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function searchChecker(){
    $data = (array) json_decode(file_get_contents('php://input'));
    $res = $this->Organizer_model->searchChecker($data);
    if($res['status'] != 0){
      $this->response($res['data']);
    }else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function getCountryCode(){
    $data = (array) json_decode(file_get_contents('php://input'));
    $res = $this->Organizer_model->getCountryCode($data);
    if($res['status'] != 0){
      $this->response($res['data']);
    }else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function getCheckerList(){
    $data = (array) json_decode(file_get_contents('php://input'));
    $res = $this->Organizer_model->getCheckerList($data);
    if($res['status'] != 0){
      $this->response($res['data']);
    }else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function getEvent(){
    $data = (array) json_decode(file_get_contents('php://input'));
    $res = $this->Organizer_model->getEvent($data);
    if($res['status'] != 0){
      $this->response($res['data']);
    }else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function checker_bookingDetails(){
    $data = (array) json_decode(file_get_contents('php://input'));
    $res = $this->Organizer_model->checkerbookingdetails($data);
    if($res['status']!=0){
      $this->response($res['data']);
    }
    else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function getCheckerBookList(){
    $data = (array) json_decode(file_get_contents('php://input'));
    $res = $this->Organizer_model->getCheckerBookList($data);
    if(isset($res['data']) && !empty($res['data'])){
      $this->response($res['data']);
    }
    else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function acceptBooking(){
    $data = (array) json_decode(file_get_contents('php://input'));
    $res = $this->Organizer_model->acceptBooking($data);
    if($res['status'] != 0){
      $this->successResponse($res);
    }else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  /*================ END : Organizer API ================*/
}
?>
