<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer_Booking extends CI_Controller {

	public function __construct() {
		parent::__construct();
        date_default_timezone_set("Asia/Riyadh");

		if(!$this->session->userdata('logged_in')) {
			redirect(base_url('Login'));
		}

		$userType = $this->session->userdata['user_type'];
		if($userType != 5){
			redirect(base_url('Dashboard'));
		}
 	}
	
	public function index() {
		$template['page'] = 'Customer_Booking/care_home';
        $template['menu'] = 'Booking Management';
        $template['smenu'] = 'View Bookings';
        $template['pTitle'] = "View Bookings";
        $template['pDescription'] = "View and Manage Bookings";
        $template['page_head'] = "Booking Management";

        if(isset($_POST) && !empty($_POST) && 
    	   ((isset($_POST['user_id']) && !empty($_POST['user_id'])) || 
    	   	(isset($_POST['booking_id']) && !empty($_POST['booking_id'])))){

        	$user_id = $booking_id = '';
        	$bookData = $custData = array();
        	$this->load->model('Booking_model');
        	$this->load->model('Customer_model');

        	if (isset($_POST['user_id']) && !empty($user_id = $_POST['user_id']) && 
        		isset($_POST['booking_id']) && !empty($booking_id = $_POST['booking_id'])){
        		$bookData = $this->Booking_model->getBookingData('','','','',$booking_id);
				$custData = $this->Customer_model->getCustomerData(decode_param($user_id));
    		} else if (isset($_POST['user_id']) && !empty($user_id = $_POST['user_id'])){
				$bookData = $this->Booking_model->getBookingData('','','',decode_param($user_id));
				$custData = $this->Customer_model->getCustomerData(decode_param($user_id));
    		} else if (isset($_POST['booking_id']) && !empty($booking_id = $_POST['booking_id'])){
    			$bookData = $this->Booking_model->getBookingData('','','','',$booking_id);
    			if (!empty($bookData)){
    				$custData = $this->Customer_model->getCustomerData($bookData[0]->customer_id);	
    			}
    		}
    		$template['bookData'] = $bookData;
    		$template['custData'] = $custData;
        }
		$this->load->view('template',$template);
	}

	public function userSearch(){
		if(!isset($_GET) || empty($_GET) || !isset($_GET['term']) || empty($key = $_GET['term'])){
			exit;
		}
		$sql = "SELECT customer_id,name,phone,email FROM customer 
				WHERE name LIKE '%".$key."%' OR phone LIKE '%".$key."%' OR email LIKE '%".$key."%' LIMIT 10";
		$cust = $this->db->query($sql)->result_array();

		if(empty($cust)){
			exit;
		}
		$custDetails = array();
		foreach ($cust AS $custData) {
			$name  = (!empty($custData['name']))?'Name : '.$custData['name']:'Name : -- ';
			$name .= (!empty($custData['email']))?', Mail: '.$custData['email']:'';
			$name .= (!empty($custData['phone']))?', Phone: '.$custData['phone']:'';
			$cust_id = encode_param($custData['customer_id']);
			$custDetails[] =array('data'=>$cust_id,'label'=>$name);
		}
		echo json_encode($custDetails);
	}
}
?>