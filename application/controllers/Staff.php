<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Staff extends CI_Controller {

	public function __construct() {
		parent::__construct();
        date_default_timezone_set("Asia/Riyadh");
		$this->load->model('Staff_model');

		if(!$this->session->userdata('logged_in')) {
			redirect(base_url());
		}
	}

	public function addStaff(){
		$template['page'] = 'Staff/staffForm';
        $template['menu'] = 'Staff Management';
        $template['smenu'] = 'Add Staff';
        $template['pTitle'] = "Add Staff";
        $template['pDescription'] = "Create New Staff";

		$this->load->view('template',$template);
	}

	public function viewStaffs(){
		$template['page'] = 'Staff/viewStaff';
        $template['menu'] = 'Staff Management';
        $template['smenu'] = 'View Staffs';
        $template['pTitle'] = "View Staffs";
        $template['pDescription'] = "View and Manage Staffs";
        $template['page_head'] = "Staff Management";

        $template['staff_data'] = $this->Staff_model->getStaffData('','0,1');

		$this->load->view('template',$template);
	}
	
    function changeStatus(){
        if(!isset($_POST) || !isset($_POST['staff_id']) || empty($_POST['staff_id']) || 
           !isset($_POST['status']) || $_POST['status'] == ''){
            echo json_encode(array('status'=>'0'));exit;
        }
        $status = $_POST['status'];
        $staff_id = decode_param($_POST['staff_id']);
        $resp = $this->Staff_model->changeStatus($staff_id,$status);
        if($resp){
            echo json_encode(array('status'=>'1'));exit;
        }
        echo json_encode(array('status'=>'0'));exit;
    }

	public function createStaff(){
		$err = 0;
		$errMsg = '';
		$flashMsg = array('message'=>'Something went wrong, please try again..!','class'=>'error');
		if(!isset($_POST) || empty($_POST) || !isset($_FILES) || empty($_FILES)){
			$this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('Staff/addStaff'));
		}

		if($err == 0 && (!isset($_POST['display_name']) || empty($_POST['display_name']))){
			$err = 1;
			$errMsg = 'Provide Staff Name';
		}else if($err == 0 && (!isset($_POST['username']) || empty($_POST['username']))){
			$err = 1;
			$errMsg = 'Provide a Staff Username (Email-ID)';
		}else if($err == 0 && (!isset($_POST['password']) || empty($_POST['password']))){
			$err = 1;
			$errMsg = 'Provide a Proper Password';
		}
        
        if($err == 0){
	        $config = set_upload_service("assets/uploads/services");
	        $this->load->library('upload');
	        $config['file_name'] = time()."_".$_FILES['profile_image']['name'];
	        $this->upload->initialize($config);
	        if(!$this->upload->do_upload('profile_image')){
	        	$err = 1;
				$errMsg = $this->upload->display_errors();
	        }else{
	            $upload_data = $this->upload->data();
	            $_POST['profile_image'] = $config['upload_path']."/".$upload_data['file_name'];
	        }
	    }

		if($err == 1){
			$flashMsg['message'] = $errMsg;
			$this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('Staff/addStaff'));
		}
		$password = $_POST['password'];
		$_POST['password']= md5($_POST['password']);
        $status = $this->Staff_model->addStaff($_POST);
        if($status == 1){
			$this->load->model('Api_model');

			$subject  = "Your TimeOut Staff Account is now active";
			$email_id = $_POST['username'];
 			$template = getNotifTemplate();

			$message = "<html>
							<body>
								Hi,\n\r Welcome to TimeOut. \r\n Please use username: 
								".$email_id." and Password: ".$password." for access your account 
								<br>
							</body>
						</html>";

            if(isset($template['staff_reg_mail']) && !empty($template['staff_reg_mail'])){
                $message = str_replace(array('{:user_name}','{:password}'),array($email_id,$password),
                                       $template['staff_reg_mail']);
            }

			$this->Api_model->send_mail($subject,$email_id,$message);

            $flashMsg =array('message'=>'Successfully Created Your Account..!','class'=>'success');
            $this->session->set_flashdata('message', $flashMsg);
            redirect(base_url('Staff/viewStaffs'));
        } else if($status == 2){
            $flashMsg = array('message'=>'Username (Email-ID) already exist..!','class'=>'error');
            $this->session->set_flashdata('message', $flashMsg);
            redirect(base_url('Staff/addStaff'));
        } else {
            $this->session->set_flashdata('message', $flashMsg);
            redirect(base_url('Staff/addStaff'));
        }
	}

	public function editStaffs($staff_id){
		$flashMsg = array('message'=>'Something went wrong, please try again..!','class'=>'error');
		if(empty($staff_id) || !is_numeric($staff_id = decode_param($staff_id))){
			$this->session->set_flashdata('message',$flashMsg);
        	redirect(base_url('Staff/viewStaffs'));
		}

		$template['page'] = 'Staff/staffForm';
        $template['menu'] = 'Staff Management';
        $template['smenu'] = 'Edit Staff';
        $template['pTitle'] = "Edit Staffs";
        $template['pDescription'] = "Update Staff Data";

        $template['staff_data'] = $this->Staff_model->getStaffData($staff_id,'0,1');
        $template['staff_id'] = encode_param($staff_id);
		$this->load->view('template',$template);
	}

	public function updateStaff($staff_id = ''){
		$err = 0;
		$errMsg = '';
		$flashMsg = array('message'=>'Something went wrong, please try again..!','class'=>'error');
		if(!isset($_POST) || empty($_POST) || !isset($_FILES) || empty($_FILES)){
			$this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('Staff/addStaff'));
		}
		
		if($err == 0 && (!isset($_POST['display_name']) || empty($_POST['display_name']))){
			$err = 1;
			$errMsg = 'Provide Staff Name';
		}else if($err == 0 && (!isset($_POST['username']) || empty($_POST['username']))){
			$err = 1;
			$errMsg = 'Provide a Staff Username (Email-ID)';
		}
    	
        if($err == 0){
	        $config = set_upload_service("assets/uploads/services");
	        $this->load->library('upload');
	        $config['file_name'] = time()."_".$_FILES['profile_image']['name'];
	        $this->upload->initialize($config);
	        if($this->upload->do_upload('profile_image')){
	            $upload_data = $this->upload->data();
	            $_POST['profile_image'] = $config['upload_path']."/".$upload_data['file_name'];
	        }
	    }

		if($err == 1){
			$flashMsg['message'] = $errMsg;
			$this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('Staff/editStaffs/'.$staff_id));
		}
		
        $status = $this->Staff_model->updateStaff(decode_param($staff_id),$_POST);
        if($status == 1){
            $flashMsg =array('message'=>'Successfully Updated Staff Details..!','class'=>'success');
            $this->session->set_flashdata('message', $flashMsg);
            redirect(base_url('Staff/viewStaffs'));
        } else if($status == 2){
            $flashMsg = array('message'=>'Username (Email-ID) already exist..!','class'=>'error');
            $this->session->set_flashdata('message', $flashMsg);
            redirect(base_url('Staff/editStaffs/'.$staff_id));
        } else {
            $this->session->set_flashdata('message', $flashMsg);
            redirect(base_url('Staff/editStaffs/'.$staff_id));
        }
	}
}
?>