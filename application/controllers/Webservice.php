<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400'); 
    }

  if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
      header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
      header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
  }
//test changes
class Webservice extends CI_Controller {
  var $auth_token;

  public function __construct() {
    parent::__construct();
    date_default_timezone_set("Asia/Riyadh");
    $this->load->model('Webservice_model');   
    $this->load->model('Validation_app_model');
    $method = $this->router->fetch_method();  
    $data = (array) json_decode(file_get_contents('php://input'));
    if($method == 'profile') {
      $data = $_POST;
    }
    if (isset(apache_request_headers()['Auth']) || isset(apache_request_headers()['auth'])) {
      $this->auth_token = (isset(apache_request_headers()['Auth']))?apache_request_headers()['Auth']:apache_request_headers()['auth'];
      $data['auth_token'] = $this->auth_token;
    }
    $res = $this->Validation_app_model->validation_check($method, $data);
    if($res['state'] == 1) {
      $this->errorResponse($res['response']['code'], $res['response']['message']);
      die;
    }     
  }

  public function update_fcm_token(){
    $data = (array) json_decode(file_get_contents('php://input'));
    $data['auth_token'] = $this->auth_token;
    $res = $this->Webservice_model->update_fcm_token($data);
    if($res['status']!=0){
      $this->successResponse($res);
    }
    else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }
  
  public function login() {
    $data = (array) json_decode(file_get_contents('php://input'));
    $res = $this->Webservice_model->login($data);
    if($res['status']!=0){
      $this->response($res['data']);
    }
    else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function check_email_availability() {
    $data = (array) json_decode(file_get_contents('php://input'));
    $res = $this->Webservice_model->availability($data);
    if($res['status']!=0){
      $this->response($res['data']);
    }
    else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function registration(){
    $data = (array) json_decode(file_get_contents('php://input'));
    $res = $this->Webservice_model->register($data);
    if($res['status']!=0){
      $this->response($res['data']);
    }
    else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function get_events_list() {
    $data = $_GET;
    $data['auth_token'] = $this->auth_token;
    $res = $this->Webservice_model->discover($data);
    if($res['status']!=0 && sizeof($res['data'])){
      $this->responseEventList($res['data']);
    }elseif($res['status']!=0 && sizeof($res['data'] == 0)){
      $this->response($res['data']);
    }else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function events_details($event_id = '') {
    if(empty($event_id) && (!isset($_GET['event_id']) || empty($event_id = $_GET['event_id']))){
      $this->errorResponse("ER16","Event id is null or empty");die;
    }
    $data['event_id'] = $event_id;
    $data['auth_token'] = $this->auth_token;
    $data['event_date_id'] = (isset($_GET['event_date_id']) && !empty($_GET['event_date_id']))?
                             $_GET['event_date_id']:'';
    
    $res = $this->Webservice_model->event($data);
    if($res['status'] != 0){
      $this->response($res['data']);
    } else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function response($data) {
    $result =  array(
      'status' => 'success',
      'data' =>$data
    );
    print json_encode($result);
  }

  public function favResponse($data) {
    $result =  array(
      'status' => 'success',
      'data' => array(
      'favorite_events' =>$data)
    );
    print json_encode($result);
  }

  public function successResponse($data='') {
    $result =  array(
      'status' => 'success',
    );
    print json_encode($result);
  }

  public function errorResponse($errorCode, $errorDesc) {
    $result =  array(
      'status' => 'error',
      'error'=> $errorCode,
      'message'=> $errorDesc
    );
    print json_encode($result);
  }

  public function get_category_list($query = null) {
    $data['query'] = !empty($_GET['query']) ? $_GET['query'] : '';
    $data['auth_token'] = $this->auth_token;
    $res = $this->Webservice_model->get_category_list($data);
    if($res['status']!=0){
      $this->response($res['data']);
    }
    else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }
  
  public function add_favorites(){
    $data = (array) json_decode(file_get_contents('php://input'));
    $data['auth_token'] = $this->auth_token;
    $res = $this->Webservice_model->add_favorites($data);
    if($res['status']!=0){
    $this->successResponse($res);
    }
    else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function get_cities_list() {
    $data['auth_token'] = $this->auth_token;
    $res = $this->Webservice_model->get_cities_list($data);
    if($res['status']!=0){
      $this->response($res['data']);
    }
    else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function update_city(){
    $data = (array) json_decode(file_get_contents('php://input'));
    $data['auth_token'] = $this->auth_token;
    $res = $this->Webservice_model->update_city($data);
    if($res['status']!=0){
      $this->response($res['data']);
    }
    else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function booking_summary($booking_id = null) {
    $data['booking_id'] = $_GET['booking_id'];
    if($data['booking_id'] == null) {
      $this->errorResponse("ER34","Booking id is null or empty");
      die;
    }
    $data['auth_token'] = $this->auth_token;
    $res = $this->Webservice_model->booking_summary($data);
    if($res['status']!=0){
      $this->response($res['data']);
    }
    else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function payment(){
    $data = (array) json_decode(file_get_contents('php://input'));
    $data['auth_token'] = $this->auth_token;
    $res = $this->Webservice_model->payment($data);
    if($res['status']!=0){
    $this->successResponse($res);
    }
    else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function event_rating(){
    $data = (array) json_decode(file_get_contents('php://input'));
    $data['auth_token'] = $this->auth_token;
    $res = $this->Webservice_model->event_rating($data);
    if($res['status']!=0){
    $this->successResponse($res);
    }
    else{
      $this->errorResponse($res['code'],$res['message']);
    }
  } 

  public function update_notification_email_status(){
    $data = (array) json_decode(file_get_contents('php://input'));
    $data['auth_token'] = $this->auth_token;
    $res = $this->Webservice_model->update_notification_email_status($data);
    if($res['status']!=0){
    $this->successResponse($res);
    }
    else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function profile_details() {
    $data['auth_token'] = $this->auth_token;
    $res = $this->Webservice_model->profile_details($data);
    if($res['status']!=0){
      $this->response($res['data']);
    }
    else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function profile_edit() {
    $data = $_POST;
    if(isset($_FILES['profile_photo'])) {
      $data['file'] = $_FILES['profile_photo'];
    } 
    $data['auth_token'] = $this->auth_token;
    $res = $this->Webservice_model->update_profile($data);
    if($res['status']!=0){
      $this->successResponse($res);
    }
    else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function booking() {
    $data = (array) json_decode(file_get_contents('php://input'));
    $data['auth_token'] = $this->auth_token;
    $res = $this->Webservice_model->booking($data);
    if($res['status']!=0){
      $this->response($res['data']);
    }
    else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function cancel_booking() {    
    $data = (array) json_decode(file_get_contents('php://input'));
    $data['auth_token'] = $this->auth_token;
    $res = $this->Webservice_model->cancel($data);
    if($res['status']!=0){
     $this->successResponse($res);
    }
    else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function get_favorites_list() {
    $data['auth_token'] = $this->auth_token;
    $res = $this->Webservice_model->favouritelist($data);
    if($res['status']!=0){
      $this->favResponse($res['data']);
    }
    else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function get_booking_list() {    
    $data = $_GET;
    $data['auth_token'] = $this->auth_token;
    $res = $this->Webservice_model->bookedlist($data);
    if(sizeof($res['data']) && $res['status']!=0){
      $this->responseBookList($res['data']);
    }elseif(sizeof($res['data'] == 0) && $res['status']!=0){
      $this->response($res['data']);
    }else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function settings() {
    $data['auth_token'] = $this->auth_token;
    $res = $this->Webservice_model->get_settings($data);
    if($res['status']!=0){
      $this->response($res['data']);
    }
    else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function forgot_password() {
    $data = (array) json_decode(file_get_contents('php://input'));
    $res = $this->Webservice_model->forgot_password($data);
    if($res['status']!=0){
      $this->successResponse($res);
    }
    else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function responseEventList($data) {
    $isLstBookAvail = (isset($data['is_last_booking_avail']))?$data['is_last_booking_avail']:'';
    $result = array(
                'status'=>'success',
                'data'=>array(
                            'is_last_booking_avail'=>$isLstBookAvail,
                            'city_name'=>(isset($data['city_name']))?$data['city_name']:'',
                            'events'=>(isset($data['events']))?$data['events']:''
                        ),
                'meta'=>(isset($data['meta']))?$data['meta']:''
             );
    print json_encode($result);
  }
  
  public function get_last_booking() {
    $data['auth_token'] = $this->auth_token;
    $res = $this->Webservice_model->get_last_booking($data);
    if($res['status']!=0){
      $this->response($res['data']);
    }
    else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }
  
  public function responseBookList($data) {
    $result =  array(
      'status' => 'success',
      'data' => array(
      'bookings' =>$data['bookings']),
      'meta' =>$data['meta']
    );
    print json_encode($result);
  }

  public function filters() {
    $data['auth_token'] = $this->auth_token;
    $res = $this->Webservice_model->filters($data);
    if($res['status']!=0){
      $this->response($res['data']);
    }
    else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }
  
  public function user_language(){
    $data = (array) json_decode(file_get_contents('php://input'));
    $data['auth_token'] = $this->auth_token;
    $res = $this->Webservice_model->user_language($data);
    if($res['status']!=0){
      $this->successResponse($res);
    }
    else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }  

  public function get_app_version(){
    $res = $this->Webservice_model->get_app_version();
    if($res['status'] != 0)
      $this->response($res['data']);
    else
      $this->errorResponse($res['code'],$res['message']);
  }
  
  public function logout(){
    $data = (array) json_decode(file_get_contents('php://input'));
    $data['auth_token'] = $this->auth_token;
    $res = $this->Webservice_model->logout($data);
    if($res['status'] != 0)
      $this->successResponse($res);
    else
      $this->errorResponse($res['code'],$res['message']);
  }

  public function event_search() {
    if(!isset($_GET) || !isset($_GET['query']) || empty($_GET['query'])){
      $this->errorResponse('ER18','Search Key Missing');
      exit;
    }
    $data = $_GET;   
    $data['auth_token'] = $this->auth_token;
    $res = $this->Webservice_model->event_search($data);
    if($res['status']!=0){
      $this->response($res['data']);
    }
    else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function convertCurrency(){
    $settings = getSettings();
    $currencyData = getCurrency();
    if(empty($settings) || empty($currencyData) || !isset($settings['currency_api']) || 
       empty($settings['currency_api'])){
      return;
    }
    $sourceCur = (!empty($settings['currency']))?$settings['currency']:'SAR';
    $coma = '';
    $convertCur = '';
    foreach ($currencyData AS $curr) {
      $convertCur .= $coma.$curr['currency'];
      $coma = ',';
    }
    $params  = 'currencies='.$convertCur;
    $params .= '&source='.$sourceCur;
    $params .= '&access_key='.$settings['currency_api'];
    $apiUrl  = "http://apilayer.net/api/live?".$params;
    $ch = curl_init($apiUrl);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
    $json = curl_exec($ch);
    curl_close($ch);
    $exchangeRates = json_decode($json, true);
    if(empty($exchangeRates) || !isset($exchangeRates['quotes']) || empty($conversion = $exchangeRates['quotes'])){
      return;
    }
    foreach($conversion AS $curr => $rate) {
      $currency = substr($curr,3);
      $this->db->update('country',array('conversion_rate'=>$rate),array('currency'=>$currency));
    }
  }

  public function sync_contacts(){
    $data = (array) json_decode(file_get_contents('php://input'));
    $data['auth_token'] = $this->auth_token;
    $res = $this->Webservice_model->sync_contacts($data);
    if($res['status']!=0){
      $this->response($res['data']);
    }
    else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function update_friend_request(){
    $data = (array) json_decode(file_get_contents('php://input'));
    $data['auth_token'] = $this->auth_token;
    $res = $this->Webservice_model->update_friend_request($data);
    if($res!=0){
      $this->successResponse($res);
    }
    else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function send_friend_request(){
    $data = (array) json_decode(file_get_contents('php://input'));
    $data['auth_token'] = $this->auth_token;
    $res = $this->Webservice_model->send_friend_request($data);
    if($res['status']!=0){
      $this->successResponse($res);
    }
    else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function get_friend_requests(){
    $data['auth_token'] = $this->auth_token;
    $res = $this->Webservice_model->get_friend_requests($data);
    if($res['status']!=0){
      $this->response($res['data']);
    }
    else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function recent_chats(){
    $data['auth_token'] = $this->auth_token;
    $res = $this->Webservice_model->recent_chats($data);
    if($res['status']!=0){
      $this->response($res['data']);
    }
    else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function upload_audio_message(){
    if(!empty($_FILES))
    {
      if (!file_exists('assets/audios/'.date('Y-m-d'))){
        mkdir('assets/audios/'.date('Y-m-d'), 0777, true);
      }
      $fileName =date('d').'_'.$_FILES['audio_message']['name'];
      $config = set_upload_service('assets/audios/'.date('Y-m-d')); 
      $config['file_name'] = $fileName;
      $this->load->library('upload', $config);
      if ( ! $this->upload->do_upload('audio_message')) 
      {
        $error = array('error' => $this->upload->display_errors('', '')); 
        $this->errorResponse('ER10',$error);
      } 
      else 
      { 
        $imagedata = $this->upload->data(); 
        $fullfilepath='assets/audios/'.date('Y-m-d').'/'.$imagedata['file_name'];
        $respArr['data'] = $fullfilepath; 
        $this->response($respArr);
      }
    }
  }

  public function getCountry() {
    $res = $this->Webservice_model->getCountry();
    if($res['status'] == 'success'){
      $this->response($res['data']);
    }
    else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function validate_promo_code(){
    $data = (array) json_decode(file_get_contents('php://input'));
    $data['auth_token'] = $this->auth_token;
    $res = $this->Webservice_model->validate_promo_code($data);
    if($res['status'] == 1 || $res['status'] == 5){
      $this->response($res['data']);
    }else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function create_guest_user() {
    $res = $this->Webservice_model->createGuestUser();
    if($res['status'] == 'success'){
      $this->response($res['data']);
    }
    else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }  

  public function update_user_visibility(){
    $data = (array) json_decode(file_get_contents('php://input'));
    $data['auth_token'] = $this->auth_token;

    $res = $this->Webservice_model->update_user_visibility($data);
    if($res['status'] != 0){
      $this->successResponse();
    } else {
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function update_user_location(){
    $data = (array)json_decode(file_get_contents('php://input'));
    $data['auth_token'] = $this->auth_token;
    $res = $this->Webservice_model->update_user_location($data);
    if($res['status'] != 0){
      $this->successResponse();
    }else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function get_nearby_users(){
    $data = (array)json_decode(file_get_contents('php://input'));
    $data['auth_token'] = $this->auth_token;
    $res = $this->Webservice_model->get_nearby_users($data);
    if($res['status'] == 'success'){
      $this->response($res['data']);
    }else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  /*********************************** START Hotel API ****************************************/
  public function get_hotel_city_list(){
    $data = $_GET;
    $data['auth_token'] = $this->auth_token;
    $res = $this->Webservice_model->get_hotel_city_list($data);
    if($res['status'] == 1){
      $this->response($res['data']);
    }else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function hotel_search(){
    $data = (array)json_decode(file_get_contents('php://input'));
    $data['auth_token'] = $this->auth_token;
    $res = $this->Webservice_model->hotel_search($data);
    if($res['status'] == 1){
      $this->response($res['data']);
    }else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function get_specific_hotel_content(){
    $data = (array)json_decode(file_get_contents('php://input'));
    $data['auth_token'] = $this->auth_token;
    $res = $this->Webservice_model->get_specific_hotel_content($data);
    if($res['status'] == 1){
      $this->response($res['data']);
    }else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function get_room_rates(){
    $data = (array)json_decode(file_get_contents('php://input'));
    $data['auth_token'] = $this->auth_token;
    $res = $this->Webservice_model->get_room_rates($data);
    if($res['status'] == 1){
      $this->response($res['data']);
    }else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function get_rate_rules(){
    $data = (array) json_decode(file_get_contents('php://input'));
    $data['auth_token'] = $this->auth_token;
    $res = $this->Webservice_model->get_rate_rules($data);
    if($res['status'] == 1){
      $this->response($res['data']);
    }else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function hotel_book(){
    $data = (array)json_decode(file_get_contents('php://input'));
    $data['auth_token'] = $this->auth_token;
    $res = $this->Webservice_model->hotel_book($data);
    if($res['status'] == 1){
      $this->response($res['data']);
    }else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function trawex_cancel_booking(){
    $data = (array)json_decode(file_get_contents('php://input'));
    $data['auth_token'] = $this->auth_token;
    $res = $this->Webservice_model->trawex_cancel_booking($data);
    if($res['status'] == 1){
      $this->response($res['data']);
    }else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }
  /*********************************** END Hotel API ****************************************/


  /********************************** START Flight API ***************************************/
  public function flight_authenticate(){
    $data = (array)json_decode(file_get_contents('php://input'));
    $data['auth_token'] = $this->auth_token;
    $res = $this->Webservice_model->flight_authenticate($data);
    if($res['status'] == 1){
      $this->response($res['data']);
    }else{
      $this->errorResponse($res['code'],$res['message']);
    }
  } 

  public function flight_availability_search(){
    $data = (array)json_decode(file_get_contents('php://input'));
    $data['auth_token'] = $this->auth_token;
    $res = $this->Webservice_model->flight_availability_search($data);
    if($res['status'] == 1){
      $this->response($res['data']);
    }else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function flight_fare_rules(){
    $data = (array)json_decode(file_get_contents('php://input'));
    $data['auth_token'] = $this->auth_token;
    $res = $this->Webservice_model->flight_fare_rules($data);
    if($res['status'] == 1){
      $this->response($res['data']);
    }else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function flight_revalidate(){
    $data = (array)json_decode(file_get_contents('php://input'));
    $data['auth_token'] = $this->auth_token;
    $res = $this->Webservice_model->flight_revalidate($data);
    if($res['status'] == 1){
      $this->response($res['data']);
    }else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function flight_book(){
    $data = (array)json_decode(file_get_contents('php://input'));
    $data['auth_token'] = $this->auth_token;
    $res = $thia->Webservice_model->flight_book($data);
    if($res['status'] == 1){
      $this->response($res['data']);
    }else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function flight_ticket_order(){
    $data = (array)json_decode(file_get_contents('php://input'));
    $data['auth_token'] = $this->auth_token;
    $res = $this->Webservice_model->flight_ticket_order($data);
    if($res['status'] == 1){
      $this->response($res['data']);
    }else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function flight_trip_details(){
    $data = (array)json_decode(file_get_contents('php://input'));
    $data['auth_token'] = $this->auth_token;
    $res = $this->Webservice_model->flight_trip_details($data);
    if($res['status'] == 1){
      $this->response($res['data']);
    }else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }

  public function cancel_flights(){
    $data = (array)json_decode(file_get_contents('php://input'));
    $data['auth_token'] = $this->auth_token;
    $res = $this->Webservice_model->cancel_flights($data);
    if($res['status'] == 1){
      $this->response($res['data']);
    }else{
      $this->errorResponse($res['code'],$res['message']);
    }
  }
  /******************************* END Flight API******************************************/
}
?>
