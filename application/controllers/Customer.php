<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends CI_Controller {

	public function __construct() {
		parent::__construct();
        date_default_timezone_set("Asia/Riyadh");
		$this->load->model('Customer_model');

		if(!$this->session->userdata('logged_in')) {
			redirect(base_url());
		}
		
        $role = roleManagement();
        if(!array_key_exists('Customer',$role)){
            redirect(base_url('Dashboard'));
        }
	}

	public function addCustomer(){
		$template['page'] = 'Customer/customerForm';
        $template['menu'] = 'Customer Management';
        $template['smenu'] = 'Add Customer';
        $template['pTitle'] = "Add Customer";
        $template['pDescription'] = "Create New Customer";

		$this->load->view('template',$template);
	}

	public function viewCustomers(){
		$template['page'] = 'Customer/viewCustomer';
        $template['menu'] = 'Customer Management';
        $template['smenu'] = 'View Customers';
        $template['pTitle'] = "View Customers";
        $template['pDescription'] = "View and Manage Customers";
        $template['page_head'] = "Customer Management";

        $provider_id = '';
        if($this->session->userdata['user_type'] == 2){
            $provider_id = $this->session->userdata['id'];
        }
        $template['customer_data'] = $this->Customer_model->getCustomerData('','0,1',$provider_id);

		$this->load->view('template',$template);
	}

	public function getCustomerData(){
		$resArr = array('status'=>0);
		if(!isset($_POST)||empty($_POST)||!isset($_POST['customer_id'])||empty($_POST['customer_id']) || 
		   !is_numeric($customer_id = decode_param($_POST['customer_id']))){
			echo json_encode($resArr);exit;
		}
		$view_all = (isset($_POST['view_all']) && $_POST['view_all'] == 1)?1:0;
		$mechData = $this->Customer_model->getCustomerData($customer_id,$view_all);

		if(empty($mechData)){
			echo json_encode($resArr);exit;
		}

		$resArr['status'] = 1;
		$resArr['data'] = $mechData;
		echo json_encode($resArr);exit;
	}
	
    function changeStatus(){
        if(!isset($_POST) || !isset($_POST['customer_id']) || empty($_POST['customer_id']) || 
           !isset($_POST['status']) || $_POST['status'] == ''){
            echo json_encode(array('status'=>'0'));exit;
        }
        $status = $_POST['status'];
        $customer_id = decode_param($_POST['customer_id']);
        $resp = $this->Customer_model->changeStatus($customer_id,$status);
        if($resp){
            echo json_encode(array('status'=>'1'));exit;
        }
        echo json_encode(array('status'=>'0'));exit;
    }

	public function createCustomer(){
		$err = 0;
		$errMsg = '';
		$flashMsg = array('message'=>'Something went wrong, please try again..!','class'=>'error');
		if(!isset($_POST) || empty($_POST) || !isset($_FILES) || empty($_FILES)){
			$this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('Customer/addCustomer'));
		}
		if($err == 0 && (!isset($_POST['name']) || empty($_POST['name']))){
			$err = 1;
			$errMsg = 'Provide Customer Name';
		}else if($err == 0 && (!isset($_POST['email']) || empty($_POST['email']))){
			$err = 1;
			$errMsg = 'Provide a Customer Email';
		}else if($err == 0 && (!isset($_POST['phone']) || empty($_POST['phone']))){
			$err = 1;
			$errMsg = 'Provide Customer Phone';
		}else if($err == 0 && (!isset($_POST['profile_city']) || empty($_POST['profile_city']))){
            $err = 1;
			$errMsg = 'Provide a City Name';
        }else if($err == 0 && (!isset($_POST['gender']) || empty($_POST['gender']))){
            $err = 1;
			$errMsg = 'Provide a Gender';
        }else if($err == 0 && (!isset($_POST['dob']) || empty($_POST['dob']))){
            $err = 1;
			$errMsg = 'Provide a Date Of Birth';
        }
        
        if($err == 0){
	        $config = set_upload_service("assets/uploads/services");
	        $this->load->library('upload');
	        $config['file_name'] = time()."_".$_FILES['profile_image']['name'];
	        $this->upload->initialize($config);
	        if(!$this->upload->do_upload('profile_image')){
	        	$err = 1;
				$errMsg = $this->upload->display_errors();
	        }else{
	            $upload_data = $this->upload->data();
	            $_POST['profile_image'] = $config['upload_path']."/".$upload_data['file_name'];
	        }
	    }

		if($err == 1){
			$flashMsg['message'] = $errMsg;
			$this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('Customer/addCustomer'));
		}
		$temp_password = rand(10000000, 99999999);
		$_POST['dob'] = strtotime($_POST['dob'])*1000;
		$_POST['password'] = md5($temp_password);
        $status = $this->Customer_model->addCustomer($_POST);
        if($status == 1){
			$this->load->model('Api_model');

			$subject  = "Your TimeOut Account is now activated";
			$email_id = $_POST['email'];
 			$template = getNotifTemplate();
 			$msgContent = "Hi, Welcome to TimeOut. Please use username: ".$_POST['email']." 
 						   for access your account.";
			$message = "<html><body>".$msgContent."<br></body></html>";

            if(isset($template['registration_mail']) && !empty($template['registration_mail'])){
                $message=str_replace(array('{:email}'),array($email_id),$template['registration_mail']);
            }
			$this->Api_model->send_mail($subject,$email_id,$message);

			if(isset($template['registration_sms']) && !empty($template['registration_sms'])){
	           $msgContent=str_replace(array('{:email}'),array($email_id),$template['registration_sms']);
	        }
	        $this->Api_model->sendSMS($_POST['phone'],$msgContent);

            $flashMsg =array('message'=>'Successfully Updated User Details..!','class'=>'success');
            $this->session->set_flashdata('message', $flashMsg);
            redirect(base_url('Customer/viewCustomers'));
        } else if($status == 2){
            $flashMsg = array('message'=>'Email ID already exist..!','class'=>'error');
            $this->session->set_flashdata('message', $flashMsg);
            redirect(base_url('Customer/addCustomer'));
        } else if($status == 3){
            $flashMsg = array('message'=>'Phone Number already exist..!','class'=>'error');
            $this->session->set_flashdata('message', $flashMsg);
            redirect(base_url('Customer/addCustomer'));
        } else if($status == 4){
            $flashMsg = array('message'=>'User Name already exist..!','class'=>'error');
            $this->session->set_flashdata('message', $flashMsg);
            redirect(base_url('Customer/addCustomer'));
        } else {
            $this->session->set_flashdata('message', $flashMsg);
            redirect(base_url('Customer/addCustomer'));
        }
	}

	public function editCustomers($customer_id){
		$flashMsg = array('message'=>'Something went wrong, please try again..!','class'=>'error');
		if(empty($customer_id) || !is_numeric($customer_id = decode_param($customer_id))){
			$this->session->set_flashdata('message',$flashMsg);
        	redirect(base_url('Customer/viewCustomers'));
		}

		$template['page'] = 'Customer/customerForm';
        $template['menu'] = 'Customer Management';
        $template['smenu'] = 'Edit Customer';
        $template['pTitle'] = "Edit Customers";
        $template['pDescription'] = "Update Customer Data";

        $template['customer_data'] = $this->Customer_model->getCustomerData($customer_id,1);
        $template['customer_id'] = encode_param($customer_id);
		$this->load->view('template',$template);
	}

	public function updateCustomer($customer_id = ''){
		$err = 0;
		$errMsg = '';
		$flashMsg = array('message'=>'Something went wrong, please try again..!','class'=>'error');
		if(!isset($_POST) || empty($_POST) || !isset($_FILES) || empty($_FILES)){
			$this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('Customer/addCustomer'));
		}
		
		if($err == 0 && (!isset($_POST['name']) || empty($_POST['name']))){
			$err = 1;
			$errMsg = 'Provide Customer Name';
		}else if($err == 0 && (!isset($_POST['email']) || empty($_POST['email']))){
			$err = 1;
			$errMsg = 'Provide a Customer Email';
		}else if($err == 0 && (!isset($_POST['phone']) || empty($_POST['phone']))){
			$err = 1;
			$errMsg = 'Provide Customer Phone';
		}else if($err == 0 && (!isset($_POST['profile_city']) || empty($_POST['profile_city']))){
            $err = 1;
			$errMsg = 'Provide a City Name';
        }else if($err == 0 && (!isset($_POST['gender']) || empty($_POST['gender']))){
            $err = 1;
			$errMsg = 'Provide a Gender';
        }else if($err == 0 && (!isset($_POST['dob']) || empty($_POST['dob']))){
            $err = 1;
			$errMsg = 'Provide a Date Of Birth';
        }
    	
        if($err == 0){
	        $config = set_upload_service("assets/uploads/services");
	        $this->load->library('upload');
	        $config['file_name'] = time()."_".$_FILES['profile_image']['name'];
	        $this->upload->initialize($config);
	        if($this->upload->do_upload('profile_image')){
	            $upload_data = $this->upload->data();
	            $_POST['profile_image'] = $config['upload_path']."/".$upload_data['file_name'];
	        }
	    }

		if($err == 1){
			$flashMsg['message'] = $errMsg;
			$this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('Customer/editCustomers/'.$customer_id));
		}
		
		$_POST['dob'] = strtotime($_POST['dob'])*1000;
        $status = $this->Customer_model->updateCustomer(decode_param($customer_id),$_POST);
        if($status == 1){
            $flashMsg =array('message'=>'Successfully Updated User Details..!','class'=>'success');
            $this->session->set_flashdata('message', $flashMsg);
            redirect(base_url('Customer/viewCustomers'));
        } else if($status == 2){
            $flashMsg = array('message'=>'Email ID already exist..!','class'=>'error');
            $this->session->set_flashdata('message', $flashMsg);
            redirect(base_url('Customer/editCustomers/'.$customer_id));
        } else if($status == 3){
            $flashMsg = array('message'=>'Phone Number already exist..!','class'=>'error');
            $this->session->set_flashdata('message', $flashMsg);
            redirect(base_url('Customer/editCustomers/'.$customer_id));
        } else if($status == 4){
            $flashMsg = array('message'=>'User Name already exist..!','class'=>'error');
            $this->session->set_flashdata('message', $flashMsg);
            redirect(base_url('Customer/editCustomers/'.$customer_id));
        } else {
            $this->session->set_flashdata('message', $flashMsg);
            redirect(base_url('Customer/editCustomers/'.$customer_id));
        }
	}
}
?>