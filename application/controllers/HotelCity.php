<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HotelCity extends CI_Controller {

	public function __construct() {
		parent::__construct();
        date_default_timezone_set("Asia/Riyadh");
        $this->load->model('HotelCity_model');
		$this->load->model('Dashboard_model');
		
		if(!$this->session->userdata('logged_in')) {
			redirect(base_url('Login'));
		}

        $role = roleManagement();
        if(!array_key_exists('City',$role)){
            redirect(base_url('Dashboard'));
        }
 	}
	
	function listHotelCity(){
		$template['page'] = 'HotelCity/viewHotelCityList';
        $template['menu'] = 'Hotel City Management';
        $template['smenu'] = 'View Hotel City List';
        $template['pTitle'] = "Hotel City Management";
        $template['pDescription'] = "View Hotel City List";

        $template['hotelCityData'] = $this->HotelCity_model->getHotelCityData();
		$this->load->view('template',$template);
	}

	function addHotelCity(){
        $this->load->model('HotelCity_model');
        $template['hotelCityData'] = $this->HotelCity_model->getHotelCityData();

		$template['page'] = 'HotelCity/hotelCityAddForm';
        $template['menu'] = 'Hotel City Management';
        $template['smenu'] = 'Add Hotel City';
        $template['pTitle'] = "Add Hotel City";
        $template['pDescription'] = "Create New Hotel City";

		$this->load->view('template',$template);
	}

	function createHotelCity(){
		$flashMsg = array('message'=>'Something went wrong, please try again..!','class'=>'error');

		if(!isset($_POST) || empty($_POST)){
            $this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('HotelCity/addHotelCity'));
        }
		$err = 0;
        $errMsg = '';
        if($err == 0 && (!isset($_POST['hotel_city_name_EN']) || empty($_POST['hotel_city_name_EN']))){
            $err = 1;
            $errMsg = 'Provide a Hotel City Name in English';
        } else if($err == 0 && (!isset($_FILES) || !isset($_FILES['hotel_city_icon']) || 
                                 empty($_FILES['hotel_city_icon']))){
            $err = 1;
            $errMsg = 'Provide a Hotel City Icon';
        }

        $latLng = getLocationLatLng($_POST['hotel_city_name_EN']);
        if($err == 0 && empty($latLng)){
            $err = 1;
            $errMsg = 'Provide a proper Hotel City Name';
        }
        $_POST['hotel_city_lat'] = $latLng['lat'];
        $_POST['hotel_city_lng'] = $latLng['lng'];

        if($err == 0){
            $config = set_upload_service("assets/uploads/services");
            $this->load->library('upload');
            $config['file_name'] = time()."_".$_FILES['hotel_city_icon']['name'];
            $this->upload->initialize($config);
            if(!$this->upload->do_upload('hotel_city_icon')){
                $err = 1;
                $errMsg = $this->upload->display_errors();
            }else{
                $upload_data = $this->upload->data();
                $_POST['hotel_city_icon'] = $config['upload_path']."/".$upload_data['file_name'];
            }
        }
        
        if($err == 1){
            $flashMsg['message'] = $errMsg;
            $this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('HotelCity/addHotelCity'));
        }

        $status = $this->HotelCity_model->createHotelCity($_POST);
        if($status == 1){
            $flashMsg['class'] = 'success';
            $flashMsg['message'] = 'HotelCity Created';

            $this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('HotelCity/listHotelCity'));
        }
        $this->session->set_flashdata('message',$flashMsg);
        redirect(base_url('HotelCity/addHotelCity'));
	}

	function editHotelCity($hotel_city_id=''){
        $flashMsg = array('message'=>'Something went wrong, please try again..!','class'=>'error');
		if(empty($hotel_city_id) || empty(decode_param($hotel_city_id))){
			$this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('HotelCity/listHotelCity'));
		}
		$template['page'] = 'HotelCity/hotelCityAddForm';
        $template['menu'] = 'Hotel City Management';
        $template['smenu'] = 'Edit Hotel City';
        $template['pTitle'] = "Edit Hotel City";
        $template['pDescription'] = "Update Hotel City Data";

        $template['hotel_city_id'] = $hotel_city_id;
        $template['hotelCityData'] = $this->HotelCity_model->getHotelCityData(decode_param($hotel_city_id));
		$this->load->view('template',$template);
	}

	function updateHotelCity($hotel_city_id=''){
		$flashMsg = array('message'=>'Something went wrong, please try again..!','class'=>'error');

        $err = 0;
        $errMsg = '';
		if(!isset($_POST) || empty($_POST) || empty($hotel_city_id)){
            $this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('HotelCity/listHotelCity'));
        }
        if($err == 0 && (!isset($_POST['hotel_city_name_EN']) || empty($_POST['hotel_city_name_EN']))){
            $err = 1;
            $errMsg = 'Provide a Hotel City Name in English';
        }
        if($err == 1){
            $flashMsg['message'] = $errMsg;
            $this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('HotelCity/editHotelCity/'.$hotel_city_id));
        }

        if(isset($_FILES) && isset($_FILES['hotel_city_icon']) && !empty($_FILES['hotel_city_icon'])){
            $config = set_upload_service("assets/uploads/services");
            $this->load->library('upload');
            $config['file_name'] = time()."_".$_FILES['hotel_city_icon']['name'];
            $this->upload->initialize($config);
            if($this->upload->do_upload('hotel_city_icon')){
                $upload_data = $this->upload->data();
                $_POST['hotel_city_icon'] = $config['upload_path']."/".$upload_data['file_name'];
            }
        }

        $latLng = getLocationLatLng($_POST['hotel_city_name_EN']);
        if($err == 0 && empty($latLng)){
            $err = 1;
            $errMsg = 'Provide a proper Hotel City Name';
        }
        $_POST['hotel_city_lat'] = $latLng['lat'];
        $_POST['hotel_city_lng'] = $latLng['lng'];

        if($err == 0){
            $config = set_upload_service("assets/uploads/services");
            $this->load->library('upload');
            $config['file_name'] = time()."_".$_FILES['hotel_city_icon']['name'];
            $this->upload->initialize($config);
            if(!$this->upload->do_upload('hotel_city_icon')){
                $err = 1;
                $errMsg = $this->upload->display_errors();
            }else{
                $upload_data = $this->upload->data();
                $_POST['hotel_city_icon'] = $config['upload_path']."/".$upload_data['file_name'];
            }
        }

        $status = $this->HotelCity_model->updateHotelCity(decode_param($hotel_city_id),$_POST);
        if($status == 1){
            $flashMsg['class'] = 'success';
            $flashMsg['message'] = 'HotelCity Created';

            $this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('HotelCity/listHotelCity'));
        }
        $this->session->set_flashdata('message',$flashMsg);
        redirect(base_url('HotelCity/editHotelCity/'.$hotel_city_id));
	}
    
    function changeStatus(){
        if(!isset($_POST) || !isset($_POST['hotel_city_id']) || empty($_POST['hotel_city_id']) || 
           !isset($_POST['status']) || $_POST['status'] == ''){
            echo json_encode(array('status'=>'0'));exit;
        }
        $status = $_POST['status'];
        $hotel_city_id = decode_param($_POST['hotel_city_id']);
        $resp = $this->HotelCity_model->changeStatus($hotel_city_id,$status);
        if($resp){
            echo json_encode(array('status'=>'1'));exit;
        }
        echo json_encode(array('status'=>'0'));exit;
    }
}
?>
