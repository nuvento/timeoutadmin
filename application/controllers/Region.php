<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Region extends CI_Controller {

	public function __construct() {
		parent::__construct();
        date_default_timezone_set("Asia/Riyadh");
        $this->load->model('Region_model');
		$this->load->model('Dashboard_model');
		
		if(!$this->session->userdata('logged_in')) {
			redirect(base_url('Login'));
		}

        $role = roleManagement();
        if(!array_key_exists('City',$role)){
            redirect(base_url('Dashboard'));
        }
 	}
	
	function listRegion(){
		$template['page'] = 'Region/viewRegionList';
        $template['menu'] = 'Region Management';
        $template['smenu'] = 'View Region List';
        $template['pTitle'] = "Region Management";
        $template['pDescription'] = "View Region List";

        $template['regionData'] = $this->Region_model->getRegionData();
		$this->load->view('template',$template);
	}

	function addRegion(){
        $this->load->model('Region_model');
        $template['regionData'] = $this->Region_model->getRegionData();

		$template['page'] = 'Region/regionAddForm';
        $template['menu'] = 'Region Management';
        $template['smenu'] = 'Add Region';
        $template['pTitle'] = "Add Region";
        $template['pDescription'] = "Create New Region";

		$this->load->view('template',$template);
	}

	function createRegion(){
		$flashMsg = array('message'=>'Something went wrong, please try again..!','class'=>'error');

		if(!isset($_POST) || empty($_POST)){
            $this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('Region/addRegion'));
        }
		$err = 0;
        $errMsg = '';
        if($err == 0 && (!isset($_POST['name_EN']) || empty($_POST['name_EN']))){
            $err = 1;
            $errMsg = 'Provide a Region Name in English';
        } else if($err == 0 && (!isset($_FILES) || !isset($_FILES['region_icon']) || 
                                 empty($_FILES['region_icon']))){
            $err = 1;
            $errMsg = 'Provide a Region Icon';
        }

        $latLng = getLocationLatLng($_POST['name_EN']);
        if($err == 0 && empty($latLng)){
            $err = 1;
            $errMsg = 'Provide a proper Region Name';
        }
        $_POST['region_lat'] = $latLng['lat'];
        $_POST['region_lng'] = $latLng['lng'];

        if($err == 0){
            $config = set_upload_service("assets/uploads/services");
            $this->load->library('upload');
            $config['file_name'] = time()."_".$_FILES['region_icon']['name'];
            $this->upload->initialize($config);
            if(!$this->upload->do_upload('region_icon')){
                $err = 1;
                $errMsg = $this->upload->display_errors();
            }else{
                $upload_data = $this->upload->data();
                $_POST['region_icon'] = $config['upload_path']."/".$upload_data['file_name'];
            }
        }
        
        if($err == 1){
            $flashMsg['message'] = $errMsg;
            $this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('Region/addRegion'));
        }

        $status = $this->Region_model->createRegion($_POST);
        if($status == 1){
            $flashMsg['class'] = 'success';
            $flashMsg['message'] = 'Region Created';

            $this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('Region/listRegion'));
        }
        $this->session->set_flashdata('message',$flashMsg);
        redirect(base_url('Region/addRegion'));
	}

	function editRegion($region_id=''){
        $flashMsg = array('message'=>'Something went wrong, please try again..!','class'=>'error');
		if(empty($region_id) || empty(decode_param($region_id))){
			$this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('Region/listRegion'));
		}
		$template['page'] = 'Region/regionAddForm';
        $template['menu'] = 'Region Management';
        $template['smenu'] = 'Edit Region';
        $template['pTitle'] = "Edit Region";
        $template['pDescription'] = "Update Region Data";

        $template['region_id'] = $region_id;
        $template['regionData'] = $this->Region_model->getRegionData(decode_param($region_id));
		$this->load->view('template',$template);
	}

	function updateRegion($region_id=''){
		$flashMsg = array('message'=>'Something went wrong, please try again..!','class'=>'error');

		if(!isset($_POST) || empty($_POST) || empty($region_id)){
            $this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('Region/listRegion'));
        }

		$err = 0;
        $errMsg = '';
        if($err == 0 && (!isset($_POST['name_EN']) || empty($_POST['name_EN']))){
            $err = 1;
            $errMsg = 'Provide a Region Name in English';
        }

        if($err == 1){
            $flashMsg['message'] = $errMsg;
            $this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('Region/editRegion/'.$region_id));
        }

        if(isset($_FILES) && isset($_FILES['region_icon']) && !empty($_FILES['region_icon'])){
            $config = set_upload_service("assets/uploads/services");
            $this->load->library('upload');
            $config['file_name'] = time()."_".$_FILES['region_icon']['name'];
            $this->upload->initialize($config);
            if($this->upload->do_upload('region_icon')){
                $upload_data = $this->upload->data();
                $_POST['region_icon'] = $config['upload_path']."/".$upload_data['file_name'];
            }
        }

        $latLng = getLocationLatLng($_POST['name_EN']);
        if($err == 0 && empty($latLng)){
            $err = 1;
            $errMsg = 'Provide a proper Region Name';
        }
        $_POST['region_lat'] = $latLng['lat'];
        $_POST['region_lng'] = $latLng['lng'];

        if($err == 0){
            $config = set_upload_service("assets/uploads/services");
            $this->load->library('upload');
            $config['file_name'] = time()."_".$_FILES['region_icon']['name'];
            $this->upload->initialize($config);
            if(!$this->upload->do_upload('region_icon')){
                $err = 1;
                $errMsg = $this->upload->display_errors();
            }else{
                $upload_data = $this->upload->data();
                $_POST['region_icon'] = $config['upload_path']."/".$upload_data['file_name'];
            }
        }

        $status = $this->Region_model->updateRegion(decode_param($region_id),$_POST);
        if($status == 1){
            $flashMsg['class'] = 'success';
            $flashMsg['message'] = 'Region Created';

            $this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('Region/listRegion'));
        }
        $this->session->set_flashdata('message',$flashMsg);
        redirect(base_url('Region/editRegion/'.$region_id));
	}
    
    function changeStatus(){
        if(!isset($_POST) || !isset($_POST['region_id']) || empty($_POST['region_id']) || 
           !isset($_POST['status']) || $_POST['status'] == ''){
            echo json_encode(array('status'=>'0'));exit;
        }
        $status = $_POST['status'];
        $region_id = decode_param($_POST['region_id']);
        $resp = $this->Region_model->changeStatus($region_id,$status);
        if($resp){
            echo json_encode(array('status'=>'1'));exit;
        }
        echo json_encode(array('status'=>'0'));exit;
    }
}
?>
