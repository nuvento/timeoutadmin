<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Venue extends CI_Controller {

	public function __construct() {
		parent::__construct();
        date_default_timezone_set("Asia/Riyadh");
        $this->load->model('Venue_model');
		$this->load->model('Dashboard_model');
		
		if(!$this->session->userdata('logged_in')) {
			redirect(base_url('Login'));
		}
 	}
	
	function listVenues(){
		$template['page'] = 'Venue/viewVenueList';
        $template['menu'] = 'Venue Management';
        $template['smenu'] = 'View Venue List';
        $template['pTitle'] = "Venue Management";
        $template['pDescription'] = "View Venue List";

        $provider_id = '';
        if($this->session->userdata['user_type'] == 2){
            $provider_id = $this->session->userdata['id'];
        }
        $template['venue_data'] = $this->Venue_model->getVenueData('','',$provider_id);
		$this->load->view('template',$template);
	}

	function addVenues(){
        $this->load->model('Host_model');
        $this->load->model('Region_model');

        $template['host_data'] = $this->Host_model->getHostCategories('','1');
        $template['regionData'] = $this->Region_model->getRegionData('','1');

		$template['page'] = 'Venue/venueAddForm';
        $template['menu'] = 'Venue Management';
        $template['smenu'] = 'Add Venue';
        $template['pTitle'] = "Add Venue";
        $template['pDescription'] = "Create New Venue";

		$this->load->view('template',$template);
	}

	function createVenues(){
		$flashMsg = array('message'=>'Something went wrong, please try again..!','class'=>'error');

		if(!isset($_POST) || empty($_POST)){
            $this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('Venue/addVenues'));
        }

		$err = 0;
        $errMsg = '';
        if($err == 0 && (!isset($_POST['region_id']) || empty($_POST['region_id']))){
            $err = 1;
            $errMsg = 'Select a region';
        } else if ($err == 0 && (!isset($_POST['venue_name_EN']) || $_POST['venue_name_EN'] == '' )){
            $err = 1;
            $errMsg = 'Provide a Venue Name (English)';
        }  else if ($err == 0 && (!isset($_POST['location']) || $_POST['location'] == '' )){
            $err = 1;
            $errMsg = 'Provide a Location';
        } else if ($err == 0 && (!isset($_POST['host_cat_id']) || $_POST['host_cat_id'] == '' )){
            $err = 1;
            $errMsg = 'Select a Host';
        } else if ($err == 0 && isset($_POST['has_layout']) && $_POST['has_layout'] == 1 && 
          (!isset($_POST['seat_color']) || empty($_POST['seat_color']) || !isset($_FILES) || 
           !isset($_POST['seat_price']) || empty($_POST['seat_price']) || empty($_FILES) || 
           count($_POST['seat_price']) != count($_POST['seat_color']) || !isset($_FILES['layout_image']) ||
           empty($_FILES['layout_image']))){
            $err = 1;
            $errMsg = 'Provide Proper Layout details';
        }
        if(isset($_POST['has_layout']) && $_POST['has_layout'] == 1){
            $config = set_upload_service("assets/uploads/services");
            $this->load->library('upload');
            $config['file_name'] = time()."_".$_FILES['layout_image']['name'];
            $this->upload->initialize($config);
            if(!$this->upload->do_upload('layout_image')){
                $err = 1;
                $errMsg = $this->upload->display_errors();
            }else{
                $upload_data = $this->upload->data();
                $_POST['layout'] = $config['upload_path']."/".$upload_data['file_name'];
            }

            $seatLayoutDetails = array();
            foreach($_POST['seat_color'] AS $index => $value){
                $seatLayoutDetails[] = array('color'=>$value,
                                             'price'=>$_POST['seat_price'][$index],
                                             'capacity'=>$_POST['seat_capacity'][$index],
                                             'weekend_price'=>$_POST['seat_price'][$index]);
            }

            $_POST['layout_details'] = json_encode($seatLayoutDetails);
        } else {
            $_POST['layout'] = $_POST['layout_details'] = '';
        }

        $locData = getLocationLatLng($_POST['location']);
        if(empty($locData)){
            $err = 1;
            $errMsg = 'Provide a valid Location';
        }

        if($err == 1){
            $flashMsg['message'] = $errMsg;
            $this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('Venue/addVenues'));
        }

        $_POST['location_lat'] = $locData['lat'];
        $_POST['location_lng'] = $locData['lng'];

        $language = getLanguages();
        $locality_id = $this->Venue_model->createLocality($language,$_POST);
        $_POST['locality_id'] = (!empty($locality_id))?$locality_id:$_POST['locality_id'];

        foreach($language AS $lang) {
            if(isset($_POST['locality_name_'.$lang])) unset($_POST['locality_name_'.$lang]);
        }
        unset($_POST['has_layout'],$_POST['seat_capacity'],
              $_POST['seat_color'],$_POST['locality_type'],$_POST['seat_price']);

        $_POST['provider_id'] = ($this->session->userdata['user_type'] == 2)?$this->session->userdata['id']:0;

        $status = $this->Venue_model->createVenue($_POST);
        if($status == 1){
            $flashMsg['class'] = 'success';
            $flashMsg['message'] = 'Venue Created';

            $this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('Venue/listVenues'));
        }
        $this->session->set_flashdata('message',$flashMsg);
        redirect(base_url('Venue/addVenues'));
	}

	function editVenues($venue_id=''){
        $flashMsg = array('message'=>'Something went wrong, please try again..!','class'=>'error');
		if(empty($venue_id) || empty(decode_param($venue_id))){
			$this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('Venue/listVenues'));
		}
		$template['page'] = 'Venue/venueAddForm';
        $template['menu'] = 'Venue Management';
        $template['smenu'] = 'Edit Venue';
        $template['pTitle'] = "Edit Venue";
        $template['pDescription'] = "Update Venue Data";

        $template['venue_id'] = $venue_id;
        $template['venue_data'] = $this->Venue_model->getVenueData(decode_param($venue_id));
        $region_id = $template['venue_data']->region_id;

        $this->load->model('Host_model');
        $this->load->model('Region_model');
        $template['host_data'] = $this->Host_model->getHostCategories();
        $template['regionData'] = $this->Region_model->getRegionData();
        $template['localityData'] = $this->Region_model->getLocalityData($region_id,'','1');

		$this->load->view('template',$template);
	}

	function updateVenues($venue_id=''){
		$flashMsg = array('message'=>'Something went wrong, please try again..!','class'=>'error');

		if(!isset($_POST) || empty($_POST) || empty($venue_id)){
            $this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('Venue/listVenues'));
        }

        $err = 0;
        $errMsg = '';
        if($err == 0 && (!isset($_POST['region_id']) || empty($_POST['region_id']))){
            $err = 1;
            $errMsg = 'Select a region';
        } else if ($err == 0 && (!isset($_POST['venue_name_EN']) || $_POST['venue_name_EN'] == '' )){
            $err = 1;
            $errMsg = 'Provide a Venue Name (English)';
        } else if ($err == 0 && (!isset($_POST['location']) || $_POST['location'] == '' )){
            $err = 1;
            $errMsg = 'Provide a Location';
        } else if ($err == 0 && (!isset($_POST['host_cat_id']) || $_POST['host_cat_id'] == '' )){
            $err = 1;
            $errMsg = 'Select a Host';
        } else if ($err == 0 && isset($_POST['has_layout']) && $_POST['has_layout'] == 1 && 
          (!isset($_FILES,$_POST['seat_price'],$_POST['seat_color'],$_FILES['layout_image']) || 
           empty($_POST['seat_color']) || empty($_POST['seat_price']) || empty($_FILES) || 
           count($_POST['seat_price']) != count($_POST['seat_color']) || empty($_FILES['layout_image']))){
            $err = 1;
            $errMsg = 'Provide Proper Layout details';
        }

        if(isset($_POST['has_layout']) && $_POST['has_layout'] == 1){
            $config = set_upload_service("assets/uploads/services");
            $this->load->library('upload');
            $config['file_name'] = time()."_".$_FILES['layout_image']['name'];
            $this->upload->initialize($config);
            if($this->upload->do_upload('layout_image')){
                $upload_data = $this->upload->data();
                $_POST['layout'] = $config['upload_path']."/".$upload_data['file_name'];
            }

            $seatLayoutDetails = array();
            foreach($_POST['seat_color'] AS $index => $value){
                $seatLayoutDetails[] = array('color'=>$value,
                                             'price'=>$_POST['seat_price'][$index],
                                             'capacity'=>$_POST['seat_capacity'][$index],
                                             'weekend_price'=>$_POST['seat_price'][$index]);
            }

            $_POST['layout_details'] = json_encode($seatLayoutDetails);
        } else {
            $_POST['layout'] = $_POST['layout_details'] = '';
        }

        $locData = getLocationLatLng($_POST['location']);
        if(empty($locData)){
            $err = 1;
            $errMsg = 'Provide a valid Location';
        }

        if($err == 1){
            $flashMsg['message'] = $errMsg;
            $this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('Venue/editVenues/'.$venue_id));
        }

        $_POST['location_lat'] = $locData['lat'];
        $_POST['location_lng'] = $locData['lng'];

        $language = getLanguages();
        $locality_id = $this->Venue_model->createLocality($language,$_POST);
        $_POST['locality_id'] = (!empty($locality_id))?$locality_id:$_POST['locality_id'];

        foreach($language AS $lang) {
            if(isset($_POST['locality_name_'.$lang])) unset($_POST['locality_name_'.$lang]);
        }
        unset($_POST['has_layout'],$_POST['seat_capacity'],
              $_POST['seat_color'],$_POST['locality_type'],$_POST['seat_price']);

        if($this->session->userdata['user_type'] == 2){
            $_POST['provider_id'] = $this->session->userdata['id'];
        }
        
        $status = $this->Venue_model->updateVenues(decode_param($venue_id),$_POST);
        if($status == 1){
            $flashMsg['class'] = 'success';
            $flashMsg['message'] = 'Venue Created';

            $this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('Venue/listVenues'));
        }
        $this->session->set_flashdata('message',$flashMsg);
        redirect(base_url('Venue/editVenues/'.$venue_id));
	}

    function changeStatus(){
        if(!isset($_POST) || !isset($_POST['venue_id']) || empty($_POST['venue_id']) || 
           !isset($_POST['status']) || $_POST['status'] == ''){
            echo json_encode(array('status'=>'0'));exit;
        }
        $status = $_POST['status'];
        $venue_id = decode_param($_POST['venue_id']);
        $resp = $this->Venue_model->changeStatus($venue_id,$status);
        if($resp){
            echo json_encode(array('status'=>'1'));exit;
        }
        echo json_encode(array('status'=>'0'));exit;
    }

    function getVenueData(){
        $resArr = array('status'=>0);
        if(!isset($_POST) || empty($_POST) || !isset($_POST['venue_id']) || empty($_POST['venue_id']) || 
           empty($venue_id = decode_param($_POST['venue_id']))){
            echo json_encode($resArr);exit;
        }
        $venueData = $this->Venue_model->getVenueData($venue_id);
        if(!empty($venueData)){
            $resArr['status'] = 1;
            $resArr['data'] = $venueData;
        }
        echo json_encode($resArr);exit;
    }

    function getLocalityData(){
        $resArr = array('status'=>0);
        if(!isset($_POST) || empty($_POST) || !isset($_POST['region_id']) || empty($_POST['region_id'])){
            echo json_encode($resArr);exit;
        }

        $this->load->model('Region_model');
        $localityData = $this->Region_model->getlocalityData($_POST['region_id'],'','1');
        if(!empty($localityData)){
            $resArr['status'] = 1;
            $resArr['data'] = $localityData;
        }
        echo json_encode($resArr);exit;
    }
}
?>
