<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promocode extends CI_Controller {

	public function __construct() {
		parent::__construct();
        date_default_timezone_set("Asia/Riyadh");
		$this->load->model('Promocode_model');
		
		if(!$this->session->userdata('logged_in')) {
			redirect(base_url('Login'));
		}
		if($this->session->userdata['user_type'] == 2 || $this->session->userdata['user_type'] == 3){
			$flashMsg = array('message'=>'Access Denied You don\'t have permission to access this Page',
							  'class'=>'error');
			$this->session->set_flashdata('message',$flashMsg);
            redirect(base_url());
		}
 	}

	public function promocode() {
		$template['page'] = 'Promocode/promocodeList';
		$template['menu'] = "Promo-codes Management";
		$template['smenu'] = "Manage Promo-codes";
        $template['pTitle'] = "Promo-code Management";
        $template['page_head'] = "Promo-code Management";
        $template['pDescription'] = "Manage Promo-codes";

		$template['promocodeData'] = $this->Promocode_model->getPromocodeData('','0,1');
		$this->load->view('template',$template);
	}

	public function addPromocode(){
		$this->load->model('Event_model');
		$this->load->model('Region_model');
		$this->load->model('Category_model');

		$template['page'] = 'Promocode/promocodeForm';
        $template['menu'] = 'Promocode Management';
        $template['smenu'] = 'Add Promocode';
        $template['pTitle'] = "Add Promocode";
        $template['pDescription'] = "Create New Promocode";

        $template['regionData'] = $this->Region_model->getRegionData('','1');
		$template['event_data'] = $this->Event_model->getEventData('','1','');
		$template['categoryData'] = $this->Category_model->getCategoryData('','1');

		$this->load->view('template',$template);
	}

	public function editPromocode($promocode_id){
		$flashMsg = array('message'=>'Something went wrong, please try again..!','class'=>'error');
		if(empty($promocode_id) || empty($promocode_id = decode_param($promocode_id))){
			$this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('Promocode/promocode'));
		}

		$this->load->model('Event_model');
		$this->load->model('Region_model');
		$this->load->model('Category_model');

		$template['page'] = "Promocode/promocodeForm";
        $template['menu'] = "Promocode Management";
        $template['smenu'] = "Edit Promocode";
        $template['pTitle'] = "Promocode Management";
        $template['pDescription'] = "Edit Promocode";

		$template['promocode_id'] = encode_param($promocode_id);
		$template['promo'] = $this->Promocode_model->getPromocodeData($promocode_id,'0,1');
        $template['regionData'] = $this->Region_model->getRegionData('','1');
		$template['event_data'] = $this->Event_model->getEventData('','1','');
		$template['categoryData'] = $this->Category_model->getCategoryData('','1');

		$this->load->view('template',$template);
	}

	public function createPromocode(){
		$err = 0;
		$errMsg = '';
		$flashMsg = array('message'=>'Something went wrong, please try again..!','class'=>'error');
		if(!isset($_POST) || empty($_POST)){
			$this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('Promocode/addPromocode'));
		}

		if ($err == 0 && (!isset($_POST['promocode_name']) || empty($_POST['promocode_name']))){
			$err = 1;
			$errMsg = 'Provide Valid Promocode';
		} else if ($err == 0 && (!isset($_POST['start_date']) || empty($_POST['start_date']))){
			$err = 1;
			$errMsg = 'Provide Start Date';
		} else if ($err == 0 && (!isset($_POST['end_date']) || empty($_POST['end_date']))){
			$err = 1;
			$errMsg = 'Provide End Date';
		} else if ($err == 0 && (!isset($_POST['discount_type']) || $_POST['discount_type'] == '')){
            $err = 1;
			$errMsg = 'Provide Discount Type';
        } else if ($err == 0 && (!isset($_POST['max_redeem']) || empty($_POST['max_redeem']))){
            $err = 1;
			$errMsg = 'Provide Max Redeem';
        } else if ($err == 0 && (!isset($_POST['use_limit']) || empty($_POST['use_limit']))){
            $err = 1;
			$errMsg = 'Provide Use Limit';
        } else if ($_POST['discount_type'] == 1 && 
          (empty($_POST['discount_percentage']) || empty($_POST['max_redeem']))){
          	$err = 1;
			$errMsg = 'Provide Discount Percentage';
        }
        
		if($err == 1){
			$flashMsg['message'] = $errMsg;
			$this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('Promocode/addPromocode'));
		}

        $status = $this->Promocode_model->createPromocode($_POST);
        if($status == 1){
            $flashMsg =array('message'=>'Successfully Created Promocode..!','class'=>'success');
            $this->session->set_flashdata('message', $flashMsg);
            redirect(base_url('Promocode/promocode'));
        } else if ($status == 2) {
        	 $flashMsg =array('message'=>'Promocode Already in Use..!','class'=>'error');
            $this->session->set_flashdata('message', $flashMsg);
            redirect(base_url('Promocode/promocode'));
        } else {
            $this->session->set_flashdata('message', $flashMsg);
            redirect(base_url('Promocode/addPromocode'));
        }
	}

	public function updatePromocode($promocode_id = ''){
		$flashMsg = array('message'=>'Something went wrong, please try again..!','class'=>'error');
		if(empty($promocode_id) || empty($promocode_id = decode_param($promocode_id))){
			$this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('Promocode/promocode'));
		}

		$err = 0;
		$errMsg = '';
		$flashMsg = array('message'=>'Something went wrong, please try again..!','class'=>'error');
		if(!isset($_POST) || empty($_POST)){
			$this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('Promocode/addPromocode'));
		}

		if ($err == 0 && (!isset($_POST['promocode_name']) || empty($_POST['promocode_name']))){
			$err = 1;
			$errMsg = 'Provide Valid Promocode';
		} else if ($err == 0 && (!isset($_POST['start_date']) || empty($_POST['start_date']))){
			$err = 1;
			$errMsg = 'Provide Start Date';
		} else if ($err == 0 && (!isset($_POST['end_date']) || empty($_POST['end_date']))){
			$err = 1;
			$errMsg = 'Provide End Date';
		} else if ($err == 0 && (!isset($_POST['discount_type']) || $_POST['discount_type'] == '')){
            $err = 1;
			$errMsg = 'Provide Discount Type';
        } else if ($err == 0 && (!isset($_POST['max_redeem']) || empty($_POST['max_redeem']))){
            $err = 1;
			$errMsg = 'Provide Max Redeem';
        } else if ($err == 0 && (!isset($_POST['use_limit']) || empty($_POST['use_limit']))){
            $err = 1;
			$errMsg = 'Provide Use Limit';
        } else if ($_POST['discount_type'] == 1 && 
          (empty($_POST['discount_percentage']) || empty($_POST['max_redeem']))){
          	$err = 1;
			$errMsg = 'Provide Discount Percentage';
        }
        
		if($err == 1){
			$flashMsg['message'] = $errMsg;
			$this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('Promocode/editPromocode/'.$promocode_id));
		}

        $status = $this->Promocode_model->updatePromocode($promocode_id,$_POST);
        if($status == 1){
            $flashMsg =array('message'=>'Successfully Updates Promocode..!','class'=>'success');
            $this->session->set_flashdata('message', $flashMsg);
            redirect(base_url('Promocode/promocode/'));
        } else if ($status == 2) {
        	 $flashMsg =array('message'=>'Promocode Already in Use..!','class'=>'error');
            $this->session->set_flashdata('message', $flashMsg);
            redirect(base_url('Promocode/editPromocode/'.$promocode_id));
        } else {
            $this->session->set_flashdata('message', $flashMsg);
            redirect(base_url('Promocode/editPromocode/'.$promocode_id));
        }
	}

	public function changeStatus(){
        if(!isset($_POST) || !isset($_POST['promocode_id']) || empty($_POST['promocode_id']) || 
           empty($promocode_id = decode_param($_POST['promocode_id'])) || !isset($_POST['status']) || 
           $_POST['status'] == ''){
            echo json_encode(array('status'=>'0'));exit;
        }
        $resp = $this->Promocode_model->changeStatus($promocode_id,$_POST['status']);
        if($resp){
        	$promo = $this->Promocode_model->getPromocodeData($promocode_id,'0,1');
        	$cStatus = '';
        	if(!empty($promo) && $promo->status == 1){
              if(strtotime($promo->start_date) < strtotime(date('d-M-y H:i:s'))){
                $cStatus = 'Yet to Activete';
              } else if(strtotime($promo->start_date) > strtotime(date('d-M-y H:i:s')) && 
                 strtotime($promo->end_date) < strtotime(date('d-M-y H:i:s'))){
                $cStatus = 'Active';
              } else $cStatus = 'Offer Expired';
            } else $cStatus = 'De-active';
            echo json_encode(array('status'=>'1', 'data'=>array('c_status'=>$cStatus)));exit;
        }
        echo json_encode(array('status'=>'0'));exit;
	}
}
?>