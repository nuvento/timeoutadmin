<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tag extends CI_Controller {

	public function __construct() {
		parent::__construct();
        date_default_timezone_set("Asia/Riyadh");
        $this->load->model('Tag_model');
		$this->load->model('Dashboard_model');
		
		if(!$this->session->userdata('logged_in')) {
			redirect(base_url('Login'));
		}

        $role = roleManagement();
        if(!array_key_exists('Tag',$role)){
            redirect(base_url('Dashboard'));
        }
 	}
	
	function listTags(){
		$template['page'] = 'Tag/viewTagList';
        $template['menu'] = 'Tag Management';
        $template['smenu'] = 'View Tag List';
        $template['pTitle'] = "Tag Management";
        $template['pDescription'] = "View Tag List";

        $template['tag_data'] = $this->Tag_model->getTagData();
		$this->load->view('template',$template);
	}

	function addTags(){
		$template['page'] = 'Tag/tagAddForm';
        $template['menu'] = 'Tag Management';
        $template['smenu'] = 'Add Tag';
        $template['pTitle'] = "Add Tag";
        $template['pDescription'] = "Create New Tag";

		$this->load->view('template',$template);
	}

	function createTags(){
		$flashMsg = array('message'=>'Something went wrong, please try again..!','class'=>'error');

		if(!isset($_POST) || empty($_POST)){
            $this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('Tag/addTags'));
        }

		$err = 0;
        $errMsg = '';
        if($err == 0 && (!isset($_POST['tag_en']) || empty($_POST['tag_en']))){
            $err = 1;
            $errMsg = 'Provide a Tag (English)';
        }

        $status = $this->Tag_model->createTag($_POST);
        if($status == 1){
            $flashMsg['class'] = 'success';
            $flashMsg['message'] = 'Tag Created';

            $this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('Tag/listTags'));
        }
        $this->session->set_flashdata('message',$flashMsg);
        redirect(base_url('Tag/addTags'));
	}

	function editTags($tag_id=''){
        $flashMsg = array('message'=>'Something went wrong, please try again..!','class'=>'error');
		if(empty($tag_id) || empty(decode_param($tag_id))){
			$this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('Tag/listTags'));
		}
		$template['page'] = 'Tag/tagAddForm';
        $template['menu'] = 'Tag Management';
        $template['smenu'] = 'Edit Tag';
        $template['pTitle'] = "Edit Tag";
        $template['pDescription'] = "Update Tag Data";

        $template['tag_id'] = $tag_id;
        $template['tag_data'] = $this->Tag_model->getTagData(decode_param($tag_id));

		$this->load->view('template',$template);
	}

	function updateTags($tag_id=''){
		$flashMsg = array('message'=>'Something went wrong, please try again..!','class'=>'error');

		if(!isset($_POST) || empty($_POST) || empty($tag_id)){
            $this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('Tag/listTags'));
        }

        $err = 0;
        $errMsg = '';
        if($err == 0 && (!isset($_POST['tag_name']) || empty($_POST['tag_name']))){
            $err = 1;
            $errMsg = 'Provide a Tag (English)';
        }
        
        $status = $this->Tag_model->updateTags(decode_param($tag_id),$_POST);
        if($status == 1){
            $flashMsg['class'] = 'success';
            $flashMsg['message'] = 'Tag Created';

            $this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('Tag/listTags'));
        }
        $this->session->set_flashdata('message',$flashMsg);
        redirect(base_url('Tag/editTags/'.$tag_id));
	}

    function changeStatus(){
        if(!isset($_POST) || !isset($_POST['tag_id']) || empty($_POST['tag_id']) || 
           !isset($_POST['status']) || $_POST['status'] == ''){
            echo json_encode(array('status'=>'0'));exit;
        }
        $tag_id = decode_param($_POST['tag_id']);
        $status = $_POST['status'];
        $resp = $this->Tag_model->changeStatus($tag_id,$status);
        if($resp){
            echo json_encode(array('status'=>'1'));exit;
        }
        echo json_encode(array('status'=>'0'));exit;
    }

    function getTagData(){
        $resArr = array('status'=>0);
        if(!isset($_POST) || empty($_POST) || !isset($_POST['tag_id']) || empty($_POST['tag_id']) || 
           empty($tag_id = decode_param($_POST['tag_id']))){
            echo json_encode($resArr);exit;
        }
        $tagData = $this->Tag_model->getTagData($tag_id);
        if(!empty($tagData)){
            $resArr['status'] = 1;
            $resArr['data'] = $tagData;
        }
        echo json_encode($resArr);exit;
    }
}
?>