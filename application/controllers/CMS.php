<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CMS extends CI_Controller {

	public function __construct() {
		parent::__construct();
        date_default_timezone_set("Asia/Riyadh");
		$this->load->model('Cms_model');
		
		if(!$this->session->userdata('logged_in')) {
			redirect(base_url('Login'));
		}
		if($this->session->userdata['user_type'] != 1){
			$flashMsg = array('message'=>'Access Denied You don\'t have permission to access this Page',
							  'class'=>'error');
			$this->session->set_flashdata('message',$flashMsg);
            redirect(base_url());
		}
 	}
	
	public function index() {

		$template['page'] = 'CMS/cms_management';
		$template['menu'] = "CMS Settings";
		$template['smenu'] = "Change CMS Data";
        $template['pTitle'] = "CMS";
        $template['page_head'] = "Booking Management";
        $template['pDescription'] = "Edit or View CMS Data";

		$template['cmsData'] = $this->Cms_model->getCMSdata();
		$this->load->view('template',$template);
	}

	public function changeCMSdata(){
		$url = 'CMS';
		$language = getLanguages();
		$flashMsg = array('message'=>'Something went wrong, please try again..!','class'=>'error');
		if(!isset($_POST) || empty($_POST)){
			$this->session->set_flashdata('message',$flashMsg);
            redirect(base_url($url));
		}
	    
	    $cmsData = array();
	    foreach($language AS $lang) {
	    	$cmsData[$lang] = array('language_code'=>$lang);
	    	if(isset($_POST['faq_'.$lang]) && !empty($_POST['faq_'.$lang])){
				$cmsData[$lang]['faq'] = $_POST['faq_'.$lang];
			}
			if(isset($_POST['instruction_'.$lang]) && !empty($_POST['instruction_'.$lang])){
				$cmsData[$lang]['instruction'] = $_POST['instruction_'.$lang];
			}
			if(isset($_POST['privacy_policy_'.$lang]) && !empty($_POST['privacy_policy_'.$lang])){
				$cmsData[$lang]['privacy_policy'] = $_POST['privacy_policy_'.$lang];
			}
			if(isset($_POST['terms_and_conditions_'.$lang]) && !empty($_POST['terms_and_conditions_'.$lang])){
				$cmsData[$lang]['terms_and_conditions'] = $_POST['terms_and_conditions_'.$lang];
			}
	    }

        $status = $this->Cms_model->updateCMS($cmsData);
 		if($status){
 			$flashMsg['class'] = 'success'; 
 			$flashMsg['message'] = 'Settings Successfully Updated..!';
 		}
 		$this->session->set_flashdata('message',$flashMsg);
        redirect(base_url($url));
	}
}
?>