<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct() {
		parent::__construct();
        date_default_timezone_set("Asia/Riyadh");
		
		if(!$this->session->userdata('logged_in')) {
			redirect(base_url('Login'));
		}
		
		$userType = $this->session->userdata['user_type'];
		if($userType == 5){
			redirect(base_url('Customer_Booking'));
		}

		redirect(base_url('Event/listEvents'));
 	}
	
	public function index() {
		$template['page'] = 'Dashboard/Dashboard';
        $template['page_desc'] = "Control Panel";
        $template['page_title'] = "Dashboard";
        
		$this->load->view('template',$template);
	}
}
?>
