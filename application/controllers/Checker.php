<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Checker extends CI_Controller {

	public function __construct() {
		parent::__construct();
        date_default_timezone_set("Asia/Riyadh");
        $this->load->model('Checker_model');
        $this->load->model('Provider_model');
		
		if(!$this->session->userdata('logged_in')) {
			redirect(base_url('Login'));
		}
 	}
	
	function viewCheckers(){
		$template['page'] = 'Checker/viewChecker';
        $template['menu'] = 'Checker Management';
        $template['smenu'] = 'View Checker';
        $template['pTitle'] = "Checker Management";
        $template['pDescription'] = "View Checker List";

        $provider_id = $this->session->userdata['id'];
        if($this->session->userdata['user_type'] == 1){
            $provider_id = (isset($_POST['provider_id'])&&$_POST['provider_id']!='')?$_POST['provider_id']:'';
        }
        $template['provider_id'] = $provider_id;
        $template['checker_data'] = $this->Checker_model->getCheckerData($provider_id);
        $template['provider_data'] = $this->Provider_model->getProviderData('','0,1');
        $template['enc_provider_id'] = encode_param($provider_id);
		$this->load->view('template',$template);
	}

	function addChecker(){
		$template['page'] = 'Checker/checkerAddForm';
        $template['menu'] = 'Checker Management';
        $template['smenu'] = 'Add Checker';
        $template['pTitle'] = "Add Checker";
        $template['pDescription'] = "Create New Checker";

        $template['provider_id'] = $_GET['provider_id'];
		$this->load->view('template',$template);
	}

	function createChecker(){
		$flashMsg = array('message'=>'Something went wrong, please try again..!','class'=>'error');

		if(!isset($_POST) || empty($_POST)){
            $this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('Checker/addChecker'));
        }

		$err = 0;
        $errMsg = '';
        if($err == 0 && (!isset($_POST['username']) || empty($_POST['username']))){
            $err = 1;
            $errMsg = 'Provide Checker User Name';
        } else if($err == 0 && (!isset($_POST['password']) || empty($_POST['password']))){
            $err = 1;
            $errMsg = 'Provide Checker Password';
        } else if($err == 0 && (!isset($_POST['provider_id']) || empty($_POST['provider_id']) || 
                                empty($provider_id = decode_param($_POST['provider_id'])))){
            $err = 1;
            $errMsg = 'Something went wrong, please try again..!';
        }
        if($err == 1){
            $flashMsg['class'] = 'error';
            $flashMsg['message'] = $errMsg;

            $this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('Checker/viewCheckers'));
        }
        $_POST['password'] = md5($_POST['password']);
        $_POST['provider_id'] = $provider_id;
        $status = $this->Checker_model->createChecker($_POST);
        if($status == 1){
            $this->load->model('Api_model');
            $subject  = "Your TimeOut Checker Account is now activated";
            $template = getNotifTemplate();
            $message = "<html>
                            <body>
                                Your Checker Account for the username 
                                <strong>".$_POST['username']."</strong> is now activated.
                            </body>
                        </html>";

            if(isset($template['checker_activation_mail']) && !empty($template['checker_activation_mail'])){
                $message = str_replace(array('{:user_name}'),array($email_id),
                                       $template['checker_activation_mail']);
            }
            $this->Api_model->send_mail($subject,$_POST['username'],$message);

            $flashMsg['class'] = 'success';
            $flashMsg['message'] = 'Checker Created';

            $this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('Checker/viewCheckers'));
        } else if ($status == 2){
            $flashMsg['class'] = 'error';
            $flashMsg['message'] = 'Checker User name already exist..!';

            $this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('Checker/viewCheckers'));
        }
        $this->session->set_flashdata('message',$flashMsg);
        redirect(base_url('Checker/addChecker'));
	}

    function changeStatus(){
        if(!isset($_POST) || !isset($_POST['checker_id']) || empty($_POST['checker_id']) || 
           !isset($_POST['status']) || $_POST['status'] == ''){
            echo json_encode(array('status'=>'0'));exit;
        }
        $status = $_POST['status'];
        $checker_id = decode_param($_POST['checker_id']);
        $resp = $this->Checker_model->changeStatus($checker_id,$status);
        if($resp){
            $this->load->model('Api_model');
            $checkerData = $this->db->get_where('checker',array('id'=>$checker_id))->row();

            if(!empty($checkerData)){
                $subject  = "Your Checker Account is now activated";
                $email_id = $checkerData->username;

                $message = "<html><body>Your Checker Account for the username 
                            <strong>".$email_id."</strong> is now activated.</body></html>";

                $template = getNotifTemplate();
                if(isset($template['checker_activation_mail']) && 
                   !empty($template['checker_activation_mail'])){
                    $message = str_replace(array('{:user_name}'),array($email_id),
                                           $template['checker_activation_mail']);
                }

                $this->Api_model->send_mail($subject,$email_id,$message);
            }
            echo json_encode(array('status'=>'1'));exit;
        }
        echo json_encode(array('status'=>'0'));exit;
    }

    function checkinReport(){
        $template['page'] = 'Checker/checkinReport';
        $template['menu'] = 'Checker Report';
        $template['smenu'] = 'View Checker CheckIn List';
        $template['pTitle'] = "Checker Report";
        $template['pDescription'] = "View Checker CheckIn List";

        // $checker_id = '';
        // $provider_id = '';
        // if($this->session->userdata['user_type'] == 1){
            
        // }

        $this->load->view('template',$template);
    }
}
?>