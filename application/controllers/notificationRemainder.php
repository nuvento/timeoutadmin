<?php
    date_default_timezone_set("Asia/Riyadh");
    $conn = mysqli_connect("localhost","nuvento_timeout","Golden_123","nuvento_timeout");
    if($conn->connect_error){
        return;
    }

    $result = mysqli_query($conn, "SELECT app_id FROM setting");

    if(mysqli_num_rows($result) <= 0 || empty($row = mysqli_fetch_assoc($result)) || 
       !isset($row['app_id']) || empty($key = $row['app_id'])){
        return;
    }

    $date  = date('Y-m-d',strtotime(date("Y-m-d H:i:s")." +30 minutes"));
    $sTime = date('H:i',strtotime(date("Y-m-d H:i:s")." +30 minutes"));
    $eTime = date("H:i",strtotime(date("Y-m-d H:i:s")." +31 minutes"));

    $sql = "SELECT CUST.customer_id,CUST.fcm_token,BOK.id AS booking_id,BOK.bookId,EVT.event_name
            FROM booking AS BOK
            INNER JOIN customer AS CUST ON (CUST.customer_id=BOK.customer_id)
            INNER JOIN users AS USR ON (USR.id=CUST.customer_id)
            INNER JOIN events AS EVT ON (EVT.event_id=BOK.event_id)
            INNER JOIN event_date_time AS EDATE ON (EDATE.event_id=BOK.event_id)
            WHERE EVT.status='1' AND BOK.status='1' AND USR.status='1' AND EDATE.status='1' AND 
                  EDATE.date='$date' AND EDATE.time<='$eTime' AND EDATE.time>='$sTime'
            GROUP BY BOK.id,CUST.customer_id";
    $result = mysqli_query($conn, $sql);

    if(mysqli_num_rows($result) > 0) {
        while($row = mysqli_fetch_assoc($result)) {

            $data = "{ \"notification\": { \"title\": \"".$row['event_name']."\", \"text\": \"Booking Reminder\", \"sound\": \"default\" }, \"time_to_live\": 60, \"data\" : {\"response\" : {\"status\" : \"success\", \"data\" : {\"booking_id\" : \"".$row['bookId']."\", \"trip_status\" : 0}}}, \"collapse_key\" : \"trip\", \"priority\":\"high\", \"to\" : \"".$row['fcm_token']."\"}";

            $ch = curl_init("https://fcm.googleapis.com/fcm/send");
            $header = array('Content-Type: application/json', 'Authorization: key='.$key);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

            $out = curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_exec($ch);
            curl_close($ch);
        }
        mysqli_query($conn, "INSERT INTO crontab_notif VALUES ('',".date('Y-m-d').",".date('H:i:s').")");
    }
    $conn->close();
?>
