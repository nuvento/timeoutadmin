<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {

	public function __construct() {
		parent::__construct();
        date_default_timezone_set("Asia/Riyadh");
        $this->load->model('Category_model');
		$this->load->model('Dashboard_model');
		
		if(!$this->session->userdata('logged_in')) {
			redirect(base_url('Login'));
		}

        $role = roleManagement();
        if(!array_key_exists('Category',$role)){
            redirect(base_url('Dashboard'));
        }
 	}
	
	function listCategory(){
		$template['page'] = 'Category/viewCategoryList';
        $template['menu'] = 'Category Management';
        $template['smenu'] = 'View Category List';
        $template['pTitle'] = "Category Management";
        $template['pDescription'] = "View Category List";

        $template['categoryData'] = $this->Category_model->getCategoryData();
		$this->load->view('template',$template);
	}

	function addCategory(){
        $this->load->model('Category_model');
        $template['categoryData'] = $this->Category_model->getCategoryData();

		$template['page'] = 'Category/categoryAddForm';
        $template['menu'] = 'Category Management';
        $template['smenu'] = 'Add Category';
        $template['pTitle'] = "Add Category";
        $template['pDescription'] = "Create New Category";

		$this->load->view('template',$template);
	}

	function createCategory(){
		$flashMsg = array('message'=>'Something went wrong, please try again..!','class'=>'error');

		if(!isset($_POST) || empty($_POST)){
            $this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('Category/addCategory'));
        }

		$err = 0;
        $errMsg = '';
        if(!isset($_POST['category_name_EN']) || empty($_POST['category_name_EN'])){
            $err = 1;
            $errMsg = 'Provide a Category Name in English';
        } else if ($err == 0 && 
                   (!isset($_FILES) || empty($_FILES) || !isset($_FILES['category_image_EN']) || 
                    empty($_FILES['category_image_EN']))){
            $err = 1;
            $errMsg = 'Provide a Category Icon';
        } else if($err == 0 && (!isset($_FILES) || empty($_FILES) || 
                                !isset($_FILES['category_banner'])||empty($_FILES['category_banner']))){
            $err = 1;
            $errMsg = 'Provide a Category Banner Image';
        }

        if($err == 0){
            $this->load->library('upload');
            $config = set_upload_service("assets/uploads/services");

            $config['file_name'] = time()."_".$_FILES['category_banner']['name'];
            $this->upload->initialize($config);
            if(!$this->upload->do_upload('category_banner')){
                $err = 1;
                $errMsg = $this->upload->display_errors();
            }else{
                $upload_data = $this->upload->data();
                $_POST['category_banner'] = $config['upload_path']."/".$upload_data['file_name'];
            }

            $language = getLanguages();
            foreach($language AS $lang) {
                if(!isset($_FILES['category_image_'.$lang]['name']) || 
                   empty($_FILES['category_image_'.$lang]['name'])){
                    continue;
                }

                $config['file_name'] = time()."_".$_FILES['category_image_'.$lang]['name'];
                $this->upload->initialize($config);
                if(!$this->upload->do_upload('category_image_'.$lang)){
                    $err = 1;
                    $errMsg = $this->upload->display_errors();
                    break;
                }else{
                    $upload_data = $this->upload->data();
                    $_POST['category_image_'.$lang]=$config['upload_path']."/".$upload_data['file_name'];
                }
            }
        }

        if($err == 1){
            $flashMsg['message'] = $errMsg;
            $this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('Category/addCategory'));
        }

        $status = $this->Category_model->createCategory($_POST);
        if($status == 1){
            $flashMsg['class'] = 'success';
            $flashMsg['message'] = 'Category Created';

            $this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('Category/listCategory'));
        }
        $this->session->set_flashdata('message',$flashMsg);
        redirect(base_url('Category/addCategory'));
	}

	function editCategory($category_id=''){
        $flashMsg = array('message'=>'Something went wrong, please try again..!','class'=>'error');
		if(empty($category_id) || empty(decode_param($category_id))){
			$this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('Category/listCategory'));
		}
		$template['page'] = 'Category/categoryAddForm';
        $template['menu'] = 'Category Management';
        $template['smenu'] = 'Edit Category';
        $template['pTitle'] = "Edit Category";
        $template['pDescription'] = "Update Category Data";

        $template['category_id'] = $category_id;
        $template['categoryData'] = $this->Category_model->getCategoryData(decode_param($category_id));
		$this->load->view('template',$template);
	}

	function updateCategory($category_id=''){
		$flashMsg = array('message'=>'Something went wrong, please try again..!','class'=>'error');

		if(!isset($_POST) || empty($_POST) || empty($category_id)){
            $this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('Category/listCategory'));
        }

		$err = 0;
        $errMsg = '';
        if(!isset($_POST['category_name_EN']) || empty($_POST['category_name_EN'])){
            $err = 1;
            $errMsg = 'Provide a Category Name in English';
        }

        if($err == 0){
            $this->load->library('upload');
            $config = set_upload_service("assets/uploads/services");

            $language = getLanguages();
            foreach($language AS $lang) {
                if(!isset($_FILES['category_image_'.$lang]['name']) || 
                   empty($_FILES['category_image_'.$lang]['name'])){
                    continue;
                }

                $config['file_name'] = time()."_".$_FILES['category_image_'.$lang]['name'];
                $this->upload->initialize($config);
                if($this->upload->do_upload('category_image_'.$lang)){
                    $upload_data = $this->upload->data();
                    $_POST['category_image_'.$lang]=$config['upload_path']."/".$upload_data['file_name'];
                }
            }
            
            $config['file_name'] = time()."_".$_FILES['category_banner']['name'];
            $this->upload->initialize($config);
            if($this->upload->do_upload('category_banner')){
                $upload_data = $this->upload->data();
                $_POST['category_banner'] = $config['upload_path']."/".$upload_data['file_name'];
            }
        }

        if($err == 1){
            $flashMsg['message'] = $errMsg;
            $this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('Category/editCategory/'.$category_id));
        }

        $status = $this->Category_model->updateCategory(decode_param($category_id),$_POST);
        if($status == 1){
            $flashMsg['class'] = 'success';
            $flashMsg['message'] = 'Category Created';

            $this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('Category/listCategory'));
        }
        $this->session->set_flashdata('message',$flashMsg);
        redirect(base_url('Category/editCategory/'.$category_id));
	}

    function changeStatus(){
        if(!isset($_POST) || !isset($_POST['category_id']) || empty($_POST['category_id']) || 
           !isset($_POST['status']) || $_POST['status'] == ''){
            echo json_encode(array('status'=>'0'));exit;
        }
        $status = $_POST['status'];
        $category_id = decode_param($_POST['category_id']);
        $resp = $this->Category_model->changeStatus($category_id,$status);
        if($resp){
            echo json_encode(array('status'=>'1'));exit;
        }
        echo json_encode(array('status'=>'0'));exit;
    }

    function categoryOrdering(){
        $template['page'] = 'Category/categoryOrder';
        $template['menu'] = 'Category Ordering';
        $template['smenu'] = 'Order Category List';
        $template['pTitle'] = "Category Ordering";
        $template['pDescription'] = "Order Category List";

        $template['categoryData'] = $this->Category_model->getCategoryData('','1');
        $this->load->view('template',$template);
    }

    function catReorder(){
        if(!isset($_POST) || !isset($_POST['category_order']) || empty($_POST['category_order'])){
            echo json_encode(array('status'=>'0'));exit;
        }
        $resp = $this->Category_model->catReorder($_POST['category_order']);
        if($resp){
            echo 1;exit;
        }
        echo 0;exit;
    }
}
?>
