<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct() {
		parent::__construct();
        date_default_timezone_set("Asia/Riyadh");
        $this->load->model('User_model');
		$this->load->model('Dashboard_model');
		
		if(!$this->session->userdata('logged_in')) {
			redirect(base_url('Login'));
		}
 	}
	
	public function viewProfile() {
        if(!isset($this->session->userdata['id']) || empty($this->session->userdata['id'])){
            redirect(base_url());
        }

        $template['provider'] = '';
        $template['customer'] = '';
        if(($user_type = $this->session->userdata('user_type')) != 1){ 
            if($user_type != 1){
                if($user_type == 2){
                    $this->load->model('Provider_model');
                    $template['provider'] = $this->Provider_model->getProviderData($this->session->userdata('id'));
                } else if ($user_type == 3){
                    $this->load->model('Customer_model');
                    $template['customer'] = $this->Customer_model->getCustomerData($this->session->userdata('id'));
                }
            }
        }
		$template['page'] = 'User/viewProfile';
        $template['menu'] = 'User';
        $template['smenu'] = 'View Profile';
        $template['pTitle'] = "User Profile";
        $template['pDescription'] = "Edit or View Profile";
		$this->load->view('template',$template);
	}

	public function editProfile() {
        $template['provider'] = '';
        $template['customer'] = '';

        if(($user_type = $this->session->userdata('user_type')) != 1){ 
            if($user_type != 1){
                if($user_type == 2){
                    $this->load->model('Provider_model');
                    $template['provider'] = $this->Provider_model->getProviderData($this->session->userdata('id'));
                } else if ($user_type == 3){
                    $this->load->model('Customer_model');
                    $template['customer'] = $this->Customer_model->getCustomerData($this->session->userdata('id'));
                }
            }
        }

		$template['page'] = 'User/editProfile';
        $template['menu'] = "Profile";
        $template['smenu'] = "Edit Profile";
        $template['pTitle'] = "Edit Profile";
        $template['pDescription'] = "Edit User Profile";
        
		$this->load->view('template',$template);
	}

	public function updateUser(){

 		$user_id = $this->session->userdata('id');
 		$user_type = $this->session->userdata('user_type');

		$flashMsg = array('message'=>'Something went wrong, please try again..!','class'=>'error');
    	if(empty($user_id)){
            $this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('User/editProfile'));
    	}

        if(isset($_FILES['profile_image']) && !empty($_FILES['profile_image'])){
            $config = set_upload_service("assets/uploads/services");
            $this->load->library('upload');

            $new_name = time()."_".$_FILES['profile_image']['name'];
            $config['file_name'] = $new_name;

            $this->upload->initialize($config);

            if($this->upload->do_upload('profile_image')){
                $upload_data = $this->upload->data();
                $_POST['profile_image'] = $config['upload_path']."/".$upload_data['file_name'];
            }
        }

        if((isset($_POST['password']) || isset($_POST['cPassword'])) && 
           (!empty($_POST['password']) || !empty($_POST['cPassword']))){
            if($_POST['password'] != $_POST['cPassword']){
                $flashMsg = array('message'=>'Re-enter Password..!','class'=>'error');
                $this->session->set_flashdata('message', $flashMsg);
                redirect(base_url('User/editProfile'));
            }
            $password = $_POST['password'];
            unset($_POST['password']);
            unset($_POST['cPassword']);
            $_POST['password'] = md5($password);
        } else {
            unset($_POST['password']);
            unset($_POST['cPassword']);
        }
        if(!isset($_POST['display_name']) || empty($_POST['display_name'])){
            $flashMsg = array('message'=>'Provide a valid Display Name..!','class'=>'error');
            $this->session->set_flashdata('message', $flashMsg);
            redirect(base_url('User/editProfile'));
        } else if (!isset($_POST['username']) || empty($_POST['username'])){
            $flashMsg = array('message'=>'Provide a valid Username..!','class'=>'error');
            $this->session->set_flashdata('message', $flashMsg);
            redirect(base_url('User/editProfile')); 
        }
        if ($user_type == 2) {
	        if (!isset($_POST['name']) || empty($_POST['name'])){
	            $flashMsg = array('message'=>'Provide a Name..!','class'=>'error');
	            $this->session->set_flashdata('message', $flashMsg);
	            redirect(base_url('User/editProfile'));
	        } else if (!isset($_POST['phone']) || empty($_POST['phone'])){
	            $flashMsg = array('message'=>'Provide a valid Phone Number..!','class'=>'error');
	            $this->session->set_flashdata('message', $flashMsg);
	            redirect(base_url('User/editProfile'));
	        } else if (!isset($_POST['email']) || empty($_POST['email'])){
                $flashMsg = array('message'=>'Provide a valid Email ID..!','class'=>'error');
                $this->session->set_flashdata('message', $flashMsg);
                redirect(base_url('User/editProfile'));
            }
	    } else if ($user_type == 3) {
            if (!isset($_POST['name']) || empty($_POST['name'])){
                $flashMsg = array('message'=>'Provide a Name..!','class'=>'error');
                $this->session->set_flashdata('message', $flashMsg);
                redirect(base_url('User/editProfile'));
            } else if (!isset($_POST['phone']) || empty($_POST['phone'])){
                $flashMsg = array('message'=>'Provide a valid Phone Number..!','class'=>'error');
                $this->session->set_flashdata('message', $flashMsg);
                redirect(base_url('User/editProfile'));
            } else if (!isset($_POST['email']) || empty($_POST['email'])){
                $flashMsg = array('message'=>'Provide a valid Email ID..!','class'=>'error');
                $this->session->set_flashdata('message', $flashMsg);
                redirect(base_url('User/editProfile'));
            }
        }

        $status = $this->User_model->updateUser($user_id,$user_type,$_POST);
        if($status == 1){
            $flashMsg =array('message'=>'Successfully Updated User Details..!','class'=>'success');
            $this->session->set_flashdata('message', $flashMsg);
            redirect(base_url('User/viewProfile'));
        } else if($status == 5){
            $flashMsg = array('message'=>'Email ID already exist..!','class'=>'error');
            $this->session->set_flashdata('message', $flashMsg);
            redirect(base_url('User/editProfile'));
        } else if($status == 6){
            $flashMsg = array('message'=>'Phone Number already exist..!','class'=>'error');
            $this->session->set_flashdata('message', $flashMsg);
            redirect(base_url('User/editProfile'));
        } else if($status == 4){
            $flashMsg = array('message'=>'User Name already exist..!','class'=>'error');
            $this->session->set_flashdata('message', $flashMsg);
            redirect(base_url('User/editProfile'));
        } else {
            $this->session->set_flashdata('message', $flashMsg);
            redirect(base_url('User/editProfile'));
        }
	}
}
?>