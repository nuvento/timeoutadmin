<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Host extends CI_Controller {
    
	public function __construct() {
		parent::__construct();
        date_default_timezone_set("Asia/Riyadh");
        $this->load->model('Host_model');
		$this->load->model('Dashboard_model');
		
		if(!$this->session->userdata('logged_in')) {
			redirect(base_url('Login'));
		}
        
        $role = roleManagement();
        if(!array_key_exists('Host',$role)){
            redirect(base_url('Dashboard'));
        }
 	}
	
	function listHostCategory(){
		$template['page'] = 'Host/viewHostCategories';
        $template['menu'] = 'Host Category Management';
        $template['smenu'] = 'View Host Categories';
        $template['pTitle'] = "Host Category Management";
        $template['pDescription'] = "View Host Categories";

        $template['host_data'] = $this->Host_model->getHostCategories();
		$this->load->view('template',$template);
	}

	function addHostCategory(){
		$template['page'] = 'Host/hostCategoryForm';
        $template['menu'] = 'Host Category Management';
        $template['smenu'] = 'Add Host Category';
        $template['pTitle'] = "Add Host Category";
        $template['pDescription'] = "Create New Host Category";

		$this->load->view('template',$template);
	}

	function createHostCategory(){
		$flashMsg = array('message'=>'Something went wrong, please try again..!','class'=>'error');

		if(!isset($_POST) || empty($_POST)){
            $this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('Host/addHostCategory'));
        }

		$err = 0;
        $errMsg = '';
        if($err == 0 && (!isset($_POST['host_category']) || empty($_POST['host_category']))){
            $err = 1;
            $errMsg = 'Provide a Host Category Name';
        }
        else if($err == 0 && (!isset($_POST['show_layout']) || $_POST['show_layout'] == '' )){
            $err = 1;
            $errMsg = 'Provide a Seat Layout Type';
        }

        if($err == 1){
            $flashMsg['message'] = $errMsg;
            $this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('Host/addHostCategory'));
        }

        $status = $this->Host_model->createHostCategory($_POST);
        if($status == 1){
            $flashMsg['class'] = 'success';
            $flashMsg['message'] = 'Host Category Created';

            $this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('Host/listHostCategory'));
        }
        $this->session->set_flashdata('message',$flashMsg);
        redirect(base_url('Host/addHostCategory'));
	}

	function editHostCategory($host_id=''){
        $flashMsg = array('message'=>'Something went wrong, please try again..!','class'=>'error');
		if(empty($host_id) || empty(decode_param($host_id))){
			$this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('Host/listHostCategory'));
		}
		$template['page'] = 'Host/hostCategoryForm';
        $template['menu'] = 'Host Category Management';
        $template['smenu'] = 'Edit Host Category';
        $template['pTitle'] = "Edit Host Category";
        $template['pDescription'] = "Update Host Category Data";

        $template['host_id'] = $host_id;
        $template['host_data'] = $this->Host_model->getHostCategories(decode_param($host_id));
		$this->load->view('template',$template);
	}

	function updateHostCategory($host_id=''){
		$flashMsg = array('message'=>'Something went wrong, please try again..!','class'=>'error');

		if(!isset($_POST) || empty($_POST) || empty($host_id)){
            $this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('Host/listHostCategory'));
        }

		$err = 0;
        $errMsg = '';
        if($err == 0 && (!isset($_POST['host_category']) || empty($_POST['host_category']))){
            $err = 1;
            $errMsg = 'Provide a Host Category Name';
        }
        else if($err == 0 && (!isset($_POST['show_layout']) || $_POST['show_layout'] == '')){
            $err = 1;
            $errMsg = 'Provide a Seat Layout Type';
        }

        if($err == 1){
            $flashMsg['message'] = $errMsg;
            $this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('Host/editHostCategory/'.$host_id));
        }

        $status = $this->Host_model->updateHostCategory(decode_param($host_id),$_POST);
        if($status == 1){
            $flashMsg['class'] = 'success';
            $flashMsg['message'] = 'Host Category Created';

            $this->session->set_flashdata('message',$flashMsg);
            redirect(base_url('Host/listHostCategory'));
        }
        $this->session->set_flashdata('message',$flashMsg);
        redirect(base_url('Host/editHostCategory/'.$host_id));
	}

    function changeStatus(){
        if(!isset($_POST) || !isset($_POST['host_id']) || empty($_POST['host_id']) || 
           !isset($_POST['status']) || $_POST['status'] == ''){
            echo json_encode(array('status'=>'0'));exit;
        }
        $status = $_POST['status'];
        $host_id = decode_param($_POST['host_id']);
        $resp = $this->Host_model->changeStatus($host_id,$status);
        if($resp){
            echo json_encode(array('status'=>'1'));exit;
        }
        echo json_encode(array('status'=>'0'));exit;
    }
}
?>