<?php
  $settings = getSettings();
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>TimeOut AdminPanel | Log in</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?php
  $this->load->view('Templates/header-script');
  ?>
  </head>
  <body class="login_wrapper">
    
  <div class="login_overlay">
            <h1 class="">Welcome !</h1>
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="login_left">
                            <div class="login_logo">
                                <img src="assets/images/asset_logo.png">
                            </div>
                            <div class="login_slider_quotes">
                                <ul>
                                    <li>
                                        <p>
                                        <strong>"</strong>
                                            To achieve great things, two things are needed: a plan and not quite enough time. 
                                        <strong>"</strong>
                                        </p>
                                        <h6>Margrim Nicholas</h6>
                                    </li>
                                    <li>
                                        <p>
                                        <strong>"</strong>
                                            Productivity is never an accident. It is always the result of a commitment to excellence, intelligent planning, and focused effort. 
                                        <strong>"</strong>
                                        </p>
                                        <h6>Marie Lorem</h6>
                                    </li>
                                    <li>
                                        <p>
                                            <strong>"</strong>
                                                There are some people who live in a dream world, and there are some who face reality; and then there are those who turn one into the other. 
                                            <strong>"</strong>
                                        </p>
                                        <h6>Lewis Adam</h6>
                                    </li>
                                </ul>
                            </div>
                            
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="login_right">
                            <div class="login_container_panel">
                                <h3>Login</h3>
                                <?php if(validation_errors()) { ?>
                                  <div class="alert alert-danger">
                                    <?= validation_errors() ?>
                                  </div>
                                <?php } ?>


                                <form action="" method="post">
                                  <div class="form-group has-feedback login_row">
                                    <input type="text" class="form-control login_input" name="username" placeholder="Email">
                                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                  </div>
                                  <div class="form-group has-feedback login_row">
                                    <input type="password" class="form-control login_input" name="password" placeholder="Password">
                                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                  </div>
                                  <div class="login_row">
                                   
                                      <button type="submit" class=" login_btn btn btn-primary btn-block btn-flat">Sign In</button>
                                    
                                  </div>
                                </form>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </body>
  <!-- <body class="hold-transition login-page">
    <div class="login-box">
      <div class="login-logo">
        <a href="<?= base_url() ?>"><b><?= $settings['title'] ?></b></a>
      </div>
      <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>
        <?php if(validation_errors()) { ?>
          <div class="alert alert-danger">
            <?= validation_errors() ?>
          </div>
        <?php } ?>
        <form action="" method="post">
          <div class="form-group has-feedback">
            <input type="text" class="form-control" name="username" placeholder="Email">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" class="form-control" name="password" placeholder="Password">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-12 right">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </body> -->

  <?php
  $this->load->view('Templates/footer-script');
  ?>
</html>
