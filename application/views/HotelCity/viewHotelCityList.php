<?php
  $role = roleManagement();
  $pAccess = $role['City'];
?>

<div class="content-wrapper" >
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
       <?= $pTitle ?>
        <small><?= $pDescription ?></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?= base_url() ?>"><i class="fa fa-star-o" aria-hidden="true"></i>Home</a></li>
      <li><?= $menu ?></li>
      <li class="active"><?= $smenu ?></li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <?php if($this->session->flashdata('message')) { 
          $flashdata = $this->session->flashdata('message'); ?>
          <div class="alert alert-<?= $flashdata['class'] ?>">
             <button class="close" data-dismiss="alert" type="button">×</button>
             <?= $flashdata['message'] ?>
          </div>
        <?php } ?>
      </div>
      <div class="col-xs-12">
        <div class="box box-warning">
          <div class="box-header with-border">
            <div class="col-md-6"><h3 class="box-title">Hotel City List</h3></div>
            <div class="col-md-6" align="right">
              <?php if(in_array('1',$pAccess)){ ?>
                <a class="btn btn-sm btn-primary" href="<?= base_url('HotelCity/addHotelCity') ?>">
                  Add New Hotel City
                </a>
              <?php } ?>
              <a class="btn btn-sm btn-primary" href="<?= base_url() ?>">Back</a>
           </div>
          </div>
          <div class="box-body">
            <table id="mechanicUsers" class="table table-bordered table-striped datatable ">
              <thead>
                <tr>
                  <th class="hidden">ID</th>
                  <th width="50px;">Icon</th>
                  <th width="125px;">Hotel City (English)</th>
                  <th width="125px;">Country</th>
                  <th width="100px;">Status</th>
                  <?php if(in_array('2',$pAccess)||in_array('3',$pAccess)||in_array('4',$pAccess)){?>
                    <th width="200px;">Action</th>
                  <?php } ?>
               </tr>
              </thead> 
              <tbody>
                <?php
                if(!empty($hotelCityData)){
                  foreach($hotelCityData as $hotelCity) { ?>
                    <tr>
                      <th class="hidden"><?= $hotelCity->hotel_city_id ?></th>
                      <th class="center textCenterAlign">
                        <img id="image_id" src="<?= base_url($hotelCity->hotel_city_icon) ?>" 
                             onerror="this.src='<?=base_url("assets/images/no_image_text.png")?>';" 
                             height="50" width="50" />
                      </th>
                      <th class="center"><?= (isset($hotelCity->hotel_city_name_EN))?$hotelCity->hotel_city_name_EN:'' ?></th>
                      <th class="center">
                        <?php 
                          echo (isset($hotelCity->country))?$hotelCity->country:'';
                          echo (isset($hotelCity->country_code))?' ('.$hotelCity->country_code.')':'';
                        ?>
                        </th>
                      <th class="center" id="statusFlag_<?= $hotelCity->hotel_city_id ?>">
                        <?= ($hotelCity->status == 1)?'Active':'De-activate' ?>
                      </th>
                      <?php if(in_array('2',$pAccess)||in_array('3',$pAccess)||in_array('4',$pAccess)){?>
                        <td class="center"> 
                          <?php if(in_array('2',$pAccess)){ ?>  
                            <a class="btn btn-sm btn-primary" 
                               href="<?= base_url('HotelCity/editHotelCity/'.encode_param($hotelCity->hotel_city_id))?>">
                              <i class="fa fa-fw fa-edit"></i>Edit
                            </a> 
                          <?php } ?>

                          <?php if(in_array('3',$pAccess)){ ?>
                            <a class="btn btn-sm btn-danger" onclick="confirmDelete(jQuery(this),'HotelCity/changeStatus',{'hotel_city_id':'<?=encode_param($hotelCity->hotel_city_id)?>'})" 
                              status="2"><i class="fa fa-fw fa-trash"></i>Delete
                            </a>
                          <?php } ?>

                          <?php if(in_array('4',$pAccess)){
                            $status = 0; $btnName = 'De-activate'; $btnClass = 'btn-warning';
                            if($hotelCity->status != 1){
                              $status = 1; $btnName = 'Activate'; $btnClass = 'btn-success';
                            } ?> 
                            <a class="btn btn-sm <?= $btnClass ?>" onclick="updateStatus(jQuery(this),'HotelCity/changeStatus',{'hotel_city_id':'<?= encode_param($hotelCity->hotel_city_id) ?>'})"
                              status="<?= $status ?>" status_id="<?= $hotelCity->hotel_city_id ?>">
                              <i class="fa fa-cog"><?= $btnName ?></i>
                            </a>
                          <?php } ?>
                        </td>
                      <?php } ?>
                    </tr>
                <?php } } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>