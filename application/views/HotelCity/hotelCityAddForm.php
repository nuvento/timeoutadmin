<?php
  $language = getLanguages();
?>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
       <?= $pTitle ?>
        <small><?= $pDescription ?></small>
    </h1>
    <ol class="breadcrumb">
     <li><a href="<?= base_url() ?>"><i class="fa fa-star-o" aria-hidden="true"></i>Home</a></li>
     <li><?= $menu ?></li>
     <li class="active"><?= $smenu ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <?php 
        $url = (!isset($hotel_city_id)||empty($hotel_city_id))
                  ?'HotelCity/createHotelCity':'HotelCity/updateHotelCity/'.$hotel_city_id;

        if($this->session->flashdata('message')) { 
          $flashdata = $this->session->flashdata('message'); ?>
          <div class="alert alert-<?= $flashdata['class'] ?>">
            <button class="close" data-dismiss="alert" type="button">×</button>
            <?= $flashdata['message'] ?>
          </div>
        <?php } ?>
      </div>
      <div class="col-md-12">
        <div class="box box-warning">
          <div class="box-body">
            <form role="form" action="<?= base_url($url) ?>" method="post" 
              class="validate" data-parsley-validate="" enctype="multipart/form-data">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Hotel City Name</label>
                  <?php foreach($language AS $lang) { 
                    $hotel_city_name = (isset($hotelCityData->{'hotel_city_name_'.$lang}))?$hotelCityData->{'hotel_city_name_'.$lang}:'' ?>
                    <div class="row">
                      <div class="col-md-12">
                        <input type="text" class="form-control <?= ($lang == 'EN')?'required':'' ?>" 
                        name="hotel_city_name_<?= $lang ?>" data-parsley-trigger="change" data-parsley-minlength="2" placeholder="Hotel City Name (<?= $lang ?>)" value="<?= $hotel_city_name ?>">
                        <span class="glyphicon form-control-feedback"></span>
                      </div>
                    </div>
                    <br>
                  <?php } ?>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Hotel City Icon</label>
                  <div class="col-md-12" style="padding-bottom:10px;">
                    <div class="col-md-3">
                      <img id="image_id" src="<?= (isset($hotelCityData->hotel_city_icon))?base_url($hotelCityData->hotel_city_icon):'' ?>" onerror="this.src='<?=base_url("assets/images/no_image_text.png")?>';" height="75" width="75" />
                    </div>
                    <div class="col-md-9" style="padding-top: 25px;">
                      <input name="hotel_city_icon" 
                        class="<?= (!isset($hotelCityData->hotel_city_icon) || empty(isset($hotelCityData->hotel_city_icon)))?'required':'' ?>" type="file" accept="image/*" onchange="setImg(this,'image_id');" />
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Country</label>
                    <div class="row">
                      <div class="col-md-12">
                      <input type="text" class="form-control" name="country" 
                             data-parsley-trigger="change" data-parsley-minlength="2" 
                             placeholder="Country Name" value="<?= (isset($hotelCityData->country))?$hotelCityData->country:'' ?>">
                        <span class="glyphicon form-control-feedback"></span>
                      </div>
                    </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Country Code</label>
                    <div class="row">
                      <div class="col-md-12">
                      <input type="text" class="form-control" name="country_code" 
                             data-parsley-trigger="change" data-parsley-minlength="2" 
                             placeholder="Country Code" value="<?= (isset($hotelCityData->country_code))?$hotelCityData->country_code:'' ?>">
                        <span class="glyphicon form-control-feedback"></span>
                      </div>
                    </div>
                </div>
              </div>
              <div class="col-md-12">      
                <div class="box-footer textCenterAlign">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <a href="<?= base_url('HotelCity/listHotelCity') ?>" class="btn btn-primary">Cancel</a>
                </div>        
              </div>        
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>