<div class="content-wrapper">
  <section class="content-header">
    <h1>
       <?= $pTitle ?>
        <small><?= $pDescription ?></small>
    </h1>
    <ol class="breadcrumb">
     <li><a href="<?= base_url() ?>"><i class="fa fa-star-o" aria-hidden="true"></i>Home</a></li>
     <li><?= $menu ?></li>
     <li class="active"><?= $smenu ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <?php if($this->session->flashdata('message')) { 
          $flashdata = $this->session->flashdata('message'); ?>
          <div class="alert alert-<?= $flashdata['class'] ?>">
            <button class="close" data-dismiss="alert" type="button">×</button>
            <?= $flashdata['message'] ?>
          </div>
        <?php } ?>
      </div>
      <div class="col-md-12">
        <div class="box box-warning">
          <div class="box-body">
            <form role="form" action="<?= base_url('Notification/changeNotifData') ?>" method="post" 
              class="validate" data-parsley-validate="" enctype="multipart/form-data">
              <div class="box-header with-border">
                <h3 class="box-title padLeft10 padTop5">Email Template</h3>
              </div>
              <div class="box-body">
                <div class="col-sm-12">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Registration Mail</label>
                      <p>Email => {:email}</p>
                      <textarea id="rich_editor" type="text" class="ip_reg_form_input form-control reset-form-custom" placeholder="Registration Mail" name="registration_mail" style="height:108px;" data-parsley-trigger="change"><?= $notificationData->registration_mail ?></textarea>
                    </div>
                  </div>

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Forgot Mail</label>
                      <p>URL => {:url}</p>
                      <textarea id="rich_editor_1" type="text" class="ip_reg_form_input form-control reset-form-custom" placeholder="Forgot Mail" name="forgot_mail" style="height:108px;" data-parsley-trigger="change"><?= $notificationData->forgot_mail ?></textarea>
                    </div>
                  </div>
                </div>

                <div class="col-sm-12">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Checker Activation Mail</label>
                      <p>Username => {:user_name}</p>
                      <textarea id="rich_editor_2" type="text" class="ip_reg_form_input form-control reset-form-custom" placeholder="Checker Activation Mail" name="checker_activation_mail" style="height:108px;" data-parsley-trigger="change"><?= $notificationData->checker_activation_mail ?></textarea>
                    </div>
                  </div>

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Provider Activation Mail</label>
                      <p>Username => {:user_name} , URL => {:url}</p>
                      <textarea id="rich_editor_6" type="text" class="ip_reg_form_input form-control reset-form-custom" placeholder="Provider Activation Mail" style="height:108px;"
                      name="provider_activation_mail" data-parsley-trigger="change"><?= $notificationData->provider_activation_mail ?></textarea>
                    </div>
                  </div>
                </div>

                <div class="col-sm-12">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Registration Verification Mail</label>
                      <p>Username => {:user_name} , Reset Link => {:reset_link}</p>
                      <textarea id="rich_editor_8" type="text" class="ip_reg_form_input form-control reset-form-custom" placeholder="Checker Activation Mail" name="verify_mail" 
                      style="height:108px;" data-parsley-trigger="change"><?= $notificationData->verify_mail ?></textarea>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Provider Password Reset Mail</label>
                      <p>Username => {:user_name} , Reset Link => {:password}</p>
                      <textarea id="rich_editor_11" type="text" class="ip_reg_form_input form-control reset-form-custom" placeholder="Checker Activation Mail" name="provider_verify_mail" 
                      style="height:108px;" data-parsley-trigger="change"><?= $notificationData->provider_verify_mail ?></textarea>
                    </div>
                  </div>
                </div>

                <div class="col-sm-12">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Staff Registration Mail</label>
                      <p>Username => {:user_name} , Password => {:password}</p>
                      <textarea id="rich_editor_9" type="text" class="ip_reg_form_input form-control reset-form-custom" placeholder="Staff Registration Mail" name="staff_reg_mail" 
                      style="height:108px;" data-parsley-trigger="change"><?= $notificationData->staff_reg_mail ?></textarea>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Booking Mail</label>
                      <p>Event Name => {:event_name_en} , Booking ID => {:booking_id} , Time => {:time}</p>
                      <textarea id="rich_editor_10" type="text" class="ip_reg_form_input form-control reset-form-custom" placeholder="Booking Mail" name="booking_mail" 
                      style="height:108px;" data-parsley-trigger="change"><?= $notificationData->booking_mail ?></textarea>
                    </div>
                  </div>
                </div>
              </div>

              <div class="box-header with-border">
                <h3 class="box-title padLeft10 padTop5">SMS Template</h3>
              </div>
              <div class="box-body">
                <div class="col-sm-12">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Registration SMS</label>
                      <p>Email => {:email}</p>
                      <textarea type="text" class="ip_reg_form_input form-control reset-form-custom" placeholder="Registration SMS" name="registration_sms" style="height:108px;" data-parsley-trigger="change"><?= $notificationData->registration_sms ?></textarea>
                    </div>
                  </div>
                  
                  <!-- <div class="col-sm-6">
                    <div class="form-group">
                      <label>Forgot SMS</label>
                      <p>URL => {:url}</p>
                      <textarea type="text" class="ip_reg_form_input form-control reset-form-custom" placeholder="Forgot SMS" name="forgot_sms" style="height:108px;" data-parsley-trigger="change"><?= $notificationData->forgot_sms ?></textarea>
                    </div>
                  </div> -->
                  
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Booking SMS</label>
                      <p>Event Name => {:event_name_en} , Booking ID => {:booking_id} , Time => {:time}</p>
                      <textarea type="text" class="ip_reg_form_input form-control reset-form-custom" placeholder="Booking SMS" name="booking_sms" style="height:108px;" data-parsley-trigger="change"><?= $notificationData->booking_sms ?></textarea>
                    </div>
                  </div>
                </div>

                <div class="col-sm-12">
                  <div class="col-md-6">      
                    <div class="box-footer textCenterAlign">
                      <button type="submit" class="btn btn-primary">Submit</button>
                      <a href="<?= base_url() ?>" class="btn btn-primary">Cancel</a>
                    </div>        
                  </div> 
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>