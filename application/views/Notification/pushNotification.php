<div class="content-wrapper">
  <section class="content-header">
    <h1>
       <?= $pTitle ?>
        <small><?= $pDescription ?></small>
    </h1>
    <ol class="breadcrumb">
     <li><a href="<?= base_url() ?>"><i class="fa fa-star-o" aria-hidden="true"></i>Home</a></li>
     <li><?= $menu ?></li>
     <li class="active"><?= $smenu ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <?php if($this->session->flashdata('message')) { 
          $flashdata = $this->session->flashdata('message'); ?>
          <div class="alert alert-<?= $flashdata['class'] ?>">
            <button class="close" data-dismiss="alert" type="button">×</button>
            <?= $flashdata['message'] ?>
          </div>
        <?php } ?>
      </div>
      <div class="col-md-12">
        <div class="box box-warning">
          <div class="box-body">
            <form name="pushNotifForm" role="form" action="<?= base_url('Notification/sendPushNotif') ?>" 
              method="post" class="validate" data-parsley-validate="" enctype="multipart/form-data">
              <div class="box-body">
                <?php if(!empty($regionData)){ ?>
                  <div class="col-sm-4">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label>Event</label>
                       <select name="event_id" class="form-control required" placeholder="Select Event" required>
                          <option selected disabled>Choose an Event</option>
                          <?php 
                            foreach ($event_data as $event) {
                              echo '<option value="'.$event->event_id.'">'.$event->event_name_EN.'</option>';
                            }
                          ?>
                        </select> 
                      </div>
                    </div>

                    <div class="col-sm-12">
                      <div class="form-group">
                        <input type="text" class="form-control required" data-parsley-trigger="change"
                          data-parsley-minlength="2" name="notifMsg" required data-parsley-pattern="^[a-zA-Z0-9\ . _ @  \/]+$" placeholder="Enter Notification Message">
                      </div>
                    </div>
                  </div>
                <?php } ?>
                <?php if(!empty($regionData)){ ?>
                  <div class="col-sm-8">
                      <label>Tags</label>
                    <div type="parent" class="header-tag-box marginTop10" style="height:82px;">
                      <?php foreach($regionData AS $region){ ?>
                        <div id="tag_<?= $region->id ?>" class="header-tag cpoint" select="0" 
                             tag_id="<?= $region->id ?>" onclick="manageTags(jQuery(this))">
                          <?= $region->region_name_EN ?>
                        </div>
                      <?php } ?>
                    </div>
                    <div id="selected_tags" class="hide"></div>
                  </div>
                <?php }  ?>
              </div>
              <div class="col-md-12">      
                <div class="box-footer textCenterAlign">
                  <button id="pushNotification" type="submit" class="btn btn-primary">Push Notification</button>
                  <a href="<?= base_url() ?>" class="btn btn-primary">Cancel</a>
                </div>        
              </div> 
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>