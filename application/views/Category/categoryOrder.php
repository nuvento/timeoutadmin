<?php
  $role = roleManagement();
  $pAccess = $role['Category'];
?>

<div class="content-wrapper" >
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
       <?= $pTitle ?>
        <small><?= $pDescription ?></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?= base_url() ?>"><i class="fa fa-star-o" aria-hidden="true"></i>Home</a></li>
      <li><?= $menu ?></li>
      <li class="active"><?= $smenu ?></li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <?php if($this->session->flashdata('message')) { 
          $flashdata = $this->session->flashdata('message'); ?>
          <div class="alert alert-<?= $flashdata['class'] ?>">
             <button class="close" data-dismiss="alert" type="button">×</button>
             <?= $flashdata['message'] ?>
          </div>
        <?php } ?>
      </div>
      <div class="col-xs-12">
        <div class="box box-warning">
          <div class="box-header with-border">
            <div class="col-md-6"><h3 class="box-title">Category List</h3></div>
            <div class="col-md-6" align="right">
              <a class="btn btn-sm btn-primary" href="<?= base_url('Category/listCategory') ?>">Back</a>
           </div>
          </div>
          <div class="box-body">
            <form name="cat_order">
              <?php if(!empty($categoryData)) { ?>
                <ul class="catUl">
                  <?php foreach($categoryData AS $category) { ?>
                    <li class="catLi" id="cat_<?= $category->cat_id ?>" draggable="true" 
                        ondragover="allowDropElement(event)" ondragstart="dragElement(event)"
                        ondrop="dropMiddle(event,this,'child')" tmporder="<?= $category->cat_id ?>">
                      <div class="catEventContainer">
                        <div class="catOverlay">
                          <h5 class="catH5" class="catH5"><?= $category->category_name_EN ?></h5>
                        </div>
                        <img class="catImg" 
                             onerror="this.src='<?=base_url("assets/images/no_image.png")?>';" 
                             src="<?= base_url($category->category_image_EN) ?>">
                        <p class="text"><?= $category->category_name_EN ?></p>
                      </div>
                      <input name="category_order[]" type="hidden" value="<?= $category->cat_id ?>">
                    </li>
                  <?php } ?>
                </ul>
              <?php } ?>
              <button id="updateOrder" type="button" class="btn btn-primary">Update Order</button>
              <a href="<?= base_url('Category/listCategory') ?>" class="btn btn-primary">Back</a>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
