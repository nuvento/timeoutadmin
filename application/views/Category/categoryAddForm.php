<?php
  $language = getLanguages();
?>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
       <?= $pTitle ?>
        <small><?= $pDescription ?></small>
    </h1>
    <ol class="breadcrumb">
     <li><a href="<?= base_url() ?>"><i class="fa fa-star-o" aria-hidden="true"></i>Home</a></li>
     <li><?= $menu ?></li>
     <li class="active"><?= $smenu ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <?php 
        $required = 'required';
        $url = 'Category/createCategory';
        if(isset($category_id) && !empty($category_id)){
          $required = '';
          $url = 'Category/updateCategory/'.$category_id;
        }

        if($this->session->flashdata('message')) { 
          $flashdata = $this->session->flashdata('message'); ?>
          <div class="alert alert-<?= $flashdata['class'] ?>">
            <button class="close" data-dismiss="alert" type="button">×</button>
            <?= $flashdata['message'] ?>
          </div>
        <?php } ?>
      </div>
      <div class="col-md-12">
        <div class="box box-warning">
          <div class="box-body">
            <form role="form" action="<?= base_url($url) ?>" method="post" 
              class="validate" data-parsley-validate="" enctype="multipart/form-data">
              <div class="col-md-12 padBottom20">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Category Name (EN)</label>
                    <input type="text" class="form-control required" data-parsley-trigger="change"
                    data-parsley-minlength="2" name="category_name_EN" value="<?= (isset($categoryData->category_name_EN))?$categoryData->category_name_EN:'' ?>" placeholder="Category Name (EN)">
                    <span class="glyphicon form-control-feedback"></span>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Category Banner Image</label>
                    <div class="col-md-12" style="padding-bottom:10px;">
                      <div class="col-md-3">
                        <img id="category_banner_image" src="<?= (isset($categoryData->category_banner))?base_url($categoryData->category_banner):'' ?>" onerror="this.src='<?=base_url("assets/images/user_avatar.jpg")?>';" height="75" width="75" />
                      </div>
                      <div class="col-md-9" style="padding-top: 25px;">
                        <input class="<?= $required ?>" name="category_banner" type="file" accept="image/*" onchange="setImg(this,'category_banner_image');" />
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Category Icon (EN)</label>
                    <div class="col-md-12" style="padding-bottom:10px;">
                      <div class="col-md-3">
                        <img id="category_image_mob" src="<?= (isset($categoryData->category_image_EN))?base_url($categoryData->category_image_EN):'' ?>" onerror="this.src='<?=base_url("assets/images/user_avatar.jpg")?>';" height="75" width="75" />
                      </div>
                      <div class="col-md-9" style="padding-top: 25px;">
                        <input class="<?= $required ?>" name="category_image_EN" type="file" accept="image/*" onchange="setImg(this,'category_image_mob');" />
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="padLeft30">
                <a id="addMultiLang_cat" block="cat" show="0" class="cpoint noSubTypeMsg">
                  + Show Add More Language Option
                </a>
              </div>
              <div id="showMultiLangBlock_cat" class="col-md-12 hide marginTop23">
                <?php foreach($language AS $lang) { if($lang == 'EN') continue; ?>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Category Name (<?= $lang ?>)</label>
                    <input type="text" class="form-control" data-parsley-trigger="change"
                    data-parsley-minlength="2" name="category_name_<?= $lang ?>" 
                    value="<?= (isset($categoryData->{'category_name_'.$lang}))?$categoryData->{'category_name_'.$lang}:'' ?>" placeholder="Category Name (English)">
                    <span class="glyphicon form-control-feedback"></span>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Category Icon (<?= $lang ?>)</label>
                    <div class="col-md-12" style="padding-bottom:10px;">
                      <div class="col-md-3">
                        <img id="category_image_<?= $lang ?>" height="75" width="75" 
                             src="<?= (isset($categoryData->{'category_image_'.$lang}))?base_url($categoryData->{'category_image_'.$lang}):'' ?>" 
                             onerror="this.src='<?=base_url("assets/images/user_avatar.jpg")?>';" />
                      </div>
                      <div class="col-md-9" style="padding-top: 25px;">
                        <input name="category_image_<?= $lang ?>" type="file" accept="image/*" 
                               onchange="setImg(this,'category_image_<?= $lang ?>');" />
                      </div>
                    </div>
                  </div>
                </div>
              <?php } ?>
              </div>
              <div class="col-md-12 marginTop10">      
                <div class="box-footer textCenterAlign">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <a href="<?= base_url('Category/listCategory') ?>" class="btn btn-primary">Cancel</a>
                </div>        
              </div>        
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
