<?php $language = getLanguages(); ?>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
       <?= $pTitle ?>
        <small><?= $pDescription ?></small>
    </h1>
    <ol class="breadcrumb">
     <li><a href="<?= base_url() ?>"><i class="fa fa-star-o" aria-hidden="true"></i>Home</a></li>
     <li><?= $menu ?></li>
     <li class="active"><?= $smenu ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <?php if($this->session->flashdata('message')) { 
          $flashdata = $this->session->flashdata('message'); ?>
          <div class="alert alert-<?= $flashdata['class'] ?>">
            <button class="close" data-dismiss="alert" type="button">×</button>
            <?= $flashdata['message'] ?>
          </div>
        <?php } ?>
      </div>
      <div class="col-md-12">
        <div class="box box-warning">
          <div class="box-body">
            <form role="form" action="<?= base_url('CMS/changeCMSdata') ?>" method="post" 
              class="validate" data-parsley-validate="" enctype="multipart/form-data">

              <div class="collapsible-tab" data-toggle="collapse" data-target="#faq">FAQ</div>
              <div id="faq" class="collaps-content-tab"><br>
                <?php foreach ($language AS $lang) { ?>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>FAQ (<?= $lang ?>)</label>
                      <textarea id="rich_editor_FAQ_<?= $lang ?>" type="text" class="ip_reg_form_input form-control reset-form-custom" placeholder="FAQ <?= $lang ?>" name="faq_<?= $lang ?>" style="height:108px;" data-parsley-trigger="change" data-parsley-minlength="2"><?= (isset($cmsData->{'faq_'.$lang}))?$cmsData->{'faq_'.$lang}:'' ?></textarea>
                    </div>
                  </div>
                <?php } ?>
              </div>
              <br>
              <div class="collapsible-tab" data-toggle="collapse" data-target="#instruction">Instruction</div>
              <div id="instruction" class="collaps-content-tab"><br>
                <?php foreach ($language AS $lang) { ?>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Instruction (<?= $lang ?>)</label>
                      <textarea id="rich_editor_INS_<?= $lang ?>" type="text" class="ip_reg_form_input form-control reset-form-custom" placeholder="Instruction <?= $lang ?>" name="instruction_<?= $lang ?>" style="height:108px;" data-parsley-trigger="change" data-parsley-minlength="2"><?= (isset($cmsData->{'instruction_'.$lang}))?$cmsData->{'instruction_'.$lang}:'' ?></textarea>
                    </div>
                  </div>
                <?php } ?>
              </div>
              <br>
              <div class="collapsible-tab" data-toggle="collapse" data-target="#privacy-policy">
                Privacy Policy
              </div>
              <div id="privacy-policy" class="collaps-content-tab"><br>
                <?php foreach ($language AS $lang) { ?>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Privacy Policy (<?= $lang ?>)</label>
                      <textarea id="rich_editor_PRY_<?= $lang ?>" type="text" placeholder="Privacy Policy <?= $lang ?>" class="ip_reg_form_input form-control reset-form-custom" name="privacy_policy_<?= $lang ?>" style="height:108px;" data-parsley-minlength="2" data-parsley-trigger="change"><?= (isset($cmsData->{'privacy_policy_'.$lang}))?$cmsData->{'privacy_policy_'.$lang}:'' ?></textarea>
                    </div>
                  </div>
                <?php } ?>
              </div>
              <br>
              <div class="collapsible-tab" data-toggle="collapse" data-target="#terms_and_conditions">
                Terms and Condition
              </div>
              <div id="terms_and_conditions" class="collaps-content-tab"><br>
                <?php foreach ($language AS $lang) { ?>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Terms and Condition (<?= $lang ?>)</label>
                      <textarea id="rich_editor_TAC_<?= $lang ?>" type="text" class="ip_reg_form_input form-control reset-form-custom" placeholder="Terms and Condition <?= $lang ?>" name="terms_and_conditions_<?= $lang ?>" style="height:108px;" data-parsley-trigger="change" data-parsley-minlength="2"><?= (isset($cmsData->{'terms_and_conditions_'.$lang}))?$cmsData->{'terms_and_conditions_'.$lang}:'' ?></textarea>
                    </div>
                  </div>
                <?php } ?>
              </div>
              <br>  
              <div style="float: right;">      
                <div class="box-footer textCenterAlign">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <a href="<?= base_url() ?>" class="btn btn-primary">Cancel</a>
                </div>        
              </div> 
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>


<script type="text/javascript">
  jQuery( document ).ready(function() {
    <?php foreach($language AS $lang) { ?>
      if(jQuery('#rich_editor_FAQ_<?= $lang ?>').length==1){CKEDITOR.replace('rich_editor_FAQ_<?= $lang ?>',{language:'<?= strtolower($lang) ?>'});}
      if(jQuery('#rich_editor_INS_<?= $lang ?>').length==1){CKEDITOR.replace('rich_editor_INS_<?= $lang ?>',{language:'<?= strtolower($lang) ?>'});}
      if(jQuery('#rich_editor_PRY_<?= $lang ?>').length==1){CKEDITOR.replace('rich_editor_PRY_<?= $lang ?>',{language:'<?= strtolower($lang) ?>'});}
      if(jQuery('#rich_editor_TAC_<?= $lang ?>').length==1){CKEDITOR.replace('rich_editor_TAC_<?= $lang ?>',{language:'<?= strtolower($lang) ?>'});}
    <?php } ?>
  });
</script>