<div class="content-wrapper">
  <section class="content-header">
    <h1>
       <?= $pTitle ?>
        <small><?= $pDescription ?></small>
    </h1>
    <ol class="breadcrumb">
     <li><a href="<?= base_url() ?>"><i class="fa fa-star-o" aria-hidden="true"></i>Home</a></li>
     <li><?= $menu ?></li>
     <li class="active"><?= $smenu ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <?php 
        $url = (!isset($country_id)||empty($country_id))
                  ?'Country/createCountry':'Country/updateCountry/'.$country_id;

        if($this->session->flashdata('message')) { 
          $flashdata = $this->session->flashdata('message'); ?>
          <div class="alert alert-<?= $flashdata['class'] ?>">
            <button class="close" data-dismiss="alert" type="button">×</button>
            <?= $flashdata['message'] ?>
          </div>
        <?php } ?>
      </div>
      <div class="col-md-12">
        <div class="box box-warning">
          <div class="box-body">
            <form role="form" action="<?= base_url($url) ?>" method="post" 
              class="validate" data-parsley-validate="" enctype="multipart/form-data">
              <div class="col-md-12">
                <div class="col-md-5">
                  <div class="form-group">
                    <label>Country Name</label>
                    <input type="text" class="form-control required" data-parsley-trigger="change"
                    data-parsley-minlength="2" data-parsley-pattern="^[a-zA-Z\ \/]+$" 
                    name="country_name" required="" value="<?= (isset($countryData->country_name))?$countryData->country_name:'' ?>"placeholder="Country Name">
                    <span class="glyphicon form-control-feedback"></span>
                  </div>
                  <div class="form-group">
                    <label>Country Code</label>
                    <input type="text" class="form-control required" data-parsley-trigger="change"
                    data-parsley-minlength="2" data-parsley-pattern="^[a-zA-Z]+$" 
                    name="country_code" required="" value="<?= (isset($countryData->country_code))?$countryData->country_code:'' ?>"placeholder="Country Name">
                    <span class="glyphicon form-control-feedback"></span>
                  </div>
                </div>
                <div class="col-md-5">
                  <div class="form-group">
                    <label>Country Flag</label>
                    <div class="col-md-12" style="padding-bottom:10px;padding-top:10px;">
                      <div class="col-md-3">
                        <img id="country_flag" src="<?= (isset($countryData->country_flag))?base_url($countryData->country_flag):'' ?>" onerror="this.src='<?=base_url("assets/images/flag_default.png")?>';" height="50" width="65" />
                      </div>
                      <div class="col-md-9" style="padding-top: 13px;">
                        <input class="<?= !empty($country_id)?'':'required' ?>" name="country_flag" type="file" accept="image/*" onchange="setImg(this,'country_flag');" />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-12">
                <div class="col-md-5">
                  <div class="form-group">
                    <label>Language</label>
                    <input type="text" class="form-control required" data-parsley-trigger="change"
                    data-parsley-minlength="2" name="language" required="" value="<?= (isset($countryData->language))?$countryData->language:'' ?>"placeholder="Country Name">
                    <span class="glyphicon form-control-feedback"></span>
                  </div>
                </div>
                <div class="col-md-5">
                  <div class="form-group">
                    <label>Language Code</label>
                    <input type="text" class="form-control required" data-parsley-trigger="change"
                    data-parsley-minlength="2" data-parsley-pattern="^[a-zA-Z]+$" 
                    name="language_code" required="" value="<?= (isset($countryData->language_code))?$countryData->language_code:'' ?>"placeholder="Language Code">
                    <span class="glyphicon form-control-feedback"></span>
                  </div>
                </div>
              </div>
              <div class="col-md-12">
                <div class="col-md-5">
                  <div class="form-group">
                    <label>Currency</label>
                    <input type="text" class="form-control required" data-parsley-trigger="change"
                    data-parsley-minlength="2" data-parsley-pattern="^[a-zA-Z]+$" 
                    name="currency" required="" value="<?= (isset($countryData->currency))?$countryData->currency:'' ?>"placeholder="Country Name">
                    <span class="glyphicon form-control-feedback"></span>
                  </div>
                </div>
                <div class="col-md-5">
                  <div class="form-group">
                    <label>Currency Symbol</label>
                    <input type="text" class="form-control required" data-parsley-trigger="change"
                    name="currency_symbol" required placeholder="Country Symbol" value="<?= (isset($countryData->currency_symbol))?$countryData->currency_symbol:'' ?>">
                    <span class="glyphicon form-control-feedback"></span>
                  </div>
                </div>
              </div>
              <div class="col-md-12">      
                <div class="box-footer textCenterAlign">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <a href="<?= base_url('Country/listCountry') ?>" class="btn btn-primary">Cancel</a>
                </div>        
              </div>        
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
