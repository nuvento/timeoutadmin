<?php
  $role = roleManagement();
  $pAccess = $role['City'];
?>

<div class="content-wrapper" >
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
       <?= $pTitle ?>
        <small><?= $pDescription ?></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?= base_url() ?>"><i class="fa fa-star-o" aria-hidden="true"></i>Home</a></li>
      <li><?= $menu ?></li>
      <li class="active"><?= $smenu ?></li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <?php if($this->session->flashdata('message')) { 
          $flashdata = $this->session->flashdata('message'); ?>
          <div class="alert alert-<?= $flashdata['class'] ?>">
             <button class="close" data-dismiss="alert" type="button">×</button>
             <?= $flashdata['message'] ?>
          </div>
        <?php } ?>
      </div>
      <div class="col-xs-12">
        <div class="box box-warning">
          <div class="box-header with-border">
            <div class="col-md-6"><h3 class="box-title">Country List</h3></div>
            <div class="col-md-6" align="right">
              <?php if(in_array('1',$pAccess)){ ?>
                <a class="btn btn-sm btn-primary" href="<?= base_url('Country/addCountry') ?>">
                  Add New Country
                </a>
              <?php } ?>
              <a class="btn btn-sm btn-primary" href="<?= base_url() ?>">Back</a>
           </div>
          </div>
          <div class="box-body">
            <table id="mechanicUsers" class="table table-bordered table-striped datatable ">
              <thead>
                <tr>
                  <th class="hidden">ID</th>
                  <th width="40px;">Country</th>
                  <th width="40px;">Language</th>
                  <th width="40px;">Currency</th>
                  <th width="70px;">Currency Rate</th>
                  <?php if(in_array('2',$pAccess)||in_array('3',$pAccess)||in_array('4',$pAccess)){?>
                    <th width="100px;">Action</th>
                  <?php } ?>
               </tr>
              </thead> 
              <tbody>
                <?php
                if(!empty($countryData)){
                  foreach($countryData as $country) { ?>
                    <tr>
                      <th class="hidden"><?= $country->country_id ?></th>
                      <th class="center"><?= $country->country_name.' ('.$country->country_code.')'?></th>
                      <th class="center">
                        <?= $country->language ?>
                        <?= (!empty($country->language_code))?' ('.$country->language_code.')':'' ?>
                      </th>
                      <th class="center"><?= $country->currency ?></th>
                      <th class="center"><?= $country->conversion_rate ?></th>
                      <?php if(in_array('2',$pAccess)||in_array('3',$pAccess)||in_array('4',$pAccess)){?>
                        <td class="center"> 
                          <?php if(in_array('2',$pAccess)){ ?>  
                            <a class="btn btn-sm btn-primary" 
                               href="<?= base_url('Country/editCountry/'.encode_param($country->country_id))?>">
                              <i class="fa fa-fw fa-edit"></i>Edit
                            </a> 
                          <?php } ?>

                          <?php if(in_array('3',$pAccess)){ ?>
                            <a class="btn btn-sm btn-danger" onclick="confirmDelete(jQuery(this),'Country/changeStatus',{'country_id':'<?=encode_param($country->country_id)?>'})" 
                              status="2"><i class="fa fa-fw fa-trash"></i>Delete
                            </a>
                          <?php } ?>
                        </td>
                      <?php } ?>
                    </tr>
                <?php } } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>