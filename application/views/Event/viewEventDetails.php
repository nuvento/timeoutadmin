<div class="box-body">
  <div class="col-sm-6">
    <div class="box-header with-border padHead">
      <h3 class="box-title"><strong>Event Details</strong></h3>
    </div>
    <div class="box-header">
      <div class="row">
        <div class="col-sm-5">Event Name</div>
        <div class="col-sm-1"><span>:</span></div>
        <div class="col-sm-6">
          <strong><?= isset($event->event_name_EN)?$event->event_name_EN:'' ?></strong>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-5">Booking Limit</div>
        <div class="col-sm-1"><span>:</span></div>
        <div class="col-sm-6">
          <p><strong><?= $event->max_booking ?></strong> Bookings / Transaction </p>
        </div>
      </div>
      <?php 
        if($event->show_layout != 1 && !empty($event->seat_pricing) && 
           !empty($seat_pricing = json_decode($event->seat_pricing,true))){ ?>
        <div class="row">
          <div class="col-sm-5">Ticket Price</div>
          <div class="col-sm-1"><span>:</span></div>
          <div class="col-sm-6"><strong><?= $seat_pricing['price'] ?></strong></div>
        </div>
        <div class="row">
          <div class="col-sm-5">About Ticket</div>
          <div class="col-sm-1"><span>:</span></div>
          <div class="col-sm-6"><p class="truncateText"><?= $seat_pricing['price_details_EN'] ?></p></div>
        </div>
      <?php } ?>
    </div>
  </div>
  <div class="col-sm-6">
    <div class="box-header with-border padHead">
      <h3 class="box-title"><strong>Venue Details</strong></h3>
    </div>
    <div class="box-header">
      <div class="row">
        <div class="col-sm-5">Venue Name</div>
        <div class="col-sm-1"><span>:</span></div>
        <div class="col-sm-6">
          <strong><?= isset($event->venue_name_EN)?$event->venue_name_EN:'' ?></strong>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-5">Venue Region</div>
        <div class="col-sm-1"><span>:</span></div>
        <div class="col-sm-6">
          <strong><?= isset($event->region_name_EN)?$event->region_name_EN:'' ?></strong>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-5">Venue Location</div>
        <div class="col-sm-1"><span>:</span></div>
        <div class="col-sm-6"><?= $event->location ?></div>
      </div>
    </div>
  </div>

  <?php
    $start_date = '';
    if(isset($event->eventSchedule) && isset($event->eventSchedule->date) && 
       isset($event->eventSchedule->date[0])){
      $start_date = strtotime($event->eventSchedule->date[0]);
      $start_date = date('m/d/Y',$start_date);
    }

    $end_date = '';
    $lstIndex = count($event->eventSchedule->date)-1;
    if(isset($event->eventSchedule->date) && 
       isset($event->eventSchedule->date[$lstIndex])){
      $end_date = strtotime($event->eventSchedule->date[$lstIndex]);
      $end_date = date('m/d/Y',$end_date);
    }
  ?>

  <div class="col-sm-6">
    <div class="box-header with-border padHead">
      <h3 class="box-title padLeft10"><strong>Event Schedule</strong></h3>
    </div>

    <div class="box-header">
      <div class="row">
        <div class="col-sm-5">Scheduled For</div>
        <div class="col-sm-1"><span>:</span></div>
        <div class="col-sm-6">
          <strong><?= $start_date ?></strong> <?= ($end_date!='')?'<strong> - '.$end_date.'</strong>':'' ?>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-5">Show Timing</div>
        <div class="col-sm-1"><span>:</span></div>
        <div class="col-sm-6">
          <?php foreach($event->eventSchedule->time AS $time){ ?>
            <div class="header-tag"><?= $time ?></div>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>

  <div class="col-sm-6">
    <div class="box-header with-border padHead marginBottom-10">
      <h3 class="box-title"><strong>Tag Details</strong></h3>
    </div>

    <?php foreach($tag_data AS $tag){ 
      if(in_array($tag->tag_id, $event->eventTags)){ ?>
        <div class="header-tag">
          <?= isset($tag->tag_EN)?$tag->tag_EN:'' ?>
        </div>
    <?php } } ?>
  </div>

  <?php if($event->show_layout == 1){ ?>
    <div class="col-sm-12">
      <div class="box-header with-border padHead marginBottom-10">
        <h3 class="box-title"><strong>Layout Details</strong></h3>
      </div>

      <div class="col-sm-4 dropZoneContainer viewLayout">
        <img class="dropZoneOverlay" id="image_id" src="<?= base_url($event->layout) ?>"
             onerror="this.src='<?=base_url("assets/images/no_image_text.png")?>';" 
             height="75" width="75" />
      </div>

      <div class="col-sm-8 padTop30">
        <div class="col-sm-12 marginTop-8">
          <div class="col-sm-3">
            <strong>Seat Division</strong>
          </div>
          <div class="col-sm-3">
            <strong>Seat Pricing</strong>
          </div>
          <div class="col-sm-4">
            <strong>Seating Capacity</strong>
          </div>
        </div>
        <div class="box-header with-border padHead marginBottom-10" style="width:410px;"></div>
        <?php 
          if($event->show_layout == 1){ 
            if(!empty($event->custom_seat_layout) && 
               !empty($custlayoutDtls = json_decode($event->custom_seat_layout,true))){
               foreach($custlayoutDtls AS $lyDtls){ ?>
                <div class="col-sm-12 marginTop-8">
                  <div class="col-sm-3">
                    <strong><?= $lyDtls['color'] ?></strong> block 
                  </div>
                  <div class="col-sm-3">
                    <strong><?= $lyDtls['price'] ?></strong> / Seat
                  </div>
                  <div class="col-sm-4">
                    <strong><?= $lyDtls['capacity'] ?></strong> Seats / Division
                  </div>
                </div>
              <?php } 
              } else { 
              $layoutDtls = json_decode($event->layout_details,true);
              foreach($layoutDtls AS $lyDtls){ ?>
                <div class="col-sm-12 marginTop-8">
                  <div class="col-sm-3">
                    <strong><?= $lyDtls['color'] ?></strong> block 
                  </div>
                  <div class="col-sm-3">
                    <strong><?= $lyDtls['price'] ?></strong> / Seat
                  </div>
                  <div class="col-sm-4">
                    <strong><?= $lyDtls['capacity'] ?></strong> Seats / Division
                  </div>
                </div>
              <?php } 
            }
          } ?>
      </div>
    </div>
  <?php } ?>
</div>
