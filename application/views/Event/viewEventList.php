<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
       <?= $pTitle ?>
        <small><?= $pDescription ?></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?= base_url() ?>"><i class="fa fa-star-o" aria-hidden="true"></i>Home</a></li>
      <li><?= $menu ?></li>
      <li class="active"><?= $smenu ?></li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <?php if($this->session->flashdata('message')) { 
          $flashdata = $this->session->flashdata('message'); ?>
          <div class="alert alert-<?= $flashdata['class'] ?>">
             <button class="close" data-dismiss="alert" type="button">×</button>
             <?= $flashdata['message'] ?>
          </div>
        <?php } ?>
      </div>
      <div class="col-xs-12">
        <div class="box box-warning">
          <div class="box-header with-border">
            <div class="col-md-6"><h3 class="box-title">Event List</h3></div>
            <div class="col-md-6" align="right">
              <a class="btn btn-sm btn-primary" href="<?= base_url('Event/addEvent') ?>">
                Add New Event
              </a>
              <a class="btn btn-sm btn-primary" href="<?= base_url() ?>">Back</a>
           </div>
          </div>
          <div class="box-body">
            <table id="mechanicUsers" class="table table-bordered table-striped datatable ">
              <thead>
                <tr>
                  <th class="hidden">ID</th>
                  <th>Event Name</th>
                  <th>Category</th>
                  <th>Region</th>
                  <th>Venue</th>
                  <th>Status</th>
                  <th width="330px">Action</th>
               </tr>
              </thead> 
              <tbody>
                <?php
                if(!empty($event_data)){
                  foreach($event_data as $event) { ?>
                    <tr>
                      <td class="hidden"><?= $event->event_id ?></td>
                      <td><?= isset($event->event_name_EN)?$event->event_name_EN:'' ?></td>
                      <td><?= isset($event->category_name_EN)?$event->category_name_EN:'' ?></td>
                      <td><?= isset($event->region_name_EN)?$event->region_name_EN:'' ?></td>
                      <td><?= isset($event->venue_name_EN)?$event->venue_name_EN:'' ?></td>
                      <?php 
                        switch($event->event_status){
                          case 1 : 
                           echo '<td class="center" id="statusFlag_'.$event->event_id.'">
                                  Active
                                 </td>'; break;
                          case 0 : 
                           echo '<td class="center" id="statusFlag_'.$event->event_id.'">
                                  De-activate
                                 </td>'; break; 
                          case 3 : 
                           echo '<td class="center" id="statusFlag_'.$event->event_id.'">
                                  Waiting For Approval
                                 </td>'; break;
                        } 
                      ?>
                      <td>   
                        <button class="btn btn-sm btn-info" id="viewEventDetails" 
                                event_id="<?= encode_param($event->event_id) ?>">
                                <i class="fa fa-fw fa-eye"></i>View
                        </button> 
                        <a class="btn btn-sm btn-primary" 
                           href="<?= base_url('Event/editEvents/'.encode_param($event->event_id))?>">
                          <i class="fa fa-fw fa-edit"></i>Edit
                        </a> 
                        <a class="btn btn-sm btn-danger" onclick="confirmDelete(jQuery(this),'Event/changeStatus',{'event_id':'<?=encode_param($event->event_id)?>'})" 
                          status="2"><i class="fa fa-fw fa-trash"></i>Delete
                        </a>  
                        <?php 
                          switch($event->event_status){
                            case 1 :
                              $status = 0; $btnName = 'De-activate'; $btnClass = 'btn-warning';
                              break;
                            case 0 :
                              $status = 1; $btnName = 'Activate'; $btnClass = 'btn-success';
                              break; 
                            case 3 :
                              $status = 1; $btnName = 'Approve'; $btnClass = 'btn-info';
                              break; 
                          }
                        if($this->session->userdata['user_type'] == 1 || 
                          ($this->session->userdata['user_type'] == 2 && $event->event_status != 3)){ ?>
                          <a class="btn btn-sm <?= $btnClass ?>" onclick="updateStatus(jQuery(this),'Event/changeStatus',{'event_id':'<?= encode_param($event->event_id) ?>'})" status="<?= $status ?>" status_id="<?= $event->event_id ?>">
                            <i class="fa fa-cog"><?= $btnName ?></i>
                          </a>
                        <?php } ?>
                      </td>
                    </tr>
                <?php } } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>