<?php
  $layoutDtls = '';
  $language = getLanguages();
?>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
       <?= $pTitle ?>
        <small><?= $pDescription ?></small>
    </h1>
    <ol class="breadcrumb">
     <li><a href="<?= base_url() ?>"><i class="fa fa-star-o" aria-hidden="true"></i>Home</a></li>
     <li><?= $menu ?></li>
     <li class="active"><?= $smenu ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <?php if($this->session->flashdata('message')) { 
          $flashdata = $this->session->flashdata('message'); ?>
          <div class="alert alert-<?= $flashdata['class'] ?>">
            <button class="close" data-dismiss="alert" type="button">×</button>
            <?= $flashdata['message'] ?>
          </div>
        <?php } 
        ?>
      </div>

      <div class="col-md-12">
        <div class="box">
          <form role="form" name="eventAddForm" class="validate" data-parsley-validate="" method="post" 
                enctype="multipart/form-data" action="<?= base_url('Event/updateEvent/'.encode_param($event_data->event_id))?>"  >
            <input type="hidden" name="venue_id" value="<?= $event_data->venue_id ?>">
            <div class="box-header with-border">
              <h3 class="box-title padLeft10 padTop5">Venue Details</h3>
            </div>
            <div class="box-body">
              <div class="row">
                <div class="col-sm-6">
                  <div class="col-sm-4">Venue Name</div>
                  <div class="col-sm-1"><span>:</span></div>
                  <div class="col-sm-7">
                    <strong>
                      <?= isset($event_data->venue_name_EN)?$event_data->venue_name_EN:'' ?>
                    </strong>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="col-sm-4">Venue Region</div>
                  <div class="col-sm-1"><span>:</span></div>
                  <div class="col-sm-7">
                    <strong>
                      <?= isset($event_data->region_name_EN)?$event_data->region_name_EN:'' ?>
                    </strong>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <div class="col-sm-4">Venue Location</div>
                  <div class="col-sm-1"><span>:</span></div>
                  <div class="col-sm-7">
                    <strong><p class="truncateText"><?= $event_data->location ?></p></strong>
                  </div>
                </div>
              </div>
            </div>

            <?php if($event_data->show_layout == 1){ ?>

              <div class="box-header with-border padTop0">
                <h3 class="box-title padLeft10 ">Layout Details</h3>
              </div>

              <div class="box-body">
                <div class="col-md-12">
                  <div class="col-sm-3 dropZoneContainer viewLayout">
                    <img class="dropZoneOverlay" id="image_id" src="<?= base_url($event_data->layout) ?>"
                         onerror="this.src='<?=base_url("assets/images/no_image_text.png")?>';" 
                         height="75" width="75" />
                  </div>
                  <div class="col-md-9 padLeft40">
                    <div class="marginTop23" id="mapTypeCntr">
                      <input type="radio" name="fare_type" value="0" <?= (empty($event_data->custom_seat_layout))?'checked':'' ?>>
                        <label class="padAll-10">Use Default Fare</label>
                      <input class="marginLeft15" type="radio" name="fare_type" value="1"
                             <?= (empty($event_data->custom_seat_layout))?'':'checked' ?>>
                        <label class="padAll-10">Create Custome Fare</label>
                    </div>

                    <div class="col-sm-12 marginTop-8">
                      <div class="col-sm-3">
                        <strong>Seat Division</strong>
                      </div>
                      <div class="col-sm-3">
                        <strong>Seat Pricing</strong>
                      </div>
                      <div class="col-sm-3">
                        <strong>Weekend</strong>
                        <small>Optional</small>
                      </div>
                      <div class="col-sm-3">
                        <strong>Seating Capacity</strong>
                      </div>
                    </div>
                    <div class="box-header with-border padHead marginBottom-10" 
                         style="width:480px;"></div>

                    <?php $layoutDtls = json_decode($event_data->layout_details,true); ?>
                    <div class="col-sm-12 <?= (empty($event_data->custom_seat_layout))?'':'hide' ?>" 
                         id="defaultFareSystem">
                      <?php foreach($layoutDtls AS $lyDtls){ ?>
                        <div class="padBottom30">
                          <div class="col-sm-3">
                            <strong><?= $lyDtls['color'] ?></strong> block 
                          </div>
                          <div class="col-sm-3">
                            <strong><?= $lyDtls['price'] ?></strong> / Seat
                          </div>
                          <div class="col-sm-3">
                            <strong><?= $lyDtls['price'] ?></strong> / Seat
                          </div>
                          <div class="col-sm-3">
                            <strong><?= $lyDtls['capacity'] ?></strong> Seats / Division
                          </div>
                        </div>
                      <?php } ?>
                    </div>
                    <div class="col-sm-12 <?= (empty($event_data->custom_seat_layout))?'hide':'' ?>" 
                         id="customFareSystem">
                      <?php foreach($layoutDtls AS $lyDtls){ ?>
                        <input type="hidden" name="seat_color[]" value="<?= $lyDtls['color'] ?>">
                        <div class="col-sm-3 padTop15">
                          <strong><?= $lyDtls['color'] ?></strong> block 
                        </div>

                        <?php if(empty($event_data->custom_seat_layout)){ ?> 
                          <div class="col-sm-3" style="padding-top:3px;"> 
                            <input id="custFareInput_<?= $lyDtls['color'] ?>" disabled type="text"  
                            class="form-control marginTop-8" data-parsley-trigger="change" 
                            data-parsley-minlength="1" name="seat_price[]" data-parsley-pattern="^[0-9\ . \/]+$" placeholder="Seat Price" style="height:25px;width: 100px;"> 
                          </div>
                          <div class="col-sm-3" style="padding-top:3px;"> 
                            <input type="text" class="form-control marginTop-8" data-parsley-trigger="change" data-parsley-minlength="1" data-parsley-pattern="^[0-9\ . \/]+$" name="weekend_price[]" placeholder="Custom Price" style="height:25px;width: 100px;"> 
                          </div>
                          <div class="col-sm-3" style="padding-top:3px;">
                            <input id="custFareInput_<?= $lyDtls['color'] ?>" disabled type="text" 
                            class="form-control marginTop-8"  name="seat_capacity[]" placeholder="Capacity" 
                            data-parsley-trigger="change" data-parsley-minlength="1" 
                            data-parsley-pattern="^[0-9\ . \/]+$" style="height:25px;width: 100px;"> 
                          </div>
                        <?php } else { 
                          $custlayoutDtls = json_decode($event_data->custom_seat_layout,true);
                          foreach ($custlayoutDtls AS $cLayout) {
                            if($cLayout['color'] != $lyDtls['color']){
                              continue;
                            } ?>
                            <div class="col-sm-3" style="padding-top:3px;"> 
                              <input id="custFareInput_<?= $lyDtls['color'] ?>" name="seat_price[]" 
                                class="form-control marginTop-8 required" data-parsley-trigger="change" data-parsley-minlength="1" data-parsley-pattern="^[0-9\ . \/]+$" placeholder="Seat Price" value="<?= $cLayout['price'] ?>" type="text" 
                                style="height:25px;width: 100px;"> 
                            </div>
                            <div class="col-sm-3" style="padding-top:3px;"> 
                              <input name="weekend_price[]" class="form-control marginTop-8" data-parsley-trigger="change" data-parsley-minlength="1" data-parsley-pattern="^[0-9\ . \/]+$" placeholder="Custom Price" value="<?= $cLayout['weekend_price'] ?>" type="text" style="height:25px;width: 100px;"> 
                            </div>
                            <div class="col-sm-3" style="padding-top:3px;">
                              <input id="custFareInput_<?= $lyDtls['color'] ?>" type="text" 
                                class="form-control marginTop-8 required" name="seat_capacity[]" 
                                data-parsley-trigger="change" data-parsley-minlength="1" data-parsley-pattern="^[0-9\ . \/]+$" placeholder="Capacity" 
                                style="height:25px;width: 100px;"  value="<?= $cLayout['capacity'] ?>" > 
                            </div>

                        <?php } } ?>
                      <?php } ?>
                    </div>
                  </div>
                </div>
              </div>
            <?php } ?>

            <div class="box-header with-border">
              <h3 class="box-title padLeft10 ">Event Details</h3>
            </div>

              <div class="box-body">
                <div class="col-sm-12">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Event Name (EN)</label>
                      <input type="text" class="form-control required" data-parsley-trigger="change" required
                      data-parsley-minlength="2" name="event_name_EN" placeholder="Enter Event Name (English)"
                      value="<?= $event_data->event_name_EN ?>">
                    </div>
                    <div class="form-group">
                      <a id="addMultiLang_name" block="name" show="0" class="cpoint noSubTypeMsg">
                        + Show Add More Language Option
                      </a>
                    </div>
                    <div id="showMultiLangBlock_name" class="hide marginTop10  ">
                      <?php foreach($language AS $lang) { if($lang == 'EN') continue; ?>
                        <div class="form-group">
                          <label>Event Name (<?= $lang ?>)</label>
                          <input type="text" class="form-control" name="event_name_<?= $lang ?>" 
                                 data-parsley-trigger="change" data-parsley-minlength="2" 
                                 placeholder="Enter Event Name (<?= $lang ?>)" 
                                 value="<?= isset($event_data->{'event_name_'.$lang})?$event_data->{'event_name_'.$lang}:'' ?>">
                        </div>
                      <?php } ?>
                    </div>
                    
                    <?php if(!empty($category_data)){ ?>
                      <div class="form-group">
                        <label>Category</label>
                        <select name="category_id" class="form-control required" 
                                placeholder="Select Event Category" required>
                          <option selected disabled>Choose Event Category</option>
                          <?php 
                            foreach ($category_data as $category) {
                             $selected = ($category->cat_id == $event_data->cat_id)?'selected':'';
                              echo '<option '.$selected.' value="'.$category->cat_id.'">'.
                                      $category->category_name_EN.
                                   '</option>';
                            }
                          ?>
                        </select> 
                      </div>
                    <?php } ?> 
                    <div class="form-group">
                      <label>Maximum Seat Booking</label>
                      <input type="text" class="form-control" data-parsley-trigger="change"
                      data-parsley-minlength="2" name="max_booking" placeholder="Maximum Seat Can Book Per Transaction (Default 14)" value="<?= $event_data->max_booking ?>">
                    </div>
                    <div class="row">
                      <div class="col-md-5">
                        <div class="form-group">
                          <label>Payment Mode</label><br>
                          <input type="checkbox" <?= ($event_data->has_payment == 1)?'checked':'' ?>
                                 name="has_payment" value="1" >
                          <p style="display:-webkit-inline-box;padding-left: 15px;">Enable Payment</p>
                        </div>
                      </div>
                      <div class="col-md-7">
                        <div class="form-group">
                          <label>Booking Approval</label><br>
                          <input type="checkbox" <?= ($event_data->approve_booking==1)?'checked':'' ?> 
                                 name="approve_booking" value="1" >
                          <p style="display:-webkit-inline-box;padding-left: 15px;">Enable Booking Approval</p>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label>Event Images</label>
                      <div id="multipleImageInputCntr">
                        <?php 
                          $count = 1;
                          if(isset($event_data->eventMedia[0]) && !empty($event_data->eventMedia[0])){
                            foreach($event_data->eventMedia[0] AS $photos){ ?>
                              <div class="dropZoneContainer" id="multiImageCntr_<?= $count ?>">
                                <input type="hidden" name="existingImages[]" value="<?= $photos->id ?>">
                                <div id="multiImageClose_<?= $count ?>" class="close_custom cpoint" 
                                     onclick="removeImage('<?= $count ?>');">&times;</div>
                                <input disabled type="file" name="event_image[]" class="multiFileUpload" 
                                       accept="image/*" onchange="setMultiImg(this,jQuery(this));" 
                                       count="<?= $count ?>" />
                                <img class="multiDropZoneOverlay" id="multiImageImg_<?= $count ?>" 
                                     src="<?= base_url($photos->media_url) ?>" onerror="this.src='<?=base_url("assets/images/add-image.png")?>';" />
                              </div>
                        <?php 
                              $count += 1; 
                            } 
                          } 
                          if(isset($event_data->eventMedia[1]) && !empty($event_data->eventMedia[1])){
                            foreach($event_data->eventMedia[1] AS $photos){ ?>
                              <div class="dropZoneContainer" id="multiImageCntr_<?= $count ?>">
                                <input type="hidden" name="existingImages[]" value="<?= $photos->id ?>">
                                <div id="multiImageClose_<?= $count ?>" class="close_custom cpoint" 
                                     onclick="removeImage('<?= $count ?>');">&times;</div>
                                <input disabled type="file" name="event_image[]" class="multiFileUpload" 
                                       accept="image/*" onchange="setMultiImg(this,jQuery(this));" 
                                       count="<?= $count ?>" />
                                <img class="multiDropZoneOverlay" id="multiImageImg_<?= $count ?>" 
                                     src="<?= base_url($photos->media_url) ?>" onerror="this.src='<?=base_url("assets/images/add-image.png")?>';" />
                              </div>
                        <?php 
                              $count += 1; 
                            } 
                          }
                        ?>
                        <div class="dropZoneContainer" id="multiImageCntr_<?= $count ?>">
                          <div id="multiImageClose_<?= $count ?>" class="close_custom cpoint hide" 
                               onclick="removeImage('<?= $count ?>');">&times;</div>
                          <input type="file" name="event_image[]" class="multiFileUpload" accept="image/*" 
                                 onchange="setMultiImg(this,jQuery(this));" count="<?= $count ?>" />
                          <img class="multiDropZoneOverlay" id="multiImageImg_<?= $count ?>" 
                               src="<?=base_url("assets/images/add-image.png")?>" />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Event Description (EN)</label>
                      <textarea id="rich_editor_EN" type="text" data-parsley-trigger="change" 
                      class="ip_reg_form_input form-control reset-form-custom" name="event_description_EN"  data-parsley-minlength="2"><?= $event_data->event_description_EN ?></textarea>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <a id="addMultiLang_desc" block="desc" show="0" class="cpoint noSubTypeMsg">
                        + Show Add More Language Option
                      </a>
                    </div>
                  </div>
                  <div id="showMultiLangBlock_desc" class="hide marginTop10">
                    <?php foreach($language AS $lang) { if($lang == 'EN') continue; ?>
                      <div class="col-sm-6"></div>
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label>Event Description (<?= $lang ?>)</label>
                          <textarea id="rich_editor_<?= $lang ?>" type="text" data-parsley-trigger="change" 
                          class="ip_reg_form_input form-control reset-form-custom" 
                          name="event_description_<?= $lang ?>" data-parsley-minlength="2"><?= isset($event_data->{'event_description_'.$lang})?$event_data->{'event_description_'.$lang}:'' ?></textarea>
                        </div>
                      </div>
                    <?php } ?>
                  </div>
                </div>
              </div>

              <?php if($event_data->show_layout != 1 && !empty($event_data->seat_pricing) && 
                     !empty($seat_pricing = json_decode($event_data->seat_pricing,true))){ ?>
                <div class="box-header with-border">
                  <h3 class="box-title padLeft10 ">Pricing Details</h3>
                </div>

                <div class="box-body">
                  <div class="col-sm-12">
                    <div class="col-sm-4">
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label>Price</label>
                          <input type="text" class="form-control required" placeholder="Price" 
                                 data-parsley-trigger="change" data-parsley-minlength="1" data-parsley-pattern="^[0-9\ . \/]+$" name="price" value="<?= isset($seat_pricing['price'])?$seat_pricing['price']:'' ?>">
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label>Capacity</label>
                          <input type="text" class="form-control required" placeholder="Provide Capacity" 
                                 data-parsley-trigger="change" data-parsley-minlength="1" 
                                 name="capacity" data-parsley-pattern="^[0-9\ . \/]+$" value="<?= isset($seat_pricing['capacity'])?$seat_pricing['capacity']:'' ?>">
                        </div>
                      </div>  
                    </div>  
                    <div class="col-sm-8">
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label>About Price Division (EN)</label>
                          <textarea type="text" name="price_details_EN" required data-parsley-trigger="change" 
                          class="ip_reg_form_input form-control reset-form-custom required custom_price" placeholder="Price Division (EN)" data-parsley-minlength="2"><?= isset($seat_pricing['price_details_EN'])?$seat_pricing['price_details_EN']:'' ?></textarea>
                        </div>
                      </div>
                      <div class="col-sm-6" style="padding-top: 41px;padding-bottom: 18px;">
                        <div class="form-group">
                          <a id="addMultiLang_price" block="price" show="0" class="cpoint noSubTypeMsg">
                            + Show Add More Language Option
                          </a>
                        </div>
                      </div>
                      <div id="showMultiLangBlock_price" class="hide">
                        <?php foreach($language AS $lang) { if($lang == 'EN') continue; ?>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label>About Price Division (<?= $lang ?>)</label>
                              <textarea type="text" name="price_details_<?= $lang ?>" data-parsley-minlength="2"
                                class="ip_reg_form_input form-control reset-form-custom custom_price" 
                                placeholder="Price Division (<?= $lang ?>)" data-parsley-trigger="change"><?= isset($seat_pricing['price_details_'.$lang])?$seat_pricing['price_details_'.$lang]:'' ?></textarea>
                            </div>
                          </div>
                        <?php } ?>
                      </div>
                    </div>
                  </div>
                </div>
              <?php } ?>

              <div class="box-header with-border">
                <h3 class="box-title padLeft10 ">Scheduler and Tags</h3>
              </div>

              <div class="box-body">
                <div class="col-sm-12">
                  <div class="col-sm-6">
                    <?php
                    $sType = (isset($event_data->eventSchedule) && !empty($event_data->eventSchedule->date))?count($event_data->eventSchedule->date):1; ?>
                    <div id="scheduleTypeCntr">
                      <input type="radio" name="schedule_type" value="0" <?= ($sType==1)?'checked':'' ?>>
                        <label class="padAll-10">For Single Show</label>
                      <input class="marginLeft15" type="radio" name="schedule_type" value="1" <?= ($sType==1)?'':'checked' ?>>
                        <label class="padAll-10">For Multiple Show</label>
                    </div>

                    <div class="col-sm-6">
                      <?php
                        $start_date = '';
                        if(isset($event_data->eventSchedule->date) && 
                           isset($event_data->eventSchedule->date[0])){
                          $start_date = strtotime($event_data->eventSchedule->date[0]);
                          $start_date = date('m/d/Y',$start_date);
                        }

                        $end_date = '';
                        $lstIndex = count($event_data->eventSchedule->date)-1;
                        if(isset($event_data->eventSchedule->date) && 
                           isset($event_data->eventSchedule->date[$lstIndex])){
                          $end_date = strtotime($event_data->eventSchedule->date[$lstIndex]);
                          $end_date = date('m/d/Y',$end_date);
                        }
                      ?>
                        <div class="input-group date" data-provide="datepicker">
                          <input id="date" type="text" class="form-control required" required 
                            data-parsley-trigger="change" data-parsley-minlength="5"
                            name="event_start_date" value="<?= $start_date ?>" 
                            placeholder="Event Start Date" autocomplete="off">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                        </div>

                        <div class="input-group date padTop10 <?= ($sType==1)?'hide':'' ?>" data-provide="datepicker" 
                             id="eventEndDate">
                          <input id="date" type="text" class="form-control" <?= ($sType != 1)?'required':'disabled' ?>  
                            data-parsley-trigger="change" data-parsley-minlength="5" name="event_end_date" 
                            placeholder="Event End Date" autocomplete="off" value="<?= $end_date ?>">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                        </div>
                      </div>

                    <div class="col-sm-4" style="padding-left: initial;">
                      <div class="col-sm-10 clockpicker" data-autoclose="true">
                        <input type="text" class="form-control required" data-parsley-minlength="2" 
                        data-parsley-trigger="change" required name="event_time[]" autocomplete="off" 
                        placeholder="Show Time" id="start_time" 
                        value="<?= $event_data->eventSchedule->time[0] ?>">
                      </div>
                      <div class="col-sm-2" style="padding-left: initial;">
                        <i class="fa fa-plus-circle cpoint fav-add-icon" 
                           onclick="addTimePicker(jQuery(this))" 
                           count="<?= count($event_data->eventSchedule->time) ?>"></i>
                      </div>

                      <div id="scheduleTimerCntr">
                        <?php $tCount = 2;
                        unset($event_data->eventSchedule->time[0]);
                        if(!empty($event_data->eventSchedule->time)){
                          foreach ($event_data->eventSchedule->time AS $time) { ?>
                            <div style="padding-left: initial;" id="timePicker_<?= $tCount ?>">
                              <div class="col-sm-10 padTop10 clockpicker" data-autoclose="true" 
                                   id="timePickerEnable_<?= $tCount ?>">
                                <input type="text" class="form-control required" data-parsley-minlength="2" 
                                       data-parsley-trigger="change" required name="event_time[]" 
                                       autocomplete="off" placeholder="Show Time" id="start_time" 
                                       value="<?= $time ?>">
                              </div>
                              <div class="col-sm-2" style="padding-left: initial;">
                                <i class="padTop12 fa fa-times-circle-o cpoint fav-rem-icon" onclick="remTimePicker(<?= $tCount ?>)"></i>
                              </div>
                            </div>
                          <?php $tCount +=1; 
                          } 
                        } ?>
                      </div>
                    </div>
                  </div>

                  <?php if(!empty($tag_data)){ ?>
                    <div class="col-sm-6">
                      <div class="box-header with-border">
                        <h3 class="box-title padLeft10 ">Tags</h3>
                      </div>
                      <div type="parent" class="header-tag-box marginTop10">
                        <?php foreach($tag_data AS $tag){ 
                          $select = '0';
                          $cstmClass = '';
                          if(in_array($tag->tag_id, $event_data->eventTags)){
                            $select = '1';
                            $cstmClass = 'tagSelected';
                          } ?>
                          <div id="tag_<?= $tag->tag_id ?>" class="header-tag cpoint <?= $cstmClass ?>" tag_id="<?= $tag->tag_id ?>" select="<?= $select ?>" onclick="manageTags(jQuery(this))">
                            <?= $tag->tag_EN ?>
                          </div>
                        <?php } ?>
                      </div>
                      <div id="selected_tags" class="hide">
                        <?php 
                          foreach($event_data->eventTags AS $tag_id) { 
                            echo '<input id="selTag_'.$tag_id.'" type="hidden" name="tags[]" 
                                 value="'.$tag_id.'">';
                          } 
                        ?>
                      </div>
                    </div>
                  <?php }  ?>
                </div>

                <div class="col-md-12 padTop10">      
                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary" id="addEventButton">Submit</button>
                    <a href="<?= base_url('Event/listEvents') ?>" class="btn btn-primary">Cancel</a>
                  </div>        
                </div> 
              </div>
            </form>
          </div>
        </div>
    </div>
  </section>
</div>

<div class="hide" id="scheduleTimerHtml">
  <div style="padding-left: initial;" id="timePicker_{:count}">
    <div class="col-sm-10 padTop10 clockpicker" data-autoclose="true" id="timePickerEnable_{:count}">
      <input type="text" class="form-control required" data-parsley-minlength="2" 
      data-parsley-trigger="change" required name="event_time[]" autocomplete="off" 
      placeholder="Show Time" id="start_time">
    </div>
    <div class="col-sm-2" style="padding-left: initial;">
      <i class="padTop12 fa fa-times-circle-o cpoint fav-rem-icon" onclick="remTimePicker({:count})"></i>
    </div>
  </div>
</div>

<div id="multipleImageInput" class="hide">
  <div class="dropZoneContainer" id="multiImageCntr_{:count}"  count="{:count}">
    <div id="multiImageClose_{:count}" class="close_custom hide" onclick="removeImage('{:count}');">&times;</div>
    <input id="event_image_{:count}" type="file" name="event_image[]" class="multiFileUpload" accept="image/*"
           onchange="setMultiImg(this,jQuery(this));" count="{:count}" />
    <img class="multiDropZoneOverlay" id="multiImageImg_{:count}" src="" 
           onerror="this.src='<?=base_url("assets/images/add-image.png")?>';" />
  </div>
</div>

<script type="text/javascript">
  jQuery( document ).ready(function() {
    <?php foreach($language AS $lang) { ?>
      if(jQuery('#rich_editor_<?= $lang ?>').length==1){CKEDITOR.replace('rich_editor_<?= $lang ?>',{language:'<?= strtolower($lang) ?>'});}
    <?php } ?>
  });
</script>
