<div class="content-wrapper" >
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
       <?= $pTitle ?>
        <small><?= $pDescription ?></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?= base_url() ?>"><i class="fa fa-star-o" aria-hidden="true"></i>Home</a></li>
      <li><?= $menu ?></li>
      <li class="active"><?= $smenu ?></li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <?php if($this->session->flashdata('message')) { 
          $flashdata = $this->session->flashdata('message'); ?>
          <div class="alert alert-<?= $flashdata['class'] ?>">
             <button class="close" data-dismiss="alert" type="button">×</button>
             <?= $flashdata['message'] ?>
          </div>
        <?php } ?>
      </div>
      <div class="col-xs-12">
        <div class="box box-warning"> 
          <div class="box-header with-border">
            <div class="col-md-6"><h3 class="box-title">Organizer List</h3></div>
            <div class="col-md-6" align="right">
              <a class="btn btn-sm btn-primary" href="<?= base_url('Provider/addProvider')?>">
                Add New Organizer
              </a>
              <a class="btn btn-sm btn-primary" href="<?= base_url() ?>">Back</a>
            </div>
          </div>
          <div class="box-body">
            <table id="mechanicUsers" class="table table-bordered table-striped datatable ">
              <thead>
                <tr>
                  <th class="hidden">ID</th>
                  <th width="80px;">Organizer</th>
                  <th width="80px;">User Name</th>
                  <th width="80px;">Email_id</th>
                  <th width="70px;">Phone</th>
                  <th width="60px;">Status</th>
                  <th width="600px;">Action</th>
               </tr>
              </thead> 
              <tbody>
                <?php
                if(!empty($provider_data)){
                  foreach($provider_data as $provider) { ?>
                    <tr>
                      <th class="hidden"><?= $provider->provider_id ?></th>
                      <th class="center"><?= $provider->name ?></th>
                      <th class="center"><?= $provider->username ?></th>
                      <th class="center"><?= $provider->email ?></th>
                      <th class="center"><?= $provider->phone ?></th>
                      <th class="center" id="statusFlag_<?= $provider->provider_id ?>">
                        <?= ($provider->status == 1)?'Active':'De-activate' ?>
                      </th>
                      <td class="center">  
                        <a class="btn btn-sm btn-link" style="margin-top:-13px;" href="<?= base_url('Provider/resetPassword/'.encode_param($provider->provider_id)) ?>">
                          <i class="fa fa-fw fa-key"></i>Reset Password
                        </a><br> 
                        <a class="btn btn-sm btn-info" id="viewProvider" provider_id="<?= encode_param($provider->provider_id) ?>">
                          <i class="fa fa-fw fa-eye"></i>View
                        </a>
                        <a class="btn btn-sm btn-primary" 
                            href="<?= base_url('Provider/editProviders/'.encode_param($provider->provider_id)) ?>">
                          <i class="fa fa-fw fa-edit"></i>Edit
                        </a>
                        <?php
                          $status = 0; $btnName = 'De-activate'; $btnClass = 'btn-warning';
                          if($provider->status != 1){
                            $status = 1; $btnName = 'Activate'; $btnClass = 'btn-success';
                          }
                        ?> 
                        <a class="btn btn-sm btn-danger" onclick="confirmDelete(jQuery(this),'Provider/changeStatus',{'provider_id':'<?=encode_param($provider->provider_id)?>'})" 
                          status="2"><i class="fa fa-fw fa-trash"></i>Delete
                        </a>  
                        <a class="btn btn-sm <?= $btnClass ?>" onclick="updateStatus(jQuery(this),'Provider/changeStatus',{'provider_id':'<?= encode_param($provider->provider_id) ?>'})" status="<?= $status ?>" status_id="<?= $provider->provider_id ?>">
                          <i class="fa fa-cog"><?= $btnName ?></i>
                        </a>
                      </td>
                    </tr>
                <?php } } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
