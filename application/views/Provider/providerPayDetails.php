<?php
  $settings = getSettings();
  $commission = (!empty($settings['service_charge']))?$settings['service_charge']:1;
?>

<div class="content-wrapper">
 <section class="content-header">
  <h1>
    <?= $page_title ?>
    <small><?= $page_desc ?></small>
  </h1>
  <ol class="breadcrumb">
   <li><a href="<?= base_url() ?>"><i class="fa fa-star-o" aria-hidden="true"></i>Home</a></li>
   <li><?= $menu ?></li>
   <li class="active"><?= $sub_menu ?></li>
 </ol>
</section>
<section class="content">
  <div class="row">
  <div class="col-sm-12">
    <?php if($this->session->flashdata('message')) { 
      $flashdata = $this->session->flashdata('message'); ?>
      <div class="alert alert-<?= $flashdata['class'] ?>">
       <button class="close" data-dismiss="alert" type="button">×</button>
       <?= $flashdata['message'] ?>
     </div>
   <?php } ?>
  </div>
  
  <?php if($this->session->userdata['user_type'] == 1){ ?>
   <div class="col-sm-12">
    <div class="box box-warning">
     <div class="box-header with-border">
       <h3 class="box-title">Payment Management</h3>
     </div>
     <div class="box-body">
        <form role="form" id="providerForm" action="<?=base_url('Provider/getProviderPayDetails')?>" method="post" class="validate" data-parsley-validate="" enctype="multipart/form-data">
          <div class="col-sm-12">
            <div class="form-group">
             <label>Select Organizer</label>
             <select name="provider_id" class="form-control required" data-parsley-trigger="change" 
              onchange="providerSubmitForm()" dmClick="0" required>
               <option selected disabled>Select Organizer</option>
               <?php 
                 if(!empty($provider_data)){
                   foreach ($provider_data as $provider) {
                     $chkFlg = ($provider_id == $provider->provider_id)?'selected':'';
                     echo '<option value="'.$provider->provider_id.'" '.$chkFlg.'>
                            '.$provider->display_name.
                          '</option>';
                   } 
                 }
               ?>
             </select> 
           </div>
          </div>
        </form>
      </div>
    </div>
   </div>
  <?php } ?>

  <?php if(!empty($provider_id)){ ?>
   <div class="col-sm-12">
    <div class="box box-warning">

      <!-- Start Outstanding amount to be paid -->
      <div class="box-header with-border">
       <h3 class="box-title">Commission to be Payed</h3>
      </div>
      <div class="box-body">
        <div class="col-sm-12">
          <div class="col-sm-6">
            <div class="col-sm-5">Booking Count</div>
            <div class="col-sm-1"><span style="padding-right:30px;">:</span></div>
            <div class="col-sm-6">
              <strong><?= (isset($payedDetails->total_booking) && !empty($payedDetails->total_booking))?$payedDetails->total_booking:0 ?></strong>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="col-sm-5">No. of Tickets Booked</div>
            <div class="col-sm-1"><span style="padding-right:30px;">:</span></div>
            <div class="col-sm-6">
              <strong><?= (isset($payedDetails->tickets_sold) && !empty($payedDetails->tickets_sold))?$payedDetails->tickets_sold:0 ?></strong>
            </div>
          </div>
        </div>
        <div class="col-sm-12">
          <div class="col-sm-6">
            <div class="col-sm-5">Total Booking Charge</div>
            <div class="col-sm-1"><span style="padding-right:30px;">:</span></div>
            <div class="col-sm-6">
              <strong><?= (isset($payedDetails->total_payed) && !empty($payedDetails->total_payed))?$payedDetails->total_payed:0 ?></strong>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="col-sm-5">Total Commission Earned</div>
            <div class="col-sm-1"><span style="padding-right:30px;">:</span></div>
            <div class="col-sm-6">
              <strong><?= (isset($payedDetails->last_commission_payed) && !empty($payedDetails->last_commission_payed))?$payedDetails->last_commission_payed:0 ?></strong>
            </div>
          </div>
        </div>
        <div class="col-sm-12">
          <div class="col-sm-6">
            <div class="col-sm-5">Last Payed Date</div>
            <div class="col-sm-1"><span style="padding-right:30px;">:</span></div>
            <div class="col-sm-6"><strong><?= (isset($payedDetails->last_payment_date) && !empty($payedDetails->last_payment_date))?$payedDetails->last_payment_date:'' ?></strong>
            </div>
          </div>
        </div>
      </div>
      <br>
      <!-- End Outstanding amount to be paid -->

      <!-- Start Payed Booking Details -->
      <div class="box-header with-border">
        <h3 class="box-title">Payed Commission Details</h3>
      </div>
      <div class="box-body">
        <div class="col-sm-12">
          <div class="col-sm-6">
            <div class="col-sm-5">Booking Count</div>
            <div class="col-sm-1"><span style="padding-right:30px;">:</span></div>
            <div class="col-sm-6"><strong><?= (isset($pendingDetails->count) && !empty($pendingDetails->count))?$pendingDetails->count:0 ?></strong>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="col-sm-5">No. Of Tickets Sold</div>
            <div class="col-sm-1"><span style="padding-right:30px;">:</span></div>
            <div class="col-sm-6"><strong><?= (isset($pendingDetails->no_of_ticket) && !empty($pendingDetails->no_of_ticket))?$pendingDetails->no_of_ticket:0 ?></strong>
            </div>
          </div>
        </div>
        <div class="col-sm-12">
          <div class="col-sm-6">
            <div class="col-sm-5">Total Booking Amount</div>
            <div class="col-sm-1"><span style="padding-right:30px;">:</span></div>
            <div class="col-sm-6"><strong><?= (isset($pendingDetails->amount) && !empty($pendingDetails->amount))?$pendingDetails->amount:0 ?></strong>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="col-sm-5">Commission to be paid</div>
            <div class="col-sm-1"><span style="padding-right:30px;">:</span></div>
            <div class="col-sm-6"><strong><?= (isset($pendingDetails->amount) && !empty($pendingDetails->amount))?(($pendingDetails->amount*$commission)/100):0 ?></strong>
            </div>
          </div>
        </div>
        <div class="col-sm-12">
          <div class="col-sm-6">
            <div class="col-sm-5">Outstanding Amount</div>
            <div class="col-sm-1"><span style="padding-right:30px;">:</span></div>
            <div class="col-sm-6"><strong><?= (isset($pendingDetails->amount) && !empty($pendingDetails->amount))?(($pendingDetails->amount*$commission)/100):0 ?></strong>
            </div>
          </div>
        </div>

        <?php if($this->session->userdata['user_type'] == 1){ ?>
          <form action="<?= base_url('Provider/updateProviderPayDate') ?>" method="post" 
                class="validate" data-parsley-validate="" enctype="multipart/form-data">
            <input type="hidden" name="provider_id" value="<?= $provider_id ?>">
            <input type="hidden" name="total_booking" value="<?= (isset($pendingDetails->count) && !empty($pendingDetails->count))?$pendingDetails->count:0 ?>">
            <input type="hidden" name="tickets_sold" value="<?= (isset($pendingDetails->no_of_ticket) && !empty($pendingDetails->no_of_ticket))?$pendingDetails->no_of_ticket:0 ?>">
            <input type="hidden" name="total_payed" value="<?= (isset($pendingDetails->amount) && !empty($pendingDetails->amount))?$pendingDetails->amount:0 ?>">
            <input type="hidden" name="last_commission_payed" value="<?= (isset($pendingDetails->amount) && !empty($pendingDetails->amount))?(($pendingDetails->amount*$commission)/100):0 ?>">
            <div class="col-sm-12" style="padding-top:10px;">
              <div class="col-sm-3" style="padding-left:31px;top:7px;">
                Update Last Payment Date
              </div>
              <div class="col-sm-3">
                <div class="input-group date" data-provide="datepicker">
                  <input id="datepicker" type="text" class="form-control required" data-parsley-trigger="change" data-parsley-minlength="2" required="" name="last_payment_date" placeholder="Pick Last Paid Date" autocomplete="off">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                </div>
              </div>
              <div class="col-sm-2" style="padding-bottom:10px;padding-left:30px;">
                <button type="submit" class="btn btn-info">Update</button>
              </div>
            </div>
          </form>
        <?php } ?>
      </div>
      <!-- End Payed Booking Details -->
    </div>
  </div>
  <?php } ?>
  </div>
 </section>
</div>