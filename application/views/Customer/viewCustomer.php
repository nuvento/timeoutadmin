<?php
  $role = roleManagement();
  $pAccess = $role['Customer'];
?>

<div class="content-wrapper" >
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
       <?= $pTitle ?>
        <small><?= $pDescription ?></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?= base_url() ?>"><i class="fa fa-star-o" aria-hidden="true"></i>Home</a></li>
      <li><?= $menu ?></li>
      <li class="active"><?= $smenu ?></li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <?php if($this->session->flashdata('message')) { 
          $flashdata = $this->session->flashdata('message'); ?>
          <div class="alert alert-<?= $flashdata['class'] ?>">
             <button class="close" data-dismiss="alert" type="button">×</button>
             <?= $flashdata['message'] ?>
          </div>
        <?php } ?>
      </div>
      <div class="col-xs-12">
        <div class="box box-warning"> 
          <div class="box-header with-border">
            <div class="col-md-6"><h3 class="box-title">Customer List</h3></div>
            <div class="col-md-6" align="right">
              <?php if(in_array('1',$pAccess)){ ?>
                <a class="btn btn-sm btn-primary" href="<?= base_url('Customer/addCustomer')?>">
                  Add New Customer
                </a>
              <?php } ?>
              <a class="btn btn-sm btn-primary" href="<?= base_url() ?>">Back</a>
            </div>
          </div>
          <div class="box-body">
            <table id="mechanicUsers" class="table table-bordered table-striped datatable ">
              <thead>
                <tr>
                  <th class="hidden">ID</th>
                  <th width="140px;">Customer Name</th>
                  <th width="110px;">Email</th>
                  <th width="70px;">Phone</th>
                  <th width="100px;">City</th>
                  <th width="40px;">Status</th>
                  <th width="350px;">Action</th>
               </tr>
              </thead> 
              <tbody>
                <?php
                if(!empty($customer_data)){
                  foreach($customer_data as $customer) { ?>
                    <tr>
                      <th class="hidden"><?= $customer->customer_id ?></th>
                      <th class="center"><?= $customer->name ?></th>
                      <th class="center"><?= $customer->email ?></th>
                      <th class="center"><?= $customer->phone ?></th>
                      <th class="center"><?= $customer->profile_city ?></th>
                      <th class="center" id="statusFlag_<?= $customer->customer_id ?>">
                        <?= ($customer->status == 1)?'Active':'De-activate' ?>
                      </th>
                      <td class="center">
                        <a class="btn btn-sm btn-info" id="viewCustomer" customer_id="<?= encode_param($customer->customer_id) ?>">
                          <i class="fa fa-fw fa-eye"></i>View
                        </a>
                        <?php if(in_array('2',$pAccess)){ ?> 
                          <a class="btn btn-sm btn-primary" 
                              href="<?= base_url('Customer/editCustomers/'.encode_param($customer->customer_id)) ?>">
                            <i class="fa fa-fw fa-edit"></i>Edit
                          </a>
                        <?php } if(in_array('3',$pAccess)){ ?> 
                          <a class="btn btn-sm btn-danger" onclick="confirmDelete(jQuery(this),'Customer/changeStatus',{'customer_id':'<?=encode_param($customer->customer_id)?>'})" 
                            status="2"><i class="fa fa-fw fa-trash"></i>Delete
                          </a> 
                        <?php }
                          $status = 0; $btnName = 'De-activate'; $btnClass = 'btn-warning';
                          if($customer->status != 1){
                            $status = 1; $btnName = 'Activate'; $btnClass = 'btn-success';
                          } if(in_array('4',$pAccess)){ ?> 
                          <a class="btn btn-sm <?= $btnClass ?>" onclick="updateStatus(jQuery(this),'Customer/changeStatus',{'customer_id':'<?= encode_param($customer->customer_id) ?>'})" status="<?= $status ?>" status_id="<?= $customer->customer_id ?>">
                            <i class="fa fa-cog"><?= $btnName ?></i>
                          </a>
                        <?php } ?>
                      </td>
                    </tr>
                <?php } } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>