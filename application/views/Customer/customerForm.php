<div class="content-wrapper">
  <section class="content-header">
    <h1>
       <?= $pTitle ?>
        <small><?= $pDescription ?></small>
    </h1>
    <ol class="breadcrumb">
     <li><a href="<?= base_url() ?>"><i class="fa fa-star-o" aria-hidden="true"></i>Home</a></li>
     <li><?= $menu ?></li>
     <li class="active"><?= $smenu ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <?php 
        $url = (!isset($customer_id) || empty($customer_id))?'Customer/createCustomer':'Customer/updateCustomer/'.$customer_id;
        if($this->session->flashdata('message')) { 
          $flashdata = $this->session->flashdata('message'); ?>
          <div class="alert alert-<?= $flashdata['class'] ?>">
            <button class="close" data-dismiss="alert" type="button">×</button>
            <?= $flashdata['message'] ?>
          </div>
        <?php } ?>
      </div>
      <div class="col-md-12">
        <div class="box box-warning">
          <div class="box-body">
            <form role="form" action="<?= base_url($url) ?>" method="post" 
              class="validate" data-parsley-validate="" enctype="multipart/form-data">
              
              <!-- Customer Data -->
              <div class="col-md-12">  
                <div class="box-header with-border padUnset">
                  <h3 class="box-title">Personal Details</h3>
                </div><br>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Customer Name</label>
                  <input type="text" class="form-control required" data-parsley-trigger="change"
                  data-parsley-minlength="2" data-parsley-pattern="^[a-zA-Z0-9\ . _ - ' \/]+$" 
                  name="name" required="" value="<?= (isset($customer_data->name))?$customer_data->name:'' ?>"placeholder="Enter Customer Name">
                </div>  
                <div class="form-group">
                  <label>Email</label>
                  <input type="email" class="form-control required" data-parsley-trigger="change"  
                  data-parsley-minlength="2" required="" name="email" placeholder="Enter email ID"  value="<?= (isset($customer_data->email))?$customer_data->email:'' ?>">
                </div> 
                <div class="form-group">
                  <label>Phone</label>
                  <input type="text" class="form-control required" data-parsley-trigger="change" 
                  data-parsley-minlength="2"  data-parsley-pattern="^[0-9\ , - + \/]+$" required=""
                  value="<?= (isset($customer_data->phone))?$customer_data->phone:'' ?>" name="phone" placeholder="Enter Phone Number" >
                </div> 
                <div class="form-group">
                  <label>City</label>
                  <input type="text" class="form-control required" data-parsley-trigger="change" 
                  id="loc_search_1" name="profile_city" placeholder="City" value="<?= (isset($customer_data->profile_city))?$customer_data->profile_city:'' ?>" required autocomplete="off">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Profile Picture</label>
                  <div class="col-md-12" style="padding-bottom:10px;">
                    <div class="col-md-3">
                      <img id="image_id" src="<?= (isset($customer_data->profile_image))?base_url($customer_data->profile_image):'' ?>" onerror="this.src='<?=base_url("assets/images/user_avatar.jpg")?>';" height="75" width="75" />
                    </div>
                    <div class="col-md-9" style="padding-top: 25px;">
                      <input name="profile_image" type="file" accept="image/*" onchange="setImg(this,'image_id');" />
                    </div>
                  </div>
                </div>
                <div class="form-group" style="margin-top: 124px;">
                  <label>Gender</label>
                  <select name="gender" class="form-control">
                    <?php
                      $gender = '';
                      if(!isset($customer_data->gender)){
                        echo '<option selected disabled>Choose Gender</option>';
                      } else {
                        $gender = $customer_data->gender;
                      }
                    ?>
                    <option value="1" <?= ($gender=='1')?'selected':'' ?>>Male</option>
                    <option value="2" <?= ($gender=='2')?'selected':'' ?>>Female</option>
                    <option value="3" <?= ($gender=='3')?'selected':'' ?>>Others</option>
                  </select>
                </div>
                <div class="input-group date" data-provide="datepicker">
                  <label>Date of Birth</label>
                  <input id="date" type="text" class="form-control required" required 
                    data-parsley-trigger="change" data-parsley-minlength="5"
                    name="dob" placeholder="Date of Birth" autocomplete="off" 
                    value="<?= (isset($customer_data->dob))?$customer_data->dob:'' ?>">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                </div>
              </div>
              <div class="col-md-12">      
                <div class="box-footer textCenterAlign">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <a href="<?= base_url('Customer/viewCustomers') ?>" class="btn btn-primary">Cancel</a>
                </div>        
              </div>        
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>


<!-- Basic Details -->
<!-- <div class="col-md-12">  
  <div class="box-header with-border padUnset">
    <h3 class="box-title">Admin User Details</h3>
  </div><br>
</div>
<div class="col-md-6">
  <div class="form-group">
    <label>Display Name</label>
    <input type="text" class="form-control required" data-parsley-trigger="change"
    data-parsley-minlength="2" name="display_name" required="" 
    placeholder="Enter Display Name" value="<?= (isset($customer_data->display_name))?$customer_data->display_name:'' ?>">
    <span class="glyphicon form-control-feedback"></span>
  </div>
  <div class="form-group">
    <label>User Name</label>
    <input type="text" class="form-control required" data-parsley-trigger="change"
    data-parsley-minlength="2" name="username" required="" value="<?= (isset($customer_data->username))?$customer_data->username:'' ?>"
    data-parsley-pattern="^[a-zA-Z0-9\ . _ @  \/]+$" placeholder="Enter User Name">
    <span class="glyphicon  form-control-feedback"></span>
  </div>
  <?php if(!isset($customer_id)){ ?>
  <div class="form-group">
    <label>Password</label>
    <input type="password" class="form-control required" name="password" placeholder="Password" required="">
    <span class="glyphicon  form-control-feedback"></span>
  </div>  
<?php } ?>
</div>
<div class="col-md-6">
  <div class="form-group">
    <label>Profile Picture</label>
    <div class="col-md-12" style="padding-bottom:10px;">
      <div class="col-md-3">
        <img id="image_id" src="<?= (isset($customer_data->profile_image))?base_url($customer_data->profile_image):'' ?>" onerror="this.src='<?=base_url("assets/images/user_avatar.jpg")?>';" height="75" width="75" />
      </div>
      <div class="col-md-9" style="padding-top: 25px;">
        <input name="profile_image" type="file" accept="image/*" onchange="setImg(this,'image_id');" />
      </div>
    </div>
  </div>
</div> -->

