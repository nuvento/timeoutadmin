<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    <h1> <?= $page_title ?>
        <small><?= $page_desc ?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="javascript:void(0);" ><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
    </section> 
</div>