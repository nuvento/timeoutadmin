<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    <h1> <?= $page_title ?>
        <small><?= $page_desc ?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="javascript:void(0);" ><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
    </section>
    <?php 
        if($this->session->flashdata('message')) { 
        $flashdata = $this->session->flashdata('message'); ?>
        <br><div class="alert alert-<?= $flashdata['class'] ?>">
           <button class="close" data-dismiss="alert" type="button">×</button>
           <?= $flashdata['message'] ?>
        </div>
    <?php } ?>  
    <section class="content">
        <div class="row">
            <?php if(isset($usersCount)){ ?>
                <div class="col-lg-3 col-xs-6">
                    <div class="small-box bg-yellow">
                        <div class="inner">
                            <h4>Users</h4>
                            <p><?= $usersCount ?></p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-add"></i>
                        </div>
                        <a href="<?= base_url('User') ?>" class="small-box-footer ">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            <?php } ?>

            <?php if(isset($driversCount)){ ?>
                <div class="col-lg-3 col-xs-6">
                    <div class="small-box bg-green">
                        <div class="inner">
                            <h4>Drivers</h4> 
                            <p>
                                <?php 
                                    echo 'Total : '.$driversCount; 
                                    if(!empty($onlineDriversCount)) 
                                        echo '&nbsp;&nbsp;Online : <strong>'.$onlineDriversCount.'</strong>';
                                ?>
                            </p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-add"></i>
                        </div>
                        <a href="<?= base_url('Driver') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            <?php } ?>

            <?php if(isset($compltOrderCnt) && in_array('delivered', $permissions)){ ?>
                <div class="col-lg-3 col-xs-6">
                    <div class="small-box bg-aqua">
                        <div class="inner">
                            <h4>Completed Orders</h4>
                            <p><?= $compltOrderCnt ?></p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <a href="<?= base_url('Orders/delivered') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            <?php } ?>

            <?php if(isset($pentOrderCnt) && in_array('uncompleted', $permissions)){ ?>
                <div class="col-lg-3 col-xs-6">
                    <div class="small-box bg-blue">
                        <div class="inner">
                            <h4>Uncompleted Orders</h4>
                            <p><?= $pentOrderCnt ?></p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <a href="<?= base_url('Orders/uncompleted') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            <?php } ?>

            <?php if(isset($processingOrderCnt) && in_array('processing', $permissions)){ ?>
                <div class="col-lg-3 col-xs-6">
                    <div class="small-box bg-blue">
                        <div class="inner">
                            <h4>Orders On Hold</h4>
                            <p><?= $processingOrderCnt ?></p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <a href="<?= base_url('Orders/processing') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            <?php } ?>


            <?php if(isset($subTotal) && in_array('delivered', $permissions)){ ?>
                <div class="col-lg-3 col-xs-6">
                    <div class="small-box bg-yellow">
                        <div class="inner">
                            <h4> Total of financial transactions </h4>
                            <p><?= $subTotal ?></p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <a href="<?= base_url('Orders/delivered') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            <?php } ?>

            <?php if(isset($deliveryTotal) && in_array('delivered', $permissions)){ ?>
                <div class="col-lg-3 col-xs-6">
                    <div class="small-box bg-red">
                        <div class="inner">
                            <h4> Total of delivery Fees </h4>
                            <p><?= $deliveryTotal ?></p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <a href="<?= base_url('Orders/delivered') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            <?php } ?>

            <?php if(isset($orderTotal) && in_array('delivered', $permissions)){ ?>
                <div class="col-lg-3 col-xs-6">
                    <div class="small-box bg-green">
                        <div class="inner">
                            <h4>Total of orders fees</h4>
                            <p><?= $orderTotal ?></p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <a href="<?= base_url('Orders/delivered') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            <?php } ?>
        </div>
    </section>
</div>