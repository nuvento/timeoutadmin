<?php
  $language = getLanguages();
?>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
       <?= $pTitle ?>
        <small><?= $pDescription ?></small>
    </h1>
    <ol class="breadcrumb">
     <li><a href="<?= base_url() ?>"><i class="fa fa-star-o" aria-hidden="true"></i>Home</a></li>
     <li><?= $menu ?></li>
     <li class="active"><?= $smenu ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <?php 
        $url = (!isset($tag_id)||empty($tag_id))?'Tag/createTags':'Tag/updateTags/'.$tag_id;

        if($this->session->flashdata('message')) { 
          $flashdata = $this->session->flashdata('message'); ?>
          <div class="alert alert-<?= $flashdata['class'] ?>">
            <button class="close" data-dismiss="alert" type="button">×</button>
            <?= $flashdata['message'] ?>
          </div>
        <?php } ?>
      </div>
      <div class="col-md-12">
        <div class="box box-warning">
          <div class="box-body">
            <form role="form" action="<?= base_url($url) ?>" method="post" 
              class="validate" data-parsley-validate="" enctype="multipart/form-data">
              <div class="col-md-12"><label>Tag</label></div>
                <?php foreach($language AS $lang) { ?>
                <div class="col-md-4">
                  <div class="form-group">
                    <?php $tagName = (isset($tag_data->{'tag_'.$lang}))?$tag_data->{'tag_'.$lang}:''; ?>
                    <input type="text" class="form-control <?= ($lang == 'EN')?'required':'' ?>"
                           data-parsley-trigger="change" data-parsley-minlength="2" 
                           name="<?= 'tag_'.$lang?>" placeholder="Tag (<?= $lang ?>)" 
                           value="<?= $tagName ?>">
                    <span class="glyphicon form-control-feedback"></span>
                  </div>
                </div>
                <?php } ?>
              <div class="col-md-12 padTop10">      
                <div class="box-footer textCenterAlign">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <a href="<?= base_url('Tag/listTags') ?>" class="btn btn-primary">Cancel</a>
                </div>        
              </div>        
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>