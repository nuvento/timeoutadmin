<div class="box-body">
 <div class="view_booking_modal">
   <div class="row">
     <div class="col-md-12 textRight">
      <img class="qr_code" src="<?= base_url($bookData->qrcode) ?>" onerror="this.src='<?=base_url("assets/images/qr_default.png")?>';" style="max-width: 100px;max-height: 100px"/>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-6">
          <div class="row">
            <div class="col-md-5">
              Show Time
            </div>
            <div class="col-md-1">
              :
            </div>
            <div class="col-md-5">
              (<?=  $bookData->date ?>) <?=  $bookData->time ?>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="row">
            <div class="col-md-5">
              No of Tickets
            </div>
            <div class="col-md-1">
              :
            </div>
            <div class="col-md-5">
              <?=  $bookData->no_of_ticket ?> Tickets
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-6">
          <div class="row">
            <div class="col-md-5">
              Ticket Details
            </div>
            <div class="col-md-1">
              :
            </div>
            <div class="col-md-5">
              <p class="truncateText">
                <?php
                if(!empty($bookData->ticket_details)){
                  $tkt = json_decode($bookData->ticket_details,true);
                  if(isset($tkt['price'],$tkt['no_ticket'],$tkt['total_price']) && 
                   !empty($tkt['price']) && !empty($tkt['no_ticket']) && 
                   !empty($tkt['total_price'])){
                    $pDiv = (isset($tkt['color']) && !empty($tkt['color']))?$tkt['color'].' Block : ':'';
                  $pDiv .= $tkt['price'].' * '.$tkt['no_ticket'].'(Seats) = '.$tkt['total_price'];
                  echo $pDiv;
                }
              }
              ?>
            </p>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="row">
          <div class="col-md-5">
            Amount
          </div>
          <div class="col-md-1">
            :
          </div>
          <div class="col-md-5">
            <?=  $bookData->amount ?>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-6">
        <div class="row">
          <div class="col-md-5">
            Reserved by
          </div>
          <div class="col-md-1">
            :
          </div>
          <div class="col-md-5">
            <?php
            switch ($bookData->reserved_by) {
              case '1': echo 'Super Admin '; break;
              case '2': echo 'Provider '; break;
              case '3': echo 'Customer'; break;
            }
            ?>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="row">
          <div class="col-md-5">
            Booking Status
          </div>
          <div class="col-md-1">
            :
          </div>
          <div class="col-md-5">
            <?php
            switch ($bookData->book_status) {
              case '0': echo 'Cancelled'; break;
              case '1': echo 'Booked'; break;
              case '2': echo 'Completed'; break;
              case '3': echo 'Pending'; break;
              case '4': echo 'Deleted'; break;
              case '5': echo 'Payment Failed'; break;
            }
            ?>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-2">
        Booking Time
      </div>
      <div class="col-md-1" style="padding-left: 52px;">
        :
      </div>
      <div class="col-md-9">
        <?= $bookData->booking_date ?>
      </div>
    </div>

    <div class="row">
      <div class="col-md-2">
        Event Name
      </div>
      <div class="col-md-1" style="padding-left: 52px;">
        :
      </div>
      <div class="col-md-9">
        <?= $bookData->event_name_EN ?>
      </div>
    </div>

    <div class="row">
      <div class="col-md-2">
        Event Description
      </div>
      <div class="col-md-1" style="padding-left: 52px;">
        :
      </div>
      <div class="col-md-9">
        <?=  $bookData->event_description_EN ?>
      </div>
    </div>

    <div class="row">
      <div class="col-md-6">
        <div class="row">
          <div class="col-md-5">
            Catagory
          </div>
          <div class="col-md-1">
            :
          </div>
          <div class="col-md-5">
            <?=  $bookData->category_name_EN ?>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-6">
        <div class="row">
          <div class="col-md-5">
            Customer Name
          </div>
          <div class="col-md-1">
            :
          </div>
          <div class="col-md-5">
            <?=  $bookData->customer_name ?>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="row">
          <div class="col-md-5">
            Customer Phone Number
          </div>
          <div class="col-md-1">
            :
          </div>
          <div class="col-md-5">
            <?=  $bookData->customer_phone ?>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-6">
        <div class="row">
          <div class="col-md-5">
            Customer Email
          </div>
          <div class="col-md-1">
            :
          </div>
          <div class="col-md-5">
            <?=  $bookData->customer_email ?>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="row">
          <div class="col-md-5">
            Customer City
          </div>
          <div class="col-md-1">
            :
          </div>
          <div class="col-md-5">
            <?=  $bookData->profile_city ?>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-6">
        <div class="row">
          <div class="col-md-5">
            Provider Name
          </div>
          <div class="col-md-1">
            :
          </div>
          <div class="col-md-5">
            <?=  $bookData->provider_name ?>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="row">
          <div class="col-md-5">
            Provider Phone Number
          </div>
          <div class="col-md-1">
            :
          </div>
          <div class="col-md-5">
            <?=  $bookData->provider_phone ?>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-6">
        <div class="row">
          <div class="col-md-5">
            Provider Email
          </div>
          <div class="col-md-1">
            :
          </div>
          <div class="col-md-5">
            <?=  $bookData->provider_email ?>
          </div>
        </div>
      </div>
      <div class="col-md-6">
      </div>
    </div>


    <div class="row">
      <div class="col-md-6">
        <div class="row">
          <div class="col-md-5">
            Venue Name
          </div>
          <div class="col-md-1">
            :
          </div>
          <div class="col-md-5">
            <?=  $bookData->venue_name_EN ?>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="row">
          <div class="col-md-5">
            Venue Address
          </div>
          <div class="col-md-1">
            :
          </div>
          <div class="col-md-5">
            <p class="truncateText"><?=  $bookData->location ?></p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>