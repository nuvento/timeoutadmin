<div class="content-wrapper" >
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
       <?= $pTitle ?>
        <small><?= $pDescription ?></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?= base_url() ?>"><i class="fa fa-star-o" aria-hidden="true"></i>Home</a></li>
      <li><?= $menu ?></li>
      <li class="active"><?= $smenu ?></li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <?php if($this->session->flashdata('message')) { 
          $flashdata = $this->session->flashdata('message'); ?>
          <div class="alert alert-<?= $flashdata['class'] ?>">
             <button class="close" data-dismiss="alert" type="button">×</button>
             <?= $flashdata['message'] ?>
          </div>
        <?php } ?>
      </div>
      <div class="col-xs-12">
        <div class="box box-warning"> 
          <div class="box-header with-border">
            <div class="col-md-6"><h3 class="box-title">Booking List</h3></div>
            <div class="col-md-6" align="right">
              <a class="btn btn-sm btn-success" href="<?=base_url('Booking/generateReport')?>">Generate Report</a>
              <a class="btn btn-sm btn-primary" href="<?= base_url() ?>">Back</a>
            </div>
          </div>
          <div class="box-body">
            <table id="mechanicUsers" class="table table-bordered table-striped datatable ">
              <thead>
                <tr>
                  <th class="hidden">ID</th>
                  <th width="50px;">Book ID</th>
                  <th width="100px;">Event</th>
                  <th width="130px;">Event Name</th>
                  <th width="130px;">Customer</th>
                  <th width="130px;">Scheduled On</th>
                  <th width="50px;">Amount</th>
                  <th width="50px;">Status</th>
                  <th width="40px;">Action</th>
               </tr>
              </thead> 
              <tbody>
                <?php
                if(!empty($booking_data)){
                  foreach($booking_data as $booking) { ?>
                    <tr>
                      <th class="hidden"><?= $booking->booking_id ?></th>
                      <th class="center"><?= $booking->bookId ?></th>
                      <th class="center"><?= $booking->category_name_EN ?></th>
                      <th class="center"><?= $booking->event_name_EN ?></th>
                      <th class="center"><?= $booking->customer_name ?></th>
                      <th class="center"><?= $booking->date.' '.$booking->time ?></th>
                      <th class="center"><?= $booking->amount ?></th>
                      <th class="center" id="bookingStatus_<?= encode_param($booking->booking_id) ?>">
                        <?php 
                          switch($booking->book_status){
                            case 0: echo 'Cancelled'; break;
                            case 1: echo 'Booked'; break;
                            case 2: echo 'Completed'; break;
                            case 3: echo 'Pending'; break;
                            case 4: echo 'Deleted'; break;
                            case 5: echo 'Payment Failed'; break;
                            case 6: echo 'Waiting for Aproval'; break;
                          }
                        ?>
                      </th>
                      <td class="center">  
                        <a class="cpoint text-primary" id="viewBooking" 
                           booking_id="<?= encode_param($booking->booking_id) ?>">
                          <i class="fa fa-fw fa-eye"></i>View
                        </a> <br>
                        <?php 
                          if(($this->session->userdata('user_type')==1 && $booking->provider_id==1 && 
                              $booking->book_status=='6') || 
                             ($this->session->userdata('user_type')==2 && $booking->book_status=='6')){
                            $msg = "Are you sure to Aprove this booking ?";
                            $bookId = encode_param($booking->booking_id);
                            $param = "{'booking_id':'$bookId','alertMsg':'$msg'}"; ?>

                            <a id="approveBooking_<?= encode_param($booking->booking_id) ?>" 
                               class="cpoint text-success" status="1" call_back="updateBokApproved"
                               status_id="<?= encode_param($booking->booking_id) ?>" 
                               onclick="confirmDelete(jQuery(this),'Booking/changeStatus',<?= $param ?>)">
                              <i class="fa fa-fw fa-check"></i>Approve
                            </a>
                          <?php } ?>
                        <?php } ?>
                      </td>
                    </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
