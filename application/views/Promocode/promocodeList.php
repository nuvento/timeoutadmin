<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
       <?= $pTitle ?>
        <small><?= $pDescription ?></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?= base_url() ?>"><i class="fa fa-star-o" aria-hidden="true"></i>Home</a></li>
      <li><?= $menu ?></li>
      <li class="active"><?= $smenu ?></li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <?php if($this->session->flashdata('message')) { 
          $flashdata = $this->session->flashdata('message'); ?>
          <div class="alert alert-<?= $flashdata['class'] ?>">
             <button class="close" data-dismiss="alert" type="button">×</button>
             <?= $flashdata['message'] ?>
          </div>
        <?php } ?>
      </div>
      <div class="col-xs-12">
        <div class="box box-warning">
          <div class="box-header with-border">
            <div class="col-md-6"><h3 class="box-title">Promocode List</h3></div>
            <div class="col-md-6" align="right">
              <a class="btn btn-sm btn-primary" href="<?= base_url('Promocode/addPromocode') ?>">
                Add New Promocode
              </a>
              <a class="btn btn-sm btn-primary" href="<?= base_url() ?>">Back</a>
           </div>
          </div>
          <div class="box-body">
            <table id="mechanicUsers" class="table table-bordered table-striped datatable ">
              <thead>
                <tr>
                  <th class="hidden">ID</th>
                  <th width="100px">Promocode</th>
                  <th>Type</th>
                  <th>Discount</th>
                  <th width="110px">Validity</th>
                  <th width="65px">Limit</th>
                  <th width="65px">Status</th>
                  <th width="250px">Action</th>
               </tr>
              </thead> 
              <tbody>
                <?php
                if(!empty($promocodeData)){
                  foreach($promocodeData as $promo) { ?>
                    <tr>
                      <td class="hidden"><?= $promo->promocode_id ?></td>
                      <td><?= $promo->promocode_name ?></td>
                      <td><?= ($promo->discount_type==1)?'Discount':'Flat' ?></td>
                      <td><?php 
                        if($promo->discount_type == 1){
                          echo $promo->discount_percentage.'% Discount '; 
                          echo (!empty($promo->max_redeem))?'Upto '.$promo->max_redeem.' SAR':'';
                          echo (!empty($promo->min_order_amount))?', for a minimum order of '.$promo->min_order_amount.' SAR<br>':'';
                        } else {
                          echo 'Flat '.$promo->max_redeem.' SAR off ';
                          echo (!empty($promo->discount_percentage))?'(Upto '.$promo->discount_percentage.'% of total amount)':'';
                          echo (!empty($promo->min_order_amount))?', for a minimum order of '.$promo->min_order_amount.' SAR<br>':'';
                        }
                      ?></td>
                      <td><?= 
                        date('d-M-y',strtotime($promo->start_date)).' - '.
                        date('d-M-y',strtotime($promo->end_date)) ?>
                      </td>
                      <td><?= 'Used: '.$promo->used.'<br>
                               Unused: '.($promo->use_limit-$promo->used).'<br>
                               Total: '.$promo->use_limit ?></td>
                      <td id="statusFlag_<?= encode_param($promo->promocode_id) ?>"><?php 
                        if($promo->status == 1){
                          if(strtotime($promo->start_date) < strtotime(date('d-M-y H:i:s'))){
                            echo 'Yet to Activete';
                          } else if(strtotime($promo->start_date) > strtotime(date('d-M-y H:i:s')) && 
                             strtotime($promo->end_date) < strtotime(date('d-M-y H:i:s'))){
                            echo 'Active';
                          } else echo 'Offer Expired';
                        } else echo 'De-active'; ?>
                      </td>
                      <td>
                        <a class="btn btn-sm btn-primary" 
                           href="<?= base_url('Promocode/editPromocode/'.encode_param($promo->promocode_id))?>">
                          <i class="fa fa-fw fa-edit"></i>Edit
                        </a> 
                        <a class="btn btn-sm btn-danger" onclick="confirmDelete(jQuery(this),'Promocode/changeStatus',{'promocode_id':'<?=encode_param($promo->promocode_id)?>'})" 
                          status="2"><i class="fa fa-fw fa-trash"></i>Delete
                        </a>  
                        <?php 
                        switch($promo->status){
                          case 1: $status = 0;$btnName = 'De-activate';$btnClass = 'btn-warning';break;
                          case 0: $status = 1;$btnName = 'Activate';$btnClass = 'btn-success';break; 
                          case 2: $status = 1;$btnName = 'Approve';$btnClass = 'btn-info';break; 
                        } ?>
                        <a class="btn btn-sm <?= $btnClass ?>" onclick="updateStatus(jQuery(this),'Promocode/changeStatus',{'promocode_id':'<?= encode_param($promo->promocode_id) ?>'})" status="<?= $status ?>" status_id="<?= encode_param($promo->promocode_id) ?>" call_back="setPromoStatus">
                          <i class="fa fa-cog"><?= $btnName ?></i>
                        </a>
                      </td>
                    </tr>
                <?php } } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>