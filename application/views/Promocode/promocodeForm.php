<?php
  $language = getLanguages();
?>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
       <?= $pTitle ?>
        <small><?= $pDescription ?></small>
    </h1>
    <ol class="breadcrumb">
     <li><a href="<?= base_url() ?>"><i class="fa fa-star-o" aria-hidden="true"></i>Home</a></li>
     <li><?= $menu ?></li>
     <li class="active"><?= $smenu ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <?php 
        $url = (!isset($promocode_id) || empty($promocode_id))?'Promocode/createPromocode':'Promocode/updatePromocode/'.$promocode_id;
        if($this->session->flashdata('message')) { 
          $flashdata = $this->session->flashdata('message'); ?>
          <div class="alert alert-<?= $flashdata['class'] ?>">
            <button class="close" data-dismiss="alert" type="button">×</button>
            <?= $flashdata['message'] ?>
          </div>
        <?php } ?>
      </div>
      <div class="col-md-12">
        <div class="box box-warning">
          <div class="box-body">
            <form role="form" action="<?= base_url($url) ?>" method="post" 
              class="validate" data-parsley-validate="" enctype="multipart/form-data">
              
              <!-- Promocode Data -->
              <div class="col-md-12">   
                <div class="box-header with-border padUnset">
                  <h3 class="box-title">Promocode Details</h3>
                </div><br>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Promocode</label>
                  <input type="text" class="form-control required" data-parsley-trigger="change"
                  data-parsley-minlength="8" data-parsley-maxlength="16" data-parsley-pattern="^[A-Z0-9]+$" name="promocode_name" required="" value="<?= (isset($promo->promocode_name))?$promo->promocode_name:'' ?>"placeholder="Enter Promocode">
                </div>
                <div class="form-group input-group date" data-provide="datepicker">
                  <label>Promo Valid From</label>
                  <input id="date" type="text" class="form-control required" required autocomplete="off" 
                    data-parsley-trigger="change" name="start_date" placeholder="Promo Valid From" 
                    value="<?= (isset($promo->start_date))?date('m/d/Y',strtotime($promo->start_date)):'' ?>">
                  <div class="input-group-addon"style="padding-top:29px;border-color:#ffffff!important;">
                    <i class="fa fa-calendar"></i>
                  </div>
                </div>
                <div class="form-group input-group date" data-provide="datepicker">
                  <label>Promo Valid To</label>
                  <input id="date" type="text" class="form-control required" required autocomplete="off" 
                    data-parsley-trigger="change" name="end_date" placeholder="Promo Valid To" 
                    value="<?= (isset($promo->end_date))?date('m/d/Y',strtotime($promo->end_date)):'' ?>">
                  <div class="input-group-addon"style="padding-top:29px;border-color:#ffffff!important;">
                    <i class="fa fa-calendar"></i>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>About Promocode (EN) 
                    <a id="addMultiLang_desc" block="desc" show="0" class="cpoint noSubTypeMsg">
                      <small> + Show Add More Language Option</small>
                    </a>
                  </label>
                  <textarea id="rich_editor_desc_EN" type="text" class="form-control" 
                    placeholder="About Promocode" name="promocode_desc_EN" style="height:107px;" data-parsley-trigger="change" data-parsley-minlength="2"><?= (isset($promo->{'promocode_desc_EN'}))?$promo->{'promocode_desc_EN'}:'' ?></textarea>
                  <div id="showMultiLangBlock_desc" class="hide marginTop10">
                    <?php foreach($language AS $lang) { if($lang == 'EN') continue; ?>
                      <div class="form-group">
                        <label>About Promocode (<?= $lang ?>)</label>
                        <textarea  id="rich_editor_desc_<?= $lang ?>" type="text" class="form-control"  
                          style="height:107px;" placeholder="About  Promocode" data-parsley-minlength="2"
                          name="promocode_desc_<?= $lang ?>" data-parsley-trigger="change"><?= (isset($promo->{'promocode_desc_'.$lang}))?$promo->{'promocode_desc_'.$lang}:'' ?></textarea>
                      </div>
                    <?php } ?>
                  </div>
                </div>
              </div>
              <div class="row"></div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>City</label>
                  <select name="city_id" class="form-control" placeholder="Category">
                    <option selected value="0">Choose an City</option>
                    <?php foreach($regionData as $city) {
                      $selected = (isset($promo->city_id) && $city->id == $promo->city_id)?'selected':'';
                      echo '<option name="city_id" '.$selected.' value="'.$city->id.'">'.
                              $city->region_name_EN.
                           '</option>';
                    } ?>
                  </select> 
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Category</label>
                  <select name="category_id" class="form-control" placeholder="Category">
                    <option selected value="0">Choose an Category</option>
                    <?php foreach($categoryData as $cat) {
                      $selected = (isset($promo->category_id) && $cat->cat_id == $promo->category_id)?'selected':'';
                      echo '<option name="category_id" '.$selected.' value="'.$cat->cat_id.'">'.
                              $cat->category_name_EN.
                           '</option>';
                    } ?>
                  </select> 
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Event</label>
                  <select name="event_id" class="form-control" placeholder="Category">
                    <option selected value="0">Choose an Event</option>
                    <?php foreach($event_data as $event) {
                      $selected = (isset($promo->event_id) && $event->event_id==$promo->event_id)?'selected':'';
                      echo '<option name="event_id" '.$selected.' value="'.$event->event_id.'">'.$event->event_name_EN.'</option>';
                    } ?>
                  </select> 
                </div>
              </div>
              <div class="row"></div>
              <div class="col-sm-6">
                <label>Offer Type</label>
                <div style="padding-left:25px;">
                  <?php
                    $checkTwo = '';
                    $checkOne = 'checked';
                    if(isset($promo->discount_type) && $promo->discount_type == 2){
                      $checkOne = '';
                      $checkTwo = 'checked';
                    }
                  ?>
                  <input type="radio" name="discount_type" value="1" <?=$checkOne?>>
                    <label class="padAll-10">Discount by percentage</label>
                  <input class="marginLeft15" type="radio" name="discount_type" value="2" <?=$checkTwo?>>
                    <label class="padAll-10">Flat discount form base price</label>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Discount Percentage / Flat off amount</label>
                  <input type="text" class="form-control required" data-parsley-trigger="change"
                    data-parsley-pattern="^[0-9]+$" name="discount_percentage" placeholder="Promo Valid To" autocomplete="off" value="<?= (isset($promo->discount_percentage))?$promo->discount_percentage:'' ?>">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Maximum Discount per Offer</label>
                  <input id="date" type="text" class="form-control required" required 
                    data-parsley-trigger="change" data-parsley-pattern="^[0-9]+$"
                    name="max_redeem" placeholder="Maximum Discount per Offer" autocomplete="off" 
                    value="<?= (isset($promo->max_redeem))?$promo->max_redeem:'' ?>">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Minimum Order amount</label>
                  <input id="date" type="text" class="form-control" data-parsley-trigger="change" 
                    data-parsley-pattern="^[0-9]+$" name="min_order_amount" placeholder="Minimum Order amount" autocomplete="off" value="<?= (isset($promo->min_order_amount))?$promo->min_order_amount:'' ?>">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Offer Limit</label>
                  <input id="date" type="text" class="form-control required" required 
                    data-parsley-trigger="change"  data-parsley-pattern="^[0-9]+$"
                    name="use_limit" placeholder="Offer Limit" autocomplete="off" 
                    value="<?= (isset($promo->use_limit))?$promo->use_limit:'' ?>">
                </div>
              </div>
              <div class="row"></div>
              <div class="col-sm-6">
                <div class="form-group">
                  <label>Terms and Conditions (EN) 
                    <a id="addMultiLang_tc" block="tc" show="0" class="cpoint noSubTypeMsg">
                      <small> + Show Add More Language Option</small>
                    </a>
                  </label>
                  <textarea id="rich_editor_tc_EN" type="text" name="promocode_tc_EN" 
                    data-parsley-trigger="change" data-parsley-minlength="2"
                    class="ip_reg_form_input form-control reset-form-custom"><?= (isset($promo->{'promocode_tc_EN'}))?$promo->{'promocode_tc_EN'}:'' ?></textarea>
                </div>
              </div>
              <div id="showMultiLangBlock_tc" class="hide">
                <?php foreach($language AS $lang) { if($lang == 'EN') continue; ?>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>About Promocode (<?= $lang ?>)</label>
                      <textarea id="rich_editor_tc_<?= $lang ?>" type="text"  name="promocode_tc_<?= $lang ?>" 
                        data-parsley-trigger="change" class="ip_reg_form_input form-control reset-form-custom"data-parsley-minlength="2"><?= (isset($promo->{'promocode_tc_'.$lang}))?$promo->{'promocode_tc_'.$lang}:'' ?></textarea>
                    </div>
                  </div>
                <?php } ?>
              </div>
              <div class="col-md-12">      
                <div class="box-footer textCenterAlign">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <a href="<?=base_url('Promocode/viewPromocodes')?>" class="btn btn-primary">Cancel</a>
                </div>        
              </div>        
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<script type="text/javascript">
  jQuery( document ).ready(function() {
    <?php foreach($language AS $lang) { ?>
      if(jQuery('#rich_editor_desc_<?= $lang ?>').length==1){
        CKEDITOR.replace('rich_editor_desc_<?= $lang ?>',{language:'<?= strtolower($lang) ?>'});
      }
      if(jQuery('#rich_editor_tc_<?= $lang ?>').length==1){
        CKEDITOR.replace('rich_editor_tc_<?= $lang ?>',{language:'<?= strtolower($lang) ?>'});
      }
    <?php } ?>
  });
</script>