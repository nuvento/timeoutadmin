<div class="content-wrapper" >
  <section class="content-header">
    <h1>
      <?= $pTitle ?>
      <small><?= $pDescription ?></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?= base_url() ?>"><i class="fa fa-star-o" aria-hidden="true"></i>Home</a></li>
      <li><?= $menu ?></li>
      <li class="active"><?= $smenu ?></li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <?php if($this->session->flashdata('message')) { 
          $flashdata = $this->session->flashdata('message'); ?>
          <div class="alert alert-<?= $flashdata['class'] ?>">
             <button class="close" data-dismiss="alert" type="button">×</button>
             <?= $flashdata['message'] ?>
          </div>
        <?php } ?>
      </div>
      <div class="col-xs-12">
        <div class="box box-warning marginBottom-10">
          <div class="box-body">
            <form role="form" action="<?= base_url('Customer_Booking') ?>" method="post" 
                  class="validate" data-parsley-validate="" enctype="multipart/form-data">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Customer Name, Email or Phone</label>
                    <div>
                      <input class="form-control" id="custSearch" placeholder="Enter Customer Details">
                      <input type="hidden" name="user_id">
                    </div> 
                </div> 
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Customer Name, Email or Phone</label>
                  <div>
                    <input type="text" placeholder="Enter Booking ID" name="booking_id" data-parsley-minlength="12" class="form-control" data-parsley-pattern="^[a-zA-Z0-9]+$" data-parsley-maxlength="12">
                  </div> 
                </div> 
              </div>
              <div class="col-md-12">
                <button class="float-right">Get Booking Details</button>
              </div>
            </form>
          </div>
        </div>

        <?php if(isset($bookData) && isset($custData)){ ?>
          <?php if(empty($bookData) && empty($custData)){ ?>
            <div class="box box-warning">
              <div class="box-header with-border">
                <div class="col-md-12 textCenterAlign">No Data Found</div>
              </div>
            </div>
          <?php } else { ?>
            <?php if (!empty($custData)) { ?>
              <div class="box box-warning">
                <div class="box-header with-border">
                  <div class="col-md-6"><h3 class="box-title">Customer Details</h3></div>
                </div>
                <div class="box-body">
                  <div class="col-md-12">
                    <div class="col-md-2">
                      <div class="form-group has-feedback">
                        <img src="<?= base_url($custData->profile_image) ?>" class="cpoint" 
                          onclick="viewImageModal('Profile Image','<?= base_url($custData->profile_image) ?>');"
                          onerror="this.src='<?=base_url("assets/images/user_avatar.jpg")?>';" height="100" width="100" />
                      </div>
                    </div>
                    <div class="col-md-5">
                      <div class="row">
                        <div class="col-md-3"><span>Name </span></div>
                        <div class="col-md-7"><span>:</span>
                          <label class="padLeft20"><?= $custData->name ?></label>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-3"><span>Email </span></div>
                        <div class="col-md-7"><span>:</span>
                          <label class="padLeft20"><?= $custData->email ?></label>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-3"><span>Phone </span></div>
                        <div class="col-md-7"><span>:</span>
                          <label class="padLeft20"><?= $custData->phone ?></label>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-3"><span>DOB </span></div>
                        <div class="col-md-7"><span>:</span>
                          <label class="padLeft20"><?= $custData->dob ?></label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            <?php } ?>

            <?php if (!empty($bookData)) { ?>
              <div class="box box-warning"> 
                <div class="box-header with-border">
                  <div class="col-md-6"><h3 class="box-title">Booking Details</h3></div>
                </div>
                <div class="box-body">
                  <table id="mechanicUsers" class="table table-bordered table-striped datatable ">
                    <thead>
                      <tr>
                        <th class="hidden">ID</th>
                        <th width="50px;">Book ID</th>
                        <th width="100px;">Event</th>
                        <th width="130px;">Event Name</th>
                        <th width="130px;">Customer</th>
                        <th width="130px;">Scheduled On</th>
                        <th width="50px;">Amount</th>
                        <th width="50px;">Status</th>
                        <th width="40px;">Action</th>
                     </tr>
                    </thead> 
                    <tbody>
                      <?php foreach($bookData as $booking) { ?>
                        <tr>
                          <th class="hidden"><?= $booking->booking_id ?></th>
                          <th class="center"><?= $booking->bookId ?></th>
                          <th class="center"><?= $booking->category_name_EN ?></th>
                          <th class="center"><?= $booking->event_name_EN ?></th>
                          <th class="center"><?= $booking->customer_name ?></th>
                          <th class="center"><?= $booking->date.' '.$booking->time ?></th>
                          <th class="center"><?= $booking->amount ?></th>
                          <th class="center">
                            <?php 
                              switch($booking->book_status){
                                case 0: echo 'Cancelled'; break;
                                case 1: echo 'Booked'; break;
                                case 2: echo 'Completed'; break;
                                case 3: echo 'Pending'; break;
                                case 4: echo 'Deleted'; break;
                                case 5: echo 'Payment Failed'; break;
                              }
                            ?>
                          </th>
                          <td class="center">   
                            <a class="btn btn-sm btn-info" id="viewBooking" booking_id="<?= encode_param($booking->booking_id) ?>">
                              <i class="fa fa-fw fa-eye"></i>View
                            </a> 
                          </td>
                        </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            <?php } ?>
          <?php } ?>
        <?php } ?>
      </div>
    </div>
  </section>
</div>