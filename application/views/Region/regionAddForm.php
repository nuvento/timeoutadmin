<?php
  $language = getLanguages();
?>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
       <?= $pTitle ?>
        <small><?= $pDescription ?></small>
    </h1>
    <ol class="breadcrumb">
     <li><a href="<?= base_url() ?>"><i class="fa fa-star-o" aria-hidden="true"></i>Home</a></li>
     <li><?= $menu ?></li>
     <li class="active"><?= $smenu ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <?php 
        $url = (!isset($region_id)||empty($region_id))
                  ?'Region/createRegion':'Region/updateRegion/'.$region_id;

        if($this->session->flashdata('message')) { 
          $flashdata = $this->session->flashdata('message'); ?>
          <div class="alert alert-<?= $flashdata['class'] ?>">
            <button class="close" data-dismiss="alert" type="button">×</button>
            <?= $flashdata['message'] ?>
          </div>
        <?php } ?>
      </div>
      <div class="col-md-12">
        <div class="box box-warning">
          <div class="box-body">
            <form role="form" action="<?= base_url($url) ?>" method="post" 
              class="validate" data-parsley-validate="" enctype="multipart/form-data">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Region Name</label>
                  <?php foreach($language AS $lang) { 
                    $regionName = (isset($regionData->{'region_name_'.$lang}))?$regionData->{'region_name_'.$lang}:'' ?>
                    <div class="row">
                      <div class="col-md-12">
                        <input type="text" class="form-control <?= ($lang == 'EN')?'required':'' ?>" 
                        name="name_<?= $lang ?>" data-parsley-trigger="change" data-parsley-minlength="2" placeholder="Region Name (<?= $lang ?>)" value="<?= $regionName ?>">
                        <span class="glyphicon form-control-feedback"></span>
                      </div>
                    </div>
                    <br>
                  <?php } ?>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Region Icon</label>
                  <div class="col-md-12" style="padding-bottom:10px;">
                    <div class="col-md-3">
                      <img id="image_id" src="<?= (isset($regionData->region_icon))?base_url($regionData->region_icon):'' ?>" onerror="this.src='<?=base_url("assets/images/no_image_text.png")?>';" height="75" width="75" />
                    </div>
                    <div class="col-md-9" style="padding-top: 25px;">
                      <input name="region_icon" 
                        class="<?= (!isset($regionData->region_icon) || empty(isset($regionData->region_icon)))?'required':'' ?>" type="file" accept="image/*" onchange="setImg(this,'image_id');" />
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-12">      
                <div class="box-footer textCenterAlign">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <a href="<?= base_url('Region/listRegion') ?>" class="btn btn-primary">Cancel</a>
                </div>        
              </div>        
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>