<div class="content-wrapper" >
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
       <?= $pTitle ?>
        <small><?= $pDescription ?></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?= base_url() ?>"><i class="fa fa-star-o" aria-hidden="true"></i>Home</a></li>
      <li><?= $menu ?></li>
      <li class="active"><?= $smenu ?></li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <?php if($this->session->flashdata('message')) { 
          $flashdata = $this->session->flashdata('message'); ?>
          <div class="alert alert-<?= $flashdata['class'] ?>">
             <button class="close" data-dismiss="alert" type="button">×</button>
             <?= $flashdata['message'] ?>
          </div>
        <?php } ?>
      </div>
      <div class="col-xs-12">
        <?php if($this->session->userdata['user_type'] == 1){ ?>
          <div class="box box-warning marginBottom-10">
            <div class="box-body">
              <form role="form" name="venueForm" action="<?= base_url('Checker/viewCheckers')?>" method="post" 
                    class="validate" data-parsley-validate="" enctype="multipart/form-data">
                <div class="col-md-12">
                  <?php if(!empty($provider_data)){ ?>
                    <div class="form-group">
                      <label>Choose Povider</label>
                      <select name="provider_id"  class="form-control" 
                              placeholder="Select Venue" onchange="venueSubmitForm()">
                        <option selected value="0">All provider</option>
                        <?php 
                          foreach ($provider_data as $provider) {
                            $select=(isset($provider_id)&&$provider->provider_id==$provider_id)?'selected':'';
                            echo '<option '.$select.' value="'.$provider->provider_id.'">'.
                                    $provider->name.
                                 '</option>';
                          }
                        ?>
                      </select> 
                    </div>
                  <?php } ?> 
                </div>
              </form>
            </div>
          </div>
        <?php } ?>

        <div class="box box-warning">
          <div class="box-header with-border">
            <div class="col-md-6"><h3 class="box-title">Creater List</h3></div>
            <div class="col-md-6" align="right">
              <?php if($this->session->userdata['user_type'] != 1){ ?>
                <a class="btn btn-sm btn-primary" href="<?= base_url('Checker/addChecker?provider_id='.$enc_provider_id) ?>">
                  Add New Checker
                </a>
              <?php } ?>
              <a class="btn btn-sm btn-primary" href="<?= base_url() ?>">Back</a>
           </div>
          </div>
          <div class="box-body">
            <table id="mechanicUsers" class="table table-bordered table-striped datatable ">
              <thead>
                <tr>
                  <th class="hidden">ID</th>
                  <th width="250px;">Creater User Name</th>
                  <th width="250px;">Provider</th>
                  <th>Status</th>
                  <th>Action</th>
               </tr>
              </thead> 
              <tbody>
                <?php
                if(!empty($checker_data)){
                  foreach($checker_data as $checker) { ?>
                    <tr>
                      <th class="hidden"><?= $checker->id ?></th>
                      <th class="center"><?= $checker->username ?></th>
                      <th class="center"><?= $checker->name ?></th>
                      <th class="center" id="statusFlag_<?= $checker->id ?>">
                        <?= ($checker->status == 1)?'Active':'De-activate' ?>
                      </th>
                      <td class="center">
                        <a class="btn btn-sm btn-danger" onclick="confirmDelete(jQuery(this),'Checker/changeStatus',{'checker_id':'<?=encode_param($checker->id)?>'})" 
                          status="2"><i class="fa fa-fw fa-trash"></i>Delete
                        </a>   
                        <?php
                          $status=0; $btnClass='btn-warning'; $btnName='De-activate'; 
                          if($checker->status != 1){
                            $status=1; $btnClass='btn-success'; $btnName='Activate'; 
                          }
                        ?> 
                        <a class="btn btn-sm <?= $btnClass ?>" onclick="updateStatus(jQuery(this),'Checker/changeStatus',{'checker_id':'<?= encode_param($checker->id)?>'})"  status="<?= $status ?>" status_id="<?= $checker->id ?>">
                          <i class="fa fa-cog"><?= $btnName ?></i>
                        </a>
                      </td>
                    </tr>
                <?php } } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>