<div class="content-wrapper" >
 <!-- Content Header (Page header) -->
 <section class="content-header">
  <h1>
     <?= $page_title ?>
      <small><?= $page_desc ?></small>
  </h1>
  <ol class="breadcrumb">
   <li><a href="<?= base_url() ?>"><i class="fa fa-star-o" aria-hidden="true"></i>Home</a></li>
   <li><?= $menu ?></li>
   <li class="active"><?= $sub_menu ?></li>
 </ol>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
        <?php if($this->session->flashdata('message')) { 
            $flashdata = $this->session->flashdata('message'); ?>
            <div class="alert alert-<?= $flashdata['class'] ?>">
               <button class="close" data-dismiss="alert" type="button">×</button>
               <?= $flashdata['message'] ?>
            </div>
        <?php } ?>
    </div>
    <div class="col-xs-12">
     <div class="box box-warning">
      <div class="box-header">
       <h3 class="box-title">Search Conditions</h3>
      </div>
      <div class="box-body">
       <form id="where_cond">
        <div class="col-md-12">
         <?php if($this->session->userdata['user_type'] == 1){ ?>
           <div class="col-md-3">
             <div class="form-group ">
              <label>Select Organizer</label>
              <div class="col-md-12">
               <select name="provider_id" class="form-control" data-parsley-trigger="change">
                 <option selected disabled>Select Organizer</option>
                 <option value="">All Bookings</option>
                 <?php 
                 if(!empty($providerData)){
                   foreach ($providerData as $provider) {
                     echo '<option value="'.$provider->provider_id.'">'.$provider->name.'</option>';
                   } 
                 }
                 ?>
               </select> 
             </div> 
             </div> 
           </div>
         <?php } else { ?>
          <input type="hidden" name="provider_id" value="<?= $this->session->userdata['id'] ?>">
         <?php } ?>

         <div class="col-md-6">
          <label>Select Report Generation Date Range</label>
           <div class="form-group ">
             <div class="col-md-6">
               <div class="input-group date" data-provide="datepicker">
                 <input id="datepicker" type="text" class="form-control" data-parsley-trigger="change" data-parsley-minlength="2" name="start_date" placeholder="Start Date" autocomplete="off">
                 <div class="input-group-addon">
                   <i class="fa fa-calendar"></i>
                 </div>
               </div>
             </div>
             <div class="col-md-6">
               <div class="input-group date" data-provide="datepicker">
                 <input id="datepicker" type="text" class="form-control" data-parsley-trigger="change" data-parsley-minlength="2" name="end_date" placeholder="End Date" autocomplete="off">
                 <div class="input-group-addon">
                   <i class="fa fa-calendar"></i>
                 </div>
               </div>
             </div>
           </div>
         </div>
         <div class="col-md-3">
           <div class="form-group ">
             <label>Select Booking Status</label>
             <div class="col-md-12">
               <select name="status" class="form-control" data-parsley-trigger="change">
                 <option selected disabled>Select Booking Status</option>
                 <option value="">All Bookings</option>
                 <?php 
                 $status = array('2'=>'Completed','1'=>'Bookings','3'=>'Pending','0'=>'Cancelled','4'=>'Deleted');
                 foreach ($status as $key => $stat) {
                   echo '<option value="'.$key.'">'.$stat.'</option>';
                 }
                 ?>
               </select> 
            </div> 
           </div> 
          </div>
         </div>
       </form>
       <div class="with-border" style="padding-top:70px;">
         <h4 class="box-title" >Fields to View In Report</h4>
       </div>
       <div class="col-md-12" style="padding-top:20px;">
         <div class="col-md-3">
           <input name="fieldType" type="radio" checked="checked" style="padding-left: 10px;" action="hide"
           value="all">
           <label style="padding-left:15px;">All Fields</label> 
         </div>
         <div class="col-md-3">
          <input name="fieldType" type="radio" onclick="checkChild($(this));" action="show" value="custom">
          <label style="padding-left:15px;">Custom Fields</label> 
         </div>
       </div>
       <div class="col-md-12">
        <form id="field_list">
         <table class="table table-striped hide" id="customFields">
           <tbody>
            <?php
            $html = '';
            $count = 0;
            $rowFlg = 0;
            $tables = array('booking_details' => 
                        array('name' => 'Basic Details',
                              'fields' => 
                               array('BOK.bookId AS Book_ID'=>'Book ID',
                                     'CONCAT(EDT.date,\' \',EDT.time) AS Show_Time'=>'Show Time',
                                     'BOK.qrcode AS QR_Code'=>'QR Code',
                                     'BOK.no_of_ticket AS No_Of_Ticket'=>'No Of Ticket',
                                     'BOK.ticket_details AS Ticket_Details'=>'Ticked Details',
                                     'BOK.amount AS Amount'=>'Booking Amount',
                                     'BOK.reserved_by AS Reserved_By'=>'Reserved By',
                                     'BOK.status AS Book_Status'=>'Booking Status'
                               )
                        ),
                        array('name' => 'Event Details',
                              'fields' => 
                               array('EVT.event_name_en AS Event_Name'=>'Event Name',
                                     'EVT.event_description_en'=>'Event Discription',
                                     'ECAT.category AS Category'=>'Event Category',
                                     'ECAT.category_description AS Category_Description'=>'Category Description'
                             )
                        ),
                        array('name' => 'Customer Details',
                              'fields' => 
                               array('CUST.name AS Customer_Name'=>'Customer Name',
                                     'CUST.phone AS Customer_Phone'=>'Customer Phone',
                                     'CUST.email AS Customer_Email'=>'Customer Email',
                                     'CUST.city AS Customer_City'=>'Customer City'
                               )
                        ),
                        array('name' => 'Provider Details',
                              'fields' => 
                               array('PRV.name AS Provider_Name'=>'Provider Name',
                                     'PRV.phone AS Provider_Phone'=>'Provider Phone',
                                     'PRV.email AS Provider_Email'=>'Provider Email'
                               )
                        ),
                        array('name' => 'Venue Details',
                              'fields' => 
                               array('VEN.venue_name AS Venue_Name'=>'Venue Name',
                                     'VEN.venue_details AS Venue_Details'=>'Venue Details',
                                     'REG.name AS Region'=>'Region',
                                     'VEN.location AS Location'=>'Location'
                               )
                        )

            );
            $baseHtml = '<tr>{:baseHtml}</tr>';
            $ottrHtml = '<td><div class="box-body" style="padding-left: 100px">
                          <label>
                          <input type="checkbox" id="table_{:table}" table="{:table}"  onclick="checkChild($(this));" >
                          {:table_name}
                          </label> 
                          {:innerHtml}
                         </div></td>';
            $innerHtml = '<div class="form-group"><div class="col-sm-10"><div class="checkbox">
                            <label>
                            <input type="checkbox" name="{:field_alias}" id="table_{:table}_{:field_alias}" value="{:field_alias}">
                            {:field_name}
                            </label>
                          </div></div></div>';
            $htmlArr = array();
            $allFields = array();
            foreach($tables AS $table => $tableDetails){
              $fieldsHtml = '';
              foreach ($tableDetails['fields'] as $alias => $field) {
                $allFields[] = $alias;
                $fieldsHtml .= str_replace(array('{:field_alias}','{:field_name}'), 
                 array($alias,$field), $innerHtml);
              }
              $fieldsHtml = str_replace('{:table}',$table,$fieldsHtml);
              $html .= str_replace(array('{:table}','{:table_name}','{:innerHtml}'), 
               array($table,$tableDetails['name'],$fieldsHtml,), $ottrHtml);

              if($count == 2){
                $rowFlg = 1; $count = 0;
                $htmlArr[] = str_replace('{:baseHtml}',$html,$baseHtml);
                $html = '';
              }else{
                $count+=1;
              }
            }
            if($rowFlg == 0){
              $html = str_replace('{:baseHtml}',$html,$baseHtml);
            }else{
              $lstRw = str_replace('{:baseHtml}',$html,$baseHtml);
              $html = implode('', $htmlArr);
              $html .= $lstRw;
            }
            echo $html;
            ?>
           </tbody>
         </table>
        </form>
       </div>
       <div class="col-md-12">          
         <div class="box-footer" >
           <input type="hidden" id="all_fields" all_fields="<?= implode(',',$allFields) ?>">
           <button id="rGenerate" type="submit" action="view" dmclick="0" class="btn btn-primary">
             View Report
           </button>
          <button id="rGenerate" type="submit" action="export" dmclick="0" class="btn btn-info">
             Export Report
          </button>
         </div>        
       </div>
      </div>
     </div>
   </div>
  <div class="col-md-12">
    <div class="box box-warning hide" id="report_table_html">
      <div class="box-body">
       <div class="col-md-12">
         <table id="report_table" class="table table-bordered table-striped datatable">
          <thead>
            <tr>
              <th width="60px;">Book ID</th>
              <th width="120px;">Event</th> 
              <th width="120px;">Customer Name</th> 
              <th width="80px;">Amount</th> 
              <th width="100px;">Show Time</th>
              <th width="50px;">Status</th>
              <th width="50px;">Action</th>
            </tr>
          </thead> 
          <tbody id="report_table_body">
            <!-- REPORT TABLE BODY -->
          </tbody>
         </table>
       </div>
     </div>
    </div>
  </div>
 </section>
</div>

<script type="text/javascript">
  jQuery(document).ready(function(){
    jQuery('[name^="BOK.bookId"],[name^="CONCAT(EDT.date,\' \',EDT.time)"],[name^="BOK.amount"],[name^="BOK.status"],[name^="EVT.event_name_en"],[name^="CUST.name"]').prop("checked", true).parent().closest('div').addClass('disable-block');
  });
</script>