<div class="content-wrapper">
  <section class="content-header">
    <h1>
       <?= $pTitle ?>
        <small><?= $pDescription ?></small>
    </h1>
    <ol class="breadcrumb">
     <li><a href="<?= base_url() ?>"><i class="fa fa-star-o" aria-hidden="true"></i>Home</a></li>
     <li><?= $menu ?></li>
     <li class="active"><?= $smenu ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
      <?php 
      if($this->session->flashdata('message')) { 
        $flashdata = $this->session->flashdata('message'); ?>
        <div class="alert alert-<?= $flashdata['class'] ?>">
          <button class="close" data-dismiss="alert" type="button">×</button>
          <?= $flashdata['message'] ?>
        </div>
      <?php } ?>
      </div>
      <div class="col-md-12">
        <div class="box box-warning">
          <div class="box-body">
            <form role="form" action="<?= base_url('Checker/createChecker') ?>" method="post" 
              class="validate" data-parsley-validate="" enctype="multipart/form-data">
              <div class="col-md-6">
                <div class="form-group">
                  <label>User Name</label>
                  <input type="email" class="form-control required" data-parsley-trigger="change"  
                  data-parsley-minlength="2" required="" name="username" placeholder="User Name">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Password</label>
                  <input type="password" class="form-control required" name="password" placeholder="Password" required="">
                </div>
              </div>
              <input type="hidden" name="provider_id" value="<?= $provider_id ?>">
              <div class="col-md-12 padTop10">      
                <div class="box-footer textCenterAlign">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <a href="<?= base_url('Checker/viewCheckers') ?>" class="btn btn-primary">Cancel</a>
                </div>        
              </div>        
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>