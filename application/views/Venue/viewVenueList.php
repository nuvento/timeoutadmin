<div class="content-wrapper" >
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
       <?= $pTitle ?>
        <small><?= $pDescription ?></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?= base_url() ?>"><i class="fa fa-star-o" aria-hidden="true"></i>Home</a></li>
      <li><?= $menu ?></li>
      <li class="active"><?= $smenu ?></li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <?php if($this->session->flashdata('message')) { 
          $flashdata = $this->session->flashdata('message'); ?>
          <div class="alert alert-<?= $flashdata['class'] ?>">
             <button class="close" data-dismiss="alert" type="button">×</button>
             <?= $flashdata['message'] ?>
          </div>
        <?php } ?>
      </div>
      <div class="col-xs-12">
        <div class="box box-warning">
          <div class="box-header with-border">
            <div class="col-md-6"><h3 class="box-title">Venue List</h3></div>
            <div class="col-md-6" align="right">
              <a class="btn btn-sm btn-primary" href="<?= base_url('Venue/addVenues') ?>">
                Add New Venue
              </a>
              <a class="btn btn-sm btn-primary" href="<?= base_url() ?>">Back</a>
           </div>
          </div>
          <div class="box-body">
            <table id="mechanicUsers" class="table table-bordered table-striped datatable ">
              <thead>
                <tr>
                  <th class="hidden">ID</th>
                  <th width="100px;">Venue Name</th>
                  <th width="100px;">Region</th>
                  <th width="100px;">Host Type</th>
                  <th width="120px;">Location</th>
                  <th width="50px;">Status</th>
                  <th width="320px;">Action</th>
               </tr>
              </thead> 
              <tbody>
                <?php
                if(!empty($venue_data)){
                  foreach($venue_data as $venue) { ?>
                    <tr>
                      <th class="hidden"><?= $venue->venue_id ?></th>
                      <th class="center"><?= $venue->venue_name_EN ?></th>
                      <th class="center"><?= $venue->region_name_EN ?></th>
                      <th class="center"><?= $venue->host_category ?></th>
                      <th class="center"><?= $venue->location ?></th>
                      <th class="center" id="statusFlag_<?= $venue->venue_id ?>">
                        <?= ($venue->status == 1)?'Active':'De-activate' ?>
                      </th>
                      <td class="center">   
                        <button class="btn btn-sm btn-info" id="viewVenueDetails" 
                                venue_id="<?= encode_param($venue->venue_id) ?>">
                                <i class="fa fa-fw fa-eye"></i>View
                        </button> 
                        <a class="btn btn-sm btn-primary" 
                           href="<?= base_url('Venue/editVenues/'.encode_param($venue->venue_id))?>">
                          <i class="fa fa-fw fa-edit"></i>Edit
                        </a> 
                        <?php
                          $status = 0; $btnName = 'De-activate'; $btnClass = 'btn-warning';
                          if($venue->status != 1){
                            $status = 1; $btnName = 'Activate'; $btnClass = 'btn-success';
                          }
                        ?> 
                        <a class="btn btn-sm btn-danger" onclick="confirmDelete(jQuery(this),'Venue/changeStatus',{'venue_id':'<?=encode_param($venue->venue_id)?>'})" 
                          status="2"><i class="fa fa-fw fa-trash"></i>Delete
                        </a>  
                        <a class="btn btn-sm <?= $btnClass ?>" onclick="updateStatus(jQuery(this),'Venue/changeStatus',{'venue_id':'<?= encode_param($venue->venue_id) ?>'})" status="<?= $status ?>" status_id="<?= $venue->venue_id ?>">
                          <i class="fa fa-cog"><?= $btnName ?></i>
                        </a>
                      </td>
                    </tr>
                <?php } } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>