<?php
  $language = getLanguages();
?>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
       <?= $pTitle ?>
        <small><?= $pDescription ?></small>
    </h1>
    <ol class="breadcrumb">
     <li><a href="<?= base_url() ?>"><i class="fa fa-star-o" aria-hidden="true"></i>Home</a></li>
     <li><?= $menu ?></li>
     <li class="active"><?= $smenu ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <?php 
        $url = (!isset($venue_id)||empty($venue_id))
                  ?'Venue/createVenues':'Venue/updateVenues/'.$venue_id;

        if($this->session->flashdata('message')) { 
          $flashdata = $this->session->flashdata('message'); ?>
          <div class="alert alert-<?= $flashdata['class'] ?>">
            <button class="close" data-dismiss="alert" type="button">×</button>
            <?= $flashdata['message'] ?>
          </div>
        <?php } ?>
      </div>
      <div class="col-md-12">
        <div class="box box-warning">
          <div class="box-body">
            <form name="venueForm" role="form" action="<?= base_url($url) ?>" method="post" 
              class="validate" data-parsley-validate="" enctype="multipart/form-data">
              <div class="col-md-6">
                <?php if(!empty($regionData)){ ?>
                  <div class="form-group">
                    <label>Region</label>
                    <select name="region_id" class="form-control required" placeholder="Select Region" required>
                      <option selected disabled>Choose a Region</option>
                      <?php 
                        foreach ($regionData as $region) {
                          $select = (isset($venue_data->region_id) && $venue_data->region_id==$region->id)?'selected':'';
                          echo '<option '.$select.' value="'.$region->id.'">'.
                                  $region->region_name_EN.
                               '</option>';
                        }
                      ?>
                    </select> 
                  </div>
                <?php } 
                if(!empty($host_data)){ ?>
                  <div class="form-group">
                    <label>Venue Type</label>
                    <select name="host_cat_id" class="form-control required" 
                            placeholder="Select Venue" required onchange="setLayout();">
                      <option selected disabled>Choose a Venue</option>
                      <?php 
                        foreach ($host_data as $host) {
                          $select = (isset($venue_data->host_cat_id) && $venue_data->host_cat_id == $host->host_cat_id)?'selected':'';
                          echo '<option '.$select.' value="'.$host->host_cat_id.'" 
                                        haveLayout="'.$host->show_layout.'">'.
                                  $host->host_category.
                               '</option>';
                        }
                      ?>
                    </select> 
                  </div>
                <?php } ?> 
                <div class="form-group">
                  <label>Venue Name (EN)</label>
                  <input type="text" class="form-control required" data-parsley-trigger="change"
                    data-parsley-minlength="1" name="venue_name_EN" required="" placeholder="Enter Venue Name (EN)" 
                    value="<?= (isset($venue_data->venue_name_EN))?$venue_data->venue_name_EN:'' ?>">
                </div>

                <div class="padLeft10">
                  <a id="addMultiLang_name" class="cpoint noSubTypeMsg" block="name" show="0">
                    + Show Add More Language Option
                  </a>
                </div>
                <div id="showMultiLangBlock_name" class="hide marginTop10  ">
                  <?php foreach($language AS $lang) { if($lang == 'EN') continue; ?>
                    <div class="form-group">
                      <label>Venue Name (<?= $lang ?>)</label>
                      <input type="text" class="form-control" name="venue_name_<?= $lang ?>" 
                             placeholder="Enter Venue Name (<?= $lang ?>)" 
                             value="<?= (isset($venue_data->{'venue_name_'.$lang}))?$venue_data->{'venue_name_'.$lang}:'' ?>">
                    </div>
                  <?php } ?>
                </div>

                <?php if(!isset($venue_id) || empty($venue_id)){ ?>
                  <div id="locality_block" class="form-group disable-div" style="padding-top: 5px;">
                    <label id="localityLabel">Venue Locality</label>
                    <div id="showType" class="hide">
                      <div class="col-md-5">
                        <input type="radio" name="locality_type" value="0" checked>
                        <label class="padAll-10">Use Existing Locality</label>
                      </div>
                      <div class="col-md-5">
                        <input class="marginLeft15" type="radio" name="locality_type" value="1">
                        <label class="padAll-10">Add New Locality</label>
                      </div>
                    </div>
                    <div id="addNew" class="form-group">
                      <div class="locality_fields">
                        <input type="text" class="form-control required" data-parsley-trigger="change" 
                               name="locality_name_EN" placeholder="Locality (EN)">
                      </div>
                      <?php foreach($language AS $lang) { if($lang == 'EN') continue; ?>
                        <div class="locality_fields hide">
                          <input type="text" class="form-control" name="locality_name_<?= $lang ?>" 
                                 placeholder="Locality (<?= $lang ?>)">
                        </div>
                      <?php } ?>
                    </div>
                    <div id="useExist" class="form-group hide">
                      <select name="locality_id" class="form-control" placeholder="Select Locality"></select>
                    </div>
                  </div>
                <?php } else { ?> 
                  <div id="locality_block" class="form-group" style="padding-top: 5px;">
                    <label id="localityLabel">Venue Locality</label>
                    <div id="showType">
                      <div class="col-md-5">
                        <input type="radio" name="locality_type" value="0" checked>
                        <label class="padAll-10">Use Existing Locality</label>
                      </div>
                      <div class="col-md-4">
                        <input class="marginLeft15" type="radio" name="locality_type" value="1">
                        <label class="padAll-10">Add New Locality</label>
                      </div>
                    </div>
                    <div id="addNew" class="form-group hide">
                      <div class="locality_fields">
                        <input type="text" class="form-control" data-parsley-trigger="change" 
                               name="locality_name_EN" placeholder="Locality (EN)">
                      </div>
                      <?php foreach($language AS $lang) { if($lang == 'EN') continue; ?>
                        <div class="locality_fields">
                          <input type="text" class="form-control" name="locality_name_<?= $lang ?>" 
                                 placeholder="Locality (<?= $lang ?>)">
                        </div>
                      <?php } ?>
                    </div>
                    <div id="useExist" class="form-group">
                      <select name="locality_id" class="form-control required" 
                              placeholder="Select Locality">
                        <option selected disabled>Choose a Locality</option>
                        <?php
                          if(!empty($localityData)){
                            foreach ($localityData as $locality) {
                              $select = (isset($venue_data->locality_id) && $venue_data->locality_id == $locality->locality_id)?'selected':'';

                              echo '<option '.$select.' value="'.$locality->id.'">'.
                                      $locality->locality_name_EN.
                                   '</option>';
                            }
                          }
                        ?>
                      </select>
                    </div>
                  </div>
                <?php } ?> 
              </div>
              <div class="col-md-6"> 
                <div class="form-group">
                  <label>Venue Address</label>
                  <input type="text" class="form-control" data-parsley-trigger="change" name="location" placeholder="Venue Address" value="<?= (isset($venue_data->location))?$venue_data->location:'' ?>">
                </div>
                <input type="hidden" id="gooLocLat" value="<?= (isset($venue_data->location_lat) && !empty($venue_data->location_lat))?$venue_data->location_lat:'' ?>" />
                <input type="hidden" id="gooLocLng" value="<?= (isset($venue_data->location_lng) && !empty($venue_data->location_lng))?$venue_data->location_lng:'' ?>" />
                <input type="hidden" id="gooLocZoom" value="<?= (isset($venue_data->location))?15:0 ?>"/>
                <div id="locPointerMap" style="height:355px;"></div> 
              </div>
              <input id="has_layout" type="hidden" name="has_layout" value='0'>
              <div class="col-md-12" id="layoutCntr" class="hide">  
                <!-- Layout Container -->
                <?php if(isset($venue_data,$venue_data->layout,$venue_data->layout_details) && 
                         !empty($venue_data) && !empty($venue_data->layout) && 
                         !empty($venue_data->layout_details) && 
                         !empty($layoutDetails = json_decode($venue_data->layout_details))){ ?>
                  <input type="hidden" name="has_layout" value='1'>
                  <div class="col-md-3">  
                    <div class="form-group">
                      <label>Layout</label>
                      <div class="dropZoneContainer">
                        <input type="file" name="layout_image" id="drop_zone" class="FileUpload" 
                               accept="image/*" onchange="setImg(this,'image_id');" />
                        <img class="dropZoneOverlay" id="image_id" src="<?= (isset($venue_data->layout))?base_url($venue_data->layout):'' ?>" onerror="this.src='<?=base_url("assets/images/add-image.png")?>';" height="75" width="75" />
                      </div>
                    </div>
                  </div>
                  <div class="col-md-9 padTop30">
                    <div id="subIssueCntr"> 
                      <div id="newSubIssue_1" class="col-md-12 dispInLine"> 
                        <div class="col-md-2">
                          <div class="row marginTop37">
                            <div class="col-md-6"></div>
                            <div class="col-md-6">
                              <i class="fa fa-plus-circle cpoint fav-add-icon" onclick="addLayoutPricing(jQuery(this))" count="<?= count($layoutDetails) ?>"></i>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-4"> 
                          <div class="form-group">
                            <label>Seat Division Color</label>   
                            <input type="text" class="form-control required marginTop-8" data-parsley-trigger="change" data-parsley-minlength="1" name="seat_color[]" value="<?= $layoutDetails[0]->color ?>" placeholder="Seating Division Color" required>
                          </div>
                        </div>
                        <div class="col-md-2">    
                          <div class="form-group">
                            <label>Capacity</label>   
                            <input type="text" class="form-control required marginTop-8" required 
                            data-parsley-trigger="change" data-parsley-minlength="1" name="seat_capacity[]"
                            value="<?= $layoutDetails[0]->capacity ?>" placeholder="Capacity" data-parsley-pattern="^[0-9\ . \/]+$" >
                          </div>
                        </div>
                        <div class="col-md-3">    
                          <div class="form-group">
                            <label>Seat Pricing</label>   
                            <input type="text" class="form-control required marginTop-8" required
                            data-parsley-trigger="change" data-parsley-minlength="1" name="seat_price[]" 
                            value="<?= $layoutDetails[0]->price ?>" data-parsley-pattern="^[0-9\ . \/]+$" placeholder="Default Seat Price">
                          </div>
                        </div>
                      </div>

                      <?php
                        unset($layoutDetails[0]);
                        if(!empty($layoutDetails)){
                          $count = 2;
                          foreach ($layoutDetails as $value) { ?>
                            <div id="newSubIssue_<?= $count ?>" class="col-md-12 dispInLine"> 
                              <div class="col-md-2">
                                <div class="row marginTop10">
                                  <div class="col-md-6"></div>
                                  <div class="col-md-6">
                                    <i class="fa fa-times-circle-o cpoint fav-rem-icon" onclick="remSubIssue(<?= $count ?>);"></i>
                                  </div>
                                </div>
                              </div>
                              <div class="col-md-4"> 
                                <div class="form-group">
                                    <input type="text" class="form-control required marginTop-8" data-parsley-trigger="change" data-parsley-minlength="1" name="seat_color[]" value="<?= $value->color ?>" placeholder="Seating Division Color" required>
                                </div>
                              </div>
                              <div class="col-md-2">    
                                <div class="form-group">
                                  <input type="text" class="form-control required marginTop-8" required
                                  data-parsley-trigger="change" data-parsley-minlength="1" name="seat_capacity[]" 
                                  value="<?= $value->capacity ?>" data-parsley-pattern="^[0-9\ . \/]+$" placeholder="capacity">
                                </div>
                              </div>
                              <div class="col-md-3"> 
                                <div class="form-group">
                                  <input type="text" class="form-control required marginTop-8" required
                                  data-parsley-trigger="change" data-parsley-minlength="1" name="seat_price[]" value="<?= $value->price ?>" data-parsley-pattern="^[0-9\ . \/]+$" placeholder="Default Seat Price">
                                </div>
                              </div>
                            </div>
                          <?php 
                            $count += 1;
                          }
                        }
                      ?>
                    </div>
                  </div> 
                <?php } ?>
              </div>    
              <div class="col-md-12 padTop10">      
                <div class="box-footer textCenterAlign">
                  <button id="venueAddBtn" type="button" class="btn btn-primary">Submit</button>
                  <a href="<?= base_url('Venue/listVenues') ?>" class="btn btn-primary">Cancel</a>
                </div>        
              </div>        
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<!-- Layout Container -->
<div id="layoutCntrHtml" class="hide">
  <input type="hidden" name="has_layout" value='1'>
  <div class="col-md-3">  
    <div class="form-group">
      <label>Layout</label>
      <div class="dropZoneContainer">
        <input type="file" name="layout_image" id="drop_zone" class="FileUpload" 
               accept="image/*" onchange="setImg(this,'image_id');" required />
        <img class="dropZoneOverlay" id="image_id" src="" onerror="this.src='<?=base_url("assets/images/add-image.png")?>';" height="75" width="75" />
      </div>
    </div>
  </div>
  <div class="col-md-9 padTop30">
    <div id="subIssueCntr"> 
      <div id="newSubIssue_1" class="col-md-12 dispInLine"> 
        <div class="col-md-2">
          <div class="row marginTop37">
            <div class="col-md-6"></div>
            <div class="col-md-6">
              <i class="fa fa-plus-circle cpoint fav-add-icon" onclick="addLayoutPricing(jQuery(this))" count="1"></i>
            </div>
          </div>
        </div>
        <div class="col-md-4">    
          <div class="form-group">
            <label>Seat Division Color</label> 
            <input type="text" class="form-control required marginTop-8" data-parsley-trigger="change" data-parsley-minlength="1" name="seat_color[]"  placeholder="Seating Division Color" required>
          </div>
        </div>
        <div class="col-md-2">  
          <div class="form-group">
            <label>Capacity</label>   
            <input type="text" class="form-control required marginTop-8" data-parsley-trigger="change" data-parsley-minlength="1" name="seat_capacity[]" data-parsley-pattern="^[0-9\ . \/]+$"  placeholder="capacity" required>
          </div>
        </div>
        <div class="col-md-3">
          <div class="form-group">
            <label>Seat Pricing</label>     
            <input type="text" class="form-control required marginTop-8" required
            data-parsley-trigger="change" data-parsley-minlength="1" name="seat_price[]"data-parsley-pattern="^[0-9\ . \/]+$" placeholder="Default Seat Price">
          </div>
        </div>
      </div>
    </div>
  </div>  
</div>  

<!-- ADD SUB ITEM HTML -->
<div id="subIssueAdd" class="hide"> 
  <div class="col-md-2">
    <div class="row marginTop10">
      <div class="col-md-6"></div>
      <div class="col-md-6">
        <i class="fa fa-times-circle-o cpoint fav-rem-icon" onclick="remSubIssue({:count});"></i>
      </div>
    </div>
  </div>
  <div class="col-md-4">    
    <input type="text" class="form-control required marginTop-8" data-parsley-trigger="change" data-parsley-minlength="1" name="seat_color[]"  placeholder="Seating Division Color" required>
  </div>
  <div class="col-md-2">    
    <input type="text" class="form-control required marginTop-8" data-parsley-trigger="change" data-parsley-minlength="1" name="seat_capacity[]"  placeholder="capacity" required data-parsley-pattern="^[0-9\ . \/]+$">
  </div>
  <div class="col-md-3">    
    <input type="text" class="form-control required marginTop-8" required
    data-parsley-trigger="change" data-parsley-minlength="1" name="seat_price[]"data-parsley-pattern="^[0-9\ . \/]+$" placeholder="Default Seat Price">
  </div>
</div>
