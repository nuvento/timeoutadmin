<div class="content-wrapper">
  <section class="content-header">
    <h1>
       <?= $pTitle ?>
        <small><?= $pDescription ?></small>
    </h1>
    <ol class="breadcrumb">
     <li><a href="<?= base_url() ?>"><i class="fa fa-star-o" aria-hidden="true"></i>Home</a></li>
     <li><?= $menu ?></li>
     <li class="active"><?= $smenu ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <?php 
        $url = (!isset($host_id)||empty($host_id))
                  ?'Host/createHostCategory':'Host/updateHostCategory/'.$host_id;

        if($this->session->flashdata('message')) { 
          $flashdata = $this->session->flashdata('message'); ?>
          <div class="alert alert-<?= $flashdata['class'] ?>">
            <button class="close" data-dismiss="alert" type="button">×</button>
            <?= $flashdata['message'] ?>
          </div>
        <?php } ?>
      </div>
      <div class="col-md-12">
        <div class="box box-warning">
          <div class="box-body">
            <form role="form" action="<?= base_url($url) ?>" method="post" 
              class="validate" data-parsley-validate="" enctype="multipart/form-data">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Host Category Name</label>
                  <input type="text" class="form-control required" data-parsley-trigger="change"
                  data-parsley-minlength="2" data-parsley-pattern="^[a-zA-Z0-9\ . _ - ' \/]+$" 
                  name="host_category" required="" value="<?= (isset($host_data->host_category))?$host_data->host_category:'' ?>"placeholder="Host Category Name">
                  <span class="glyphicon form-control-feedback"></span>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Have Layout</label>
                  <div>
                    <?php
                      $dCheck = 'checked';
                      if(isset($host_data->show_layout) && $host_data->show_layout != ''){
                        $dCheck = $host_data->show_layout;
                      }
                    ?>
                    <input type="radio" name="show_layout" value="1" <?=($dCheck=='1')?'checked':$dCheck ?>>
                      <label class="padAll-10">Yes</label>
                    <input type="radio" name="show_layout" value="0" <?=($dCheck=='0')?'checked':'' ?>>
                      <label class="padAll-10">No</label>
                  </div>
                  <span class="glyphicon form-control-feedback"></span>
                </div> 
              </div>
              <div class="col-md-12">      
                <div class="box-footer textCenterAlign">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <a href="<?= base_url('Host/listHostCategory') ?>" class="btn btn-primary">Cancel</a>
                </div>        
              </div>        
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>