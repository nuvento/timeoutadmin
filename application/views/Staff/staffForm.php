<div class="content-wrapper">
  <section class="content-header">
    <h1>
       <?= $pTitle ?>
        <small><?= $pDescription ?></small>
    </h1>
    <ol class="breadcrumb">
     <li><a href="<?= base_url() ?>"><i class="fa fa-star-o" aria-hidden="true"></i>Home</a></li>
     <li><?= $menu ?></li>
     <li class="active"><?= $smenu ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <?php 
        $url = (!isset($staff_id) || empty($staff_id))?'Staff/createStaff':'Staff/updateStaff/'.$staff_id;
        if($this->session->flashdata('message')) { 
          $flashdata = $this->session->flashdata('message'); ?>
          <div class="alert alert-<?= $flashdata['class'] ?>">
            <button class="close" data-dismiss="alert" type="button">×</button>
            <?= $flashdata['message'] ?>
          </div>
        <?php } ?>
      </div>
      <div class="col-md-12">
        <div class="box box-warning">
          <div class="box-body">
            <form role="form" action="<?= base_url($url) ?>" method="post" 
              class="validate" data-parsley-validate="" enctype="multipart/form-data">
              
              <!-- Staff Data -->
              <div class="col-md-6">
                <div class="form-group">
                  <label>Name</label>
                  <input type="text" class="form-control required" data-parsley-trigger="change"
                  data-parsley-minlength="2" data-parsley-pattern="^[a-zA-Z0-9\ . _ - ' \/]+$" 
                  name="display_name" required="" value="<?= (isset($staff_data->display_name))?$staff_data->display_name:'' ?>"placeholder="Enter Staff Name">
                </div>  
                <div class="form-group">
                  <label>Username</label>
                  <input type="email" class="form-control required" data-parsley-trigger="change"  
                  data-parsley-minlength="2" required="" name="username" placeholder="Enter Email-ID"  value="<?= (isset($staff_data->username))?$staff_data->username:'' ?>">
                </div> 
                <?php if(!isset($staff_id) || empty($staff_id)){ ?>
                  <div class="form-group">
                    <label>Password</label>
                    <input type="password" class="form-control required" data-parsley-trigger="change" 
                    data-parsley-minlength="2" required="" name="password" placeholder="Enter Password" >
                  </div> 
                <?php } ?>
              </div>
              <div class="col-md-6"> 
                <?php if(!isset($staff_id) || empty($staff_id)){ ?>
                <div class="form-group">
                  <label>User Role</label>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="col-md-3">
                        <input type="radio" class="required padLeft10" data-parsley-trigger="change" 
                        required value="4" name="user_type" checked>
                        <label class="padLeft10">Staff</label>
                      </div>
                      <div class="col-md-5">
                        <input type="radio" class="required padLeft10" data-parsley-trigger="change" 
                        required value="5" name="user_type">
                        <label class="padLeft10">Customer Care</label>
                      </div>
                    </div> 
                  </div>
                </div>
                <?php } ?>
                <div class="form-group padTop10">
                  <label>Profile Picture</label>
                  <div class="col-md-12" style="padding-bottom:10px;">
                    <div class="col-md-3">
                      <img id="image_id" src="<?= (isset($staff_data->profile_image))?base_url($staff_data->profile_image):'' ?>" onerror="this.src='<?=base_url("assets/images/user_avatar.jpg")?>';" height="75" width="75" />
                    </div>
                    <div class="col-md-9" style="padding-top: 25px;">
                      <input name="profile_image" type="file" accept="image/*" onchange="setImg(this,'image_id');" />
                    </div>
                  </div>
                </div>
              </div> 
              <div class="col-md-12">      
                <div class="box-footer textCenterAlign">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <a href="<?= base_url('Staff/viewStaffs') ?>" class="btn btn-primary">Cancel</a>
                </div>        
              </div>       
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
