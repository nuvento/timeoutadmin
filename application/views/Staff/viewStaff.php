<div class="content-wrapper" >
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
       <?= $pTitle ?>
        <small><?= $pDescription ?></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?= base_url() ?>"><i class="fa fa-star-o" aria-hidden="true"></i>Home</a></li>
      <li><?= $menu ?></li>
      <li class="active"><?= $smenu ?></li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <?php if($this->session->flashdata('message')) { 
          $flashdata = $this->session->flashdata('message'); ?>
          <div class="alert alert-<?= $flashdata['class'] ?>">
             <button class="close" data-dismiss="alert" type="button">×</button>
             <?= $flashdata['message'] ?>
          </div>
        <?php } ?>
      </div>
      <div class="col-xs-12">
        <div class="box box-warning"> 
          <div class="box-header with-border">
            <div class="col-md-6"><h3 class="box-title">Staff List</h3></div>
            <div class="col-md-6" align="right">
              <a class="btn btn-sm btn-primary" href="<?= base_url('Staff/addStaff')?>">
                Add New Staff
              </a>
              <a class="btn btn-sm btn-primary" href="<?= base_url() ?>">Back</a>
            </div>
          </div>
          <div class="box-body">
            <table id="mechanicUsers" class="table table-bordered table-striped datatable ">
              <thead>
                <tr>
                  <th class="hidden">ID</th>
                  <th width="50px;">Photo</th> 
                  <th width="130px;">Name</th>
                  <th width="130px;">Username</th>
                  <th width="120px;">Role</th>
                  <th width="50px;">Status</th>
                  <th width="200px;">Action</th>
               </tr>
              </thead> 
              <tbody>
                <?php
                if(!empty($staff_data)){
                  foreach($staff_data as $staff) { ?>
                    <tr>
                      <th class="hidden"><?= $staff->id ?></th>
                      <th class="center textCenterAlign">
                        <img id="image_id" src="<?= base_url($staff->profile_image) ?>" 
                             onerror="this.src='<?=base_url("assets/images/user_avatar.jpg")?>';" 
                             height="50" width="50" />
                      </th>
                      <th class="center"><?= $staff->display_name ?></th>
                      <th class="center"><?= $staff->username ?></th>
                      <th class="center"><?= ($staff->user_type == 4)?'Staff User':'Customer Care' ?></th>
                      <th class="center" id="statusFlag_<?= $staff->id ?>">
                        <?= ($staff->status == 1)?'Active':'De-active' ?>
                      </th>
                      <td class="center">   
                        <a class="btn btn-sm btn-primary" 
                            href="<?= base_url('Staff/editStaffs/'.encode_param($staff->id)) ?>">
                          <i class="fa fa-fw fa-edit"></i>Edit
                        </a> 
                        <?php
                          $status = 0; $btnName = 'De-activate'; $btnClass = 'btn-warning';
                          if($staff->status != 1){
                            $status = 1; $btnName = 'Activate'; $btnClass = 'btn-success';
                          }
                        ?> 
                        <a class="btn btn-sm btn-danger" onclick="confirmDelete(jQuery(this),'Staff/changeStatus',{'staff_id':'<?=encode_param($staff->id)?>'})" 
                          status="2"><i class="fa fa-fw fa-trash"></i>Delete
                        </a>  
                        <a class="btn btn-sm <?= $btnClass ?>" onclick="updateStatus(jQuery(this),'Staff/changeStatus',{'staff_id':'<?= encode_param($staff->id) ?>'})" 
                          status="<?= $status ?>" status_id="<?= $staff->id ?>">
                          <i class="fa fa-cog"><?= $btnName ?></i>
                        </a>
                      </td>
                    </tr>
                <?php } } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
