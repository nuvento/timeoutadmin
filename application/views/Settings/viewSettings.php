<div class="content-wrapper">
  <section class="content-header">
    <h1>
      <?= $page_title ?>
      <small><?= $page_desc ?></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?= base_url() ?>"><i class="fa fa-star-o" aria-hidden="true"></i>Home</a></li>
      <li><?= $menu ?></li>
      <li class="active"><?= $sub_menu ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <?php if($this->session->flashdata('message')) { 
          $flashdata = $this->session->flashdata('message'); ?>
          <div class="alert alert-<?= $flashdata['class'] ?>">
            <button class="close" data-dismiss="alert" type="button">×</button>
            <?= $flashdata['message'] ?>
          </div>
        <?php } ?>
      </div>
      <div class="col-md-12">
        <div class="box box-warning">
          <div class="box-header with-border">
            <div class="col-md-6">
              <h3 class="box-title">Site Details</h3>
            </div>
            <div class="col-md-6" align="right">
              <a class="btn btn-sm btn-primary" href="<?= base_url() ?>">Back</a>
            </div>
          </div>
          <form method="post" class="validate" role="form" action="<?= base_url().'Settings/change_settings'?>" enctype="multipart/form-data" data-parsley-validate="">
            <div class="box-body">
              <div class="form-group col-xs-7">
                <div class="form-group col-xs-6">
                  <label>Site Title</label>
                  <input type="text" name="title" class="form-control required" placeholder="Enter Site Title" value="<?= $data['title'] ?>">
                </div>
                <div class="form-group col-xs-6">
                  <label>Title Short</label>
                  <input type="text" name="title_short" class="form-control required" placeholder="Enter Site Title" value="<?= $data['title_short'] ?>">
                </div>
                <div class="form-group col-xs-6">
                  <label>Country Code</label>
                  <input type="text" name="country_flag" class="form-control required" placeholder="Enter SMTP Username" value="<?= $data['country_flag'] ?>">
                </div>
                <div class="form-group col-xs-6">
                  <label>Currency</label>
                  <input type="text" name="currency" class="form-control required" placeholder="Enter SMTP Password" value="<?= $data['currency'] ?>">
                </div>
                <div class="form-group col-xs-6">
                  <label>Google API Key</label>
                  <input type="text" name="google_api_key" class="form-control required" placeholder="Enter Google API" value="<?= $data['google_api_key'] ?>">
                </div>
                <div class="form-group col-xs-6">
                  <label>Commission Per Booking</label>
                  <input type="text" name="service_charge" class="form-control required" placeholder="Enter Service Charge" value="<?= $data['service_charge'] ?>">
                </div>
                <div class="form-group col-xs-6">
                  <label>Admin Email-ID</label>
                  <input type="email" name="admin_mail_id" class="form-control required" placeholder="Admin Email-ID" value="<?= $data['admin_mail_id'] ?>">
                </div>
                <div class="form-group col-xs-6">
                  <label>Web Base URL</label>
                  <input type="text" name="web_base_url" class="form-control required" 
                  placeholder="Web Base URL" value="<?= $data['web_base_url'] ?>">
                </div>
              </div>
              <div class="form-group col-xs-5">
                <div class="form-group">
                  <label>Site Logo</label>
                  <div class="col-md-12">
                    <div class="col-md-3">
                      <img id="site_logo" src="<?= base_url($data['site_logo']) ?>" onerror="this.src='<?=base_url("assets/images/no_image.png")?>';" height="75" width="75">
                    </div>
                    <div class="col-md-9" style="padding-top: 25px;">
                      <input name="site_logo" type="file" accept="image/*" onchange="setImg(this,'site_logo');" />
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label>Favicon Icon</label>
                  <div class="col-md-12">
                    <div class="col-md-3">
                      <img id="fav_icon_image" src="<?= base_url($data['fav_icon']) ?>" onerror="this.src='<?=base_url("assets/images/no_image.png")?>';" height="75" width="75">
                    </div>
                    <div class="col-md-9" style="padding-top: 25px;">
                      <input name="fav_icon" type="file" accept="image/*" onchange="setImg(this,'fav_icon_image');" />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="box-header with-border">
              <div class="col-md-6">
                <h3 class="box-title">Payment Gateway Details</h3>
              </div>
            </div>
            <div class="box-body">
              <div class="form-group col-xs-12">
                <div class="form-group col-xs-4">
                  <label>BayanPay Merchant ID</label>
                  <input type="text" name="merchant_id" class="form-control required" placeholder="Enter Merchant ID" value="<?= $data['merchant_id'] ?>">
                </div>                
                <div class="form-group col-xs-4">
                  <label>BayanPay Merchant IV</label>
                  <input type="text" name="merchant_iv" class="form-control required" placeholder="Enter Merchant IV" value="<?= $data['merchant_iv'] ?>">
                </div>
                <div class="form-group col-xs-4">
                  <label>BayanPay Merchant Key</label>
                  <input type="text" name="merchant_key" class="form-control required" placeholder="Enter Merchant Key" value="<?= $data['merchant_key'] ?>">
                </div>
                <div class="form-group col-xs-4">
                  <label>BayanPay Collaborator ID</label>
                  <input type="text" name="collaborator_id" class="form-control required" placeholder="Enter Collaborator ID" value="<?= $data['collaborator_id'] ?>">
                </div>
                <div class="form-group col-xs-8">
                  <label>BayanPay Gateway URL</label>
                  <input type="text" name="payment_gateway_url" class="form-control required" placeholder="Enter Gateway URL" value="<?= $data['payment_gateway_url'] ?>">
                </div>
              </div>
            </div>
            <div class="box-header with-border">
              <div class="col-md-6">
                <h3 class="box-title">APP Details</h3>
              </div>
            </div>
            <div class="box-body">
              <div class="form-group col-xs-7">
                <div class="form-group col-xs-12">
                  <label>FireBase App-ID</label>
                  <input type="text" name="app_id" class="form-control required" placeholder="App-ID" value="<?= $data['app_id'] ?>">
                </div>
                <div class="form-group col-xs-6">
                  <label>IOS Version</label>
                  <input type="text" name="ios_version" class="form-control required" placeholder="Enter IOS Version" value="<?= $data['ios_version'] ?>">
                </div>
                <div class="form-group col-xs-6">
                  <label>IOS Playstore URL</label>
                  <input type="text" name="ios_playstore_url" class="form-control required" placeholder="Enter IOS Playstore URL" value="<?= $data['ios_playstore_url'] ?>">
                </div>
                <div class="form-group col-xs-6">
                  <label>Android Version</label>
                  <input type="text" name="android_version" class="form-control required" placeholder="Enter Android Version" value="<?= $data['android_version'] ?>">
                </div>
                <div class="form-group col-xs-6">
                  <label>Android Playstore URL</label>
                  <input type="text" name="android_playstore_url" class="form-control required" placeholder="Enter Android Playstore URL" value="<?= $data['android_playstore_url'] ?>">
                </div>
                <div class="form-group col-xs-6">
                  <label>Force Update App</label>
                  <input type="text" name="force_update" class="form-control required" placeholder="Force Update App (0/1)" value="<?=$data['force_update']?>" data-parsley-pattern="^[0,1]+$">
                </div>
              </div>
            </div>
            <div class="box-footer" style="padding-left:46%">
              <button type="submit" class="btn btn-info">Update</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>
</div>
