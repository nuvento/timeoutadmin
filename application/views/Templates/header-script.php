<?php
  $settings = getSettings();
?>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?= $settings['title_short'] ?></title>
  <link rel="icon" href="<?= base_url($settings['fav_icon'])?>" type="image/x-icon"/>
  <link rel="shortcut icon" href="<?= base_url($settings['fav_icon']) ?>" type="image/x-icon"/>

  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.min.css') ?>">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="<?= base_url('assets/css/select2.min.css') ?>">
  <link rel="stylesheet" href="<?= base_url('assets/css/dataTables.bootstrap.css') ?>">
  <link rel="stylesheet" href="<?= base_url('assets/css/AdminLTE.min.css') ?>">
  <link rel="stylesheet" href="<?= base_url('assets/css/pace.css') ?>">
  <link rel="stylesheet" href="<?= base_url('assets/css/'.$this->config->item("theme_color").'.css') ?>">
  <link rel="stylesheet" href="<?= base_url('assets/css/custom-style.css?'.time()) ?>">
  <link rel="stylesheet" href="<?= base_url('assets/css/parsley/parsley.css') ?>">
  <link rel="stylesheet" href="<?= base_url('assets/css/bootstrap-datepicker3.css') ?>">
  <link rel="stylesheet" href="<?= base_url('assets/css/clockpicker.css') ?>" type="text/css" >
  <link rel="stylesheet" href="<?= base_url('assets/css/theme.css') ?>">
  <link rel="stylesheet" href="<?= base_url('assets/css/custom.css') ?>">
  <link rel="stylesheet" href="<?= base_url('assets/css/slick.min.css') ?>" type="text/css" >
  <link rel="stylesheet" href="<?= base_url('assets/css/jquery-ui.min.css') ?>" type="text/css" /> 

  <script src="<?= base_url('assets/js/jQuery-2.1.4.min.js') ?>"></script>
  <script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>

  <script type="text/javascript">
    report_data = ride_ids = [];
  </script>
</head>
