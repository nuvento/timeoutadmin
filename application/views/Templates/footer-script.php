<?php
  $settings = getSettings();
  $languages = getLanguages();
  $gKey = $settings['google_api_key'];
?>
<script>
    base_url = "<?= base_url() ?>";
    languages = <?= json_encode($languages) ?>;
    country_flag = '<?= $settings['country_flag'] ?>';
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=<?= $gKey ?>&libraries=places"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
<script src="<?= base_url('assets/js/pace.js') ?>"></script>
<script src="<?= base_url('assets/js/select2.full.min.js') ?>"></script>
<script src="<?= base_url('assets/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url('assets/js/dataTables.bootstrap.min.js') ?>"></script>
<script src="<?= base_url('assets/js/bootbox.min.js') ?>"></script>
<script src="<?= base_url('assets/js/app.min.js') ?>"></script>
<script src="<?= base_url('assets/js/locationpicker.js') ?>" type="text/javascript"></script>
<script src="<?= base_url('assets/js/jquery-ui.min.js') ?>" type="text/javascript"></script>
<script src="<?= base_url('assets/js/custom-script.js?ts='.time()) ?>"></script>
<script src="<?= base_url('assets/js/parsley.min.js') ?>"></script>
<script src="<?= base_url('assets/js/bootstrap-datepicker.js') ?>"></script>
<script src="<?= base_url('assets/js/clockpicker.js') ?>" type="text/javascript"></script>
<script src="<?= base_url('assets/js/slick.min.js') ?>" type="text/javascript"></script>
<script src="<?= base_url('assets/js/custom.js') ?>" type="text/javascript"></script>

<script>
    jQuery('.clockpicker').clockpicker();

    function doconfirm(){
        action = confirm("Are you sure to delete permanently?");
        if(action != true) return false;
    }

    <?php
        $ci = & get_instance();
        $controllerName = $ci->uri->segment(1);
        $actionName = $ci->uri->segment(2);
        $page = $controllerName . '-' . $actionName;

        switch ($page) {
            case 'Event-addEvent': ?>
                jQuery(function () {
                    jQuery('[date="start"]').datepicker({
                        format: 'dd - MM - yyyy',
                        startDate: '-0d',
                        endDate: '+90d'
                    });
                    jQuery('[date="end"]').datepicker({
                        format: 'dd - MM - yyyy',
                        startDate: '+1d',
                        endDate: '+90d'
                    });
                });
                jQuery('.datatable').DataTable({
                    "ordering" : jQuery(this).data("ordering"),
                    "order": [[ 0, "desc" ]]
                });
                <?php break; 
            default : ?>
                jQuery(function () {
                    jQuery('.datatable').DataTable({
                        "ordering" : jQuery(this).data("ordering"),
                        "order": [[ 0, "desc" ]]
                    });
                });
    <?php } ?>
</script>
