<!-- POP-UP VIEW MODAL END -->
<div class="modal fade" id="popup_modal" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="modal_header"></h4>
      </div>
      <div class="modal-body col-md-12" id="modal_content" style="border-bottom:1px solid #e5e5e5;">
        <!-- POP-UP VIEW MODAL CONTENT -->
      </div>
      <div class="modal-footer">
        <div>&nbsp;</div>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>  
<!-- POP-UP VIEW MODAL END -->   
<footer class="main-footer"></footer>