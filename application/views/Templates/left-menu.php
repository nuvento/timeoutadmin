<?php
    $menus = roleManagement();
?>

<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?=base_url($this->session->userdata('profile_pic'))?>" onerror="this.src='<?=base_url("assets/images/user_avatar.jpg")?>'" class="user-image left-sid" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?php echo $this->session->userdata('logged_in_admin')['username']; ?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <ul class="sidebar-menu">
            <?php if(array_key_exists('Dashboard',$menus)){ ?>
                <li>
                    <a href="<?= base_url('Dashboard') ?>">
                        <img src="<?=base_url("assets/images/m1.png") ?>">
                        <span>Dashboard</span>
                    </a>
                </li>
            <?php }if(array_key_exists('Customer_Booking',$menus)){ ?>
                <li>
                    <a href="<?= base_url('Customer_Booking') ?>">
                        <img src="<?=base_url("assets/images/m5.png") ?>">
                        <span>Care Home</span>
                    </a>
                </li>
            <?php } if(array_key_exists('Tag',$menus)){ ?>
                <li>
                    <a href="<?= base_url('Tag/listTags') ?>">
                        <img src="<?=base_url("assets/images/m2.png") ?>">
                        <span>Tag Management</span>
                    </a>
                </li>
            <?php } if(array_key_exists('Host',$menus)){ ?>
                <li class="treeview">
                    <a href="#">
                        <img src="<?=base_url("assets/images/m3.png") ?>">
                        <span>Host Categories</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            <a href="<?= base_url('Host/addHostCategory') ?>">
                                <i class="fa fa-circle-o text-aqua"></i>
                                Add Host Category
                            </a>
                        </li>
                        <li>
                            <a href="<?= base_url('Host/listHostCategory') ?>">
                                <i class="fa fa-circle-o text-aqua"></i>
                                View Host Category
                            </a>
                        </li>
                    </ul>
                </li>
            <?php } if(array_key_exists('City',$menus)){ ?>
                <li>
                    <a href="<?= base_url('Region/listRegion') ?>">
                        <img src="<?=base_url("assets/images/m4.png") ?>">
                        <span>City Management</span>
                    </a>
                </li>
            <?php } if(array_key_exists('Country',$menus)){ ?>
                <li>
                    <a href="<?= base_url('Country/listCountry') ?>">
                        <img src="<?=base_url("assets/images/m7.png") ?>">
                        <span>Country Management</span>
                    </a>
                </li>
            <?php } if(array_key_exists('Category',$menus)){ ?>
                <li>
                    <a href="<?= base_url('Category/listCategory') ?>">
                        <img src="<?=base_url("assets/images/m5.png") ?>">
                        <span>Category Management</span>
                    </a>
                </li>
            <?php } if(array_key_exists('Venue',$menus)){ ?>
                <li class="treeview">
                    <a href="#">
                        <img src="<?=base_url("assets/images/m6.png") ?>">
                        <span>Venue Management</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            <a href="<?= base_url('Venue/addVenues') ?>">
                                <i class="fa fa-circle-o text-aqua"></i>
                                Add Venue
                            </a>
                        </li>
                        <li>
                            <a href="<?= base_url('Venue/listVenues') ?>">
                                <i class="fa fa-circle-o text-aqua"></i>
                                View Venue
                            </a>
                        </li>
                    </ul>
                </li>
            <?php } if(array_key_exists('Event',$menus)){ ?>
                <li class="treeview">
                    <a href="#">
                        <img src="<?=base_url("assets/images/m7.png") ?>">
                        <span>Event Management</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            <a href="<?= base_url('Event/addEvent') ?>">
                                <i class="fa fa-circle-o text-aqua"></i>
                                Add Event
                            </a>
                        </li>
                        <li>
                            <a href="<?= base_url('Event/listEvents') ?>">
                                <i class="fa fa-circle-o text-aqua"></i>
                                View Events
                            </a>
                        </li>
                    </ul>
                </li>
            <?php } if(array_key_exists('Organizer',$menus)){ ?>
                <li class="treeview">
                    <a href="#">
                        <img src="<?=base_url("assets/images/m3.png") ?>">
                        <span>Organizer Management</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            <a href="<?= base_url('Provider/addProvider') ?>">
                                <i class="fa fa-circle-o text-aqua"></i>
                                Add Organizer
                            </a>
                        </li>
                        <li>
                            <a href="<?= base_url('Provider/viewProviders') ?>">
                                <i class="fa fa-circle-o text-aqua"></i>
                                View Organizer
                            </a>
                        </li>
                    </ul>
                </li>
            <?php } if(array_key_exists('Checker',$menus)){ ?>
                <li>
                    <a href="<?= base_url('Checker/viewCheckers') ?>">
                        <img src="<?=base_url("assets/images/m2.png") ?>">
                        <span>Checker Management</span>
                    </a>
                </li>
            <?php } if(array_key_exists('Customer',$menus)){ ?>
                <li class="treeview">
                    <a href="#">
                        <img src="<?=base_url("assets/images/m9.png") ?>">
                        <span>Customer Management</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <?php if(in_array('1',$menus['Customer'])){ ?>
                            <li>
                                <a href="<?= base_url('Customer/addCustomer') ?>">
                                    <i class="fa fa-circle-o text-aqua"></i>
                                    Add Customer
                                </a>
                            </li>
                        <?php } ?>
                        <li>
                            <a href="<?= base_url('Customer/viewCustomers') ?>">
                                <i class="fa fa-circle-o text-aqua"></i>
                                View Customer
                            </a>
                        </li>
                    </ul>
                </li>
            <?php } if(array_key_exists('Promocode',$menus)){ ?>
                <li class="treeview">
                    <a href="#">
                        <img src="<?=base_url("assets/images/m2.png") ?>">
                        <span>Promocode Management</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            <a href="<?= base_url('Promocode/promocode') ?>">
                                <i class="fa fa-circle-o text-aqua"></i>
                                Promo-codes
                            </a>
                        </li>
                        <li>
                            <a href="<?= base_url('Promocode/addPromocode') ?>">
                                <i class="fa fa-circle-o text-aqua"></i>
                                Add Promo-code
                            </a>
                        </li>
                    </ul>
                </li>
            <?php } if(array_key_exists('Booking',$menus)){ ?>
                <li>
                    <a href="<?= base_url('Booking/viewBookings') ?>">
                        <img src="<?=base_url("assets/images/m8.png") ?>">
                        <span>Booking Details</span></a>
                </li>
            <?php } if(array_key_exists('HotelCity',$menus)){ ?>
                <li>
                    <a href="<?= base_url('HotelCity/listHotelCity') ?>">
                        <img src="<?=base_url("assets/images/m4.png") ?>">
                        <span>Hotel Cities</span></a>
                </li>
            <?php } if(array_key_exists('',$menus)){ ?>
                <li><a href="<?= base_url('Provider/getProviderPayDetails') ?>">
                    <img src="<?=base_url("assets/images/m2.png") ?>">
                    <span>Commission Management</span></a>
                </li>
            <?php } if(array_key_exists('Staff',$menus)){ ?>
                <li class="treeview">
                    <a href="#">
                        <img src="<?=base_url("assets/images/m9.png") ?>">
                        <span>Staff Management</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            <a href="<?= base_url('Staff/addStaff') ?>">
                                <i class="fa fa-circle-o text-aqua"></i>
                                Add Staff User
                            </a>
                        </li>
                        <li>
                            <a href="<?= base_url('Staff/viewStaffs') ?>">
                                <i class="fa fa-circle-o text-aqua"></i>
                                View Staff Users
                            </a>
                        </li>
                    </ul>
                </li>
            <?php } if(array_key_exists('Notification',$menus)){ ?>
                <li class="treeview">
                    <a href="#">
                        <img src="<?=base_url("assets/images/m2.png") ?>">
                        <span>Notification Management</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            <a href="<?= base_url('Notification/pushNotification') ?>">
                                <i class="fa fa-circle-o text-aqua"></i>
                                Push Notification
                            </a>
                        </li>
                        <li>
                            <a href="<?= base_url('Notification/notification') ?>">
                                <i class="fa fa-circle-o text-aqua"></i>
                                Notification Templates
                            </a>
                        </li>
                    </ul>
                </li>
            <?php } if(array_key_exists('CMS',$menus)){ ?>
                <li>
                    <a href="<?= base_url('CMS') ?>">
                        <img src="<?=base_url("assets/images/m5.png") ?>">
                        <span>CMS Management</span>
                    </a>
                </li>
            <?php } if(array_key_exists('Settings',$menus)){ ?>
                <li>
                    <a href="<?= base_url('Settings') ?>">
                        <i class="fa fa-wrench" aria-hidden="true">
                        </i><span>Settings</span>
                    </a>
                </li>
            <?php } ?>
        </ul>
    </section>
</aside>
